package com.tsolution.base;


import android.app.Application;

import androidx.annotation.NonNull;

import com.tsolution.base.Utils.RxSchedulers;
import com.tsolution.base.Utils.RxSubscriber;
import com.tsolution.base.listener.ResponseResult;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

public class BaseViewModelV2<Service> extends BaseViewModel {
    private static Retrofit mRetrofit;
    protected static Object service;
    public static boolean isLogin = false;
    public BaseViewModelV2(@NonNull Application application) {
        super(application);


    }
    public void init(Class<Service> clazz){
        service = mRetrofit.create(clazz);
    }
    protected <Response> void callApiV2(Flowable<Response> flowable, ResponseResult<Response> result) {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(flowable.compose(RxSchedulers.io_main())
                .subscribeWith(new RxSubscriber<Response>(getApplication().getBaseContext()) {
                    @Override
                    public void onSuccess(Response arr) {
                        result.onResponse(null, null, arr, null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        result.onResponse(null, null, null, e);
                    }
                }));


    }

    public static void setApiService(Retrofit retrofit, Class<?> clazz, boolean isLogin){
        mRetrofit = retrofit;
        service = mRetrofit.create(clazz);
        BaseViewModelV2.isLogin = isLogin;
    }
    public Service getService(){
        return (Service)service;
    }

}
