package com.nextsolution.graphql.api;


import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Mutation;
import com.apollographql.apollo.api.Query;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.nextsolution.graphql.codegen.business.LoginMutation;
import com.nextsolution.graphql.codegen.dto.User;
import com.nextsolution.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class AWSClientBase {
    static AWSAppSyncClient mAWSAppSyncClient;
    public static User loginUser;
    public static void requestApolloLogin(Context context, String url, MutableLiveData<Throwable> appException, String username, String password,
                                          ResponseApollo result, final Object... params) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder builder = original.newBuilder().method(original.method(), original.body());
                    return chain.proceed(builder.build());
                })
                .build();

        mAWSAppSyncClient = AWSAppSyncClient.builder()
                .serverUrl(url)
                .context(context)
//                .awsConfiguration(new AWSConfiguration(context))
                .okHttpClient(okHttpClient)
                .build();



        mAWSAppSyncClient.mutate(LoginMutation.builder()
                .username(username)
                .password(password)
                .build()).enqueue(new GraphQLCall.Callback<LoginMutation.Data>(){
            @Override
            public void onResponse( Response<LoginMutation.Data> response) {
                if (response.data() != null && response.data().getLogin() != null && !response.hasErrors()) {
                    String token = Objects.requireNonNull(response.data().getLogin()).getJwt();
                    if (StringUtils.isNotNullAndNotEmpty(token)) {
                        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(chain -> {
                                    Request request = chain.request();
                                    Request.Builder newRequest = request.newBuilder().addHeader("Authorization", "Bearer " + token);
                                    return chain.proceed(newRequest.build());
                                });
                        mAWSAppSyncClient = AWSAppSyncClient.builder()
                                .serverUrl(url)
                                .okHttpClient(okHttpClient.build())
                                .context(context)
//                                .awsConfiguration(new AWSConfiguration(context))
                                .build();
                        //
                        loginUser = response.data().getLogin();

                        result.onResponse(response, null, params);
                    }else{
                        result.onResponse(null, new Throwable(response.errors().toString()), params);
                    }
                }else {
                    result.onResponse(null, null, null, null);
                }
            }

            @Override
            public void onFailure(ApolloException e) {
                Log.d("login", e.getMessage());
                result.onResponse(null, e);
            }
        });
    }

    public static <D extends Query.Data, T, V extends Query.Variables> void apolloQuery(@Nonnull Query<D, T, V> query, final ResponseApollo result, final Object... params) {
        mAWSAppSyncClient.query(query)
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(new GraphQLCall.Callback<T>() {
            @Override
            public void onResponse(@NotNull Response<T> response) {
                if (response.hasErrors()) {

                    result.onResponse(null, new Throwable(response.errors().toString()), params);
                } else {
                    result.onResponse(response, null, params);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("query", e.getMessage());
                e.printStackTrace();
                result.onResponse(null, e);
            }
        });
    }

    public static  <D extends Mutation.Data, T, V extends Query.Variables> void apolloMutation(Mutation<D, T, V> query, ResponseApollo result, Object... params) {
        mAWSAppSyncClient.mutate(query).enqueue(new GraphQLCall.Callback<T>() {
            @Override
            public void onResponse(@NotNull Response<T> response) {
                if (response.hasErrors()) {
                    result.onResponse(null, new Throwable(response.errors().toString()), params);
                } else {
                    result.onResponse(response, null, params);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("query", e.getMessage());
                result.onResponse(null, e);
            }
        });
    }
}
