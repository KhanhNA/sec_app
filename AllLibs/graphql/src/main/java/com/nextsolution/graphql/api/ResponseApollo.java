package com.nextsolution.graphql.api;

import com.apollographql.apollo.api.Response;

public interface ResponseApollo {
    void onResponse(Response res, Throwable thr, Object... params);
}
