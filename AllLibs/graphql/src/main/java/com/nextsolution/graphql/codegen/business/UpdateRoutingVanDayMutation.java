package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Mutation;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import java.lang.Integer;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.RoutingVanDay;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class UpdateRoutingVanDayMutation implements Mutation<UpdateRoutingVanDayMutation.Data, UpdateRoutingVanDayMutation.Data, UpdateRoutingVanDayMutation.Variables> {
  public static String OPERATION_DEFINITION = "mutation UpdateRoutingVanDay($id: Int!, $vanId: Int!) {   updateRoutingVanDay(id: $id, vanId: $vanId) {     ...RoutingVanDay   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "UpdateRoutingVanDay";
    }
  };
  private UpdateRoutingVanDayMutation.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public UpdateRoutingVanDayMutation.Data wrapData(UpdateRoutingVanDayMutation.Data data) {
    return data;
  }
  
  @Override
   public UpdateRoutingVanDayMutation.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<UpdateRoutingVanDayMutation.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "739ab4bf94360066830bff099416f1da";
  }
  
  public UpdateRoutingVanDayMutation(@Nonnull Integer id, @Nonnull Integer vanId) {
    Utils.checkNotNull(id, "id == null");
    Utils.checkNotNull(vanId, "vanId == null");
    this.variables = new UpdateRoutingVanDayMutation.Variables(id, vanId);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable RoutingVanDay updateRoutingVanDay;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("updateRoutingVanDay", "updateRoutingVanDay", new UnmodifiableMapBuilder<String, Object>(2).put("id", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "id").build()).put("vanId", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "vanId").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable RoutingVanDay updateRoutingVanDay) {
      this.updateRoutingVanDay = updateRoutingVanDay;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "updateRoutingVanDay=" + updateRoutingVanDay + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.updateRoutingVanDay == null) ? (that.updateRoutingVanDay == null) : this.updateRoutingVanDay.equals(that.updateRoutingVanDay));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (updateRoutingVanDay == null) ? 0 : updateRoutingVanDay.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], updateRoutingVanDay != null ? updateRoutingVanDay.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private RoutingVanDay.Mapper updateRoutingVanDayFieldMapper = new RoutingVanDay.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final RoutingVanDay updateRoutingVanDay = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<RoutingVanDay>() {
                  @Override
                  public RoutingVanDay read(ResponseReader reader) {
                    return updateRoutingVanDayFieldMapper.map(reader);
                  }
                });
        return new Data(updateRoutingVanDay);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable Integer id;
    private @Nullable Integer vanId;
    Builder() {
      
    }
    
    public Builder id(@Nullable Integer id) {
      this.id = id;
      return this;
    }
    
    public Builder vanId(@Nullable Integer vanId) {
      this.vanId = vanId;
      return this;
    }
    
    public UpdateRoutingVanDayMutation build() {
      return new UpdateRoutingVanDayMutation(id, vanId);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull Integer id;
    private @Nonnull Integer vanId;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public Integer id() {
      return id;
    }
    
    public Integer vanId() {
      return vanId;
    }
    
    public Variables(@Nonnull Integer id, @Nonnull Integer vanId) {
      this.id = id;
      this.valueMap.put("id", id);
      this.vanId = vanId;
      this.valueMap.put("vanId", vanId);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeInt("id", id);
          writer.writeInt("vanId", vanId);
        }
      };
    }
  }
  
}

