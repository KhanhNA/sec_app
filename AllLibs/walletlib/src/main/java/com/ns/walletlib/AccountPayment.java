
package com.ns.walletlib;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountPayment extends TsBaseModel {

    private String clientAccount;
    private String password;
    private Long clientId;
    @SerializedName("WALLET")
    @Expose
    private Wallet wallet;

    @SerializedName("POINT")
    @Expose
    private Point point;

    @SerializedName("coin")
    @Expose
    private Coin coin;


}
