package com.ns.walletlib;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Currency {
    private String code;
    private String name;
    private float decimalPlaces;
    private float inMultiplesOf;
    private String displaySymbol;
    private String nameCode;
    private String displayLabel;
}
