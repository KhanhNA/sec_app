package com.ns.walletlib;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionType {
    private float id;
    private String code;
    private String value;
    private boolean deposit;
    private boolean dividendPayout;
    private boolean withdrawal;
    private boolean interestPosting;
    private boolean feeDeduction;
    private boolean initiateTransfer;
    private boolean approveTransfer;
    private boolean withdrawTransfer;
    private boolean rejectTransfer;
    private boolean overdraftInterest;
    private boolean writtenoff;
    private boolean overdraftFee;
    private boolean withholdTax;
    private boolean escheat;
    private boolean amountHold;
    private boolean amountRelease;


}
