package com.sample;


public class SharevanDistanceCompute {

  private long id;
  private String name;
  private double distance;
  private String depotSeq;
  private String toSeq;
  private String status;
  private String type;
  private String nameSeq;
  private String description;
  private long createUid;
  private java.sql.Timestamp createDate;
  private long writeUid;
  private java.sql.Timestamp writeDate;
  private double minPrice;
  private double maxPrice;
  private String fromSeq;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }


  public String getDepotSeq() {
    return depotSeq;
  }

  public void setDepotSeq(String depotSeq) {
    this.depotSeq = depotSeq;
  }


  public String getToSeq() {
    return toSeq;
  }

  public void setToSeq(String toSeq) {
    this.toSeq = toSeq;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public String getNameSeq() {
    return nameSeq;
  }

  public void setNameSeq(String nameSeq) {
    this.nameSeq = nameSeq;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public long getCreateUid() {
    return createUid;
  }

  public void setCreateUid(long createUid) {
    this.createUid = createUid;
  }


  public java.sql.Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(java.sql.Timestamp createDate) {
    this.createDate = createDate;
  }


  public long getWriteUid() {
    return writeUid;
  }

  public void setWriteUid(long writeUid) {
    this.writeUid = writeUid;
  }


  public java.sql.Timestamp getWriteDate() {
    return writeDate;
  }

  public void setWriteDate(java.sql.Timestamp writeDate) {
    this.writeDate = writeDate;
  }


  public double getMinPrice() {
    return minPrice;
  }

  public void setMinPrice(double minPrice) {
    this.minPrice = minPrice;
  }


  public double getMaxPrice() {
    return maxPrice;
  }

  public void setMaxPrice(double maxPrice) {
    this.maxPrice = maxPrice;
  }


  public String getFromSeq() {
    return fromSeq;
  }

  public void setFromSeq(String fromSeq) {
    this.fromSeq = fromSeq;
  }

}
