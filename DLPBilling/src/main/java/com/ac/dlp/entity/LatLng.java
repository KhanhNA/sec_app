package com.ac.dlp.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LatLng {
    private Double latitude;
    private Double longitude;
}
