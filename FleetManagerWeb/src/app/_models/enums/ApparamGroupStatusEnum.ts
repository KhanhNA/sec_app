export enum ApparamGroupStatusEnum {
  _ = 'customer.status.all',
  _1 = 'customer.status.active',
  _0 = 'customer.status.deactivate'
}
