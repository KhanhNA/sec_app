import {FormGroup} from '@angular/forms';
import {DriverLicenseModel} from './driverlicense.model';
import {FleetModel} from './fleet.model';
import {ApparamValueModel} from './appparamvalue.model';
import {StaffStatusEnum} from './enums/StaffStatusEnum';

export class StaffModel {
  id: number;
  appParamValueId: number;
  staffCode: string;
  fullName: string;
  province: string;
  nation: string;
  country: string;
  phone: string;
  email: string;
  hireDate: string;
  leaveDate: string;
  birthDate: string;
  laborRate: number;
  billingRate: number;
  SSN: number;
  latitude: number;
  longitude: number;
  stateProvince: string;
  imei: string;
  point: number;
  idNo: string;
  status: StaffStatusEnum._1 | StaffStatusEnum._0;
  driverLicence: DriverLicenseModel;
  firstName: string;
  lastName: string;
  userName: string;
  fleetId: number;
  fleet : FleetModel;
  apparamValue : ApparamValueModel;


  constructor(form: FormGroup | number) {
    if (form instanceof FormGroup) {
      if (form.get('id')) {
        this.id = form.get('id').value;
      }
      if (form.get('staffCode')) {
        this.staffCode = form.get('staffCode').value;
      }
      if (form.get('fullName')) {
        this.fullName = form.get('fullName').value;
      }
      if (form.get('birthDate')) {
        this.birthDate = form.get('birthDate').value;
      }
      if (form.get('hireDate')) {
        this.hireDate = form.get('hireDate').value;
      }
      if (form.get('status')) {
        this.status = form.get('status').value === 1 ? StaffStatusEnum._1 : StaffStatusEnum._0;
      }
    } else {
      this.id = form;
    }
  }
}
