import {environment} from "../../../environments/environment";

export class AuthoritiesUtils {
  static hasAuthority(authority: string): boolean {
    if (!authority) {
      return false;
    }
    return environment.AUTHORITIES.includes(authority.toLowerCase());
  }
}
