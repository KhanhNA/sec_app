package ac.dlp.locationService.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationVehicle {
    private Long id;
    private Integer actionId;
    private String token;
    private String topic;
    private String click_action = "com.ts.sharevandriver.TARGET_ROUTING_PLAN_DAY";
    private String title;
    private String message;
    private String body;

    public NotificationVehicle(FleetVehicle vehicle, List<LocLatLng> tracings) {

        NotificationVehicleBody notificationVehicleBody = new NotificationVehicleBody(vehicle, tracings);
//        Gson gson = new Gson();
        Gson gson = new GsonBuilder()
                .setDateFormat(DateFormat.LONG)
                .create();
        this.body = gson.toJson(notificationVehicleBody);
        this.title = "update location";

    }
}
