package ac.dlp.locationService.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationVehicleBody {
    private Integer vehicleId;

    private String name;
    private Boolean active;
    private Integer companyId;

    private String licensePlate;
    private String vinSn;
    private Integer color;
    private Integer seats;
    private String modelYear;
    private Integer doors;
//    private Double latitude;
//    private Double longitude;
    private List<LocLatLng> locLatLngs;
    private Integer vehicleType;
    private Double engineSize;

    public NotificationVehicleBody(FleetVehicle vehicle, List<LocLatLng> lstTracing) {
        this.vehicleId = vehicle.getId();

        this.name = vehicle.getName();


        this.licensePlate = vehicle.getLicensePlate();

        this.color = vehicle.getColor();
        this.seats = vehicle.getSeats();
        this.modelYear = vehicle.getModelYear();
        this.doors = vehicle.getDoors();
//        this.latitude = vehicle.getLatitude();
//        this.longitude = vehicle.getLongitude();
        lstTracing.sort(Comparator.comparing(LocLatLng::getTime));
        locLatLngs = lstTracing;
        this.vehicleType = vehicle.getVehicleType();
        this.engineSize = vehicle.getEngineSize();

    }
}
