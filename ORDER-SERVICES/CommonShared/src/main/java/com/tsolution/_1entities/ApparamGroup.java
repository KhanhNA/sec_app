package com.tsolution._1entities;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "apparam_group")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class ApparamGroup extends SuperEntity implements Serializable {

    private static final long serialVersionUID = -4512271306292974315L;

    @Column(name = "group_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String groupCode;

    @Column(name = "group_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String groupName;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "ord")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer ord;

    @OneToMany
    @JoinColumn(name = "app_param_group_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<AppParam> appParams;

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOrd() {
        return ord;
    }

    public void setOrd(Integer ord) {
        this.ord = ord;
    }

    public List<AppParam> getAppParams() {
        return appParams;
    }

    public void setAppParams(List<AppParam> appParams) {
        this.appParams = appParams;
    }
}