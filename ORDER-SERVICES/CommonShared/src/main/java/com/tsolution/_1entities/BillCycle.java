package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "bill_cycle")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillCycle extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "cycle_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer cycleType;

    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @Column(name = "start_date", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime startDate;

    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @Column(name = "end_date", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime endDate;

    @Column(name = "monday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer monday;

    @Column(name = "tuesday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer tuesday;

    @Column(name = "wednesday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer wednesday;

    @Column(name = "thursday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer thursday;

    @Column(name = "friday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer friday;

    @Column(name = "saturday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer saturday;

    @Column(name = "sunday")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer sunday;

    @Column(name = "week1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer week1;

    @Column(name = "week2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer week2;

    @Column(name = "week3")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer week3;

    @Column(name = "week4")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer week4;

    @Column(name = "sequence")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer sequence;

    @Column(name = "delivery_date")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String deliveryDate;

    public Integer getCycleType() {
        return cycleType;
    }

    public void setCycleType(Integer cycleType) {
        this.cycleType = cycleType;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getMonday() {
        return monday;
    }

    public void setMonday(Integer monday) {
        this.monday = monday;
    }

    public Integer getTuesday() {
        return tuesday;
    }

    public void setTuesday(Integer tuesday) {
        this.tuesday = tuesday;
    }

    public Integer getWednesday() {
        return wednesday;
    }

    public void setWednesday(Integer wednesday) {
        this.wednesday = wednesday;
    }

    public Integer getThursday() {
        return thursday;
    }

    public void setThursday(Integer thursday) {
        this.thursday = thursday;
    }

    public Integer getFriday() {
        return friday;
    }

    public void setFriday(Integer friday) {
        this.friday = friday;
    }

    public Integer getSaturday() {
        return saturday;
    }

    public void setSaturday(Integer saturday) {
        this.saturday = saturday;
    }

    public Integer getSunday() {
        return sunday;
    }

    public void setSunday(Integer sunday) {
        this.sunday = sunday;
    }

    public Integer getWeek1() {
        return week1;
    }

    public void setWeek1(Integer week1) {
        this.week1 = week1;
    }

    public Integer getWeek2() {
        return week2;
    }

    public void setWeek2(Integer week2) {
        this.week2 = week2;
    }

    public Integer getWeek3() {
        return week3;
    }

    public void setWeek3(Integer week3) {
        this.week3 = week3;
    }

    public Integer getWeek4() {
        return week4;
    }

    public void setWeek4(Integer week4) {
        this.week4 = week4;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String toString() {
      return "BillCycle{cycleType=" + cycleType + 
        ", startDate=" + startDate + 
        ", endDate=" + endDate + 
        ", monday=" + monday + 
        ", tuesday=" + tuesday + 
        ", wednesday=" + wednesday + 
        ", thursday=" + thursday + 
        ", friday=" + friday + 
        ", saturday=" + saturday + 
        ", sunday=" + sunday + 
        ", week1=" + week1 + 
        ", week2=" + week2 + 
        ", week3=" + week3 + 
        ", week4=" + week4 + 
        ", sequence=" + sequence + 
        ", deliveryDate=" + deliveryDate + 
        "}";
    }
}