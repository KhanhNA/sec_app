package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "bill_package")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillPackage extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "bill_lading_detail_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long billLadingDetailId;

    @Column(name = "length")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float length;

    @Column(name = "width")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float width;

    @Column(name = "height")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float height;


    @OneToOne
    @JoinColumn(name = "app_param_value_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private ApparamValue apparamValue;

    @Column(name = "quantity_parcel")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer quantityParcel;

    @Column(name = "net_weight")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double netWeight;

    @Column(name = "capacity")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double capacity;

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Long getBillLadingDetailId() {
        return billLadingDetailId;
    }

    public void setBillLadingDetailId(Long billLadingDetailId) {
        this.billLadingDetailId = billLadingDetailId;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public ApparamValue getApparamValue() {
        return apparamValue;
    }

    public void setApparamValue(ApparamValue apparamValue) {
        this.apparamValue = apparamValue;
    }

    public Integer getQuantityParcel() {
        return quantityParcel;
    }

    public void setQuantityParcel(Integer quantityParcel) {
        this.quantityParcel = quantityParcel;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

}