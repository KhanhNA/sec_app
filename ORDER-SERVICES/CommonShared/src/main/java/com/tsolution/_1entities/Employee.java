package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Table(name = "employee")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Employee extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "app_param_value_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private AppParam appParam;

    @OneToMany
    @Transient
    @JsonIgnore
    @JoinColumn(name = "id", referencedColumnName = "object_id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<ObjAttachment> objAttachments;

    @Column(name = "employee_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String code;

    @Column(name = "employee_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String name;

    @Column(name = "has_image")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasImage;

    @Column(name = "has_attachment")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasAttachment;

    @Column(name = "birthday")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime birthday;

    @Column(name = "province")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String province;

    @Column(name = "nation")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String nation;

    @Column(name = "country")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String country;

    @Column(name = "address")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @Column(name = "phone")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone;

    @Column(name = "email")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String email;

    @Column(name = "driver_licence_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String driverLicenceId;

    @Column(name = "driver_licence_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer driverLicenceType;

    @Column(name = "hire_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime hireDate;

    @Column(name = "leave_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime leaveDate;

    @Column(name = "labor_rate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer laborRate;

    @Column(name = "billing_rate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer billingRate;

    @Column(name = "SSN")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer SSN;

    @Column(name = "latitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double latitude;

    @Column(name = "longitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double longitude;

    @Column(name = "state_province")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String stateProvince;

    @Column(name = "license_note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String licenseNote;

    @Column(name = "imei")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String imei;

    @Column(name = "point")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer point;

    @ManyToOne
    @JoinColumn(name = "object_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Warehouse warehouse;

    @Column(name = "objectType")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer objectType;

    @Column(name = "id_no")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String idNo;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    public AppParam getAppParam() {
        return appParam;
    }

    public void setAppParam(AppParam appParam) {
        this.appParam = appParam;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Integer getHasImage() {
        return hasImage;
    }

    public void setHasImage(Integer hasImage) {
        this.hasImage = hasImage;
    }

    public Integer getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(Integer hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDriverLicenceId() {
        return driverLicenceId;
    }

    public void setDriverLicenceId(String driverLicenceId) {
        this.driverLicenceId = driverLicenceId;
    }

    public Integer getDriverLicenceType() {
        return driverLicenceType;
    }

    public void setDriverLicenceType(Integer driverLicenceType) {
        this.driverLicenceType = driverLicenceType;
    }

    public LocalDateTime getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDateTime hireDate) {
        this.hireDate = hireDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(LocalDateTime leaveDate) {
        this.leaveDate = leaveDate;
    }

    public Integer getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(Integer laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getBillingRate() {
        return billingRate;
    }

    public void setBillingRate(Integer billingRate) {
        this.billingRate = billingRate;
    }

    public Integer getSSN() {
        return SSN;
    }

    public void setSSN(Integer SSN) {
        this.SSN = SSN;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getLicenseNote() {
        return licenseNote;
    }

    public void setLicenseNote(String licenseNote) {
        this.licenseNote = licenseNote;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getObjectType() {
        return objectType;
    }

    public void setObjectType(Integer objectType) {
        this.objectType = objectType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ObjAttachment> getObjAttachments() {
        return objAttachments;
    }

    public void setObjAttachments(List<ObjAttachment> objAttachments) {
        this.objAttachments = objAttachments;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "appParamValue=" + appParam +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", hasImage=" + hasImage +
                ", hasAttachment=" + hasAttachment +
                ", birthday=" + birthday +
                ", province='" + province + '\'' +
                ", nation='" + nation + '\'' +
                ", country='" + country + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", driverLicenceId='" + driverLicenceId + '\'' +
                ", driverLicenceType=" + driverLicenceType +
                ", hireDate=" + hireDate +
                ", leaveDate=" + leaveDate +
                ", laborRate=" + laborRate +
                ", billingRate=" + billingRate +
                ", SSN=" + SSN +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", stateProvince='" + stateProvince + '\'' +
                ", licenseNote='" + licenseNote + '\'' +
                ", imei='" + imei + '\'' +
                ", point=" + point +
                ", warehouse=" + warehouse +
                ", objectType=" + objectType +
                ", idNo='" + idNo + '\'' +
                ", status=" + status +
                '}';
    }
}