package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Table(name = "employee_log")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class EmployeeLog extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;
 

    @ManyToOne
    @JoinColumn(name = "employee_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Employee employeeId;

    @OneToMany
    @Transient
    @JsonIgnore
    @JoinColumn(name = "id", referencedColumnName = "object_id", insertable = false, updatable = false)
    @JsonView(JsonEntityViewer.Human.Detail.class)
    private List<Warehouse> warehouses;

    @Column(name = "employee_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String employeeCode;

    @Column(name = "employee_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String employeeName;

    @ManyToOne
    @JoinColumn(name = "app_param_value_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private AppParam appParam;

    @Column(name = "has_image")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasImage;

    @Column(name = "has_attachment")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer hasAttachment;

    @Column(name = "birthday")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime birthday;

    @Column(name = "province")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String province;

    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String nation;

    @Column(name = "country")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String country;

    @Column(name = "address")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @Column(name = "phone")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone;

    @Column(name = "email")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String email;

    @Column(name = "driver_licence_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String driverLicenceId;

    @Column(name = "driver_licence_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer driverLicenceType;

    @Column(name = "hire_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime hireDate;

    @Column(name = "leave_date")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime leaveDate;

    @Column(name = "from_time")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime fromTime;

    @Column(name = "to_time")
    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime toTime;

    @Column(name = "labor_rate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer laborRate;

    @Column(name = "billing_rate")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer billingRate;

    @Column(name = "SSN")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer SSN;

    @Column(name = "latitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double latitude;

    @Column(name = "longitude")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double longitude;

    @Column(name = "state_province")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String stateProvince;

    @Column(name = "license_note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String licenseNote;

    @Column(name = "imei")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String imei;

    @Column(name = "point")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer point;

    @ManyToOne
    @JoinColumn(name = "object_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Warehouse warehouse;

    @Column(name = "object_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer objectType;

    @Column(name = "id_no")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String idNo;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "is_active")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer isActive;

    @Column(name = "pre_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long preId;

    public Employee getEmployee() {
        return employeeId;
    }

    public void setEmployee(Employee employee) {
        this.employeeId = employee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Warehouse> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(List<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }

    public AppParam getAppParam() {
        return appParam;
    }

    public void setAppParam(AppParam appParam) {
        this.appParam = appParam;
    }

    public Integer getHasImage() {
        return hasImage;
    }

    public void setHasImage(Integer hasImage) {
        this.hasImage = hasImage;
    }

    public Integer getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(Integer hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDriverLicenceId() {
        return driverLicenceId;
    }

    public void setDriverLicenceId(String driverLicenceId) {
        this.driverLicenceId = driverLicenceId;
    }

    public Integer getDriverLicenceType() {
        return driverLicenceType;
    }

    public void setDriverLicenceType(Integer driverLicenceType) {
        this.driverLicenceType = driverLicenceType;
    }

    public LocalDateTime getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDateTime hireDate) {
        this.hireDate = hireDate;
    }

    public LocalDateTime getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(LocalDateTime leaveDate) {
        this.leaveDate = leaveDate;
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(LocalDateTime fromTime) {
        this.fromTime = fromTime;
    }

    public LocalDateTime getToTime() {
        return toTime;
    }

    public void setToTime(LocalDateTime toTime) {
        this.toTime = toTime;
    }

    public Integer getLaborRate() {
        return laborRate;
    }

    public void setLaborRate(Integer laborRate) {
        this.laborRate = laborRate;
    }

    public Integer getBillingRate() {
        return billingRate;
    }

    public void setBillingRate(Integer billingRate) {
        this.billingRate = billingRate;
    }

    public Integer getSSN() {
        return SSN;
    }

    public void setSSN(Integer SSN) {
        this.SSN = SSN;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getLicenseNote() {
        return licenseNote;
    }

    public void setLicenseNote(String licenseNote) {
        this.licenseNote = licenseNote;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }


    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Integer getObjectType() {
        return objectType;
    }

    public void setObjectType(Integer objectType) {
        this.objectType = objectType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Long getPreId() {
        return preId;
    }

    public void setPreId(Long preId) {
        this.preId = preId;
    }

    public Employee getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employee employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}
