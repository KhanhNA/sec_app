package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "notification")
@Entity
public class Notification extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "notifycation_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer notifycationType;

    @Column(name = "notifycation_content")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String notifycationContent;

    @Column(name = "title")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String title;

    @Column(name = "topic")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String topic;

    @Column(name = "description")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String description;

    @Column(name = "object_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long objectId;

    @Column(name = "object_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer objectType;

    @Column(name = "priority")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer priority;

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getNotifycationType() {
        return notifycationType;
    }

    public void setNotifycationType(Integer notifycationType) {
        this.notifycationType = notifycationType;
    }

    public String getNotifycationContent() {
        return notifycationContent;
    }

    public void setNotifycationContent(String notifycationContent) {
        this.notifycationContent = notifycationContent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Integer getObjectType() {
        return objectType;
    }

    public void setObjectType(Integer objectType) {
        this.objectType = objectType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}