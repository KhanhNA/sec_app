package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "routing_van")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class RoutingVan extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "routing_van_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String routingVanCode;

    @ManyToOne
    @JoinColumn (name = "van_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Van van;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "from_time")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime fromTime;

    @Column(name = "to_time")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDateTime toTime;

    @Column(name = "status_rout")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer statusRout;

    @Transient
	@JsonView(JsonEntityViewer.Human.Summary.class)
    private List<RoutingPlanDay> planDayList;

    @ManyToOne
    @JoinColumn(name = "driver_van_id", columnDefinition = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private DriverVan driverVan;

    public DriverVan getDriverVan() {
        return driverVan;
    }

    public void setDriverVan(DriverVan driverVan) {
        this.driverVan = driverVan;
    }

    public List<RoutingPlanDay> getPlanDayList() {
        return planDayList;
    }

    public void setPlanDayList(List<RoutingPlanDay> planDayList) {
        this.planDayList = planDayList;
    }

    public Integer getStatusRout() {
        return statusRout;
    }

    public void setStatusRout(Integer statusRout) {
        this.statusRout = statusRout;
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(LocalDateTime fromTime) {
        this.fromTime = fromTime;
    }

    public LocalDateTime getToTime() {
        return toTime;
    }

    public void setToTime(LocalDateTime toTime) {
        this.toTime = toTime;
    }

    public String getRoutingVanCode() {
        return routingVanCode;
    }

    public void setRoutingVanCode(String routingVanCode) {
        this.routingVanCode = routingVanCode;
    }

    public Van getVan() {
        return van;
    }

    public void setVan(Van van) {
        this.van = van;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public String toString() {
      return "RoutingVan{routingVanCode=" + routingVanCode + 
      //  ", vanId=" + vanId +
        ", status=" + status + 
      //  ", runningDate=" + runningDate +
        "}";
    }
}