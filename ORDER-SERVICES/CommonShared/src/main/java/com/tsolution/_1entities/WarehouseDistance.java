package com.tsolution._1entities;

import com.tsolution._1entities.base.SuperEntity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.tsolution.utils.Constants;
import com.tsolution.utils.SerializeDateHandler;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalTime;

@Table(name = "warehouse_distance")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class WarehouseDistance extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "customer_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer customerId;

    @Column(name = "code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String code;

    @Column(name = "from_warehouse_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fromWarehouseId;

    @Column(name = "to_warehouse_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long toWarehouseId;

    @Column(name = "from_warehouse_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String fromWarehouseName;

    @Column(name = "to_warehouse_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String toWarehouseName;

    @Column(name = "distance")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float distance;

    @JsonFormat(pattern = Constants.DATE_PATTERN, shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = SerializeDateHandler.class)
    @Column(name = "estimated_delivery", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalTime estimatedTime;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;
    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getFromWarehouseId() {
        return fromWarehouseId;
    }

    public void setFromWarehouseId(Long fromWarehouseId) {
        this.fromWarehouseId = fromWarehouseId;
    }

    public Long getToWarehouseId() {
        return toWarehouseId;
    }

    public void setToWarehouseId(Long toWarehouseId) {
        this.toWarehouseId = toWarehouseId;
    }

    public String getFromWarehouseName() {
        return fromWarehouseName;
    }

    public void setFromWarehouseName(String fromWarehouseName) {
        this.fromWarehouseName = fromWarehouseName;
    }

    public String getToWarehouseName() {
        return toWarehouseName;
    }

    public void setToWarehouseName(String toWarehouseName) {
        this.toWarehouseName = toWarehouseName;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public LocalTime getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(LocalTime estimatedTime) {
        this.estimatedTime = estimatedTime;
    }
}
