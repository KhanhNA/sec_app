package com.tsolution._1entities.enums;

public enum EqPartType {
    USING(1), STOPPED(2);

    private Integer value;

    private EqPartType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
