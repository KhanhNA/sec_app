package com.tsolution._1entities.enums;

public enum OrganizationStatus {

    ACTIVE(true),
    DEACTIVE(false);

    private boolean value;

    OrganizationStatus(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return this.value;
    }
}
