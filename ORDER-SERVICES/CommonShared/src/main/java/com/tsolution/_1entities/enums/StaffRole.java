package com.tsolution._1entities.enums;

public enum StaffRole {
	ROLE_NAME_DRIVER("DRIVER"),ROLE_NAME_DRIVER_MANAGER("DRIVER_MANAGER");

	private String value;

	StaffRole(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
