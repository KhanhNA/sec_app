package com.tsolution._1entities.enums;

public enum TokenFbStatus {
    EMPLOYEE(0),
    STORE_KEEPER(1),
    FLEET_MANAGER(2),
    DRIVER(3),
    SALE_MAN(4),
    SHARING_VAN_ADMIN(5);

    private Integer value;

    TokenFbStatus(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}