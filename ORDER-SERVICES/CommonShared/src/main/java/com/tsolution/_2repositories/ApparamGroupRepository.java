package com.tsolution._2repositories;

import com.tsolution._2repositories.impl.ApparamGroupRepositoryCustomImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ApparamGroup;

import java.util.Optional;

public interface ApparamGroupRepository extends JpaRepository<ApparamGroup, Long>, JpaSpecificationExecutor<ApparamGroup>, ApparamGroupRepositoryCustom {
    Optional<ApparamGroup> findByGroupCode(String code);
}