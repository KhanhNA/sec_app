package com.tsolution._2repositories;

import com.tsolution._1entities.ApparamGroup;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ApparamGroupRepositoryCustom {
    Page<ApparamGroup> find(ApparamGroup entity, Pageable pageable) throws BusinessException;

}
