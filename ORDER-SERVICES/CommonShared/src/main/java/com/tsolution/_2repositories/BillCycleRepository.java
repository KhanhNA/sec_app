package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.BillCycle;

public interface BillCycleRepository extends JpaRepository<BillCycle, Long>, JpaSpecificationExecutor<BillCycle> {

}