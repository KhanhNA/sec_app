package com.tsolution._2repositories;

import com.tsolution._1entities.BillPackageRouting;
import com.tsolution.excetions.BusinessException;

import java.util.List;

public interface BillPackageRoutingRepositoryCustom {
    List<BillPackageRouting> getPackageByRoutingPlan(Long routingPlanDayId) throws BusinessException;
}
