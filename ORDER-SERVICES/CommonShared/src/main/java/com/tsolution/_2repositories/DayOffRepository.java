package com.tsolution._2repositories;

import com.tsolution.excetions.BusinessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.DayOff;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;

public interface DayOffRepository extends JpaRepository<DayOff, Long>, JpaSpecificationExecutor<DayOff>, DayOffRepositoryCustom {

    //phục vụ cho việc search khi check staff nghỉ
    @Query(value = "Select * from day_off do WHERE 1=1 and do.status = 1  " +
            "and do.staff_id = ?1 and DATE_FORMAT(do.from_date, '%Y-%m-%d') <= ?2 and do.to_date >= ?3"
            , nativeQuery = true)
    DayOff findAvailable(Long staffId, Date fdate, Date tdate) throws BusinessException;
}