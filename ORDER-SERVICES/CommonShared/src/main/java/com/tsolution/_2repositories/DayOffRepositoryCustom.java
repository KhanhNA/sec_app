package com.tsolution._2repositories;

import com.tsolution._1entities.DayOff;
import com.tsolution.excetions.BusinessException;

import java.sql.Date;
import java.util.List;

public interface DayOffRepositoryCustom {
    List<DayOff> find(Long staffId, Date fDate, Date tDate, Integer spec, Integer status) throws BusinessException;
}
