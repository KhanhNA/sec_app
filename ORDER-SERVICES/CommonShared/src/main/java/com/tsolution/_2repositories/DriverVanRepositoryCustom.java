package com.tsolution._2repositories;

import com.tsolution._1entities.DriverVan;
import com.tsolution.excetions.BusinessException;

import java.time.LocalDateTime;
import java.util.List;

public interface DriverVanRepositoryCustom {
    List<DriverVan> getVanForDriver(Long driverId, LocalDateTime dateCheck) throws BusinessException;
}
