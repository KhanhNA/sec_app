package com.tsolution._2repositories;

import com.tsolution._1entities.Employee;
import com.tsolution._1entities.EmployeeLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import scala.Int;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EmployeesLogRepository extends JpaRepository<EmployeeLog, Long>, JpaSpecificationExecutor<EmployeeLog> {
    List<EmployeeLog> findAllByEmployeeCode(String code);

    EmployeeLog findByEmployeeCodeAndIsActive(String code,int status);

    @Query("SELECT u FROM EmployeeLog u WHERE u.employeeId = ?1 ")
    List<EmployeeLog> findListEmployeeLogByEmployeeId(Long employeeId);

    Optional<EmployeeLog> findByIsActiveAndEmployeeCode(int isAcive,String code);

    @Query("SELECT u FROM EmployeeLog u WHERE u.employeeCode = ?1 and u.isActive = 1 ")
    Optional<EmployeeLog> findEmployeeCodeAndIsActve(String  code);

}