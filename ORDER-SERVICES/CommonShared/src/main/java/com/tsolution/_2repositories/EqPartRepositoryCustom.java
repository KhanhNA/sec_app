package com.tsolution._2repositories;

import com.tsolution._1entities.EqPart;
import com.tsolution.excetions.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EqPartRepositoryCustom {
    Page<EqPart> findByCondition(EqPart searchData, Pageable pageable) throws BusinessException;
}
