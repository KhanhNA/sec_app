package com.tsolution._2repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.Rating;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long>, JpaSpecificationExecutor<Rating> {
    @Query("select r from Rating r where r.routingPlanDayId = ?1 and r.status = 1")
    Rating findByRoutingPlanDayId(Long routingPlanDayId);

    @Query(value = "SELECT num_rating rating, count(*) count  FROM rating " +
            "where driver_id = ? and status = 1 group by num_rating",
            countQuery = "select count(*) from rating",
            nativeQuery = true)
    List<Object[]> getDriverRating(Long driverId);// Get rating of driver, return: <rating, ratingCount>

    @Query(value = "SELECT num_rating rating, COUNT(*) count FROM " +
            " (select num_rating from rating where driver_id = ? AND status = 1 order by create_date desc limit 100) r " +
            " GROUP BY num_rating",
            countQuery = "select count(*) from rating",
            nativeQuery = true)
    List<Object[]> get100LatestRating(Long driverId);// Get rating of driver, return: <rating, ratingCount>

}