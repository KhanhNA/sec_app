package com.tsolution._2repositories.impl;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamValue;
import com.tsolution._1entities.Warehouse;
import com.tsolution._1entities.enums.WarehouseStatus;
import com.tsolution.utils.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

//import com.tsolution._1entities.dto.FreeVolumeDto;
import com.tsolution._2repositories.WarehouseRepositoryCustom;
import com.tsolution.excetions.BusinessException;

@Repository
public class WarehouseRepositoryCustomImpl implements WarehouseRepositoryCustom {

	@PersistenceContext
	private EntityManager em;

	private static final int PRODUCT_TYPE = 13;

//	@Override
//	public List<FreeVolumeDto> calculateFreeVolumeWarehouse(Date fromDate, Date toDate, List<Long> warehouseIds,
//			List<Integer> customerRentStatus) throws BusinessException {
//		StringBuilder sb = new StringBuilder();
//		sb.append(" select crd.warehouse_id id, xdate.Date, crd.warehouse_id, w.code warehouse_code, ");
//		sb.append("		   null parcel_id, null parcel_code, ");
//		sb.append("		   nvl(w.total_volume, 0) - sum(case ");
//		sb.append("							    when xdate.Date between crd.from_date and crd.to_date ");
//		sb.append("							    then crd.total_volume else 0 end) free_volume ");
//		sb.append(" from customer_rent_detail crd ");
//		sb.append("      join customer_rent cr on cr.id = crd.customer_rent_id ");
//		sb.append("      join warehouse w on w.id = crd.warehouse_id ");
////		sb.append("      join parcel p on p.id = crd.parcel_id ")
//		sb.append("      ,( ");
//		sb.append("			select a.Date ");
//		sb.append("         from ( ");
//		sb.append("               select :toDate - INTERVAL (a.a +(10*b.a)+(100*c.a)) DAY as Date ");
//		sb.append(" 			  from (select 0 as a union all select 1 union all select 2 union all select 3 ");
//		sb.append("							   union all select 4 union all select 5 union all select 6 ");
//		sb.append("							   union all select 7 union all select 8 union all select 9) as a ");
//		sb.append("               cross join (select 0 as a union all select 1 union all select 2 union all select 3 ");
//		sb.append("									 union all select 4 union all select 5 union all select 6 ");
//		sb.append("									 union all select 7 union all select 8 union all select 9) as b ");
//		sb.append("               cross join (select 0 as a union all select 1 union all select 2 union all select 3 ");
//		sb.append("									 union all select 4 union all select 5 union all select 6 ");
//		sb.append("									 union all select 7 union all select 8 union all select 9) as c ");
//		sb.append("         ) a where a.Date between :fromDate and :toDate ");
//		sb.append("		 ) xdate ");
//		sb.append(" where crd.from_date <= :toDate and crd.to_date >= :fromDate ");
//		sb.append(" 	  and cr.status in (:customerRentStatus) ");
//		sb.append(" 	  and crd.warehouse_id in (:waehouseIds) ");
//		sb.append(" group by xdate.Date, crd.warehouse_id ");
//		sb.append(" order by xdate.Date, w.code ");
//
//		Map<String, Object> params = new HashMap<>();
//		params.put("fromDate", fromDate);
//		params.put("toDate", toDate);
//		params.put("waehouseIds", warehouseIds);
//		params.put("customerRentStatus", customerRentStatus);
//
//		return BaseRepository.getResultListNativeQuery(this.em, sb.toString(), params, FreeVolumeDto.class);
//	}

	@Override
	public List<Warehouse> getListWarehouseByCustomer(Long customerId) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from warehouse where status= :statusWarehouse and customer_id = :customerId");
		Map<String,Object> params = new HashMap<>();
		Integer status = WarehouseStatus.ACTIVED.getValue();
		params.put("statusWarehouse", status);
		params.put("customerId",customerId);

		return BaseRepository.getResultListNativeQuery(em,sb.toString(),params,Warehouse.class);
	}


	@Override
	public List<ApparamValue> getListProductType() throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT av.* FROM apparam_group apg ");
		sb.append(" JOIN app_param ap ON ap.app_param_group_id = apg.id ");
		sb.append(" JOIN apparam_value av ON av.param_id = ap.id");
		sb.append(" WHERE 1=1 AND  av.status = :status  AND ap.id  = :productType");
		Map<String,Object> params = new HashMap<>();
		Integer status = WarehouseStatus.ACTIVED.getValue();
		params.put("status", status);
		params.put("productType",PRODUCT_TYPE);
		return BaseRepository.getResultListNativeQuery(em,sb.toString(),params,ApparamValue.class);
	}

	@Override
	public Page<Warehouse> findListWareHouse(Integer customerId,String code, String name, Integer status, Pageable pageable) throws BusinessException {
		if(customerId == null){
			return null;
		}
		StringBuilder subQuery = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		subQuery.append(" From warehouse wh");
		subQuery.append(" Where wh.customer_id = :customerId");
		params.put("customerId",customerId);
		if(!StringUtils.isNullOrEmpty(code)){
			subQuery.append(" And LOWER(wh.code) LIKE LOWER(CONCAT('%',:code,'%'))");
			params.put("code",code);
		}
		if(status != null){
			subQuery.append(" And wh.status =:status");
			params.put("status",status);
		}
		if(!StringUtils.isNullOrEmpty(name)){
			subQuery.append(" And LOWER(wh.name) LIKE LOWER(CONCAT('%',:name,'%'))");
			params.put("name",name);
		}
		StringBuilder queryGetCount = new StringBuilder();
		queryGetCount.append("Select count(wh.id)");
		queryGetCount.append(subQuery);

		StringBuilder queryGetList = new StringBuilder();
		queryGetList.append("Select wh.*");
		queryGetList.append(subQuery);

		return BaseRepository.getPagedNativeQuery(this.em, queryGetList.toString(), queryGetCount.toString(), params,
				pageable, Warehouse.class);
	}


}
