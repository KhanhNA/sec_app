package com.tsolution._3services;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tsolution.excetions.BusinessException;

public interface BaseService<T extends Serializable> {
	 ResponseEntity<Object> findAll();

	 ResponseEntity<Object> findById(Long id) throws BusinessException;

	 ResponseEntity<Object> create(List<T> entities) throws BusinessException;

	 ResponseEntity<Object> update(Long id, T source) throws BusinessException;

}
