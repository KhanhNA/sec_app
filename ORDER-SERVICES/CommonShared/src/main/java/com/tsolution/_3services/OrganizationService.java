package com.tsolution._3services;

import com.tsolution._1entities.Organization;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface OrganizationService extends BaseService<Organization> {
    ResponseEntity<Object> find(Organization org, Integer pageNumber, Integer pageSize) throws BusinessException;

    ResponseEntity<Object> activeOrDeactivate(Long id, boolean isActive)
            throws BusinessException;
}
