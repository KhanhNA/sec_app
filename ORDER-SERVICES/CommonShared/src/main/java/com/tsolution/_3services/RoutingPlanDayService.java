package com.tsolution._3services;

import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution.excetions.BusinessException;

import java.time.LocalDateTime;
import java.util.List;

public interface RoutingPlanDayService  extends  BaseService<RoutingPlanDay>{
    RoutingPlanDay updateStatusRoutingPlanDay(Long routingPlanDayId, Integer status) throws BusinessException;
    List<RoutingPlanDay> getRoutPlanForVanByDate(Long vanId, LocalDateTime fDate, LocalDateTime tDate) throws BusinessException;
}
