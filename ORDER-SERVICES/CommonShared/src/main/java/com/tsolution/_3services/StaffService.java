package com.tsolution._3services;

import com.tsolution._1entities.Staff;
import com.tsolution.excetions.BusinessException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;

public interface StaffService {
    ResponseEntity<Object> create(String authorization, String language, Staff entities) throws BusinessException;
    ResponseEntity<Object> update(String authorization, String acceptLanguage, Staff entities) throws BusinessException;
    ResponseEntity<Object> findAll();

    ResponseEntity<Staff> findById( Long id) throws BusinessException;
    ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, Staff entity)
            throws BusinessException;
    ResponseEntity<Object> activeOrDeActivate(String authorization, String acceptLanguage, Staff entity, boolean isActive)
            throws BusinessException;

    ResponseEntity<Object> findAllStaffAvailable(LocalDateTime fdate, LocalDateTime tdate) throws BusinessException;

    ResponseEntity<Staff> findByUserName(String authorization, String acceptLanguage, String username) throws BusinessException;

    ResponseEntity<Object> findStaff(Integer feetId,String userName,String fullName,String objType,String email,String phoneNumber,Integer status,Integer pageNumber, Integer pageSize)throws BusinessException;

    Resource getExcelTemplate(String fileName) throws BusinessException, IOException;

    ResponseEntity<Object> updateStaffByExcel(MultipartFile file) throws IOException, InvalidFormatException;
}
