package com.tsolution._3services;

import org.springframework.http.ResponseEntity;

import com.tsolution._1entities.User;
import com.tsolution.excetions.BusinessException;

public interface UserService {

	ResponseEntity<Object> findAll();

	ResponseEntity<Object> findById(String authorization, String acceptLanguage, String username)
			throws BusinessException;

	ResponseEntity<Object> find(User user, Integer pageNumber, Integer pageSize) throws BusinessException;

	ResponseEntity<Object> create(String authorization, String acceptLanguage, User entities) throws BusinessException;

	ResponseEntity<Object> update(String authorization, String acceptLanguage, String username, User source)
			throws BusinessException;

	ResponseEntity<Object> active(String authorization, String acceptLanguage, User user) throws BusinessException;

	ResponseEntity<Object> deactive(String authorization, String acceptLanguage, User user) throws BusinessException;

	ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, User user)
			throws BusinessException;
}
