package com.tsolution._3services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamGroup;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._3services.AppParamService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;

@Service
public class AppParamServiceImpl  extends BaseServiceImpl implements AppParamService {
    private SingleSignOnUtils singleSignOnUtils;

    @Autowired
    public AppParamServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(AppParam entities) throws BusinessException {

        if (entities == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }

        Optional<AppParam> oApparam;
        if (!StringUtils.isNullOrEmpty(entities.getAppParamCode())) {
            String appParamCode = entities.getAppParamCode();
            oApparam = this.appParamRepository.findByAppParamCode(appParamCode);
            if (oApparam.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("apparam.is.already.exist.code",appParamCode));
            }
        }
        else {
            throw new BusinessException(this.translator.toLocaleByFormatString("apparam.input.missing.code"));
        }
        if (entities.getStatus()==null || "".equals(entities.getStatus())){
            entities.setStatus(StatusType.RUNNING.getValue());
        }
      //  entities.setApparamValues(null);

        entities = this.appParamRepository.save(entities);
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findAll() throws BusinessException {
        return new ResponseEntity<>(this.appParamRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.appParamRepository.findById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> find(String apparamCode,Integer status) throws BusinessException {
        return new ResponseEntity<>(this.appParamRepository.findByAppParamCodeAndAndStatus(apparamCode,status) , HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(Long ApparamId, AppParam entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(
                    this.translator.toLocale("common.input.info.invalid"));
        }
        if (entities == null || entities.getId() == null || !ApparamId.equals(entities.getId())) {
            throw new BusinessException(
                    this.translator.toLocale("apparam.input.missing.id"));
        }
        Optional<AppParam> oApparam= this.appParamRepository.findById(ApparamId);
       /* if (entities.getApparamGroup().getId()!=null){
            Optional<ApparamGroup> ogrp = this.apparamGroupRepository.findById(entities.getApparamGroup().getId());
            if (!ogrp.isPresent()) {
                throw new BusinessException(
                        this.translator.toLocaleByFormatString("apparamgr.does.not.exist"));
            }
            entities.setApparamGroup(ogrp.get());
        }*/
        if (!oApparam.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("apparam.does.not.exist", ApparamId));
        }


        AppParam newApparam = oApparam.get();
        newApparam.setAppParamName(entities.getAppParamName());
        newApparam.setStatus(entities.getStatus());
        newApparam.setDescription(entities.getDescription());
     //   newApparam.setApparamGroup(entities.getApparamGroup());
        newApparam = this.appParamRepository.save(newApparam);
        return new ResponseEntity<>(newApparam, HttpStatus.OK);
    }
}
