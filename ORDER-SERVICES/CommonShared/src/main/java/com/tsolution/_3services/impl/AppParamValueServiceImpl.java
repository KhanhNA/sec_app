package com.tsolution._3services.impl;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamValue;
import com.tsolution._3services.AppParamValueService;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AppParamValueServiceImpl extends BaseServiceImpl implements AppParamValueService {

    private SingleSignOnUtils singleSignOnUtils;

    @Autowired
    public AppParamValueServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(ApparamValue entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }

        Optional<ApparamValue> oApparamValue;
        if (!StringUtils.isNullOrEmpty(entities.getParamCode())) {
            String appParamValueCode = entities.getParamCode();
            oApparamValue = this.apparamValueRepository.findByParamCode(appParamValueCode);
            if (oApparamValue.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("apparamvalue.is.already.exist.code"));
            }
        }
        else {
            throw new BusinessException(this.translator.toLocaleByFormatString("apparamvalue.input.missing.code"));
        }
        if (entities.getStatus()==null || "".equals(entities.getStatus())){
            entities.setStatus(StatusType.RUNNING.getValue());
        }
        entities = this.apparamValueRepository.save(entities);
        return new ResponseEntity<>(entities, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findAll() throws BusinessException {
        return new ResponseEntity<>(this.apparamValueRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.apparamValueRepository.findById(id), HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update( Long id, ApparamValue entities) throws BusinessException {
        if (entities == null) {
            throw new BusinessException(
                    this.translator.toLocale("common.input.info.invalid"));
        }
        if (entities == null || entities.getId() == null || !id.equals(entities.getId())) {
            throw new BusinessException(
                    this.translator.toLocale("apparamvalue.input.missing.id"));
        }
        Optional<ApparamValue> oAppValue= this.apparamValueRepository.findById(id);
        if (!oAppValue.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("apparamvalue.does.not.exist", id));
        }

/*        if (entities.getAppParam().getId() != null ){
            Optional<AppParam> oApparam = this.appParamRepository.findById(entities.getAppParam().getId());
            if (!oApparam.isPresent()) {
                throw new BusinessException(
                        this.translator.toLocaleByFormatString("apparam.does.not.exist"));
            }
            entities.setAppParam(oApparam.get());
        }*/

        ApparamValue newApparamV = oAppValue.get();
        newApparamV.setParamName(entities.getParamName());
        //newApparamV.setAppParam(entities.getAppParam());
        newApparamV.setStatus(entities.getStatus());
        newApparamV.setOrd(entities.getOrd());
        newApparamV = this.apparamValueRepository.save(newApparamV);
        return new ResponseEntity<>(newApparamV, HttpStatus.OK);
    }
}
