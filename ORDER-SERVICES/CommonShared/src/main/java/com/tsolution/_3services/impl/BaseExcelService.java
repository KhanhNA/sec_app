package com.tsolution._3services.impl;

import com.tsolution.excetions.BusinessException;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public abstract class BaseExcelService<T> extends BaseServiceImpl {
    private static final Logger log = LogManager.getLogger(VanServiceImpl.class);
    @Autowired
    private FileStorageService fileStorageService;

    protected int dataRow;

    @Value("${file.excel-template}")
    String templatePath;
    @Value("${file.temp-export-excel}")
    String outputFolder;

    public static class ExcelTemplate {
        static final String VAN_EXCEL_ADD = "import_van";
        static final String STAFF_EXCEL_ADD = "import_staff";
    }

    protected abstract Resource exportExcelDataTemplate(List<List<Object>> lstData, String tempFileName,
                                                        Map<String, Object> fileHeaderParams) throws Exception;

    protected abstract T readRow(Row row, Map<String, Object> dynamicValues) throws BusinessException;

    @Transactional
    protected abstract void persistAll(List<T> listData);

    protected abstract Map<String, Object> searchValidateValue() throws BusinessException;

    @Transactional
    public ResponseEntity<Object> importExcel(MultipartFile file) throws BusinessException {
        try {
            InputStream inputStream = file.getInputStream();
            Workbook workbook = WorkbookFactory.create(inputStream);
            Sheet dataTypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = dataTypeSheet.iterator();

            List<T> listData = new ArrayList<>();
            for (int i = 0; i < this.dataRow; i++)
                iterator.next();// Move to the row that has data, dataRow = header row + 1
            Map<String, Object> validateValue = searchValidateValue();
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                T rowData = readRow(currentRow, validateValue);
                if (rowData == null)
                    throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
                listData.add(rowData);
            }

            persistAll(listData);
        } catch (IOException | InvalidFormatException e) {
            log.error(e);
            throw new BusinessException(this.translator.toLocale(BusinessException.COMMON_INPUT_INFO_INVALID));
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public Resource exportTemplate() throws Exception {
        // Get file template name
        // Create list of CellBean - List value for dropdown
        // put all in list <List<Object>>
        return null;
    }


    protected Resource updateExcelFile(Map<String, Object> beans, String fileName)
            throws BusinessException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String path = "template/";
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream(path + fileName + "_" + LocaleContextHolder.getLocale()+
                        ".xlsx");
        Resource result = null;
        Path folderPath = Paths.get(this.fileStorageService.getFileStorageLocation().normalize().toString(), path).normalize();
        Path filePath = folderPath.resolve(path + fileName);
        if (!folderPath.toFile().exists()) {
            try {
                FileUtils.forceMkdirParent(filePath.toFile());
            } catch (IOException e) {
                BaseExcelService.log.error(e.getMessage(), e);
                throw new BusinessException(this.translator.toLocale("folder.can.not.create"));
            }
        }
        while (filePath.toFile().exists()) {
            String prefix = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
            filePath = folderPath.resolve(prefix + "_" + fileName);
        }
        try {
            Workbook workbook = WorkbookFactory.create(inputStream);
            XLSTransformer transformer = new XLSTransformer();
            transformer.transformWorkbook(workbook, beans);
            workbook.setActiveSheet(0);
            workbook.write(out);
            result = new ByteArrayResource(out.toByteArray());
        } catch (Exception e) {
            BaseExcelService.log.error(e.getMessage(), e);
        }
        return result;
    }

    public void setDataRow(int dataRow) {
        this.dataRow = dataRow;
    }
}
