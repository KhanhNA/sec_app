package com.tsolution._3services.impl;

import com.tsolution._1entities.BillCycle;
import com.tsolution._2repositories.BillCycleRepository;
import com.tsolution._3services.BillCycleService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BillCycleServiceImpl extends BaseServiceImpl implements BillCycleService {


    @Autowired
    private BillCycleRepository billCycleRepository;
    private static final String BILL_CYCLE_ID_NOT_EXISITS = "bill.lading.cycle.is.not.exist";

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.billCycleRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<BillCycle> optionalBillLadingDetail = this.billCycleRepository.findById(id);
        if (!optionalBillLadingDetail.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString(BillCycleServiceImpl.BILL_CYCLE_ID_NOT_EXISITS, id));
        }
        return new ResponseEntity<>(optionalBillLadingDetail.get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> create(List<BillCycle> entities) throws BusinessException {
        List<BillCycle> billCycles = new ArrayList<>();
        for (BillCycle entity : entities) {
            if (entity.getId() != null) {
                throw new BusinessException(this.translator.toLocale("bill.cycle.input.can.not.exists.id"));
            }
            this.validateBillCycle(entity);

            BillCycle billCycle = this.billCycleRepository.save(entity);
            billCycles.add(billCycle);
        }
        return new ResponseEntity<>(billCycles, HttpStatus.OK);
    }

    private void validateBillCycle(BillCycle billCycle) throws BusinessException {
        if (StringUtils.isNullOrEmpty(billCycle.getCycleType().toString())) {
            throw new BusinessException(this.translator.toLocale("bill.cycle.input.missing.cycle.type"));
        }

    }

    @Override
    public ResponseEntity<Object> update(Long id, BillCycle source) throws BusinessException {
        if (id == null) {
            throw new BusinessException(this.translator.toLocale("bill.cycle.input.missing.id"));
        }
        Optional<BillCycle> oBillCycle = billCycleRepository.findById(id);
        if (!oBillCycle.isPresent()) {
            throw new BusinessException(this.translator.toLocale("bill.cycle.id.not.exisits"));
        }
        this.validateBillCycle(source);

        BillCycle billCycle = oBillCycle.get();
        billCycle.setCycleType(source.getCycleType());
        billCycle.setDeliveryDate(source.getDeliveryDate());
        billCycle.setEndDate(source.getEndDate());
        billCycle.setMonday(source.getMonday());
        billCycle.setTuesday(source.getTuesday());
        billCycle.setWednesday(source.getWednesday());
        billCycle.setThursday(source.getThursday());
        billCycle.setFriday(source.getFriday());
        billCycle.setSaturday(source.getSaturday());
        billCycle.setSunday(source.getSunday());
        billCycle.setCycleType(source.getCycleType());
        billCycle.setWeek1(source.getWeek1());
        billCycle.setWeek2(source.getWeek2());
        billCycle.setWeek3(source.getWeek3());
        billCycle.setWeek4(source.getWeek4());
        billCycle.setSequence(source.getSequence());

        this.billCycleRepository.save(billCycle);
        return new ResponseEntity<>(billCycle, HttpStatus.OK);
    }




}
