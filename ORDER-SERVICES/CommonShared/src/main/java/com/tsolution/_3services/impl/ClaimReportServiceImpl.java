package com.tsolution._3services.impl;

import com.tsolution._1entities.*;
import com.tsolution._1entities.enums.*;
import com.tsolution._2repositories.*;
import com.tsolution._3services.ClaimReportService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class ClaimReportServiceImpl extends BaseServiceImpl implements ClaimReportService {

    private static final String CLAIM_REPORT_IMAGE_ID_NOT_EXISITS = "claim.report.image.id.not.exisits";
    private static final Logger log = LogManager.getLogger(ClaimReportServiceImpl.class);
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ClaimReportRepository claimReportRepository;

    @Autowired
    private ObjAttachmentRepository objAttachmentRepository;

    @Autowired
    private RoutingPlanDetailRepository routingPlanDetailRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public ResponseEntity<Object> findAll() {
        List<ClaimReport> lstClaimReport = this.claimReportRepository.findAll();
        return !lstClaimReport.isEmpty() ? new ResponseEntity<>(getObjectAttachmentList(lstClaimReport), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

	private ResponseEntity<Object> getObjectAttachmentList(List<ClaimReport> lstClaimReport) {
        lstClaimReport.forEach(claimReport -> {
            List<ObjAttachment> lstObjAttachments = this.objAttachmentRepository.findListObjAttachmentByObjId(claimReport.getId(),
                    OBJECT_ATTACHMENT.CLAIMREPORT.getValue());
            if (!lstObjAttachments.isEmpty()) {
                claimReport.setObjAttachments(lstObjAttachments);
            }
        });
		return new ResponseEntity<>(lstClaimReport, HttpStatus.OK);
	}

	@Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<ClaimReport> oClaimReport = this.claimReportRepository.findById(id);
        if (!oClaimReport.isPresent()) {
            throw new BusinessException(this.translator.toLocaleByFormatString("claim.report.id.not.existed", id));
        }
        List<ObjAttachment> lstObjAttachments = this.objAttachmentRepository.findListObjAttachmentByObjId(oClaimReport.get().getId(),
                OBJECT_ATTACHMENT.CLAIMREPORT.getValue());
        ClaimReport claimReport = oClaimReport.get();
        if (lstObjAttachments.size() > 0) {
            claimReport.setObjAttachments(lstObjAttachments);
        }
        return new ResponseEntity<>(claimReport, HttpStatus.OK);
    }

    public ResponseEntity<Object> findByRoutingPlanDetail(Long id) throws BusinessException {

        Optional<RoutingPlanDetail> optionalRoutingPlanDetail = this.routingPlanDetailRepository.findById(id);
        if (!optionalRoutingPlanDetail.isPresent()) {
            throw new BusinessException(this.translator.toLocale("claim.report.routing.plan.day.not.existed"));
        }

        List<ClaimReport> lstClaimReport = this.claimReportRepository
                .findAllByRoutingPlanDetail(optionalRoutingPlanDetail.get());
        return !lstClaimReport.isEmpty()
                ? new ResponseEntity<>( getObjectAttachmentList(lstClaimReport), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(String acceptLanguage, ClaimReport entity) throws BusinessException {

        if (entity == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        Optional<ClaimReport> oClaimReport;
        if (entity.getId() != null) {
            Long claimReportId = entity.getId();
            oClaimReport = this.claimReportRepository.findById(claimReportId);
            if (oClaimReport.isPresent()) {
                throw new BusinessException(
                        this.translator.toLocaleByFormatString("claim.report.id.existed", claimReportId));
            }
        }
        this.validateClaimReport(entity);
        entity.setHasImage(0);
        entity.setHasAttachment(0);
        ClaimReport claimReport = this.claimReportRepository.save(entity);
        return new ResponseEntity<>(claimReport, HttpStatus.OK);
    }

    private void validateClaimReport(ClaimReport claimReport) throws BusinessException {

        if (StringUtils.isNullOrEmpty(claimReport.getNote())) {
            throw new BusinessException(this.translator.toLocale("claim.report.note.not.exisits"));
        }

        if (claimReport.getApparamValue() == null) {
            throw new BusinessException(this.translator.toLocale("claim.report.product.type.not.exisits"));
        }
        Optional<ApparamValue> oApparamValue = this.apparamValueRepository
                .findById(claimReport.getApparamValue().getId());
        if (!oApparamValue.isPresent()) {
            throw new BusinessException(this.translator.toLocale("claim.report.product.type.not.exisits"));
        }
        claimReport.setApparamValue(oApparamValue.get());

        if (claimReport.getDriver() == null) {
            throw new BusinessException(this.translator.toLocale("claim.report.driver.id.not.exisits"));
        }
        Optional<Staff> oDriver = this.staffRepository.findById(claimReport.getDriver().getId());
        if (!oDriver.isPresent()) {
            throw new BusinessException(this.translator.toLocale("claim.report.driver.id.not.exisits"));
        }
        claimReport.setDriver(oDriver.get());

        if (claimReport.getWarehouse() == null) {
            throw new BusinessException(this.translator.toLocale("warehouse.id.not.exisits"));
        }
        Optional<Warehouse> oWareHouse = this.warehouseRepository.findById(claimReport.getWarehouse().getId());
        if (!oWareHouse.isPresent()) {
            throw new BusinessException(this.translator.toLocale("warehouse.id.not.exisits"));
        }
        claimReport.setWarehouse(oWareHouse.get());

        if (claimReport.getStockman() == null) {
            throw new BusinessException(this.translator.toLocale("claim.report.stock.man.id.not.exisits"));
        }
        Optional<Employee> oEmployee = this.employeeRepository.findById(claimReport.getStockman().getId());
        if (!oEmployee.isPresent()) {
            throw new BusinessException(this.translator.toLocale("claim.report.driver.id.not.exisits"));
        }
        Optional<RoutingPlanDetail> optionalRoutingPlanDetail = this.routingPlanDetailRepository
                .findById(claimReport.getRoutingPlanDetail().getId());
        if (!optionalRoutingPlanDetail.isPresent()) {
            throw new BusinessException(this.translator.toLocale("claim.report.routing.plan.day.not.existed"));
        }
        claimReport.setStockman(oEmployee.get());
        claimReport.setStatus(StatusType.RUNNING.getValue());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(String acceptLanguage, Long id, ClaimReport claimReport)
            throws BusinessException {
        if (claimReport == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        Optional<ClaimReport> oClaimReport = Optional.empty();
        if (claimReport.getId() != null) {
            Long claimReportId = claimReport.getId();
            oClaimReport = this.claimReportRepository.findById(claimReportId);
            if (!oClaimReport.isPresent()) {
                throw new BusinessException(
                        this.translator.toLocaleByFormatString("claim.report.id.not.existed", claimReportId));
            }
        }
        ClaimReport result;
        result = oClaimReport.get();
        this.validateClaimReport(claimReport);

        result.setId(claimReport.getId());
        result.setApparamValue(claimReport.getApparamValue());
        result.setDriver(claimReport.getDriver());
        result.setWarehouse(claimReport.getWarehouse());
        result.setHasAttachment(claimReport.getHasAttachment());
        result.setHasImage(claimReport.getHasImage());
        result.setStatus(claimReport.getStatus());
        result.setNote(claimReport.getNote());
        result.setRoutingPlanDetail(claimReport.getRoutingPlanDetail());
        result.setQuantity(claimReport.getQuantity());
        result.setStockman(claimReport.getStockman());
        this.claimReportRepository.save(result);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> updateClaimReportImage(String acceptLanguage, Long id, List<ObjAttachment> deletes,
                                                         List<ObjAttachment> addOrEdits, MultipartFile[] multipartFiles) throws BusinessException {
        if (id == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        Optional<ClaimReport> oClaimReport;
        oClaimReport = this.claimReportRepository.findById(id);
        if (!oClaimReport.isPresent()) {
            throw new BusinessException(this.translator.toLocaleByFormatString("claim.report.id.not.existed", id));
        }
        if ((deletes != null) && !deletes.isEmpty()) {
            this.deleteOldClaimReportImages(id,deletes);
        }
        if ((addOrEdits != null) && !addOrEdits.isEmpty()) {
            this.addOrEditObjAttachment(id, addOrEdits, multipartFiles);
            ClaimReport checkClaim = oClaimReport.get();
            checkClaim.setHasImage(CLAIM_HAS_UPLOAD_TYPE.UPLOADED.getValue());
            this.claimReportRepository.save(checkClaim);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public void deleteOldClaimReportImages(Long claimReportId,List<ObjAttachment> deletes) throws BusinessException {
        Optional< ClaimReport> optionalClaimReport = this.claimReportRepository.findById(claimReportId);
        if (!optionalClaimReport.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("claim.report.id.not.existed", claimReportId));
        }
        for (ObjAttachment objAttachment : deletes) {
            if (objAttachment.getId() == null) {
                throw new BusinessException(this.translator.toLocale("warehouse.image.input.invalid.data.struct"));
            }
            Optional<ObjAttachment> oObjAttachment = this.objAttachmentRepository.findById(objAttachment.getId());
            if (!oObjAttachment.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString(
                        ClaimReportServiceImpl.CLAIM_REPORT_IMAGE_ID_NOT_EXISITS, objAttachment.getId()));
            }
            ObjAttachment result = oObjAttachment.get();
            result.setStatus(StatusType.STOPPED.getValue());
            this.objAttachmentRepository.save(result);
        }

        List<ObjAttachment> lstObjAttachment = this.objAttachmentRepository.findListObjAttachmentByObjId(claimReportId,
                OBJECT_ATTACHMENT.CLAIMREPORT.getValue());
        if(lstObjAttachment.isEmpty()){
            ClaimReport result= optionalClaimReport.get();
            result.setHasImage(CLAIM_HAS_UPLOAD_TYPE.DELETE_UPLOADED.getValue());
        }
    }

    public void addOrEditObjAttachment(Long claimReportId, List<ObjAttachment> addOrEdits,
                                       MultipartFile[] multipartFiles) throws BusinessException {

		for (MultipartFile multipartFile : multipartFiles) {
			ObjAttachment objCheck = null;
			for (ObjAttachment obj : addOrEdits) {
				String urlSave = obj.getAttachUrl();
				if (StringUtils.isNullOrEmpty(urlSave)) {
					throw new BusinessException(this.translator.toLocale("claim.report.image.data.struc.not.exists"));
				}
				Pattern regex = Pattern.compile("[$/]");

				if (regex.matcher(urlSave).find()) {
					String[] urlArray = urlSave.split("/");
					urlSave = urlArray[1];
				}
				if (urlSave.equals(multipartFile.getOriginalFilename())) {
					objCheck = obj;
				}
			}
			if (objCheck == null) {
				throw new BusinessException(this.translator.toLocale("claim.report.image.data.struc.not.exists"));
			}
			String fileName = this.fileStorageService.saveFile("sharevan-image", multipartFile);
			ClaimReportServiceImpl.log.info(fileName);
			if (objCheck.getId() == null) {
				objCheck.setObjId(claimReportId);
			} else {
				Optional<ObjAttachment> oObjAttachment = this.objAttachmentRepository.findById(objCheck.getId());
				if (!oObjAttachment.isPresent()) {
					throw new BusinessException(this.translator.toLocaleByFormatString(
							ClaimReportServiceImpl.CLAIM_REPORT_IMAGE_ID_NOT_EXISITS, objCheck.getId()));
				}
				if (!oObjAttachment.get().getObjId().equals(claimReportId)) {
					throw new BusinessException(this.translator.toLocale("claim.report.image.id.not.exists"));
				}
			}
			objCheck.setAttachUrl(fileName);
			objCheck.setAttachType(OBJECT_ATTACHMENT_TYPE.OBJ_ATTACH_TYPE_IMAGE.getValue());
			objCheck.setAttachName(OBJECT_ATTACHMENT.CLAIMREPORT.getValue());
			objCheck.setStatus(StatusType.RUNNING.getValue());
			this.objAttachmentRepository.save(objCheck);
		}
    }
}
