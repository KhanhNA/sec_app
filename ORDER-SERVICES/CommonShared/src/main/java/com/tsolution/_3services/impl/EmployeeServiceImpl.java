package com.tsolution._3services.impl;

import com.tsolution._1entities.Employee;
import com.tsolution._1entities.EmployeeLog;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution._1entities.User;
import com.tsolution._1entities.enums.*;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto;
import com.tsolution._1entities.oauth2.RoleDto;
import com.tsolution._2repositories.EmployeeRepository;
import com.tsolution._2repositories.ObjAttachmentRepository;
import com.tsolution._3services.EmployeeService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.MathUtils;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import scala.Int;

import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EmployeeServiceImpl extends BaseServiceImpl implements EmployeeService {

    private static final Logger log = LogManager.getLogger( EmployeeServiceImpl.class );
    private static final String EMPLOYEE_IMAGE_ID_NOT_EXISITS = "employee.image.id.not.exisits";
    private SingleSignOnUtils singleSignOnUtils;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ObjAttachmentRepository objAttachmentRepository;

    public EmployeeServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(String authorization, String acceptLanguage, Employee entities)
            throws BusinessException {
        if (entities == null) {
            throw new BusinessException( this.translator.toLocale( "common.input.info.invalid" ) );
        }
        if (entities.getObjectType() != null) {
            throw new BusinessException( this.translator.toLocale( "employee.do.not.have.permission.to.edit" ) );
        }
        Optional<Employee> oEmployee;
        if (entities.getCode() != null) {
            String employeeCode = entities.getCode();
            oEmployee = this.employeeRepository.findByCode( employeeCode );
            if (oEmployee.isPresent()) {
                throw new BusinessException( this.translator.toLocaleByFormatString( "employee.code.already.exists" ) );
            }
        }


        Long count = this.employeeRepository.getOneResult( "EMP" );
        String employeeCode;
        if (count != null) {
            employeeCode = "EMP" + MathUtils.fixLengthString( ++count, 4 );
            boolean checkCustomer = false;
            while (!checkCustomer) {
                oEmployee = this.employeeRepository.getEmployeeByCodeByCode( employeeCode );
                if (oEmployee.isPresent()) {
                    employeeCode = "EMP" + MathUtils.fixLengthString( ++count, 4 );
                } else {
                    checkCustomer = true;
                }
            }
        } else employeeCode = "EMP" + MathUtils.fixLengthString( count++, 4 );

        this.validEmployee( entities );

        // tạo employee
        entities.setCode( employeeCode );
        Employee employee = this.employeeRepository.save( entities );

        User user = this.mapEmployeeToUser( employee );

        ResponseEntity<Object> result = this.singleSignOnUtils.post( authorization, acceptLanguage, "/user", user );
        if (!HttpStatus.OK.equals( result.getStatusCode() )) {
            throw new BusinessException( this.translator.toLocale( "employee.add.failed" ) );
        }

        return new ResponseEntity<>( employee, HttpStatus.OK );
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(String authorization, String acceptLanguage,
                                         Employee entities) throws BusinessException {


        Optional<Employee> oEmployee = this.employeeRepository.findById( entities.getId() ); //Thông tin của đối tượng cần sửa

        OAuth2AuthenticationDto oAuth2AuthenticationDto = this.getCurrentOAuth2Details();
        Integer objectType = null;
        if (oAuth2AuthenticationDto == null) {
            throw new BusinessException( this.translator.toLocale( "token.account.id.not.exisits" ) );
        }
        String ROLE = oAuth2AuthenticationDto.getAuthorities().get( 0 ).getAuthority();
        String userName = oAuth2AuthenticationDto.getName();

        if (entities == null) {
            throw new BusinessException( this.translator.toLocale( "common.input.info.invalid" ) );
        }


        if (entities.getAppParam() == null) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.apparam" ) );
        }
        if (entities.getWarehouse() == null) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.warehouse" ) );
        }


        // Valid Các field của Employee
        this.validEmployee( entities );
        Employee employee = oEmployee.get(); //employee lấy từ đối tượng truyền vào
        Employee employeeUpdate = new Employee(); //employee để set giá trị update

        //ROle admin
        if (ROLE.equals( "SHAREVAN" )) {
            //Sua thong tin nơi làm việc của quản lý kho và quản lý của các kho
            if (entities.getObjectType().equals( EmployeeType.GROUP_STOCK_MANAGER.getValue() ) && entities.getObjectType().equals( employee.getObjectType() ) ||
                    entities.getObjectType().equals( EmployeeType.STOCK_KEEPER.getValue() ) && entities.getObjectType().equals( employee.getObjectType() )) {
                //Điều kiện được sửa nếu là GROUP_STOCK_MANAGER và STOCK_KEEPER và đối tượng entityes truyền vào phải có role  == với role của đổi tượng tìm được từ id của entity
                employeeUpdate = this.setUpdateEmployee( employee, entities );
            }


            if (employee.getObjectType().equals( EmployeeType.GROUP_STOCK_MANAGER.getValue() ) && !entities.getObjectType().equals( employee.getObjectType() ) ||
                    employee.getObjectType().equals( EmployeeType.STOCK_KEEPER.getValue() ) && !entities.getObjectType().equals( employee.getObjectType() )) {
                //Điều kiện để sửa role của GROUP_STOCK_MANAGER và STOCK_KEEPER và đối tượng entityes truyền vào phải có role  != với role của đổi tượng tìm được từ id của entity
                employeeUpdate = this.setUpdateEmployee( employee, entities );
                User user = new User();

                Map<String, Long> pathVariables = new HashMap<>();

                if (entities.getObjectType().equals( EmployeeType.ADMIN_GROUP.getValue() ) && employee.getStatus() == 1) {
                    user = this.mapEmployeeToUserUpdate( employee, 92L );

                } else if (entities.getObjectType().equals( EmployeeType.GROUP_STOCK_MANAGER.getValue() )
                        && employee.getStatus() == 1) {
                    user = this.mapEmployeeToUserUpdate( employee, 91L );

                } else if (entities.getObjectType().equals( EmployeeType.STOCK_KEEPER.getValue() ) && employee.getStatus() == 1) {
                    user = this.mapEmployeeToUserUpdate( employee, 89L );

                }

                pathVariables.put( "id", 0L );

                ResponseEntity<Object> result = this.singleSignOnUtils.patch( authorization, acceptLanguage, "/user/{id}",
                        pathVariables, user );

                if (!HttpStatus.OK.equals( result.getStatusCode() )) {
                    throw new BusinessException( this.translator.toLocale( "employee.update.failed" ) );
                }
            }

            //Thay đổi thông tin admin
            if (entities.getObjectType().equals( EmployeeType.ADMIN_GROUP.getValue() ) && entities.getCode().equals( userName )) {
                //Thay đổi thông tin cá nhân của ADMIN_GROUP
                //Admin k dc thay quyen cua mk

                employeeUpdate = this.setUpdateEmployee( employee, entities );
            }


            if (!entities.getObjectType().equals( EmployeeType.ADMIN_GROUP.getValue() ) && ROLE.equals( "SHAREVAN" ) && userName.equals( entities.getCode() )) {
                throw new BusinessException( this.translator.toLocale( "employee.do.not.have.permission.to.edit" ) );
            }

        }

        //ROle là GROUP_STOCK_MANAGER
        if (ROLE.equals( "Group_Stock_Manager" )) {
            if (entities.getObjectType().equals( EmployeeType.ADMIN_GROUP.getValue() ) || !employee.getObjectType().equals( entities.getObjectType() )) {
                throw new BusinessException( this.translator.toLocale( "employee.do.not.have.permission.to.edit" ) );
            }
            if (entities.getObjectType().equals( EmployeeType.STOCK_KEEPER.getValue() )) {//thay đổi nơi làm việc
                employeeUpdate = this.setUpdateEmployee( employee, entities );
            }
//
            if ((entities.getObjectType().equals( EmployeeType.GROUP_STOCK_MANAGER.getValue() )) && (entities.getCode().equals( userName ))) {
                //Thay đổi thông tin cá nhân của GROUP_STOCK_MANAGER
                employeeUpdate = this.setUpdateEmployee( employee, entities );
            }
        }

        //Role STOCK_KEEPER
        if (ROLE.equals( "Stock_Keeper" )) {
            if (!entities.getCode().equals( userName ) || !entities.getObjectType().equals( employee.getObjectType() )) {
                throw new BusinessException( this.translator.toLocale( "employee.do.not.have.permission.to.edit" ) );
            }

            if (entities.getObjectType().equals( EmployeeType.STOCK_KEEPER.getValue() ) && entities.getCode().equals( userName )) {
                //Thay đổi thông tin cá nhân của STOCK_KEEPER
                employeeUpdate = this.setUpdateEmployee( employee, entities );
            }
        }


        Employee resultEmployee = this.employeeRepository.save( employeeUpdate );

        return new ResponseEntity<>( resultEmployee, HttpStatus.OK );
    }

    @Override
    public ResponseEntity<Object> findAll() {
        List<Employee> lstEmployee = this.employeeRepository.findAll();
        for (Employee employee : lstEmployee) {
            List<ObjAttachment> lst = this.objAttachmentRepository.findListObjAttachmentByObjId( employee.getId(),
                    OBJECT_ATTACHMENT.IMAGE.getValue() );
            if (lst.size() > 0) {
                employee.setObjAttachments( lst );
            }
        }
        return new ResponseEntity<>( lstEmployee, HttpStatus.OK );
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<Employee> oEmployee = this.employeeRepository.findById( id );
        if (!oEmployee.isPresent()) {
            throw new BusinessException( this.translator.toLocaleByFormatString( "employee.id.not.exists" ) );
        }

        List<ObjAttachment> lst = this.objAttachmentRepository.findListObjAttachmentByObjId( oEmployee.get().getId(),
                OBJECT_ATTACHMENT.IMAGE.getValue() );
        if (lst.size() > 0) {
            oEmployee.get().setObjAttachments( lst );
        }

        return new ResponseEntity<>( oEmployee, HttpStatus.OK );
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> addEmployeeImage(String acceptLanguage, Long id, MultipartFile[] multipartFiles) throws BusinessException {
        //check chi nhap vao 1 anh
        if (multipartFiles.length > 1) {
            throw new BusinessException( this.translator.toLocale( "employee.do.not.push.onto.multiple.images" ) );
        }

        if (id == null) {
            throw new BusinessException( this.translator.toLocale( "common.input.info.invalid" ) );
        }
        //check anh da ton tai chua

        Optional<ObjAttachment> imagePresent = objAttachmentRepository.findListObjAttachmentByAttachNameImage( id, OBJECT_ATTACHMENT.IMAGE.getValue() );

        String fileName = multipartFiles[0].getOriginalFilename();

        Optional<Employee> oEmployee = Optional.empty();
        oEmployee = this.employeeRepository.findById( id );
        if (!oEmployee.isPresent()) {
            throw new BusinessException( this.translator.toLocaleByFormatString( "employee.not.exists" ) );
        }

        //lay ten anh
        MultipartFile multipartFile = multipartFiles[0];
        String fileNameURL = this.fileStorageService.saveFile( "sharevan-image", multipartFile );
        ObjAttachment objCheck = new ObjAttachment();
        EmployeeServiceImpl.log.info( fileName );

        //Them anh
        objCheck.setObjId( id );
        imagePresent.get().setStatus( StatusType.STOPPED.getValue() );
        objAttachmentRepository.save( imagePresent.get() );

        objCheck.setAttachUrl( fileNameURL );
        objCheck.setAttachType( ExportStatementStatus.IMAGE.getValue() );
        objCheck.setAttachName( OBJECT_ATTACHMENT.IMAGE.getValue() );
        objCheck.setStatus( StatusType.RUNNING.getValue() );
        this.objAttachmentRepository.save( objCheck );

        Employee checkEmployee = oEmployee.get();
        checkEmployee.setHasImage( 1 );
        this.employeeRepository.save( checkEmployee );

        return new ResponseEntity<>( HttpStatus.OK );

    }

    @Override
    public ResponseEntity<Object> deleteEmployeeImage(String acceptLanguage, Long id, ObjAttachment deletes) throws BusinessException {

        if (deletes.getId() == null) {
            throw new BusinessException( this.translator.toLocale( "employee.image.input.invalid.data.struct" ) );
        }
        Optional<ObjAttachment> oObjAttachment = this.objAttachmentRepository.findById( deletes.getId() );
        if (!oObjAttachment.isPresent()) {
            throw new BusinessException( this.translator.toLocaleByFormatString(
                    EmployeeServiceImpl.EMPLOYEE_IMAGE_ID_NOT_EXISITS, deletes.getId() ) );
        }
        ObjAttachment result = oObjAttachment.get();
        result.setStatus( StatusType.STOPPED.getValue() );
        this.objAttachmentRepository.save( result );

        return new ResponseEntity<>( HttpStatus.OK );
    }

    @Override
    public ResponseEntity<Object> activeOrDeactivate(String authorization, String acceptLanguage, Long id,
                                                     boolean isActive) throws BusinessException {
        Optional<Employee> user = this.employeeRepository.findById( id );
        if (user.get() == null) {
            throw new BusinessException( this.translator.toLocale( "common.input.info.invalid" ) );
        }
        if (user.get().getId() == null) {
            throw new BusinessException( this.translator.toLocale( "customer.input.missing.id" ) );
        }

        Optional<Employee> optionalEmployee = this.employeeRepository.findById( user.get().getId() );
        if (!optionalEmployee.isPresent()) {
            throw new BusinessException( this.translator.toLocaleByFormatString( "customer.id.not.exists", user.get().getId() ) );
        }

        Employee employee = optionalEmployee.get();

        this.employeeRepository.save( employee );

        if (isActive) {
            ResponseEntity<Object> result = this.singleSignOnUtils.post( authorization, acceptLanguage, "/user/active",
                    user.get().getCode() );

            if (!HttpStatus.OK.equals( result.getStatusCode() )) {
                throw new BusinessException( this.translator.toLocale( "customer.active.failed" ) );
            }
        } else {
            ResponseEntity<Object> result = this.singleSignOnUtils.post( authorization, acceptLanguage, "/user/deactive",
                    user.get().getCode() );

            if (!HttpStatus.OK.equals( result.getStatusCode() )) {
                throw new BusinessException( this.translator.toLocale( "customer.deactivate.failed" ) );
            }
        }

        return new ResponseEntity<>( HttpStatus.OK );
    }


    private void validEmployee(Employee employee) throws BusinessException {


        if (StringUtils.isNullOrEmpty( employee.getName() )) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.name" ) );
        }
        if (employee.getBirthday() == null) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.birthday" ) );
        }
        if (employee.getPhone() == null) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.phone" ) );
        }
        if (employee.getStatus() == null) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.status" ) );
        }
        if (StringUtils.isNullOrEmpty( employee.getEmail() )) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.email" ) );
        }
        if (StringUtils.isNullOrEmpty( employee.getAddress() )) {
            throw new BusinessException( this.translator.toLocale( "employee.input.missing.adress" ) );
        }

        //check ngay sinh
        if (employee.getBirthday().isAfter( this.getSysdate() )) {
            throw new BusinessException( this.translator.toLocaleByFormatString( "employee.Incorrect.birth.date" ) );
        }

        //check mail
        if (!Pattern.matches( "^\\w+[a-z0-9]*@\\w+mail.com$", employee.getEmail() )) {
            throw new BusinessException( this.translator.toLocaleByFormatString( "employee.incorrect.email.format" ) );

        }

        //check số điện thoại
        if (!Pattern.matches( "[0]{1}\\d{9}+", employee.getPhone() )) {
            throw new BusinessException( this.translator.toLocaleByFormatString( "employee.phone.number.not.correct.format" ) );
        }

    }

    private User mapEmployeeToUser(Employee employee) {
        User result = new User();

        if (!StringUtils.isNullOrEmpty( employee.getCode() )) {
            result.setUsername( employee.getCode() );
        }
        if (!StringUtils.isNullOrEmpty( employee.getName() )) {
            result.setLastName( employee.getName() );
        }
        result.setFirstName( "" );
        result.setRoles( Collections.singletonList( new RoleDto( 1L ) ) );
        return result;
    }

    private User mapEmployeeToUserUpdate(Employee employee, Long role) {
        User result = new User();

        if (!StringUtils.isNullOrEmpty( employee.getCode() )) {
            result.setUsername( employee.getCode() );
        }
        if (!StringUtils.isNullOrEmpty( employee.getName() )) {
            result.setLastName( employee.getName() );
        }
        result.setFirstName( "" );
        result.setRoles( Collections.singletonList( new RoleDto( role ) ) );
        return result;
    }

    public EmployeeLog mappingEmployeeToEmployeeLog(EmployeeLog result, Employee entities) {

        result.setFromTime( this.getSysdate() );
        result.setEmployeeCode( entities.getCode() );
        result.setEmployeeName( entities.getName() );
        result.setWarehouse( entities.getWarehouse() );
        result.setAppParam( entities.getAppParam() );
        result.setEmployee( entities );
        result.setHasImage( entities.getHasImage() );
        result.setHasAttachment( entities.getHasAttachment() );
        result.setBirthday( entities.getBirthday() );
        result.setProvince( entities.getProvince() );
        result.setNation( entities.getNation() );
        result.setCountry( entities.getCountry() );
        result.setAddress( entities.getAddress() );
        result.setPhone( entities.getPhone() );
        result.setEmail( entities.getEmail() );
        result.setDriverLicenceType( entities.getDriverLicenceType() );
        result.setDriverLicenceId( entities.getDriverLicenceId() );
        result.setHireDate( entities.getHireDate() );
        result.setLeaveDate( entities.getLeaveDate() );
        result.setLaborRate( entities.getLaborRate() );
        result.setBillingRate( entities.getBillingRate() );
        result.setSSN( entities.getSSN() );
        result.setLatitude( entities.getLatitude() );
        result.setLongitude( entities.getLongitude() );
        result.setStateProvince( entities.getStateProvince() );
        result.setLicenseNote( entities.getLicenseNote() );
        result.setLatitude( entities.getLatitude() );
        result.setEmail( entities.getEmail() );
        result.setPoint( entities.getPoint() );
        result.setObjectType( entities.getObjectType() );
        result.setWarehouse( entities.getWarehouse() );
        result.setIdNo( entities.getIdNo() );
        result.setStatus( entities.getStatus() );
        result.setIsActive( 1 );

        return result;
    }

    public Employee setUpdateEmployee(Employee employee, Employee inputEntities) {

        employee.setCode( inputEntities.getCode() );
        employee.setName( inputEntities.getName() );
        employee.setWarehouse( inputEntities.getWarehouse() );
        employee.setAppParam( inputEntities.getAppParam() );
        employee.setHasImage( inputEntities.getHasImage() );
        employee.setHasAttachment( inputEntities.getHasAttachment() );
        employee.setBirthday( inputEntities.getBirthday() );
        employee.setProvince( inputEntities.getProvince() );
        employee.setNation( inputEntities.getNation() );
        employee.setCountry( inputEntities.getCountry() );
        employee.setAddress( inputEntities.getAddress() );
        employee.setPhone( inputEntities.getPhone() );
        employee.setEmail( inputEntities.getEmail() );
        employee.setDriverLicenceType( inputEntities.getDriverLicenceType() );
        employee.setDriverLicenceId( inputEntities.getDriverLicenceId() );
        employee.setHireDate( inputEntities.getHireDate() );
        employee.setLeaveDate( inputEntities.getLeaveDate() );
        employee.setLaborRate( inputEntities.getLaborRate() );
        employee.setBillingRate( inputEntities.getBillingRate() );
        employee.setSSN( inputEntities.getSSN() );
        employee.setLatitude( inputEntities.getLatitude() );
        employee.setLongitude( inputEntities.getLongitude() );
        employee.setStateProvince( inputEntities.getStateProvince() );
        employee.setLicenseNote( inputEntities.getLicenseNote() );
        employee.setLatitude( inputEntities.getLatitude() );
        employee.setEmail( inputEntities.getEmail() );
        employee.setPoint( inputEntities.getPoint() );
        employee.setObjectType( inputEntities.getObjectType() );
        employee.setWarehouse( inputEntities.getWarehouse() );
        employee.setIdNo( inputEntities.getIdNo() );
        employee.setStatus( inputEntities.getStatus() );
        return employee;
    }
}
