package com.tsolution._3services.impl;

import com.tsolution._1entities.Customer;
import com.tsolution._1entities.Employee;
import com.tsolution._1entities.Notification;
import com.tsolution._1entities.Staff;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution._1entities.enums.*;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto;
import com.tsolution._2repositories.EmployeeRepository;
import com.tsolution._2repositories.NotificationRepository;
import com.tsolution._3services.NotificationService;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl extends BaseServiceImpl implements NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public ResponseEntity<Object> findAll() {
        List<Notification> lstNotification = this.notificationRepository.findAll();
        if(lstNotification.size()==0){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lstNotification, HttpStatus.OK);
    }


    /**
     * @param authorization
     * @return mail box of user get by token and id in staff,employee,customer
     * @throws BusinessException
     */
    @Override
    public ResponseEntity<Object> findNotificationByToken(String authorization) throws BusinessException {
        OAuth2AuthenticationDto oAuth2AuthenticationDto = this.getCurrentOAuth2Details();
        Integer objectType = null;
        if (oAuth2AuthenticationDto==null){
            throw new BusinessException(this.translator.toLocale("token.account.id.not.exisits"));
        }
        String ROLE=  oAuth2AuthenticationDto.getAuthorities().get(0).getAuthority();
        String userName = oAuth2AuthenticationDto.getName();
        Long id = 0L;
        if(NOTIFICATION_OBJECT_TYPE.DRIVER.getValue().equalsIgnoreCase(ROLE)){
            objectType= NOTIFICATION_OBJECT_TYPE_STATUS.DRIVER.getValue();
            Optional<Staff> optionalStaff = this.staffRepository.findByUserName(userName) ;
            if(!optionalStaff.isPresent()){
                throw new BusinessException(this.translator.toLocale("claim.report.driver.id.not.exisits"));
            }
            id=optionalStaff.get().getId();
        }else if (NOTIFICATION_OBJECT_TYPE.STOCK_KEEPER.getValue().equalsIgnoreCase(ROLE)){
            objectType= NOTIFICATION_OBJECT_TYPE_STATUS.STOCK_KEEPER.getValue();
            Optional<Employee> optionalEmployee = this.employeeRepository.findByCode(userName);
            if(!optionalEmployee.isPresent()){
                throw new BusinessException(this.translator.toLocale("notification.driver.manage.id.not.exisits "));
            }id=optionalEmployee.get().getId();
        }else if (NOTIFICATION_OBJECT_TYPE.DRIVER_MANAGER.getValue().equalsIgnoreCase(ROLE)){
            objectType= NOTIFICATION_OBJECT_TYPE_STATUS.DRIVER_MANAGER.getValue();
            Optional<Staff> optionalStaff = this.staffRepository.findByUserName(userName) ;
            if(!optionalStaff.isPresent()){
                throw new BusinessException(this.translator.toLocale("notification.driver.manage.id.not.exisits "));
            }id=optionalStaff.get().getId();
        }else if (NOTIFICATION_OBJECT_TYPE.GROUP_STOCK_MANAGER.getValue().equalsIgnoreCase(ROLE)){
            objectType= NOTIFICATION_OBJECT_TYPE_STATUS.GROUP_STOCK_MANAGER.getValue();
            Optional<Employee> optionalEmployee = this.employeeRepository.findByCode(userName);
            if(!optionalEmployee.isPresent()){
                throw new BusinessException(this.translator.toLocale("notification.driver.manage.id.not.exisits "));
            }
            id=optionalEmployee.get().getId();
        }else if (ROLE.equalsIgnoreCase(NOTIFICATION_OBJECT_TYPE.ADMIN.getValue())){
            objectType= NOTIFICATION_OBJECT_TYPE_STATUS.SHAREVAN_ADMIN.getValue();
            Optional<Customer> optionalCustomer = this.customerRespository.findByCode(userName);
            if(!optionalCustomer.isPresent()){
                throw new BusinessException(this.translator.toLocale("customer.not.exists"));
            }
            id=optionalCustomer.get().getId();
        }
        if(objectType==null){
            throw new BusinessException(this.translator.toLocale("notification.user.not.exists "));
        }
        List<Notification> oNotification = this.notificationRepository.findByObjectIdAndObjectType(id, objectType);

        if(oNotification.isEmpty()){
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(oNotification, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<Notification> oNotification= this.notificationRepository.findById(id);

        return oNotification.
                <ResponseEntity<Object>>map(notification -> new ResponseEntity<>(notification, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    public ResponseEntity<Object> create(String acceptLanguage, Notification entity) throws BusinessException, ParseException {
        Notification result = this.notificationRepository.save(entity);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<Object> update(String acceptLanguage, Long id, Notification source) throws BusinessException, ParseException {

        Optional<Notification> oNotification = this.notificationRepository.findById(id);
        if (!oNotification.isPresent()) {
            throw new BusinessException(this.translator.toLocale("notifacation.id.not.exists"));
        }
        Notification result = oNotification.get();
        this.notificationRepository.save(result);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    public void saveMessage(PushNotificationRequest pushNotificationRequest) {
        Notification result = new Notification();
        if (pushNotificationRequest.getNotifycationType() == null){
            result.setNotifycationType(NotificationType.SYSTEM_MESSAGE.getValue());
        }else{
            result.setNotifycationType(pushNotificationRequest.getNotifycationType());
        }
        result.setNotifycationContent(pushNotificationRequest.getMessage());
        result.setTitle(pushNotificationRequest.getTitle());
        result.setTopic(pushNotificationRequest.getTopic());
        if (pushNotificationRequest.getObjectId() == null){
            result.setObjectId(0L);
        }else{
            result.setObjectId(pushNotificationRequest.getObjectId());
        }
        if (pushNotificationRequest.getObjectType() == null){
            result.setObjectType(TokenFbStatus.SHARING_VAN_ADMIN.getValue());
        }else{
            result.setObjectType(pushNotificationRequest.getObjectType());
        }
        if(pushNotificationRequest.getPriority()==null){
            //auto set là tin nhắn hệ thống
            result.setPriority(StatusType.STOPPED.getValue());
        }else{
            result.setPriority(pushNotificationRequest.getPriority());
        }
    }
}
