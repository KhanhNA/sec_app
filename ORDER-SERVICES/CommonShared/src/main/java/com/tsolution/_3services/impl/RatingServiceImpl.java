package com.tsolution._3services.impl;

import com.tsolution._1entities.ObjAttachment;
import com.tsolution._1entities.Rating;
import com.tsolution._1entities.dto.RatingDto;
import com.tsolution._1entities.enums.OBJECT_ATTACHMENT;
import com.tsolution._1entities.enums.RatingType;
import com.tsolution._2repositories.ObjAttachmentRepository;
import com.tsolution._2repositories.RatingRepository;
import com.tsolution._2repositories.RatingRepositoryCustom;
import com.tsolution._3services.RatingService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class RatingServiceImpl extends BaseServiceImpl implements RatingService {

    final int POINT_FROM_NUMBER_RATING5 = 10;
    final int POINT_FROM_NUMBER_RATING4 = 5;
    final int POINT_FROM_NUMBER_RATING3 = 3;
    final int POINT_FROM_NUMBER_RATING2 = 0;
    final int POINT_FROM_NUMBER_RATING1 = -10;
    final private static String RATING_ID_NOT_EXISITS = "rating.id.not.exists";

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    ObjAttachmentRepository objAttachmentRepository;

    @Autowired
    RatingRepositoryCustom ratingRepositoryCustom;

    @Override
    public ResponseEntity<Object> create(String acceptLanguage, Rating rating) throws BusinessException {
        if (rating == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        this.validRating(rating);
        switch (rating.getNumRating()) {
            case POINT_FROM_NUMBER_RATING5:
                rating.setPoint(POINT_FROM_NUMBER_RATING5);
                break;
            case POINT_FROM_NUMBER_RATING4:
                rating.setPoint(POINT_FROM_NUMBER_RATING4);
                break;
            case POINT_FROM_NUMBER_RATING3:
                rating.setPoint(POINT_FROM_NUMBER_RATING3);
                break;
            case POINT_FROM_NUMBER_RATING2:
                rating.setPoint(POINT_FROM_NUMBER_RATING2);
                break;
            case POINT_FROM_NUMBER_RATING1:
                rating.setPoint(POINT_FROM_NUMBER_RATING1);
                break;
            default:
                rating.setPoint(POINT_FROM_NUMBER_RATING3);

        }
        Rating result = this.ratingRepository.save(rating);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> update(String acceptLanguage, Long notificationId, Rating entities) throws BusinessException {
        return new ResponseEntity<>(this.translator.toLocale("common.not.support"), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.ratingRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<Rating> oRating = this.ratingRepository.findById(id);
        if (!oRating.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString(RatingServiceImpl.RATING_ID_NOT_EXISITS, id));
        }
        return new ResponseEntity<>(oRating.get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> find(Rating entities) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> findByStaffId(Long staffId) throws BusinessException {
        if (staffId == null) {
            throw new BusinessException(this.translator.toLocale("common.missing.input.staff.id"));
        }
        List<Rating> ratingList = this.ratingRepositoryCustom.findByStaffId(staffId);
        List<Integer> integers = new ArrayList<>();
        integers.add(RatingType.Friendly.getValue());
        integers.add(RatingType.PoorAttitude.getValue());
        integers.add(RatingType.EnthusiasticDriver.getValue());

        Rating r = new Rating();
        if (ratingList.size() > 0) {
            r = ratingList.get(0);
        }

        RatingDto ratingDto = new RatingDto();
        int size = ratingList.size();
        int totalNumRating = 0;
        for (Rating rating : ratingList) {
            totalNumRating += rating.getNumRating();
        }
        float ratingAverage = (float) totalNumRating / size;
        DecimalFormat df = new DecimalFormat("#.#");
        String ratingAverageFormatted = df.format(ratingAverage);
        ratingDto.setRatingAverage(ratingAverageFormatted);
        ratingDto.setDriverId(r.getStaff().getId());
        ratingDto.setDriverName(r.getStaff().getFullName());
        ratingDto.setDriverAddress(r.getStaff().getAddress());
        ratingDto.setDriverPhone(r.getStaff().getPhone());
        List<ObjAttachment> objAttachment = this.objAttachmentRepository.findListObjAttachmentByObjId(r.getStaff().getId(), OBJECT_ATTACHMENT.STAFF.getValue());
        ratingDto.setImageURL(objAttachment.get(0).getAttachUrl());
        ratingDto.setEvaluationCriteria(integers);
        return new ResponseEntity<>(ratingDto, HttpStatus.OK);

    }


    private void validRating(Rating rating) throws BusinessException {
        if (rating.getStaff().getId() == null) {
            throw new BusinessException(this.translator.toLocale("driver.id.can.not.be.null"));
        }

        if (rating.getRoutingPlanDayId() == null) {
            throw new BusinessException(this.translator.toLocale("routing.plan.day.id.can.not.be.null"));
        }
        if (rating.getNumRating() < 1 || rating.getNumRating() > 5) {
            throw new BusinessException(this.translator.toLocale("number.rating.can.must.to.1.from.5"));
        }

        if (rating.getRatingType() != null) {
            if (rating.getRatingType() != RatingType.Friendly.getValue() || rating.getRatingType() != RatingType.EnthusiasticDriver.getValue() || rating.getRatingType() != RatingType.PoorAttitude.getValue()) {
                throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
            }
        }

        if (!StringUtils.isNullOrEmpty(rating.getNote()) && rating.getNote().length() > 200) {
            throw new BusinessException(this.translator.toLocale("rating.note.length.must.to.less.than.200.characters"));
        }

    }

    @Override
    public ResponseEntity<Object> findByDriverId(Long id) {
        try{
            Map<Integer, Long> resultMap = new HashMap<>();
            List<Object[]> list = this.ratingRepository.getDriverRating(id);
            for(int i=1;i<6;i++) resultMap.put(i,0L);
            for (Object[] ob : list) {
                Integer key = (Integer) ob[0];
                Long value = ((BigInteger) ob[1]).longValue();
                resultMap.put(key, value);
            }
            return new ResponseEntity<>(resultMap, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Object> find100LatestOfDriver(Long id) {
        try{
            Map<Integer, Long> resultMap = new HashMap<>();
            List<Object[]> list = this.ratingRepository.get100LatestRating(id);
            for(int i=1;i<6;i++) resultMap.put(i,0L);
            for (Object[] ob : list) {
                Integer key = (Integer) ob[0];
                Long value = ((BigInteger) ob[1]).longValue();
                resultMap.put(key, value);
            }
            return new ResponseEntity<>(resultMap, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
