package com.tsolution._3services.impl;

import com.tsolution._1entities.BillPackageRouting;
import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._1entities.RoutingVan;
import com.tsolution._1entities.Van;
import com.tsolution._2repositories.BillPackageRoutingRepository;
import com.tsolution._2repositories.RoutingPlanDayRepository;
import com.tsolution._2repositories.RoutingVanRepository;
import com.tsolution._2repositories.VanRepository;
import com.tsolution._3services.RoutingPlanDayService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RoutingPlanDayServiceImpl extends BaseServiceImpl implements RoutingPlanDayService {

    @Autowired
    private RoutingVanRepository routingVanRepository;
    @Autowired
    private RoutingPlanDayRepository routingPlanDayRepository;
    @Autowired
    private VanRepository vanRepository;
    @Autowired
    private BillPackageRoutingRepository billPackageRoutingRepository;

    @Override
    public ResponseEntity<Object> findAll() {
        return null;
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> create(List<RoutingPlanDay> entities) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, RoutingPlanDay source) throws BusinessException {
        return null;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public RoutingPlanDay updateStatusRoutingPlanDay(Long routingPlanDayId, Integer status) throws BusinessException {
        if(routingPlanDayId == null){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        RoutingPlanDay routingPlanDay = this.routingPlanDayRepository.findById(routingPlanDayId).get();
        if(routingPlanDay == null){
            throw new BusinessException(this.translator.toLocale("routing.plan.day.id.not.exist"));
        }
       // RoutingVan routingVan = routingPlanDay.getRoutingVan();
        RoutingVan routingVan = null;
        //trang thai xac nhan van don van chuyen
        if(status.equals(Constants.CONFIRMED_BILL)){
            //check trang thai van don truoc do (tu 0 ->1, tu 1-> 2)
            if(!routingPlanDay.getStatus().equals(Constants.NOT_COMPLETE)){
                throw new BusinessException(this.translator.toLocale("invalid.status.change"));
            }
            routingPlanDay.setStatus(Constants.CONFIRMED_BILL);
            routingPlanDay = this.routingPlanDayRepository.save(routingPlanDay);
        }
        //trang thai hoan thanh van don
        else if(status.equals(Constants.COMPLETED_BILL)){
            //check trang thai van don truoc do (tu 0 ->1, tu 1-> 2)
            if(!routingPlanDay.getStatus().equals(Constants.CONFIRMED_BILL)){
                throw new BusinessException(this.translator.toLocale("invalid.status.change"));
            }
            //update trang thai van don
            routingPlanDay.setStatus(Constants.COMPLETED_BILL);
            routingPlanDay = this.routingPlanDayRepository.save(routingPlanDay);


            //update lai capacity cho xe
            List<BillPackageRouting> lstBillPackageRouting = this.billPackageRoutingRepository.getPackageByRoutingPlan(routingPlanDay.getId());
            if(lstBillPackageRouting.isEmpty()){
                throw new BusinessException(this.translator.toLocale("common.error"));
            }
            double capacity = (double) 0.0;
            for(BillPackageRouting billPackageRouting : lstBillPackageRouting){
                capacity += billPackageRouting.getCapacity().doubleValue();
            }
            Van van= routingVan.getVan();
            if(van == null){
                throw new BusinessException(this.translator.toLocale("common.error"));
            }
            double capacityVan = van.getAvailableCapacity().doubleValue();
            //neu la hoa don nhap => them capacity
            if(routingPlanDay.getType().equals(Constants.IMPORT_BILL)){
                capacityVan += capacity;
            }
            //neu la hoa don xuat => giam capacity
            else if(routingPlanDay.getType().equals(Constants.EXPORT_BILL)){
                capacityVan -= capacity;
            }
            van.setAvailableCapacity(Double.valueOf(capacityVan));
            this.vanRepository.save(van);


            //check lai toan bo van don cho tuyen de update lai tuyen
            List<RoutingPlanDay> listRoutingPlanDayNotComplete = this.routingPlanDayRepository.findAllByStatusAndRoutingVan(Constants.NOT_COMPLETE, routingVan);
            //neu k con van don nao chua hoan thanh => update lai trang thai routing_van la hoan thanh(1)
            if(listRoutingPlanDayNotComplete.isEmpty()){
                routingVan.setStatus(Constants.COMPLETE);
                this.routingVanRepository.save(routingVan);
            }
        }
        else{
            throw new BusinessException(this.translator.toLocale("invalid.status.change"));
        }
        return routingPlanDay;
    }
    @Override
    public List<RoutingPlanDay> getRoutPlanForVanByDate(Long vanId, LocalDateTime fDate, LocalDateTime tDate) throws BusinessException {
        if(vanId == null){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if(fDate == null || tDate == null){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        List<RoutingPlanDay>  lstRoutingPlanDay = this.routingPlanDayRepository.getRoutPlanForVanByDate(vanId,fDate, tDate);
        return lstRoutingPlanDay;
    }

}
