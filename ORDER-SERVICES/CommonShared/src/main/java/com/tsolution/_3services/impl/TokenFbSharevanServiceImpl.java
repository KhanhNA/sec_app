package com.tsolution._3services.impl;

import com.tsolution._1entities.Employee;
import com.tsolution._1entities.Staff;
import com.tsolution._1entities.TokenFbSharevan;
import com.tsolution._1entities.enums.OBJECT_ATTACHMENT;
import com.tsolution._2repositories.EmployeeRepository;
import com.tsolution._2repositories.TokenFbSharevanRepository;
import com.tsolution._3services.TokenFbSharevanService;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static com.tsolution._1entities.enums.TokenFbStatus.*;

@Service
public class TokenFbSharevanServiceImpl extends BaseServiceImpl implements TokenFbSharevanService {

@Autowired
private TokenFbSharevanRepository tokenFbSharevanRepository;
@Autowired
private EmployeeRepository employeeRepository;
    @Override
    public ResponseEntity<Object> findAll() {
        List<TokenFbSharevan> lstTokenFbShareVan= this.tokenFbSharevanRepository.findAll();
        if(lstTokenFbShareVan.size()==0){
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(lstTokenFbShareVan , HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<TokenFbSharevan> optionalTokenFbSharevan=this.tokenFbSharevanRepository.findById(id);

        return optionalTokenFbSharevan.
                <ResponseEntity<Object>>map(tokenFbSharevan -> new ResponseEntity<>(tokenFbSharevan, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }
    public ResponseEntity<Object> findByObjectIdAndObjectType(Long objectId, Integer objectType) throws BusinessException {
        Optional<TokenFbSharevan> optionalTokenFbSharevan=this.tokenFbSharevanRepository.findByObjectId(objectId, objectType);

        return optionalTokenFbSharevan.
                <ResponseEntity<Object>>map(tokenFbSharevan -> new ResponseEntity<>(tokenFbSharevan, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));

    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(String acceptLanguage, TokenFbSharevan entity) throws BusinessException, ParseException {
        if (entity == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        this.validateToken(entity);
        TokenFbSharevan result = this.tokenFbSharevanRepository.save(entity);
        //this.addOrUpdateWithObjectType(result);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(String acceptLanguage, Long id, TokenFbSharevan entity) throws BusinessException, ParseException {
        if (entity == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        TokenFbSharevan result = this.tokenFbSharevanRepository.findByImei(entity.getImei());
        if (result == null) {
            this.validateToken(entity);
            result = this.tokenFbSharevanRepository.save(entity);
            this.addOrUpdateWithObjectType(result);
        } else {
            this.validateToken(entity);
            result.setObjectId(entity.getObjectId());
            result.setObjectType(entity.getObjectType());
            result.setToken(entity.getToken());
            result = this.tokenFbSharevanRepository.save(result);
            this.addOrUpdateWithObjectType(result);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public void validateToken(TokenFbSharevan tokenFbSharevan) throws BusinessException {

        if (tokenFbSharevan.getImei() == null) {
            throw new BusinessException(this.translator.toLocale("token.device.imei.not.exists"));
        }
        if (tokenFbSharevan.getToken() == null) {
            throw new BusinessException(this.translator.toLocale("firebase.token.not.exists"));
        }
    }
    public  void addOrUpdateWithObjectType(TokenFbSharevan tokenFbSharevan) throws BusinessException {
        if (tokenFbSharevan.getObjectId() == null || tokenFbSharevan.getObjectType() == null) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("token.account.id.not.exisits", tokenFbSharevan.getObjectId()));
        }
        if(tokenFbSharevan.getObjectType().equals(OBJECT_ATTACHMENT.EMPLOYEE.getValue()) || tokenFbSharevan.getObjectType().equals(STORE_KEEPER.getValue())){
            Optional< Employee> oEmployee= this.employeeRepository.findById(tokenFbSharevan.getObjectId());
            if(!oEmployee.isPresent()){
                throw new BusinessException(
                        this.translator.toLocaleByFormatString("token.account.id.not.exisits", tokenFbSharevan.getObjectId()));
            }
            Employee employee= oEmployee.get();
            employee.setImei(tokenFbSharevan.getImei());
            this.employeeRepository.save(employee);
        }else if(tokenFbSharevan.getObjectType().equals(DRIVER.getValue())||tokenFbSharevan.getObjectType().equals(FLEET_MANAGER.getValue())){
            Optional< Staff> oStaff= this.staffRepository.findById(tokenFbSharevan.getObjectId());
            if(!oStaff.isPresent()){
                throw new BusinessException(
                        this.translator.toLocaleByFormatString("token.account.id.not.exisits", tokenFbSharevan.getObjectId()));
            }
            Staff staff= oStaff.get();
            staff.setImei(tokenFbSharevan.getImei());
            this.staffRepository.save(staff);
        }
    }
}
