package com.tsolution.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SerializeTimeHandler extends StdSerializer<LocalTime> {

	private static final long serialVersionUID = -601499764403006185L;
	private static final Logger logger = LogManager.getLogger(SerializeTimeHandler.class);

	public SerializeTimeHandler() {
		this(null);
	}

	public SerializeTimeHandler(Class<LocalTime> t) {
		super(t);
	}

    @Override
    public void serialize(LocalTime value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        try {
            gen.writeString(value.format(DateTimeFormatter.ofPattern(Constants.TIME_PATTERN)));
        } catch (Exception e) {
            SerializeTimeHandler.logger.error(e.getMessage(), e);
        }
    }
}