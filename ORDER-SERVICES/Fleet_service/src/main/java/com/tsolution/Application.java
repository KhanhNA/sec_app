package com.tsolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

import com.tsolution.config.FileStorageProperties;

@SpringBootApplication
@ComponentScan("com.tsolution.*")
@EnableConfigurationProperties({ FileStorageProperties.class })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

//	private static String multiValueMapToUri()
//		MultiValueMap<String, String> xxx = new LinkedMultiValueMap<>()
//		xxx.add("7", "2019-09-29 00:00:00.000Z")
//		xxx.add("7", "2019-08-30 00:00:00.000Z")
//		UriComponents uriComponents = UriComponentsBuilder.newInstance().scheme("http").host("localhost").port(8888)
//				.queryParams(xxx).build()
//
//		return uriComponents.toUriString()
//
}
