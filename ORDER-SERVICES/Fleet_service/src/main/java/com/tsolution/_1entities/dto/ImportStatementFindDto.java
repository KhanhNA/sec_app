package com.tsolution._1entities.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ImportStatementFindDto implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -6625972325007290087L;

	@Id
	private Long id;
	private String code;
	private Long organizationId;
	private Long customerId;
	private String customer;
	private Long warehouseId;
	private String warehouse;
	private Long parcelId;
	private String parcel;
	private String description;
	private Integer status;
	private Integer statusDetail;
	private Integer dateType;
	private LocalDateTime fromDate;
	private LocalDateTime toDate;
	private String product;
	private LocalDateTime expireFromDate;
	private LocalDateTime expireToDate;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getWarehouseId() {
		return this.warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouse() {
		return this.warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public Long getParcelId() {
		return this.parcelId;
	}

	public void setParcelId(Long parcelId) {
		this.parcelId = parcelId;
	}

	public String getParcel() {
		return this.parcel;
	}

	public void setParcel(String parcel) {
		this.parcel = parcel;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatusDetail() {
		return this.statusDetail;
	}

	public void setStatusDetail(Integer statusDetail) {
		this.statusDetail = statusDetail;
	}

	public Integer getDateType() {
		return this.dateType;
	}

	public void setDateType(Integer dateType) {
		this.dateType = dateType;
	}

	public LocalDateTime getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getToDate() {
		return this.toDate;
	}

	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public LocalDateTime getExpireFromDate() {
		return this.expireFromDate;
	}

	public void setExpireFromDate(LocalDateTime expireFromDate) {
		this.expireFromDate = expireFromDate;
	}

	public LocalDateTime getExpireToDate() {
		return this.expireToDate;
	}

	public void setExpireToDate(LocalDateTime expireToDate) {
		this.expireToDate = expireToDate;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

}
