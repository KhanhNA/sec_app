package com.tsolution._2repositories;

import com.tsolution._1entities.ClaimReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClaimReportRepository extends JpaRepository<ClaimReport, Long>, JpaSpecificationExecutor<ClaimReport> {

}