package com.tsolution._4controllers.impl;


import com.tsolution._1entities.ApparamGroup;
import com.tsolution._3services.ApparamGroupService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/grpapparams")
public class ApparamGroupController extends BaseController {

    @Autowired
    private ApparamGroupService apparamGroupService;

    @PostMapping
    //@PreAuthorize("hasAuthority('post/grpapparams')")
    public ResponseEntity<Object> create(
            @RequestBody List<ApparamGroup> entity)
            throws BusinessException {
        return this.apparamGroupService.create(entity) ;
    }

    @PatchMapping("/{id}")
    //@PreAuthorize("hasAuthority('patch/{id}')")
    public ResponseEntity<Object> update(
            @RequestBody ApparamGroup entity,
            @PathVariable("id") Long id)
            throws BusinessException {
        return this.apparamGroupService.update(id, entity);
    }
    @GetMapping("/all")
    //@PreAuthorize("hasAuthority('get/all')")
    public ResponseEntity<Object> findAll() {
        return this.apparamGroupService.findAll();
    }

    @GetMapping("/{id}")
    //@PreAuthorize("hasAuthority('get/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        System.out.println(this.apparamGroupService.findById(id));
        return this.apparamGroupService.findById(id);
    }

    @GetMapping
    //@PreAuthorize("hasAuthority('get/grpapparams')")
    public ResponseEntity<Object> find(@RequestParam(required = false) String code,
                                       @RequestParam(required = false) String name,
                                       @RequestParam(required = false) Integer status,
                                       @RequestParam(required = true) Integer pageNumber,
                                       @RequestParam(required = true) Integer pageSize
    ) throws BusinessException {
        ApparamGroup apparamGroup = new ApparamGroup();
        apparamGroup.setGroupName(name);
        apparamGroup.setGroupCode(code);
        apparamGroup.setStatus(status);
        return this.apparamGroupService.find(apparamGroup,pageNumber,pageSize);
    }

}
