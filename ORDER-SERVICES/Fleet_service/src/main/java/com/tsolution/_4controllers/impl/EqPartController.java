package com.tsolution._4controllers.impl;

import com.tsolution._1entities.EqPart;
import com.tsolution._3services.EqPartService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/eq-part")
public class EqPartController extends BaseController {
    @Autowired
    private EqPartService eqPartService;

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/eq-part/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Find part with provided id",
            notes = "Required 1 Long parameter",
            response = EqPart.class, responseContainer = "EqPart")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.eqPartService.findById(id);
    }

    @GetMapping("/van/{id}")
    @PreAuthorize("hasAuthority('get/eq-part/van/{id}')")
    @ApiOperation(httpMethod = "GET",
            value = "Find part with of Van with van id",
            notes = "Required 1 Long parameter",
            response = EqPart.class, responseContainer = "EqPart")
    public ResponseEntity<Object> findByVanId(@PathVariable("id") Long id) throws BusinessException {
        return this.eqPartService.findByVanId(id);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('get/eq-part')")
    @ApiOperation(httpMethod = "GET",
            value = "Find parts with condition",
            notes = "Required 1 Long parameter",
            response = EqPart.class, responseContainer = "EqPart")
    public ResponseEntity<Object> findByCondition(EqPart searchData, Pageable pageable) throws BusinessException {
        return this.eqPartService.findByCondition(searchData, pageable);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/eq-part')")
    @ApiOperation(httpMethod = "POST",
            value = "Create new part",
            response = EqPart.class, responseContainer = "EqPart")
    public ResponseEntity<Object> create(@RequestBody EqPart entity) throws BusinessException {
        return this.eqPartService.create(entity);
    }
    @PatchMapping
    @PreAuthorize("hasAuthority('patch/eq-part')")
    @ApiOperation(httpMethod = "PATCH",
            value = "Update existing part",
            response = EqPart.class, responseContainer = "EqPart")
    public ResponseEntity<Object> update(@RequestBody EqPart entity) throws BusinessException {
        return this.eqPartService.create(entity);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('delete/eq-part/{id}')")
    @ApiOperation(httpMethod = "DELETE",
            value = "Delete part",
            response = HttpStatus.class, responseContainer = "HttpStatus")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) throws BusinessException {
        return this.eqPartService.delete(id);
    }



}
