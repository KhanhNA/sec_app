package com.tsolution._1entities.enums;

public enum BaseStatus {
    ACTIVE(true),
    DEACTIVE(false);

    private boolean value;

    BaseStatus(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return this.value;
    }
}
