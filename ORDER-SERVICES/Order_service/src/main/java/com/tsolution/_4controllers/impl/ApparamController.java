package com.tsolution._4controllers.impl;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamValue;
import com.tsolution._3services.AppParamService;
import com.tsolution._3services.AppParamValueService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apparam")
public class ApparamController extends BaseController {

    @Autowired
    private AppParamService appParamService;

    @Autowired
    private AppParamValueService appParamValueService;

    @PostMapping
    //@PreAuthorize("hasAuthority('post/apparam')")
    public ResponseEntity<Object> create(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody AppParam entity)
            throws BusinessException {
        return this.appParamService.create(entity);
    }

    @GetMapping("/all")
   // @PreAuthorize("hasAuthority('get/apparam/all')")
    public ResponseEntity<Object> findAll() throws BusinessException {
        return this.appParamService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/apparam/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.appParamService.findById(id);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/apparam/{id}')")
    public ResponseEntity<Object> update(
            @RequestBody AppParam entity,
            @PathVariable("id") Long id)
            throws BusinessException {
        return this.appParamService.update(id, entity);
    }

    //apparamvalue
   @PostMapping("/value")
  // @PreAuthorize("hasAuthority('post/apparam/value')")
    public ResponseEntity<Object> create(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody ApparamValue entity)
            throws BusinessException {
        return this.appParamValueService.create(entity);
    }

    @GetMapping("/value/all")
    @PreAuthorize("hasAuthority('get/apparam/value/all')")
    public ResponseEntity<Object> findValueAll() throws BusinessException {
        return this.appParamValueService.findAll();
    }

    @GetMapping("/value/{id}")
    @PreAuthorize("hasAuthority('get/apparam/value/{id}')")
    public ResponseEntity<Object> findValueId(@PathVariable("id") Long id) throws BusinessException {
        return this.appParamValueService.findById(id);
    }

    @PatchMapping("value/{id}")
    @PreAuthorize("hasAuthority('patch/apparam/value/{id}')")
    public ResponseEntity<Object> updateValue(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody ApparamValue entity,
            @PathVariable("id") Long id)
            throws BusinessException {
        return this.appParamValueService.update(id, entity);
    }




}
