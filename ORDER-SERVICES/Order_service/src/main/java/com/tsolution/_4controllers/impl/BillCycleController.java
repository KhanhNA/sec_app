package com.tsolution._4controllers.impl;

import com.tsolution._1entities.BillCycle;
import com.tsolution._3services.BillCycleService;
import com.tsolution._4controllers.BaseController;
import com.tsolution._4controllers.IBaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/bill-cycles")
public class BillCycleController  extends BaseController  implements IBaseController<BillCycle> {

    @Autowired
    private BillCycleService billCycleService;
    @Override
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/bill-cycles/all')")
    public ResponseEntity<Object> findAll() {
        return this.billCycleService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/bill-cycles/{id}')")
    @Override
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.billCycleService.findById(id);
    }

    @Override
    @PostMapping
    @PreAuthorize("hasAuthority('post/bill-cycles')")
    public ResponseEntity<Object> create(@RequestBody List<BillCycle> entity) throws BusinessException {
        return this.billCycleService.create(entity);
    }

    @Override
    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/bill-cycles/{id}')")
    public ResponseEntity<Object> update(@PathVariable("id")  Long id, @RequestBody Optional<BillCycle> source) throws BusinessException {
        if (source.isPresent()) {
            return this.billCycleService.update(id, source.get());
        } else {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
    }
}