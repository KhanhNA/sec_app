package com.tsolution._4controllers.impl;


import com.tsolution._1entities.ClaimReport;
import com.tsolution._1entities.ObjAttachment;
import com.tsolution._3services.ClaimReportService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/claim_reports")
public class ClaimReportController extends BaseController {

    @Autowired
    private ClaimReportService claimReportService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/claim_reports/all')")
    public ResponseEntity<Object> findAll() {
        return this.claimReportService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/claim_reports/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.claimReportService.findById(id);
    }
    @GetMapping("/findByRoutingPlanDetail/{id}")
    @PreAuthorize("hasAuthority('get/claim_reports/findByRoutingPlanDetail/{id}')")
    public ResponseEntity<Object> findByRoutingPlanDetail(@PathVariable("id") Long id) throws BusinessException {
        return this.claimReportService.findByRoutingPlanDetail(id);
    }
    @PostMapping
    @PreAuthorize("hasAuthority('post/claim_reports')")
    public ResponseEntity<Object> create(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody ClaimReport claimReport)
            throws BusinessException {
        return this.claimReportService.create(acceptLanguage, claimReport);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/claim_reports/{id}')")
    public ResponseEntity<Object> update(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestBody ClaimReport claimReport
    ) throws BusinessException {
        return this.claimReportService.update(acceptLanguage, id, claimReport);
    }

    @PatchMapping(path = "/{id}/update-image", consumes = { "multipart/form-data" })
    @PreAuthorize("hasAuthority('patch/claim_reports/{id}/update-image')")
    public ResponseEntity<Object> updateClaimReportImage(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestPart(name = "deletes", required = false) List<ObjAttachment> deletes,
            @RequestPart(name = "addOrEdits", required = false) List<ObjAttachment> addOrEdits,
            @RequestParam(name = "files", required = false) MultipartFile[] files) throws BusinessException {
        return this.claimReportService.updateClaimReportImage(acceptLanguage,id, deletes, addOrEdits, files);
    }
}
