package com.tsolution._1entities.enums;

public enum CustomerRentStatus {
	NEW(0), APPROVED(1), REJECTED(2);

	private Integer value;

	CustomerRentStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
