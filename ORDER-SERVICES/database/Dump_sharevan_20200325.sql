-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: traccar
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alert`
--

Use sharevan;

--
-- Table structure for table `app_param`
--

DROP TABLE IF EXISTS `app_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_param` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_param_code` varchar(50) ,
  `app_param_name` varchar(50) ,
  `status` int ,
  `ord` int ,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  `app_param_group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group` (`app_param_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_param`
--

LOCK TABLES `app_param` WRITE;
/*!40000 ALTER TABLE `app_param` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apparam_group`
--

DROP TABLE IF EXISTS `apparam_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `apparam_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_code` varchar(20) ,
  `status` int ,
  `ord` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apparam_group`
--

LOCK TABLES `apparam_group` WRITE;
/*!40000 ALTER TABLE `apparam_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `apparam_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apparam_value`
--

DROP TABLE IF EXISTS `apparam_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `apparam_value` (
  `id` int NOT NULL AUTO_INCREMENT,
  `param_id` int ,
  `param_code` varchar(20) ,
  `param_name` varchar(20) ,
  `ord` int ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`),
  KEY `fk_apparam_value` (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apparam_value`
--

LOCK TABLES `apparam_value` WRITE;
/*!40000 ALTER TABLE `apparam_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `apparam_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_cycle`
--

DROP TABLE IF EXISTS `bill_cycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill_cycle` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cycle_type` int ,
  `start_date` timestamp ,
  `end_date` timestamp ,
  `monday` int ,
  `tuesday` int ,
  `wednesday` int ,
  `thursday` int ,
  `friday` int ,
  `saturday` int ,
  `sunday` int ,
  `week1` int ,
  `week2` int ,
  `week3` int ,
  `week4` int ,
  `sequence` int ,
  `delivery_date` timestamp ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_cycle`
--

LOCK TABLES `bill_cycle` WRITE;
/*!40000 ALTER TABLE `bill_cycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_cycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_lading_detail`
--

DROP TABLE IF EXISTS `bill_lading_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill_lading_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bill_lading_detail_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `bill_lading_id` int ,
  `warehouse_id` int ,
  `warehouse_type` int ,
  `total_weight` double ,
  `status` int ,
  `status_order` int ,
  `approved_type` int ,
  `from_bill_lading_detail_id` int ,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  `service_id` int ,
  `expected_from_time` timestamp ,
  `expected_to_time` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_lading_detail`
--

LOCK TABLES `bill_lading_detail` WRITE;
/*!40000 ALTER TABLE `bill_lading_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_lading_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_lading`
--

DROP TABLE IF EXISTS `bill_lading`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill_lading` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bill_lading_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `customer_id` int ,
  `insurance_id` int ,
  `total_weight` double ,
  `amount` double ,
  `bill_cycle_id` int ,
  `release_type` int ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_lading`
--

LOCK TABLES `bill_lading` WRITE;
/*!40000 ALTER TABLE `bill_lading` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_lading` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claim_report`
--

DROP TABLE IF EXISTS `claim_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `claim_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) ,
  `has_image` int(11) ,
  `has_attachment` int(11) ,
  `note` varchar(100) CHARACTER SET utf8 ,
  `warehouse_id` int(11) ,
  `driver_id` int(11) ,
  `stock_man_id` int(11) ,
  `app_param_value_id` int(11) ,
  `parcel_routing_plan_id` int(11) ,
  `create_date` timestamp NOT NULL ,
  `create_user` varchar(100) ,
  `update_date` timestamp NOT NULL ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claim_report`
--

LOCK TABLES `claim_report` WRITE;
/*!40000 ALTER TABLE `claim_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contract` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contract_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `phone1` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `phone2` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `fax` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `website` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `contract_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `saleman_id` int ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8 ,
  `name` varchar(100) CHARACTER SET utf8 ,
  `address` varchar(100) CHARACTER SET utf8 ,
  `address1` varchar(100) CHARACTER SET utf8 ,
  `address2` varchar(100) CHARACTER SET utf8 ,
  `tax_code` varchar(50) CHARACTER SET utf8 ,
  `contact_name` varchar(50) CHARACTER SET utf8 ,
  `city` varchar(50) CHARACTER SET utf8 ,
  `state_province` varchar(100) CHARACTER SET utf8 ,
  `postal_code` varchar(20) ,
  `phone1` varchar(15) ,
  `phone2` varchar(15) ,
  `fax` varchar(100) CHARACTER SET utf8 ,
  `website` varchar(50) ,
  `has_image` int(11) ,
  `has_attachment` int(11) ,
  `email` varchar(50) ,
  `payment_type` varchar(50) ,
  `pay_by_day` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `discount_type` varchar(50) ,
  `discount` double ,
  `status` int(11) ,
  `app_param_value_id` int(11) ,
  `create_date` timestamp NOT NULL ,
  `create_user` varchar(100) ,
  `update_date` timestamp NOT NULL ,
  `update_user` varchar(100) ,
  `mobile_phone` varchar(100) ,
  `id_no` varchar(100) ,
  `phone` varchar(45) ,
  `is_vip` varchar(45) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `daily_inspection`
--

DROP TABLE IF EXISTS `daily_inspection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `daily_inspection` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maintenance_template_id` int ,
  `name` varchar(50) ,
  `type` varchar(50) ,
  `priority` varchar(50) ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `daily_inspection`
--

LOCK TABLES `daily_inspection` WRITE;
/*!40000 ALTER TABLE `daily_inspection` DISABLE KEYS */;
/*!40000 ALTER TABLE `daily_inspection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `day_off`
--

DROP TABLE IF EXISTS `day_off`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `day_off` (
  `id` int NOT NULL AUTO_INCREMENT,
  `update_date` timestamp ,
  `create_date` timestamp ,
  `from_date` timestamp ,
  `to_date` timestamp ,
  `reason` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `status` int ,
  `spec` int ,
  `create_user` varchar(100) ,
  `staff_id` int not null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `day_off`
--

LOCK TABLES `day_off` WRITE;
/*!40000 ALTER TABLE `day_off` DISABLE KEYS */;
/*!40000 ALTER TABLE `day_off` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driver_van`
--

DROP TABLE IF EXISTS `driver_van`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `driver_van` (
	`id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int ,
  `van_id` int ,
  `from_date` timestamp ,
  `to_date` timestamp ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver_van`
--

LOCK TABLES `driver_van` WRITE;
/*!40000 ALTER TABLE `driver_van` DISABLE KEYS */;
/*!40000 ALTER TABLE `driver_van` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_renewal`
--

DROP TABLE IF EXISTS `emp_renewal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_renewal` (
  `id` int NOT NULL AUTO_INCREMENT,
  `staff_id` int ,
  `renewal_type` int ,
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `ord` int ,
  `status` int ,
  `issued_date` timestamp ,
  `expire_date` timestamp ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_renewal`
--

LOCK TABLES `emp_renewal` WRITE;
/*!40000 ALTER TABLE `emp_renewal` DISABLE KEYS */;
/*!40000 ALTER TABLE `emp_renewal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_depreciation`
--

DROP TABLE IF EXISTS `eq_depreciation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eq_depreciation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `van_id` int ,
  `org_value` int ,
  `available_value` double ,
  `depreciation_time` timestamp ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_depreciation`
--

LOCK TABLES `eq_depreciation` WRITE;
/*!40000 ALTER TABLE `eq_depreciation` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_depreciation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_expense`
--

DROP TABLE IF EXISTS `eq_expense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eq_expense` (
  `id` int NOT NULL AUTO_INCREMENT,
  `expense_type` int ,
  `interval_type` varchar(20) ,
  `interval_value` varchar(20) ,
  `quantity` int ,
  `cost` double ,
  `last_date` timestamp ,
  `next_date` timestamp ,
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_expense`
--

LOCK TABLES `eq_expense` WRITE;
/*!40000 ALTER TABLE `eq_expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_expense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_insurance`
--

DROP TABLE IF EXISTS `eq_insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eq_insurance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `van_id` int ,
  `vendor_id` int ,
  `policy_no` varchar(100) ,
  `start_date` timestamp ,
  `end_date` timestamp ,
  `payment` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `deductible` double ,
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `log_payment_interval_type` varchar(20) ,
  `log_payment_interval_value` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_insurance`
--

LOCK TABLES `eq_insurance` WRITE;
/*!40000 ALTER TABLE `eq_insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_lease`
--

DROP TABLE IF EXISTS `eq_lease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eq_lease` (
  `id` int NOT NULL AUTO_INCREMENT,
  `van_id` int ,
  `vendor_id` int ,
  `account_no` varchar(100) ,
  `start_date` timestamp ,
  `end_date` timestamp ,
  `payment` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `residual` double ,
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `log_payment_interval_type` varchar(20) ,
  `log_payment_interval_value` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_lease`
--

LOCK TABLES `eq_lease` WRITE;
/*!40000 ALTER TABLE `eq_lease` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_lease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_part`
--

DROP TABLE IF EXISTS `eq_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eq_part` (
  `id` int NOT NULL AUTO_INCREMENT,
  `part_no` int ,
  `name` varchar(20) ,
  `van_id` int ,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `manufacturer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `category_type` varchar(20) ,
  `unit_cost` varchar(50) ,
  `unit_measure` varchar(50) ,
  `univeral_product_code` varchar(50) ,
  `has_image` int ,
  `has_attachment` int ,
  `parent_part_id` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_part`
--

LOCK TABLES `eq_part` WRITE;
/*!40000 ALTER TABLE `eq_part` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fleet`
--

DROP TABLE IF EXISTS `fleet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fleet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fleet_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `fleet_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `business_registration` varchar(100) ,
  `address_registration` varchar(100) ,
  `address1` varchar(100) ,
  `address2` varchar(100) ,
  `contact_name` varchar(100) ,
  `city` varchar(100) ,
  `postal_code` varchar(100) ,
  `tax_code` varchar(50) ,
  `create_user` varchar(100) ,
  `create_date` timestamp ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fleet`
--

LOCK TABLES `fleet` WRITE;
/*!40000 ALTER TABLE `fleet` DISABLE KEYS */;
/*!40000 ALTER TABLE `fleet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fuel_consume`
--

DROP TABLE IF EXISTS `fuel_consume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fuel_consume` (
  `id` int NOT NULL AUTO_INCREMENT,
  `routing_van_id` int ,
  `fuel_first` double ,
  `fuel_live` double ,
  `kilometer` double ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fuel_consume`
--

LOCK TABLES `fuel_consume` WRITE;
/*!40000 ALTER TABLE `fuel_consume` DISABLE KEYS */;
/*!40000 ALTER TABLE `fuel_consume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance`
--

DROP TABLE IF EXISTS `insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `insurance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `insurance_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `insurance_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `vender_id` int ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  `from_date` timestamp ,
  `to_date` timestamp ,
  `deductible` double ,
  `note` varchar(1000) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance`
--

LOCK TABLES `insurance` WRITE;
/*!40000 ALTER TABLE `insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list_package`
--

DROP TABLE IF EXISTS `list_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `list_package` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bill_lading_detail_id` int ,
  `insurance_id` int ,
  `length` float ,
  `width` float ,
  `height` float ,
  `service_id` int ,
  `app_param_value_id` int ,
  `package_category_id` int ,
  `quantity_parcel` int ,
  `net_weight` double ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_package`
--

LOCK TABLES `list_package` WRITE;
/*!40000 ALTER TABLE `list_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maintenance_template`
--

DROP TABLE IF EXISTS `maintenance_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maintenance_template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `schedule_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `track_type` varchar(20) ,
  `track_type_detail` varchar(100) ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maintenance_template`
--

LOCK TABLES `maintenance_template` WRITE;
/*!40000 ALTER TABLE `maintenance_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `maintenance_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `notifycation_type` int ,
  `notifycation_content` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `object_id` int ,
  `object_type` int ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obj_attachment`
--

DROP TABLE IF EXISTS `obj_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obj_attachment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `obj_id` int ,
  `attach_type` int ,
  `attach_url` varchar(200) ,
  `ord` int ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obj_attachment`
--

LOCK TABLES `obj_attachment` WRITE;
/*!40000 ALTER TABLE `obj_attachment` DISABLE KEYS */;
INSERT INTO `obj_attachment` VALUES (1,1,0,'sdgrhthyyjhj',1,1,'2020-03-23 17:15:25',NULL,NULL,'2020-03-23 17:15:25'),(2,1,1,'sfrtyty',2,1,'2020-03-23 17:15:25',NULL,NULL,'2020-03-23 17:15:25'),(3,2,0,'sdgrhthyyjhj',1,1,'2020-03-23 17:33:33',NULL,NULL,'2020-03-23 17:33:33'),(4,3,0,'sdgrhthyyjhj',1,1,'2020-03-23 18:00:40',NULL,NULL,'2020-03-23 18:00:40'),(5,4,0,'sdgrhthyyjhj',1,1,'2020-03-23 18:02:09',NULL,NULL,'2020-03-23 18:02:09');
/*!40000 ALTER TABLE `obj_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcel_routing_plan`
--

DROP TABLE IF EXISTS `parcel_routing_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parcel_routing_plan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` int ,
  `length` float ,
  `width` float ,
  `height` float ,
  `total_weight` float ,
  `bill_lading_detail_id` int ,
  `insurance_id` int ,
  `service_id` int ,
  `quantity` int ,
  `routing_plan_day_id` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcel_routing_plan`
--

LOCK TABLES `parcel_routing_plan` WRITE;
/*!40000 ALTER TABLE `parcel_routing_plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `parcel_routing_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pm_service`
--

DROP TABLE IF EXISTS `pm_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pm_service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maintenance_template_id` int ,
  `name` varchar(50) ,
  `type` varchar(50) ,
  `priority` varchar(50) ,
  `enabled` int ,
  `start_date` timestamp ,
  `end_date` timestamp ,
  `meter_type` varchar(50) ,
  `notify_meter_before` varchar(50) ,
  `start_hour` timestamp ,
  `end_hour` timestamp ,
  `hour_type` varchar(50) ,
  `notify_hour_before` varchar(50) ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pm_service`
--

LOCK TABLES `pm_service` WRITE;
/*!40000 ALTER TABLE `pm_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `pm_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `position` (
  `id` int NOT NULL AUTO_INCREMENT,
  `position_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `van_id` int ,
  `lat` double ,
  `lng` int ,
  `time` date ,
  `speed` double ,
  `flue_percent` float ,
  `motion` varchar(10) ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  `create_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating` (
  `id` int NOT NULL AUTO_INCREMENT,
  `routing_plan_day_id` int ,
  `num_rating` int ,
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `driver_id` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward`
--

DROP TABLE IF EXISTS `reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reward` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reward_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `reward_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `level` int ,
  `from_date` timestamp ,
  `to_date` timestamp ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward`
--

LOCK TABLES `reward` WRITE;
/*!40000 ALTER TABLE `reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int ,
  `staff_id` int ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) ,
  `role_code` varchar(100) ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routing_plan_day`
--

DROP TABLE IF EXISTS `routing_plan_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `routing_plan_day` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date_plan` timestamp ,
  `type` int ,
  `routing_van_id` int ,
  `billing_lading_detail_id` int ,
  `order` int ,
  `warehouse_id` int ,
  `warehouse_type` int ,
  `status_van_motion` int ,
  `bill_lading_bill_type` int ,
  `capacity_expected` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  `expected_from_time` timestamp ,
  `expected_to_time` timestamp ,
  `actual_time` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routing_plan_day`
--

LOCK TABLES `routing_plan_day` WRITE;
/*!40000 ALTER TABLE `routing_plan_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `routing_plan_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routing_van`
--

DROP TABLE IF EXISTS `routing_van`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `routing_van` (
  `id` int NOT NULL AUTO_INCREMENT,
  `routing_van_code` varchar(500) ,
  `van_id` int ,
  `status` int ,
  `running_date` timestamp ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routing_van`
--

LOCK TABLES `routing_van` WRITE;
/*!40000 ALTER TABLE `routing_van` DISABLE KEYS */;
/*!40000 ALTER TABLE `routing_van` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saleman`
--

DROP TABLE IF EXISTS `saleman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saleman` (
  `id` int NOT NULL AUTO_INCREMENT,
  `saleman_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `saleman_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `address` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `phone` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `username` varchar(100) ,
  `password` varchar(1000) ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saleman`
--

LOCK TABLES `saleman` WRITE;
/*!40000 ALTER TABLE `saleman` DISABLE KEYS */;
/*!40000 ALTER TABLE `saleman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor_log`
--

DROP TABLE IF EXISTS `sensor_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sensor_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `capacity` double ,
  `alert_consillion` bit(1) ,
  `current_product_status` int ,
  `temporature` double ,
  `attribute` varchar(1000) ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `create_date` timestamp ,
  `update_date` timestamp ,
  `van_id` int ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor_log`
--

LOCK TABLES `sensor_log` WRITE;
/*!40000 ALTER TABLE `sensor_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensor_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `service_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `service_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `service_type` int ,
  `price` double ,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `status` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_date` timestamp ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_param_value_id` int(11) ,
  `staff_code` varchar(200) CHARACTER SET utf8 ,
  `staff_name` varchar(200) CHARACTER SET utf8 ,
  `has_image` int(11) ,
  `has_attachment` int(11) ,
  `birthday` timestamp NOT NULL ,
  `province` varchar(200) CHARACTER SET utf8 ,
  `nation` varchar(200) CHARACTER SET utf8 ,
  `country` varchar(200) CHARACTER SET utf8 ,
  `address` varchar(200) CHARACTER SET utf8 ,
  `phone` varchar(15) CHARACTER SET utf8 ,
  `email` varchar(100) CHARACTER SET utf8 ,
  `driver_licence_id` varchar(20) ,
  `driver_licence_type` int(11) ,
  `create_date` timestamp NOT NULL ,
  `hire_date` timestamp NOT NULL ,
  `leave_date` timestamp NOT NULL ,
  `birth_date` timestamp NOT NULL ,
  `labor_rate` int(11) ,
  `billing_rate` int(11) ,
  `SSN` int(11) ,
  `latitude` double ,
  `longitude` double ,
  `state_province` varchar(100) CHARACTER SET utf8 ,
  `license_note` varchar(100) CHARACTER SET utf8 ,
  `create_user` varchar(100) ,
  `update_date` timestamp NOT NULL ,
  `imei` varchar(50) ,
  `point` int(11) ,
  `object_id` int(11) ,
  `idNo` varchar(100) ,
  `status` int(11) ,
  `object_type` int(2)  COMMENT '1: fleet, 2: customer',
  `description` varchar(255) ,
  `update_user` varchar(100) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,0,'DRIVER_TEST','TEST',1,1,'1993-03-22 17:00:00','HAI PHONG','VINH BAO','VIET NAM','DUNG TIEN','0977568067','test@gmail.com','GH0091282893',0,'2020-03-23 17:15:25',NULL,NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,'2020-03-23 17:15:25','1234545',0,0,'031981636',2),(2,0,'LONG LONG','TEST',1,0,'1999-03-22 17:00:00','HA NOI','SON TAY','VIET NAM','QUOC OAI','0977568067','test@gmail.com','GH0091282893',0,'2020-03-23 17:33:33',NULL,NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,'2020-03-23 17:33:33','1234545',0,0,'031981636',2),(3,0,'LONG LONG','TEST',1,0,'1999-03-22 17:00:00','HA NOI','SON TAY','VIET NAM','QUOC OAI','0977568067','test@gmail.com','GH0091282893',0,'2020-03-23 18:00:40',NULL,NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,'2020-03-23 18:00:40','1234545',0,0,'031981636',2),(4,0,'LONG LONG','TEST',1,0,'1999-03-22 17:00:00','HA NOI','SON TAY','VIET NAM','QUOC OAI','0977568067','test@gmail.com','GH0091282893',0,'2020-03-23 18:02:09',NULL,NULL,NULL,0,0,0,0,0,NULL,NULL,NULL,'2020-03-23 18:02:09','1234545',0,0,'031981636',2);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `van`
--

DROP TABLE IF EXISTS `van`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `van` (
  `id` int NOT NULL AUTO_INCREMENT,
  `licence_plate` varchar(50) ,
  `location` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `driver` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `cost_center` double ,
  `maintenance_template_id` int ,
  `axles` double ,
  `tire_front_size` double ,
  `tire_front_pressure` double ,
  `tire_rear_size` double ,
  `tire_rear_pressure` double ,
  `has_image` int ,
  `has_attachment` int ,
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `warranty_name1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `warranty_date1` timestamp ,
  `warranty_meter1` double ,
  `warranty_name2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `warranty_date2` timestamp ,
  `warranty_meter2` double ,
  `van_type_Id` int ,
  `app_param_value_id` int ,
  `vehicle_tonage` varchar(50) ,
  `van_inspection` varchar(50) ,
  `capacity` double ,
  `available_capacity` double ,
  `status_car` int ,
  `status_available` int ,
  `fleet_id` int ,
  `vehicle_registration` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `description` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  `body_length` double ,
  `body_width` double ,
  `height` double ,
  `wheelbase` double ,
  `gross_weight` double ,
  `engine_size` double ,
  `fuel_type` varchar(100) ,
  PRIMARY KEY (`id`),
  KEY `fk_tc_vanfleet_id` (`fleet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `van`
--

LOCK TABLES `van` WRITE;
/*!40000 ALTER TABLE `van` DISABLE KEYS */;
/*!40000 ALTER TABLE `van` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `contact_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `address1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `address2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `state_province` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci ,
  `postal_code` varchar(20) ,
  `phone1` varchar(20) ,
  `phone2` varchar(20) ,
  `fax` varchar(20) ,
  `website` varchar(20) ,
  `payment_type` int ,
  `pay_by_day` timestamp ,
  `discount` double ,
  `discount_type` int ,
  `create_date` timestamp ,
  `create_user` varchar(100) ,
  `update_user` varchar(100) ,
  `update_date` timestamp ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) ,
  `address` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `lat` double ,
  `lng` double ,
  `area` int(11) ,
  `phone` varchar(100) ,
  `status` int(11) ,
  `customer_id` int(11) ,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `create_user` varchar(100) ,
  `update_date` timestamp NOT NULL ,
  `update_user` varchar(100) ,
  `name` varchar(145) NOT NULL,
  `description` varchar(45) ,
  `approve_date` timestamp NULL ,
  `approve_user` varchar(145) ,
  `organization_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse`
--

LOCK TABLES `warehouse` WRITE;
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
INSERT INTO `warehouse` VALUES (1,'WAREHOUSE','LE VAN THIEM, THANH XUAN, HA NOI',24.0923,4.324,'HA NOI','0320198998',1,1,'2020-03-23 18:27:05',NULL,'2020-03-23 18:27:05',NULL);
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime ,
  `create_user` varchar(255) ,
  `update_date` datetime ,
  `update_user` varchar(255) ,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `create_date` datetime ,
  `create_user` varchar(255) ,
  `update_date` datetime ,
  `update_user` varchar(255) ,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `object_id` int(11) ,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `email` varchar(45) ,
  `type` varchar(45) ,
  `organization_id` int(11) ,
  `tel` varchar(45) ,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-25 14:44:46
