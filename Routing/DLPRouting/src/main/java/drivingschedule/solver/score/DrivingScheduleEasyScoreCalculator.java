package drivingschedule.solver.score;

import drivingschedule.domain.*;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

import java.util.HashMap;
import java.util.List;

public class DrivingScheduleEasyScoreCalculator implements EasyScoreCalculator<DrivingScheduleSolution> {
    @Override
    public HardSoftLongScore calculateScore(DrivingScheduleSolution drivingScheduleSolution) {
        List<Driver> lstDriver = drivingScheduleSolution.getLstDriver();
        List<DrivingSchedule> lstDriving = drivingScheduleSolution.getLstSchedule();
     //   Pair<Long, Long> hardSoftScore = Pair.of(0L, 0L);
        long sorfScore = 0, hardScore = 0;
        for(DrivingSchedule schedule: lstDriving){
            if(isSameDriver(lstDriving) == true){
                hardScore -= 10;
            }
            boolean check1 = checkLicenseType(schedule);
            if(check1 == false){
                hardScore -= 10;
            }
            boolean check2= checkCompany(schedule);
            if (check2 == false){
                hardScore -= 10;
            }
            boolean check3 = isSameTeam(schedule);
            if(check3 == false){
                hardScore -= 10;
            }
            sorfScore += checkSoftScoreByDriver(schedule);
        }
//        if(hardScore == 0 ){
//            for(DrivingSchedule schedule: lstDriving){
//                if(schedule.getDriver() != null)
//                    System.out.println("driver: " + schedule.getDriver().getName());
//                if(schedule.getVehicle() != null)
//                    System.out.println("vehicle: " + schedule.getVehicle().getName());
//                System.out.println("--------------------------------");
//            }
//        }
        return HardSoftLongScore.of(hardScore, sorfScore);

    }
    public boolean isSameDriver(List<DrivingSchedule> lstDriving){
        for(int i = 0; i < lstDriving.size() - 1; i ++ ){
            for (int j = i +1 ; j < lstDriving.size(); j++){
                Driver firstDriver = lstDriving.get(i).getDriver();
                Driver secondDriver = lstDriving.get(j).getDriver();
                if(firstDriver != null && secondDriver != null && firstDriver.getId().equals(secondDriver.getId())){
                    return true;
                }
            }
        }
        return false;
    }
    public boolean isSameTeam(DrivingSchedule schedule){
        Driver driver = schedule.getDriver();
        Vehicle vehicle = schedule.getVehicle();
        if(driver == null || vehicle == null)
            return true;
        if(driver.getTeamId() == null || vehicle.getTeamId() == null)
            return false;
        if (!driver.getTeamId().equals(vehicle.getTeamId())){
            return false;
        }
        if(driver.getTeamId().equals(vehicle.getTeamId())){
            return true;
        }
        return false;
    }

    public boolean checkLicenseType(DrivingSchedule schedule){
        Driver driver = schedule.getDriver();
        Vehicle vehicle = schedule.getVehicle();
        if(driver == null || vehicle == null)
            return true;
        boolean check = LicenseType.checkLicense(driver,vehicle);
        return check;
    }
    public boolean checkCompany(DrivingSchedule schedule){
        Driver driver = schedule.getDriver();
        Vehicle vehicle = schedule.getVehicle();
        if(driver == null || vehicle == null)
            return true;
        if(driver.getCompanyId().equals(vehicle.getCompanyId())){
            return true;
        }
        return false;
    }
    public int checkSoftScoreByDriver(DrivingSchedule schedule){
        int score = 0;
        Driver driver = schedule.getDriver();
        Vehicle vehicle = schedule.getVehicle();
        if(driver == null || vehicle == null){
            return score;
        }
        HashMap<Integer,Integer> lstHisDriver = vehicle.getLstHisDriver();
        for(Integer key: lstHisDriver.keySet()){
            if(key.equals(driver.getId())){
                score = lstHisDriver.get(key) * 10;
            }
        }
        return score;
    }
}
