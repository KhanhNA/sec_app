/*
 * Copyright 2013 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.solver.score;


import common.constants.Constansts;
import maps.GoogleDistanceAPI;
import ns.utils.JPAUtility;
import ns.vtc.constants.Constants;
import ns.vtc.entity.LocationData;
import ns.vtc.service.DBService;
import org.apache.commons.math3.analysis.function.Constant;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;
import vehiclerouting.customize.StaticData;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.customize.types.DistanceInfo;
import vehiclerouting.customize.types.Pair;
import vehiclerouting.customize.types.ShipType;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.domain.location.Location;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VehicleRoutingEasyScoreCalculatorV2 implements EasyScoreCalculator<VehicleRoutingSolutionV2> {

    /*
    1. shipmentPotList: danh sach cac diem giao nhan cua khach hang
    2. vehicleList: danh sach cac xe dung de giao nhan
    * */
    public static int max_distance_routing = 0;
    public static int driver_check_duration =0;
    static long totalDistance = 0;
    public static int delta_distance = 0, dem = 0;
    static HashMap<String, LocationData> locationDataHashMap = new HashMap<>();
   // public static HashMap<VehicleRoutingSolutionV2,Long> mapSolution = new HashMap<>();
    @Override
    public HardSoftLongScore calculateScore(VehicleRoutingSolutionV2 solution) {
//        if(max_distance_routing != 0)
//          //  max_distance_routing = DBService.getInstance().getConfigParamRouting(Constants.MAX_DISTANCE_ROUTING);
//            if(max_distance_routing == 0){
//                max_distance_routing = Constants.DEFAULT_MAX_DISTANCE_ROUTING;
//            }
        max_distance_routing = Constants.DEFAULT_MAX_DISTANCE_ROUTING;
        if(driver_check_duration != 0)
            driver_check_duration = DBService.getInstance().getConfigParamRouting(Constants.DRIVER_CHECK_POINT_DURATION);
        if(locationDataHashMap == null || locationDataHashMap.size() ==0){
            locationDataHashMap = StaticData.locationDataHashMap;
        }
        delta_distance = Constants.DELTA_DISTANCE;
      //  System.out.println("max distance: " + max_distance_routing);
        boolean timeWindowed = solution instanceof TimeWindowedVehicleRoutingSolution;
        List<ShipmentPot> shipmentPotList = solution.getShipmentPotList();
        List<VehicleV2> vehicleList = solution.getVehicleList();
        Map<VehicleV2, Integer> vehicleDemandMap = new HashMap<>(vehicleList.size());

        // Map<String, Integer> customerMap = new HashMap<>();

        long hardPickupScore = 0L;
        long hardScore = 0l;
        long softScore = 0L;
        Pair<Long, Long> hardSoftScore = Pair.of(0L, 0L), tmp;

        for (VehicleV2 vehicle : vehicleList) {
            tmp = calcHardSoftScore(solution, vehicle, timeWindowed);

            if(tmp.getLeft()== 0L){
              //  System.out.println("thom vu");
                checkTimeShipmentPot(vehicle, hardScore);
            }

            hardSoftScore.set(hardSoftScore.getLeft() + tmp.getLeft() + hardScore, hardSoftScore.getRight() + tmp.getRight());
        }
        System.out.println("score: " + hardSoftScore.getRight());
        int check = 0;
//        for(VehicleV2 v: vehicleList){
//
//            ShipmentPot shipmentPot = v.getNextShipmentPot();
//            if(shipmentPot != null ){
//                check ++;
////                System.out.println("vehicle: " + v.getId() + "  tong quang duwong: " + v.getTotalDistance());
////                System.out.print(shipmentPot.getId() + "->");
////                while (shipmentPot.getNextShipmentPot() != null){
////                    shipmentPot = shipmentPot.getNextShipmentPot();
////                    System.out.print(shipmentPot.getId() + "->");
////                }
////                System.out.println();
////                System.out.println("----------------------------------------");
//            }
//        }
//        if(check > 2){
//            for(VehicleV2 v: vehicleList){
//
//                ShipmentPot shipmentPot = v.getNextShipmentPot();
//                if(shipmentPot != null ){
//                    check ++;
//                    System.out.println("vehicle: " + v.getId() + "  tong quang duwong: " + v.getTotalDistance());
//                    System.out.print(shipmentPot.getId() + "->");
//                    while (shipmentPot.getNextShipmentPot() != null){
//                        shipmentPot = shipmentPot.getNextShipmentPot();
//                        System.out.print(shipmentPot.getId() + "->");
//                    }
//                    System.out.println();
//                    System.out.println("----------------------------------------");
//                }
//            }
//        }
    //    System.out.println("**********************************************");
        if(hardSoftScore.getLeft() == 0){
            dem ++;
            Constansts.mapSolution.put(solution,hardSoftScore.right);
            for(VehicleV2 v: vehicleList){
                System.out.println("vehicle: " + v.getId());
                ShipmentPot shipmentPot = v.getNextShipmentPot();
                if(shipmentPot != null){
                    System.out.print(shipmentPot.getId() + "->");
                    while (shipmentPot.getNextShipmentPot() != null){
                        shipmentPot = shipmentPot.getNextShipmentPot();
                        System.out.print(shipmentPot.getId() + "->");
                    }
                    System.out.println();
                    System.out.println("******************************************");
                }
            }
        }
        //System.out.println("dem phuong an toi uu: " + dem);

        // Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
        return HardSoftLongScore.of(hardSoftScore.getLeft(), hardSoftScore.getRight());
    }
    //    @Override
//    public HardSoftLongScore calculateScore(VehicleRoutingSolutionV2 solution) {
//        boolean timeWindowed = false; //solution instanceof TimeWindowedVehicleRoutingSolution;
//        List<ShipmentPot> shipmentPotList = solution.getShipmentPotList();
//        List<VehicleV2> vehicleList = solution.getVehicleList();
//        Map<VehicleV2, Integer> vehicleDemandMap = new HashMap<>(vehicleList.size());
//
//       // Map<String, Integer> customerMap = new HashMap<>();
//
//        long hardPickupScore = 0L;
//        for (VehicleV2 vehicle : vehicleList) {
//            vehicleDemandMap.put(vehicle, 0);
//            //check constraint pickup-delivery
//            //hardPickupScore += scorePickupDelivery(vehicle);
//        }
//
//        long hardScore = 0l;
//        long softScore = 0L;
//
//
//        for (ShipmentPot shipmentPot : shipmentPotList) {
//            ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
//            if (previousStandstill != null) {
//                VehicleV2 vehicle = shipmentPot.getVehicle();
//                vehicleDemandMap.put(vehicle, vehicleDemandMap.get(vehicle) + shipmentPot.getDemand());
//
//                // Score constraint distanceToPreviousStandstill
//                softScore -= shipmentPot.getDistanceFromPreviousStandstill();
//                if (shipmentPot.getNextShipmentPot() == null) {
//                    // Score constraint distanceFromLastCustomerToDepot
//                   // softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
//                }
////                if (timeWindowed) {
////                    TimeWindowedCustomer timeWindowedCustomer = (TimeWindowedCustomer) customer;
////                    long dueTime = timeWindowedCustomer.getDueTime();
////                    Long arrivalTime = timeWindowedCustomer.getArrivalTime();
////                    if (dueTime < arrivalTime) {
////                        // Score constraint arrivalAfterDueTime
////                        hardScore -= (arrivalTime - dueTime);
////                    }
////                }
//            }
//        }
//
//        for (Map.Entry<VehicleV2, Integer> entry : vehicleDemandMap.entrySet()) {
//            int capacity = entry.getKey().getCapacity();
//            int demand = entry.getValue();
//            if (demand > capacity) {
//                // Score constraint vehicleCapacity
//                hardScore -= (demand - capacity);
//            }
//        }
//        hardPickupScore = 0L;
//        for (VehicleV2 vehicle : vehicleList) {
//            //vehicleDemandMap.put(vehicle, 0);
//            //check constraint pickup-delivery
//            hardScore += calcHardSoftScore(vehicle);
//        }
//
//
//        // Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
//        return HardSoftLongScore.of(hardScore, softScore);
//    }
    private long scorePickupDelivery(VehicleV2 vehicle) {
        ShipmentPot shipmentPot = vehicle.getNextShipmentPot();
        if (shipmentPot == null) {
            return 0;
        }
        Double shipmentDemand = 0d;
        String shipKey;
        Map<String, Double> pickupMap = new HashMap<>();
        Integer tongnhan = 0;

        while (shipmentPot != null) {
            if (shipmentPot.getShipType() == ShipType.PICKUP) {
                shipKey = "NHAN";
                String temKey = shipmentPot.getDepotId() + "----" + shipKey;
                if (pickupMap.get(temKey) != null) {
                    Double a = pickupMap.get(temKey) + shipmentPot.getDemand();
                    pickupMap.put(temKey, a);
                } else {
                    pickupMap.put(temKey, shipmentPot.getDemand());
                }
            } else {
                Double a = pickupMap.get(shipmentPot.getDepotId() + "----" + "NHAN");
                if (a != null) {
                    if (a < shipmentPot.getDemand() * (-1)) {
                        return -4000000;
                    }
                    pickupMap.put(shipmentPot.getDepotId() + "----" + "NHAN", a + shipmentPot.getDemand());
                } else {
                    return -3000000;
                }
            }
            ;

            shipmentDemand += shipmentPot.getDemand();
            if (shipmentDemand < 0) {
                return -3000000;
            }
            shipmentPot = shipmentPot.getNextShipmentPot();
        }
        return 0;
    }

    private long calcHardScore(VehicleV2 vehicle) { //ra soat tren cung 1 xe
        ShipmentPot shipmentPot = vehicle.getNextShipmentPot();
        if (shipmentPot == null) {
            return 0;
        }
        Integer shipmentDemand = 0;
        Integer shipKey;
        Map<Integer, Double> pickupMap = new HashMap<>();
        Double tongnhan = 0d, tmpShipTotal, pickupScore = 0d;

        while (shipmentPot != null) {
            shipKey = shipmentPot.getDepotId();
            tmpShipTotal = pickupMap.get(shipKey);//lay tong pickup va delivery cua 1 depot den hien tai
            tmpShipTotal = tmpShipTotal == null ? 0 : tmpShipTotal;
            tmpShipTotal += shipmentPot.getDemand();//lay bo sung
            pickupScore = 0d;
            if (tmpShipTotal < 0) {
                pickupScore = -1000000 + tmpShipTotal;//1m
                return pickupScore.longValue();
            }
            if (tmpShipTotal > vehicle.getCapacity()) {
                pickupScore -= (tmpShipTotal - vehicle.getCapacity());
                return pickupScore.longValue();
            }
            if (pickupScore < 0) {
                return pickupScore.longValue();
            }
            pickupMap.put(shipKey, tmpShipTotal);

            shipmentPot = shipmentPot.getNextShipmentPot();//lay diem den tiep theo
        }
        return 0;
    }

    private Pair<Long, Long> calcHardSoftScore(VehicleRoutingSolutionV2 solution, VehicleV2 vehicle, boolean timeWindowed) { //ra soat tren cung 1 xe
        long hardScore = 0;
        long softScore = 0;
        Integer shipmentDemand = 0;
        Map<VehicleV2, Double> pickupMap = new HashMap<>();
        Map<VehicleV2, Long> totalTime = new HashMap<>();
        Integer tongnhan = 0, tmpShipTotal, pickupScore = 0;

        ShipmentPot shipmentPot = vehicle.getNextShipmentPot();
        if (shipmentPot == null) {
            return Pair.of(hardScore, softScore); //HardSoftLongScore.of(hardScore, softScore);
        }
        totalDistance = 0;
     //   while (shipmentPot != null && hardScore >= 0)
        int i = 0;
        long distanceToParking = 0;

        while (shipmentPot != null) { //lap tuan tu tu depot den het cac diem nhan
            long distance = calcSoftScoreAtPot(vehicle, shipmentPot, timeWindowed);
            softScore += distance;
            hardScore += calcHardScoreAtPot(vehicle, shipmentPot, timeWindowed, pickupMap, totalTime);
            shipmentPot = shipmentPot.getNextShipmentPot();
            if( i == 0){
                distanceToParking = distance;
            }
            i++;
        }
        totalDistance += distanceToParking;
        //System.out.println("total Distance: " + totalDistance);
        vehicle.setTotalDistance(totalDistance);
        if(totalDistance > max_distance_routing){
            hardScore += (max_distance_routing - totalDistance);
        }
      //  System.out.println("total_distance: " + totalDistance);
        //if(totalDistance > max_distance_routing){
//        if(totalDistance > 200000){
//           // System.out.println("max_distance_routing "+ max_distance_routing);
//            hardScore += (max_distance_routing - totalDistance);
//        }


        return Pair.of(hardScore, softScore);//HardSoftLongScore.of(hardScore, softScore);
    }

//    private long calcSoftScoreAtPot(VehicleV2 vehicle, ShipmentPot shipmentPot, boolean timeWindowed) {
//        ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
//        long softScore = 0;
//
//        if (previousStandstill != null) {
//            HashMap<String, DistanceInfo<Double, Double>> locDataMap = StaticData.getLocDataMap();
//            //softcode la chi phi de di tu previous to hien tai. chi phi nay duoc luu trong bang location_data
//            softScore -= StaticData.getCost(previousStandstill, shipmentPot);
//            if (shipmentPot.getNextShipmentPot() == null) {
//                // Score constraint distanceFromLastCustomerToDepot
//                softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
//            }
//        }
//        //
//        if(shipmentPot.getNextShipmentPot() == null){
//            softScore -= vehicle.getVehicleCost();
//        }
//
//        return softScore;
//    }
    private long calcSoftScoreAtPot(VehicleV2 vehicle, ShipmentPot shipmentPot, boolean timeWindowed) {
        ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
        long softScore = 0;

        if (previousStandstill != null) {
            HashMap<String, DistanceInfo<Double, Double>> locDataMap = StaticData.getLocDataMap();
            //softcode la chi phi de di tu previous to hien tai. chi phi nay duoc luu trong bang location_data
          //  softScore -= StaticData.getCost(previousStandstill, shipmentPot)* (vehicle.getCostPerUnit() == null? 1: vehicle.getCostPerUnit());
            double distance = StaticData.getCost(previousStandstill, shipmentPot);
            softScore -= distance;
            totalDistance  += distance;
            //System.out.println("cost: " + StaticData.getCost(previousStandstill, shipmentPot));
            if (shipmentPot.getNextShipmentPot() == null) {
                // Score constraint distanceFromLastCustomerToDepot
               // softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation())* (vehicle.getCostPerUnit() == null? 1: vehicle.getCostPerUnit());
                softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
              //  totalDistance += shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
                //System.out.println("cost: " + shipmentPot.getLocation().getDistanceTo(vehicle.getLocation()));
               // softScore = softScore/(vehicle.getPriority() == null? 1: vehicle.getPriority() );
            }
        }
     //   System.out.println("ttdistan: " + totalDistance);
        //
//        if(shipmentPot.getNextShipmentPot() == null){
//            softScore -= vehicle.getVehicleCost();
//        }

        return softScore;
    }

    private long calcHardScoreAtPot(VehicleV2 vehicle, ShipmentPot shipmentPot, boolean timeWindowed, Map<VehicleV2, Double> pickupMap,
                                    Map<VehicleV2, Long> totalTime) {
       // Integer shipKey = shipmentPot.getDepotId();
        Double tmpShipTotal, hardScore = 0d;
        tmpShipTotal = pickupMap.get(vehicle);//lay tong pickup va delivery cua 1 depot den hien tai
        tmpShipTotal = tmpShipTotal == null ? 0 : tmpShipTotal;
        tmpShipTotal += shipmentPot.getDemand();//lay bo sung

        if (tmpShipTotal < 0) {
            hardScore = -1000000 + tmpShipTotal;//1m
            return hardScore.longValue();
        }
        int check = checkShipmentPot(vehicle,shipmentPot);
        if(check != 0){
            return check;
        }

        if (tmpShipTotal > vehicle.getCapacity()) {
            hardScore -= (tmpShipTotal - vehicle.getCapacity());
            return hardScore.longValue();
        }
        ShipmentStandstill previous = shipmentPot.getPreviousStandstill();

//        if (timeWindowed) {
//            TimeWindowedShipmentPot timeWindowedShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
//            long dueTime = timeWindowedShipmentPot.getDueTime();
//            Long arrivalTime = timeWindowedShipmentPot.getArrivalTime();
//            if (dueTime < arrivalTime) {
//                // Score constraint arrivalAfterDueTime
//                hardScore -= (arrivalTime - dueTime);
//                return hardScore;
//            }
//        }


//        if (timeWindowed && previous != null ) {
//            HashMap<String, DistanceInfo<Long, Long>> locDataMap = StaticData.getLocDataMap();
//            TimeWindowedShipmentPot timeWindowedshipmentPot = (TimeWindowedShipmentPot) shipmentPot;
//            Long arrivalTime = totalTime.get(vehicle);
//            arrivalTime = arrivalTime == null?0: arrivalTime;
//            arrivalTime += StaticData.getTime(previous, shipmentPot);
//            long dueTime = timeWindowedshipmentPot.getDueTime();
//
//            if (dueTime < arrivalTime) {
//                // Score constraint arrivalAfterDueTime
//                hardScore -= (arrivalTime - dueTime);
//                return hardScore;
//
//            }
//            totalTime.put(vehicle, arrivalTime);
//        }


        pickupMap.put(vehicle, tmpShipTotal);

        return hardScore.longValue();
    }
    //---------------------Bo sung cac rang buoc---------------
    private int checkShipmentPot(VehicleV2 vehicle, ShipmentPot shipmentPot){
        int dem = 0;
        int numReceive = shipmentPot.getNumReceive();
        Integer id = Math.toIntExact(shipmentPot.getId());
        Integer fromId = shipmentPot.getFromShipmentPotId();
        //nhan hang phai tra het cac diem
        if(shipmentPot.getShipType().getType().equals(ShipType.PICKUP.getType())){
            while (shipmentPot.getNextShipmentPot() != null){
                shipmentPot = shipmentPot.getNextShipmentPot();
                if(id.equals(shipmentPot.getFromShipmentPotId())){
                    dem ++;
                }
                if(dem == numReceive){
                    return 0;
                }
            }
            return -1000000;
        }
        //khong duoc tra truoc nhan
        else if(shipmentPot.getShipType().getType().equals(ShipType.DELIVERY.getType())){
            ShipmentPot sp = vehicle.getNextShipmentPot();
            while (sp != null && !sp.getId().equals(id)){
                Integer shipPotId = Math.toIntExact(sp.getId());
                if(shipPotId.equals(fromId)){
                    return 0;
                }
                sp = sp.getNextShipmentPot();
            }
            return -2000000;
        }
        return -3000000;
    }
    //get time plan
    private void checkTimeShipmentPot(VehicleV2 v2, long hardScore){

//        for(String key : locationDataHashMap.keySet()){
//            System.out.println("key: " + locationDataHashMap.get(key));
//        }
        ShipmentPot shipmentPot = v2.getNextShipmentPot();
        if(shipmentPot != null){
            String time = shipmentPot.getDate_plan() + " " + Constansts.START_TIME;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime startTime = LocalDateTime.parse(time, formatter);
            double capacity = 0;
            Location beforeLocation = v2.getLocation();
            LocalDateTime endTime;
            int dem = 1;
            int durationTime;
            while ( shipmentPot != null){
                durationTime = (int) getCodeLocation(beforeLocation, shipmentPot.getLocation());
            //    System.out.println("sizeeee1: " + locationDataHashMap.size());
                startTime = startTime.plusMinutes(durationTime);
                shipmentPot.setFromExpectedTime(startTime);
                shipmentPot.setToExpectedTime(startTime.plusMinutes(driver_check_duration));
                beforeLocation = shipmentPot.getLocation();
                shipmentPot.setOrderNumber(dem);
                capacity += shipmentPot.getDemand();
                shipmentPot.setCapacityVehicle(capacity);
                dem++;
//                System.out.println("id: "+ shipmentPot.getId());
//                System.out.println("from: " + startTime);
//                System.out.println("------------------------------");
                if(shipmentPot.getNextShipmentPot() != null)
                    shipmentPot = shipmentPot.getNextShipmentPot();
                else
                    break;
            }
            if(shipmentPot != null){
                System.out.println("edjfndjsndjsn");
                durationTime = (int) getCodeLocation(shipmentPot.getLocation(), v2.getLocation());
         //       System.out.println("sizeeee1: " + locationDataHashMap.size());
                startTime = shipmentPot.getToExpectedTime().plusMinutes(durationTime);
//                if(startTime.isAfter(LocalDateTime.parse(shipmentPot.getDate_plan() + " 18:00", formatter))){
//                    hardScore = -10;
//                }
            }
        }
    }
    private double  getCodeLocation(Location l1, Location l2){
        String code = LocationData.getLocationCode(l1.getLatitude(),l1.getLongitude(),l2.getLatitude(),l2.getLongitude());
        LocationData data = locationDataHashMap.get(code);
        //de sau
       // System.out.println(l1.toString());
       // System.out.println(l2.toString());
        if(data == null){
            data = GoogleDistanceAPI.getDistance(true,l1.getLatitude(),l1.getLongitude(), l2.getLatitude(), l2.getLongitude());
            if( Double.compare(data.getMinutes(),0.0) == 0  &&
                    Double.compare(data.getCost(),0.0) == 0   &&
                    !data.getFromLatitude().equals(data.getToLatitude())){
                getCodeLocation(l1,l2);
            }
            else{
                data = JPAUtility.getInstance().save(data);
                System.out.println("code: " + data.getCode());
                System.out.println("cost1: " + data.getCost() + " time1: " + data.getMinutes());
                JPAUtility.getInstance().close();
                locationDataHashMap.put(code,data);
            }
            //            data = JPAUtility.getInstance().save(data);
//            JPAUtility.getInstance().close();
//            locationDataHashMap.put(code,data);
        }





//        if(data == null){
//            Random generator = new Random();
//            double minutes = generator.nextInt(5) + 1;
//            data = new LocationData( l1.getLatitude(), l1.getLongitude(), l2.getLatitude(), l2.getLongitude(),minutes*10,minutes*1000,"A","B");
//            locationDataHashMap.put(code,data);
//        }
        return data.getMinutes() == null ? 0: data.getMinutes();
    }

}
