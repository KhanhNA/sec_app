/*
 * Copyright 2020 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.solver.solution.initializer;

import org.optaplanner.core.api.score.Score;

import org.optaplanner.core.api.score.director.ScoreDirector;
import org.optaplanner.core.impl.phase.custom.CustomPhaseCommand;
import org.optaplanner.core.impl.score.director.InnerScoreDirector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;

import java.util.*;

// TODO PLANNER-380 Delete this class. Temporary implementation until BUOY_FIT is implemented as a Construction Heuristic
public class BuoyVehicleRoutingSolutionInitializer implements CustomPhaseCommand<VehicleRoutingSolutionV2> {

    protected final transient Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void changeWorkingSolution(ScoreDirector<VehicleRoutingSolutionV2> scoreDirector) {
        VehicleRoutingSolutionV2 solution = scoreDirector.getWorkingSolution();
        List<VehicleV2> vehicleList = solution.getVehicleList();
        List<ShipmentPot> shipmentPotList = solution.getShipmentPotList();
        List<ShipmentStandstill> standstillList = new ArrayList<>(vehicleList.size() + shipmentPotList.size());
        standstillList.addAll(vehicleList);
        standstillList.addAll(shipmentPotList);
        logger.info("Starting sorting");
        Map<ShipmentStandstill, ShipmentPot[]> nearbyMap = new HashMap<>(standstillList.size());
        for (final ShipmentStandstill origin : standstillList) {
            ShipmentPot[] nearbyCustomers = shipmentPotList.toArray(new ShipmentPot[0]);
            Arrays.sort(nearbyCustomers, new Comparator<ShipmentStandstill>() {
                @Override
                public int compare(ShipmentStandstill a, ShipmentStandstill b) {
                    double aDistance = origin.getLocation().getDistanceTo(a.getLocation());
                    double bDistance = origin.getLocation().getDistanceTo(b.getLocation());
                    return Double.compare(aDistance, bDistance);
                }
            });
            nearbyMap.put(origin, nearbyCustomers);
        }
        logger.info("Done sorting");

        List<ShipmentStandstill> buoyList = new ArrayList<>(vehicleList);

        int NEARBY_LIMIT = 40;
        while (true) {
            Score stepScore = null;
            int stepBuoyIndex = -1;
            ShipmentPot stepEntity = null;
            for (int i = 0; i < buoyList.size(); i++) {
                ShipmentStandstill buoy = buoyList.get(i);

                ShipmentPot[] nearbyShipmentPot = nearbyMap.get(buoy);
                int j = 0;
                for (ShipmentPot shipmentPot : nearbyShipmentPot) {
                    if (shipmentPot.getPreviousStandstill() != null) {
                        continue;
                    }
                    scoreDirector.beforeVariableChanged(shipmentPot, "previousStandstill");
                    shipmentPot.setPreviousStandstill(buoy);
                    scoreDirector.afterVariableChanged(shipmentPot, "previousStandstill");
                    scoreDirector.triggerVariableListeners();
                    Score score = ((InnerScoreDirector<VehicleRoutingSolutionV2>) scoreDirector).calculateScore();
                    scoreDirector.beforeVariableChanged(shipmentPot, "previousStandstill");
                    shipmentPot.setPreviousStandstill(null);
                    scoreDirector.afterVariableChanged(shipmentPot, "previousStandstill");
                    scoreDirector.triggerVariableListeners();
                    if (stepScore == null || score.withInitScore(0).compareTo(stepScore.withInitScore(0)) > 0) {
                        stepScore = score;
                        stepBuoyIndex = i;
                        stepEntity = shipmentPot;
                    }
                    if (j >= NEARBY_LIMIT) {
                        break;
                    }
                    j++;
                }
            }
            if (stepEntity == null) {
                break;
            }
            ShipmentStandstill stepValue = buoyList.set(stepBuoyIndex, stepEntity);
            scoreDirector.beforeVariableChanged(stepEntity, "previousStandstill");
            stepEntity.setPreviousStandstill(stepValue);
            scoreDirector.afterVariableChanged(stepEntity, "previousStandstill");
            scoreDirector.triggerVariableListeners();
            logger.debug("    Score ({}), assigned customer ({}) to stepValue ({}).", stepScore, stepEntity, stepValue);
        }
    }

}
