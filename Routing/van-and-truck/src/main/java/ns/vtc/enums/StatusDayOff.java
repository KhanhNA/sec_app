package ns.vtc.enums;

public enum StatusDayOff {

    WAITING("1"),
    ACCEPTED("2"),
    DENIED("3"),
    CANCELED("4");
    private String value;

    StatusDayOff(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public static StatusDayOff of(String value) throws Exception {
        if(WAITING.getValue().equals(value)){
            return WAITING;
        }
        if(ACCEPTED.getValue().equals(value)){
            return ACCEPTED;
        }
        if(DENIED.getValue().equals(value)){
            return DENIED;
        }
        if(CANCELED.getValue().equals(value)){
            return CANCELED;
        }
        throw new Exception ("Wrong value!");
    }
}
