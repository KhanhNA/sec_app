package com.ac.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "sharevan_bill_lading_detail")
public class BillLadingDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name_seq")
    private String nameSeq;
    @Column(name = "bill_Lading_Id")
    private Integer billLadingId;
    @Column(name = "total_Weight")
    private Double totalWeight;
    @Column(name = "warehouse_Id")
    private Integer warehouseId;
    @Column(name = "warehouse_Type")
    private String warehouseType;

    @Column(name = "from_Bill_Lading_Detail_Id")
    private Integer fromBillLadingDetailId;
    @Column(name = "description")
    private String description;

    @Column(name = "expected_From_Time")
    private java.sql.Timestamp expectedFromTime;
    @Column(name = "expected_To_Time")
    private java.sql.Timestamp expectedToTime;

    @Column(name = "status")
    private String status;
    @Column(name = "approved_Type")
    private String approvedType;

    @Column(name = "from_Warehouse_Id")
    private Integer fromWarehouseId;
    @Column(name = "status_Order")
    private String statusOrder;

    @Column(name = "name")
    private String name;

    @Column(name="last_scan")
    private LocalDateTime lastScan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameSeq() {
        return nameSeq;
    }

    public void setNameSeq(String nameSeq) {
        this.nameSeq = nameSeq;
    }

    public Integer getBillLadingId() {
        return billLadingId;
    }

    public void setBillLadingId(Integer billLadingId) {
        this.billLadingId = billLadingId;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseType() {
        return warehouseType;
    }

    public void setWarehouseType(String warehouseType) {
        this.warehouseType = warehouseType;
    }

    public Integer getFromBillLadingDetailId() {
        return fromBillLadingDetailId;
    }

    public void setFromBillLadingDetailId(Integer fromBillLadingDetailId) {
        this.fromBillLadingDetailId = fromBillLadingDetailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getExpectedFromTime() {
        return expectedFromTime;
    }

    public void setExpectedFromTime(Timestamp expectedFromTime) {
        this.expectedFromTime = expectedFromTime;
    }

    public Timestamp getExpectedToTime() {
        return expectedToTime;
    }

    public void setExpectedToTime(Timestamp expectedToTime) {
        this.expectedToTime = expectedToTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovedType() {
        return approvedType;
    }

    public void setApprovedType(String approvedType) {
        this.approvedType = approvedType;
    }

    public Integer getFromWarehouseId() {
        return fromWarehouseId;
    }

    public void setFromWarehouseId(Integer fromWarehouseId) {
        this.fromWarehouseId = fromWarehouseId;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getLastScan() {
        return lastScan;
    }

    public void setLastScan(LocalDateTime lastScan) {
        this.lastScan = lastScan;
    }
}
