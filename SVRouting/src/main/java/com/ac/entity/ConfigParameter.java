package com.ac.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name="ir_config_parameter")
@Data
@Getter
@Setter
public class ConfigParameter implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    @Column(name = "key")
    private String key;
    @Column(name = "value")
    private Float value;
    @Column(name = "create_Uid")
    private Integer createUid;

    @Column(name = "create_Date")
    private java.sql.Timestamp createDate;

    @Column(name = "write_Uid")
    private Integer writeUid;

    @Column(name = "write_Date")
    private java.sql.Timestamp writeDate;

}
