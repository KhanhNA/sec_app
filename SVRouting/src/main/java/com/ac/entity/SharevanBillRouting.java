package com.ac.entity;

import com.ac.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "sharevan_bill_routing")
public class SharevanBillRouting implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "code")
    private String code;
    @Column(name = "insurance_id")
    private Integer insuranceId;
    @Column(name = "total_weight")
    private Double totalWeight;
    @Column(name = "total_amount")
    private Double totalAmount;
    @Column(name = "tolls")
    private Double tolls;
    @Column(name = "surcharge")
    private Double surcharge;
    @Column(name = "total_volume")
    private Double totalVolume;
    @Column(name = "vat")
    private Double vat;
    @Column(name = "promotion_code")
    private Double promotionCode;
    @Column(name = "release_type")
    private Integer releaseType;
    @Column(name = "total_parcel")
    private Integer totalParcel;
    @Column(name = "company_id")
    private Integer companyId;
    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "end_date")
    private LocalDate endDate;
    @Column(name = "subscribe_id")
    private Integer subscribeId;
    @Column(name = "cycle_type")
    private String cycleType;
    @Column(name = "week_choose")
    private String weekChoose;
    @Column(name = "qr_code")
    private String qrCode;
    @Column(name = "sbl_type")
    private String sblType;
    @Column(name = "status_routing")
    private String statusRouting;
    @Column(name = "description")
    private String description;
    @Column(name = "name")
    private String name;
    @Column(name = "from_bill_lading_id")
    private Integer fromBillLadingId;
    @Column(name = "change_bill_lading_detail_id")
    private Integer changeBillLadingDetailId;
    @Column(name = "order_package_id")
    private Integer orderPackageId;
    @Column(name = "trouble_type")
    private String troubleType;
    @Column(name = "award_company_id")
    private Integer awardCompanyId;
    @Column(name = "create_Uid")
    private Integer createUid;

    @Column(name = "create_Date")
    private java.sql.Timestamp createDate;

    @Column(name = "write_Uid")
    private Integer writeUid;

    @Column(name = "write_Date")
    private java.sql.Timestamp writeDate;

    @Column(name = "price_actual")
    private Double priceActual;

    @Column(name = "routing_scan")
    private Boolean routingScan;

    @Column(name = "inzone_num")
    private Integer inzoneNum;

    @Column(name = "outzone_num")
    private Integer outzoneNum;

    @Column(name = "create_bol")
    private java.sql.Timestamp createBOL;

//    @Column(name = "chooseDay")
//    private String dayChoose;


    public SharevanBillRouting() {
    }


    public SharevanBillRouting(SharevanBillRouting other) {
       // this.id = other.id;
        this.code = other.code;
        this.insuranceId = other.insuranceId;
        this.totalWeight = other.totalWeight;
        this.totalAmount = other.totalAmount;
        this.tolls = other.tolls;
        this.surcharge = other.surcharge;
        this.totalVolume = other.totalVolume;
        this.vat = other.vat;
        this.promotionCode = other.promotionCode;
        this.releaseType = other.releaseType;
        this.totalParcel = other.totalParcel;
        this.companyId = other.companyId;
        this.startDate = other.startDate;
        this.endDate = other.endDate;
        this.subscribeId = other.subscribeId;
        this.cycleType = other.cycleType;
        this.weekChoose = other.weekChoose;
        this.qrCode = other.qrCode;
        this.sblType = other.sblType;
        this.statusRouting = other.statusRouting;
        this.description = other.description;
        this.name = other.name;
        this.fromBillLadingId = other.fromBillLadingId;
        this.changeBillLadingDetailId = other.changeBillLadingDetailId;
        this.orderPackageId = other.orderPackageId;
        this.troubleType = other.troubleType;
        this.awardCompanyId = other.awardCompanyId;
        this.createUid = other.createUid;
        this.createDate = Constants.getNowUTC();
        this.writeUid = other.writeUid;
        this.writeDate = Constants.getNowUTC();
        this.priceActual = other.priceActual;
        this.inzoneNum = other.inzoneNum;
        this.outzoneNum = other.outzoneNum;
    }
}
