package com.ac.enums;

public enum BiddingOrderStatus {
    NOT_CONFIRM ("0"),
    RECEIVED("1"),
    RETURNED("2");
    String value;

    public String getValue() {
        return value;
    }

    BiddingOrderStatus(String value) {
        this.value = value;
    }
    public static BiddingOrderStatus of(String v) throws Exception {
        if(NOT_CONFIRM.getValue().equals(v)){
            return NOT_CONFIRM;
        }
        else if(RECEIVED.getValue().equals(v)){
            return RECEIVED;
        }
        else if(RETURNED.getValue().equals(v)){
            return RETURNED;
        }
        else
            throw new Exception("Error");
    }
}
