package com.ac.enums;

public enum CargoPackingStatus {
    IN_ROUTING("0"),
    WAITING_PACKAGE("1"),
    PACKAGED("2"),
    UNPACKAGED("3");
    protected String value;

    CargoPackingStatus(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
    public static CargoPackingStatus of(String value) throws Exception {
        if(IN_ROUTING.getValue().equals(value)){
            return IN_ROUTING;
        }
        if(WAITING_PACKAGE.getValue().equals(value)){
            return WAITING_PACKAGE;
        }
        if(PACKAGED.getValue().equals(value)){
            return PACKAGED;
        }
        if(UNPACKAGED.getValue().equals(value)){
            return UNPACKAGED;
        }
        throw new Exception ("Wrong value!");
    }
}
