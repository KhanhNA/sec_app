package com.ac.model;

import com.ac.entity.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoutingDetail {
    private RoutingPlanDay fromRoutingPlanDay;
    private List<SharevanBillPackageRoutingPlan>  billPackageRoutingPlan;
    private List<SharevanBillPackageRoutingImport> billPackageRoutingImport;
    private List<SharevanBillPackageRoutingExport> billPackageRoutingExport;
    private SharevanBillRouting billRouting;
    private List<SharevanRoutingPlanDayService> routingPlanDayService;

}
