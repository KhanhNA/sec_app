package com.ac.model;

import com.ac.entity.bidding_entity.SharevanBiddingPackage;

import java.util.ArrayList;

public class SocketResult {
    private String actionType;
    private ArrayList<SharevanBiddingPackage> lstBiddingPackages;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public ArrayList<SharevanBiddingPackage> getLstBiddingPackages() {
        return lstBiddingPackages;
    }

    public void setLstBiddingPackages(ArrayList<SharevanBiddingPackage> lstBiddingPackages) {
        this.lstBiddingPackages = lstBiddingPackages;
    }
}
