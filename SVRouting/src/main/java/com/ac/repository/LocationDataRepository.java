package com.ac.repository;

import com.ac.entity.LocationData;
import com.ac.entity.bidding_entity.SharevanBiddingPackage;
import com.ac.repository.custom.SharevanBiddingPackageRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface LocationDataRepository extends JpaRepository<LocationData, Integer>, JpaSpecificationExecutor<LocationData> {
    @Override
    List<LocationData> findAll();

    @Override
    <S extends LocationData> S save(S s);
}
