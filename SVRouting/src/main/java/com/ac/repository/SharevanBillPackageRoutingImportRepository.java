package com.ac.repository;

import com.ac.entity.SharevanBillPackageRoutingImport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanBillPackageRoutingImportRepository extends JpaRepository<SharevanBillPackageRoutingImport, Integer>, JpaSpecificationExecutor<SharevanBillPackageRoutingImport> {

}