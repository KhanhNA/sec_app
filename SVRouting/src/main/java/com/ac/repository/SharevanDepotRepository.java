package com.ac.repository;

import com.ac.entity.bidding_entity.SharevanDepot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanDepotRepository extends JpaRepository<SharevanDepot, Integer>, JpaSpecificationExecutor<SharevanDepot> {

}