package com.ac.service;

import com.ac.entity.RoutingPlanDay;
import com.ac.entity.SharevanBillPackageRoutingImport;
import com.ac.entity.SharevanBillPackageRoutingPlan;
import com.ac.model.RoutingDetail;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Service
public class BillPackageRoutingImportService {
    @PersistenceContext
    protected EntityManager entityManager;

    public List<SharevanBillPackageRoutingImport> getListBillRoutingImportByLstRoutingId(Integer[] ids){
        String sql = "select routing from SharevanBillPackageRoutingImport routing where routing.routingPlanDayId in " ;
        String idInput = "(";
        for(int i =0; i < ids.length; i++){
            idInput += ":id" + i + ",";
        }
        idInput = idInput.substring(0,idInput.length()-1) + ")";
        sql += idInput + " order by routing.routingPlanDayId";;

        Query query = entityManager.createQuery(sql,SharevanBillPackageRoutingImport.class);
        for(int i =0; i < ids.length; i++){
            query.setParameter("id" + i,ids[i]);
        }
        List<SharevanBillPackageRoutingImport> result = query.getResultList();

        return result;
    }
    public HashMap<RoutingPlanDay, RoutingDetail> getMapBillPackageRoutingImport(Integer[] ids, List<RoutingPlanDay> lstClaim,HashMap<RoutingPlanDay, RoutingDetail> mapDetailClaim){
        List<SharevanBillPackageRoutingImport> result = getListBillRoutingImportByLstRoutingId(ids);
       // HashMap<RoutingPlanDay,SharevanBillPackageRoutingImport> mapPackageImport = new HashMap<>();
        if(result != null && !result.isEmpty()){
            for(RoutingPlanDay routing: lstClaim){
                List<SharevanBillPackageRoutingImport> lstNewImport = new ArrayList<>();
                for(SharevanBillPackageRoutingImport plan : result){
                    if(routing.getId().equals(plan.getRoutingPlanDayId())){
                        lstNewImport.add(plan);
//                        RoutingDetail routingDetail = mapDetailClaim.get(routing);
//                        routingDetail.setBillPackageRoutingImport(plan);
//                        break;
                    }
                }
                RoutingDetail routingDetail = mapDetailClaim.get(routing);
                routingDetail.setBillPackageRoutingImport(lstNewImport);
            }
        }
        return mapDetailClaim;
    }
}
