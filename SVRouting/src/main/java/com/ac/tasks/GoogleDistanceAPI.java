package com.ac.tasks;

import com.ac.entity.LocationData;
import com.ac.entity.WarehouseLog;
import com.ac.model.*;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class GoogleDistanceAPI {


    public static final String API_GOOGLE_MAP_DESTINATION = "AIzaSyCmzEKuZOtAuDR5-iHmJvLScbSolUJEhBk";
    private static RestTemplate restTemplate = null;
//    @PersistenceContext
//    private static EntityManager entityManager;

    private static void initRest() {

        if (restTemplate != null) {
            return;
        }
        restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);

    }

    public static LocationData getDistance(double fromLat, double fromLng, double toLat, double toLng) {

        return getDistance(true, fromLat, fromLng,
                toLat, toLng);
    }

    public static LocationData getDistance(Boolean shoudBeStandard, Double fLatitude, Double fLongitude, Double tLatitude, Double tLongitude) {

        fLatitude = shoudBeStandard ? LocationData.standardLocation(fLatitude) : fLatitude;
        fLongitude = shoudBeStandard ? LocationData.standardLocation(fLongitude) : fLongitude;
        tLatitude = shoudBeStandard ? LocationData.standardLocation(tLatitude) : tLatitude;
        tLongitude = shoudBeStandard ? LocationData.standardLocation(tLongitude) : tLongitude;

        final String requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
                "mode=driving&"
                + "transit_routing_preference=less_driving&"
                + "origin=" + fLatitude + "," + fLongitude + "&"
                + "destination=" + tLatitude + "," + tLongitude + "&"
                + "key=" + API_GOOGLE_MAP_DESTINATION;
        initRest();
        GoogleDistance result = restTemplate.getForObject(requestUrl, GoogleDistance.class);
        LocationData locationData = getDistanceInfo(result);
        DistanceInfo<Double, Double> distanceInfo = locationData.getDistanceInfo();
        System.out.println(result);
        locationData.setMinutes(distanceInfo.getMinute());
        locationData.setCost(distanceInfo.getMinute());
        locationData = new LocationData(fLatitude, fLongitude, tLatitude, tLongitude,
                distanceInfo.getMinute(), distanceInfo.getCost(), locationData.getStartAddress(), locationData.getEndAddress());

        //  locationData =  entityManager.merge(locationData);
        return locationData;
    }

    public static LocationData getDistanceInfo(GoogleDistance googleDistance) {
        Double minutes = 0d, cost = 0d;
        if (googleDistance == null || googleDistance.getRoutes() == null) {
            return null;
        }
        String startAddress = "", endAddress = "";
        for (Route route : googleDistance.getRoutes()) {
            for (Leg leg : route.getLegs()) {
                minutes += leg.getDuration().getValue() / 60;
                cost += leg.getDistance().getValue();
                startAddress = leg.getStart_address();
                endAddress = leg.getEnd_address();
            }
        }
        LocationData data = new LocationData();
        data.setDistanceInfo(DistanceInfo.of(minutes, cost));
        data.setStartAddress(startAddress);
        data.setEndAddress(endAddress);
        return data;
    }

    public static List<LocationData> getListDistanceInfo(GoogleDistance googleDistance, List<LatLng> latLngList) {
        if (googleDistance == null || googleDistance.getRoutes() == null) {
            return null;
        }
        List<LocationData> dataList = new ArrayList<>();
        String startAddress = "", endAddress = "";
        if (latLngList.size() != googleDistance.getRoutes().get(0).getLegs().size() + 1) {
            System.out.println("Not macth between latlng list and step");
        }
        for (Route route : googleDistance.getRoutes()) {
            for (int i = 0; i < route.getLegs().size(); i++) {
                startAddress = route.getLegs().get(i).getStart_address();
                endAddress = route.getLegs().get(i).getEnd_address();
                LocationData data = new LocationData();
                data.setDistanceInfo(DistanceInfo.of(route.getLegs().get(i).getDuration().getValue(), route.getLegs().get(i).getDistance().getValue()));
                data.setStartAddress(startAddress);
                data.setEndAddress(endAddress);
                data.setFromLatitude(LocationData.standardLocation(latLngList.get(i).getLat()));
                data.setFromLongtitude(LocationData.standardLocation(latLngList.get(i).getLng()));
                data.setToLatitude(LocationData.standardLocation(latLngList.get(i+1).getLat()));
                data.setToLongitude(LocationData.standardLocation(latLngList.get(+1).getLng()));
                data.setMinutes(route.getLegs().get(i).getDuration().getValue());
                data.setCost(route.getLegs().get(i).getDistance().getValue());
                dataList.add(data);
            }
        }

        return dataList;
    }

    public static List<LocationData> getListDistance(List<LatLng> lstUpdate) {
        LatLng origin = null;
        LatLng destination = null;
        List<LatLng> listWaypoints = new ArrayList<>();
        List<LatLng> dataWaypoint = new ArrayList<>();
        for (LatLng location : lstUpdate) {
            if (origin == null) {
                origin = location;
                dataWaypoint.add(location);
            } else {
                listWaypoints.add(location);
            }
        }

        destination = listWaypoints.get(listWaypoints.size() - 1);
        listWaypoints.remove(listWaypoints.size() - 1);
        StringBuilder waypoint = new StringBuilder("waypoints=");
        boolean check_waypoint = false;
        if (listWaypoints.size() > 0) {
            LatLng lastOne = null;
            for (LatLng point : listWaypoints) {
                if (lastOne != null) {
                    if (lastOne.getLat() != point.getLat() && lastOne.getLng() != point.getLng()) {
                        waypoint.append(point.getLat()).append(",").append(point.getLng()).append("|");
                        lastOne = point;
                        dataWaypoint.add(point);
                    }
                } else {
                    waypoint.append(point.getLat()).append(",").append(point.getLng()).append("|");
                    dataWaypoint.add(point);
                    lastOne = point;
                }
                check_waypoint = true;
            }
        }
        dataWaypoint.add(destination);
        String requestUrl = null;
        if (check_waypoint) {
            String way = waypoint.toString();
            way = way.substring(0, way.length() - 1);
            requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
                    "mode=driving&"
                    + "transit_routing_preference=less_driving&"
                    + "origin=" + origin.getLat() + "," + origin.getLng() + "&"
                    + "destination=" + destination.getLat() + "," + destination.getLng() + "&"
                    + way + "&"
                    + "key=" + API_GOOGLE_MAP_DESTINATION;
        } else {
            requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
                    "mode=driving&"
                    + "transit_routing_preference=less_driving&"
                    + "origin=" + origin.getLat() + "," + origin.getLng() + "&"
                    + "destination=" + destination.getLat() + "," + destination.getLng()+ "&"
                    + "key=" + API_GOOGLE_MAP_DESTINATION;
        }
        initRest();
        GoogleDistance result = restTemplate.getForObject(requestUrl, GoogleDistance.class);
        return getListDistanceInfo(result, dataWaypoint);
    }
//    public static GoogleCheckResult getGoogleCheckResult(List<LatLng> lstUpdate, List<WarehouseLog> listExtra) {
//        LatLng origin = null;
//        LatLng destination = null;
//        List<LatLng> listWaypoints = new ArrayList<>();
//        List<LatLng> dataWaypoint = new ArrayList<>();
//        List<WarehouseLog> listSuccess = new ArrayList<>();
//        for (int i = 0;i< lstUpdate.size();i++){
//            if (origin == null) {
//                origin = lstUpdate.get(i);
//                listWaypoints.add(lstUpdate.get(i));
//            } else {
//                listWaypoints.add(lstUpdate.get(i));
//            }
//        }
//        GoogleCheckResult googleCheckResult = new GoogleCheckResult();
//        destination = listWaypoints.get(listWaypoints.size() - 1);
//        listWaypoints.remove(listWaypoints.size() - 1);
//        StringBuilder waypoint = new StringBuilder("waypoints=");
//        boolean check_waypoint = false;
//        if (listWaypoints.size() > 0) {
//            LatLng lastOne = null;
//            for( int i = 0; i<listWaypoints.size();i++){
////            for (LatLng point : listWaypoints) {
//                if (lastOne != null) {
//                    if (lastOne.lat != listWaypoints.get(i).lat && lastOne.lng != listWaypoints.get(i).lng) {
//                        waypoint.append(listWaypoints.get(i).lat).append(",").append(listWaypoints.get(i).lng).append("|");
//                        lastOne = listWaypoints.get(i);
//                        dataWaypoint.add(listWaypoints.get(i));
//                    }
//                } else {
//                    waypoint.append(listWaypoints.get(i).lat).append(",").append(listWaypoints.get(i).lng).append("|");
//                    dataWaypoint.add(listWaypoints.get(i));
//                    lastOne = listWaypoints.get(i);
//                }
//                check_waypoint = true;
//            }
//            if(dataWaypoint.size()<7){
//                for( WarehouseLog log : listExtra){
//                    if (listWaypoints.size()==8||listWaypoints.size()==7){
//                        break;
//                    }
//                    if(lastOne.lat!= log.getFromLatitude() &&lastOne.lng!= log.getFromLongitude() ){
//                        waypoint.append(log.getFromLatitude()).append(",").append(log.getFromLongitude()).append("|");
//                        lastOne = new LatLng(log.getFromLatitude(),log.getFromLongitude());
//                        dataWaypoint.add(lastOne);
//                        waypoint.append(log.getToLatitude()).append(",").append(log.getToLongitude()).append("|");
//                        lastOne = new LatLng(log.getToLatitude(),log.getToLongitude());
//                        dataWaypoint.add(lastOne);
//                        listSuccess.add(log);
//                    }
//                }
//            }
//        }
//        dataWaypoint.add(destination);
//        String requestUrl = null;
//        if (check_waypoint) {
//            String way = waypoint.toString();
//            way = way.substring(0, way.length() - 1);
//            requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
//                    "mode=driving&"
//                    + "transit_routing_preference=less_driving&"
//                    + "origin=" + origin.lat + "," + origin.lng + "&"
//                    + "destination=" + destination.lat + "," + destination.lng + "&"
//                    + way + "&"
//                    + "key=" + API_GOOGLE_MAP_DESTINATION;
//        } else {
//            requestUrl = "https://maps.googleapis.com/maps/api/directions/json?" +
//                    "mode=driving&"
//                    + "transit_routing_preference=less_driving&"
//                    + "origin=" + origin.lat + "," + origin.lng + "&"
//                    + "destination=" + destination.lat + "," + destination.lng + "&"
//                    + "key=" + API_GOOGLE_MAP_DESTINATION;
//        }
//        initRest();
//        GoogleDistance result = restTemplate.getForObject(requestUrl, GoogleDistance.class);
//        googleCheckResult.setLocationData(getListDistanceInfo(result, dataWaypoint));
//        googleCheckResult.setSuccessList(listSuccess);
//        return googleCheckResult;
//    }


}

