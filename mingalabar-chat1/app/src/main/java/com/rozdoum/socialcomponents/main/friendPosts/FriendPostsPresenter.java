/*
 * Copyright 2018 Rozdoum
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.rozdoum.socialcomponents.main.friendPosts;

import android.content.Context;
import android.view.View;

import com.rozdoum.socialcomponents.main.base.BasePresenter;
import com.rozdoum.socialcomponents.managers.PostManager;
import com.rozdoum.socialcomponents.managers.listeners.OnDataChangedListener;
import com.rozdoum.socialcomponents.managers.listeners.OnDataChangedListenerV2;
import com.rozdoum.socialcomponents.managers.listeners.OnPostChangedListener;
import com.rozdoum.socialcomponents.model.FollowingPost;
import com.rozdoum.socialcomponents.model.Post;

import java.util.List;

import in.tvac.akshaye.lapitchat.R;

/**
 * Created by Alexey on 03.05.18.
 */

class FriendPostsPresenter extends BasePresenter<FriendPostsView> {

    private PostManager postManager;


    FriendPostsPresenter(Context context) {
        super(context);
        postManager = PostManager.getInstance(context);
    }

    void onPostClicked(final String postId, final View postView) {
        postManager.isPostExistSingleValue(postId, exist -> ifViewAttached(view -> {
            if (exist) {
                view.openPostDetailsActivity(postId, postView);
            } else {
                view.showSnackBar(R.string.error_post_was_removed);
            }
        }));
    }
    private void loadPosts(FriendPostsView view, List<FollowingPost> list){
        OnDataChangedListenerV2<Post> onPostsDataChangedListener = new OnDataChangedListenerV2<Post>() {
            @Override
            public void onListChanged(List<Post> list) {
                view.onFollowingPostsLoaded(list);
            }

            @Override
            public void onAdd(Post element) {
                view.onFollowingPostsAdd(element);
            }

            @Override
            public void onRemove(Post element) {
                view.onFollowingPostsRemoved(element);
            }

            @Override
            public void onChange(Post element) {
                view.onFollowingPostsChange(element);
            }
        };
        postManager.getPostsListByFollows(onPostsDataChangedListener, list);
    }
    public void loadFollowingPosts() {
        if (checkInternetConnection()) {
            if (getCurrentUserId() != null) {
                ifViewAttached(FriendPostsView::showLocalProgress);
                postManager.getFollowingPosts(getCurrentUserId(), list -> ifViewAttached(view -> {
                    view.hideLocalProgress();
                    loadPosts(view, list);

                    view.showEmptyListMessage(list.isEmpty());
                }));
            } else {
                ifViewAttached(view -> {
                    view.showEmptyListMessage(true);
                    view.hideLocalProgress();
                });
            }
        } else {
            ifViewAttached(FriendPostsView::hideLocalProgress);
        }
    }

    public void onRefresh() {
        loadFollowingPosts();
    }

    public void onAuthorClick(String postId, View authorView) {
        postManager.getSinglePostValue(postId, new OnPostChangedListener() {
            @Override
            public void onObjectChanged(Post obj) {
                ifViewAttached(view -> view.openProfileActivity(obj.getAuthorId(), authorView));
            }

            @Override
            public void onError(String errorText) {
                ifViewAttached(view -> view.showSnackBar(errorText));
            }
        });
    }
}
