package com.nextsolution.db.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolution.db.dto.IModel;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.network.SOService;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.adapter.HaiSer;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.model.OdooSessionDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ns.odoolib_retrofit.wrapper.OArguments;
import com.ns.odoolib_retrofit.wrapper.OdooClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseApi {
    protected static final String METHOD_CREATE = "create";
    protected static final String METHOD_UPDATE = "write";
    protected static OdooClient mOdoo;
    protected static Gson mGson = new GsonBuilder()
            .registerTypeAdapter(OdooRelType.class, new HaiSer<>())
            .registerTypeAdapter(OdooDate.class, Adapter.DATE)
            .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
            .create();

    public static void setOdoo(OdooClient odoo) {
        mOdoo = odoo;
    }

    public static void requestLogin(String username, String password, IOdooResponse<OdooSessionDto> result) {

        mOdoo.authenticate(username, password, AppController.DATABASE, result);

    }

    public static void createRecord(IModel obj, IResponse response) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        callMethod(obj.getModelName(), METHOD_CREATE, null, obj, map, response);
    }

    public static void updateRecord(List<Integer> ids, IModel obj, IResponse response) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        callMethod(obj.getModelName(), METHOD_UPDATE, ids, obj, map, response);
    }

    protected static void callMethod(String model, String method, List<Integer> ids, IModel obj, HashMap kwargs, IResponse response) throws Exception {
        OArguments args = new OArguments();

        if(obj != null){
            JSONObject jsonObject = new JSONObject(mGson.toJson(obj));
            args.add(jsonObject);
        }

        if (ids != null) {
            args.add(new JSONArray(ids.toString()));
        }

        mOdoo.callMethod(model, method, args, kwargs, null, new SharingOdooResponse2() {
            @Override
            public void onSuccess(Object result) {
                response.onSuccess(result);
            }

            @Override
            public void onFail(Throwable error) {

            }
        }.getResponse(obj.getClass()));

    }
    public static void logout() {

        SOService soService = mOdoo.getClient().createService(SOService.class);

        Call<ResponseBody> call = soService.logOut(mOdoo.getSessionCokie(), StaticData.getOdooSessionDto().getUid() + "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });

    }
}
