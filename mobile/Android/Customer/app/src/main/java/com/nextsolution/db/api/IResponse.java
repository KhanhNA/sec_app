package com.nextsolution.db.api;


public interface IResponse<Result>{
    void onSuccess(Result result);
    void onFail(Throwable error);

}
