package com.nextsolution.db.api;

import com.nextsolution.db.dto.Insurance;
import com.nextsolution.vancustomer.enums.StatusType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class InsuranceApi extends BaseApi{
    private static final String INSURANCE_MODEL = "sharevan.insurance";
    private static final String ROUTE_LIST_ACTIVE = "/share_van_order/insurance/list_active";

    public static void getInsurance(boolean status, IResponse result) {
//        ODomain domain = new ODomain();
//        domain.add("status_insurance", "=", status ? "active": "inactive");
//
//        mOdoo.searchRead(INSURANCE_MODEL, null, domain, 0, 0, "", new SharingOdooResponse<OdooResultDto<Insurance>>() {
//            @Override
//            public void onSuccess(OdooResultDto<Insurance> o) {
//                result.onSuccess(o);
//            }
//        });
        JSONObject params;
        String status_service = StatusType.RUNNING;
        try {
            params = new JSONObject();
            params.put("status",status_service);
            mOdoo.callRoute(ROUTE_LIST_ACTIVE, params, new SharingOdooResponse<OdooResultDto<Insurance>>() {
                @Override
                public void onSuccess(OdooResultDto<Insurance> insurances) {
                    result.onSuccess(insurances);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

}
