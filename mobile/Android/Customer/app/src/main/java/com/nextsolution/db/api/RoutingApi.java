package com.nextsolution.db.api;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.db.dto.Distance;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.DriverRating;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.db.dto.ServiceArisingDTO;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.network.DistanceService;
import com.nextsolution.vancustomer.network.SOBillingService;
import com.nextsolution.vancustomer.network.SOService;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.wrapper.BaseClient;
import com.ns.odoolib_retrofit.wrapper.ODomain;

import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutingApi extends BaseApi {
    private static final String SHARE_VAN_ROUTING_PLAN_DAY = "/share_van_order/routing_plan_day_by_employeeid";
    private static final String GET_SERVICE_ARISING = "/routing_plan_day/sos";
    private static final String GET_DISTANCE_BY_LAT_AND_LONG = "/routing_plan_day/check_driver_waiting_time";

    public static void GetServiceArising(Integer routing_plan_day_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_id", routing_plan_day_id);

            mOdoo.callRoute(GET_SERVICE_ARISING, params, new SharingOdooResponse<OdooResultDto<ServiceArisingDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<ServiceArisingDTO> sosTypeOdooResultDto) {
                    result.onSuccess(sosTypeOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getDistanceByLatAndLong(LatLng startPosition, LatLng endPosition, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("from_latitude", mGson.toJson(startPosition.latitude));
            params.put("from_longitude", mGson.toJson(startPosition.longitude));
            params.put("to_latitude", mGson.toJson(endPosition.latitude));
            params.put("to_longitude", mGson.toJson(endPosition.longitude));

            mOdoo.callRoute(GET_DISTANCE_BY_LAT_AND_LONG, params, new SharingOdooResponse<DistanceDTO>() {
                @Override
                public void onSuccess(DistanceDTO distanceDTO) {
                    result.onSuccess(distanceDTO);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getDistance(Context context, LatLng startPosition, LatLng endPosition, IResponse result) {
        try{
            BaseClient<DistanceService> distanceService = new BaseClient(context, AppController.DISTANCE_URL, DistanceService.class);
            DistanceService call = distanceService.createService(DistanceService.class);
            Call<DistanceDTO> distanceDTOCall = call.getDistance(startPosition.latitude, startPosition.longitude, endPosition.latitude, endPosition.longitude);
            distanceDTOCall.enqueue(new Callback<DistanceDTO>() {
                @Override
                public void onResponse(Call<DistanceDTO> call, Response<DistanceDTO> response) {
                    DistanceDTO distanceDTO= response.body();
                    result.onSuccess(distanceDTO);
                }

                @Override
                public void onFailure(Call<DistanceDTO> call, Throwable t) {
                    result.onSuccess(null);
                }
            });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public static void getRoutingPlanDay(Integer vanId, String date, IResponse result) {
        ODomain domain = new ODomain();
        domain.add("date_plan", "=", date);
        domain.add("vehicle_id", "=", vanId);
        mOdoo.searchRead("sharevan.routing.plan.day", null, domain, 0, 0, "", responseInstance(result));
    }

    public static void getRoutingPlanDayById(Integer id, IResponse result) {
        ODomain domain = new ODomain();
        domain.add("id", "=", id);
        mOdoo.searchRead("sharevan.routing.plan.day", null, domain, 0, 0, "", responseInstance(result));
    }

    public static void getRoutingDetail(Integer id, IResponse result, Object... params) {

    }

    public static void updateVanForNewRoute(Integer routeId, Integer vanId, IResponse result, Object... params) {

    }

    public static void pickup(Integer routeId, IResponse result, Object... params) {
//        PickupMutation mutation = PickupMutation.builder().routingPlanDayId(routeId).build();
//        AWSClientBase.apolloMutation(mutation, result, params);
    }

    public static void getRoutingPlanDayByCustomerId(Long id, String date, IResponse result) {
//        GetRoutingPlanDayByCustomerIdQuery query = GetRoutingPlanDayByCustomerIdQuery.builder().customerId(id).date(date).build();
//        AWSClientBase.apolloQuery(query, result, params);
        ODomain domain = new ODomain();
        domain.add("id", "=", id);
        mOdoo.searchRead("sharevan.routing.plan.day", null, domain, 0, 0, "", responseInstance(result));
    }

    private static SharingOdooResponse responseInstance(IResponse result) {
        return new SharingOdooResponse<List<RoutingVanDay>>() {
            @Override
            public void onSuccess(List<RoutingVanDay> o) {
                result.onSuccess(o);
            }

            @Override
            public void onFail(Throwable error) {

            }
        };
    }

    public static void getRoutingDay(Date currentDate, Integer uID, String orderCode, Date fDate, Date tDate, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("currentDate", MyDateUtils.convertDateToString(currentDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("uID", 1);
            params.put("orderCode", "00");
            params.put("fromDate", MyDateUtils.convertDateToString(fDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("toDate", MyDateUtils.convertDateToString(tDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));


            mOdoo.callRoute(SHARE_VAN_ROUTING_PLAN_DAY, params, new SharingOdooResponse<OdooResultDto<RoutingDay>>() {
                @Override
                public void onSuccess(OdooResultDto<RoutingDay> odooResultDto) {
                    result.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void confirmRouting(String description, RoutingDay routingDay, List<LocalMedia> lstFileSelected, IResponse result) {

        SOService soService = mOdoo.getClient().createService(SOService.class);

        Integer routingId = routingDay.getId();
        MultipartBody.Part[] parts = null;
        String json = "{\"id\":" + routingId
                + ",\"routing_plan_day_code\":\"" + routingDay.getRouting_plan_day_code()
                + "\", \"status\": \"2\""
                + ", \"so_type\": " + routingDay.getSo_type()
                + " , \"description\":\"" + description + "\"}";
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);
        if (lstFileSelected != null) {
            parts = new MultipartBody.Part[lstFileSelected.size()];
            for (int i = 0; i < lstFileSelected.size(); i++) {
                File file = new File(lstFileSelected.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("ufile", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }

        Call<ResponseBody> call = soService.confirmRouting(mOdoo.getSessionCokie(), bodyJson, parts);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onSuccess(null);
            }
        });
    }

    public static void getBillLadingByDate(String date, int page, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("date", date);
            params.put("offset", page);
            params.put("limit", "10");

            mOdoo.callRoute("/share_van_order/get_bill_routing_by_day", params, new SharingOdooResponse<OdooResultDto<BillRouting>>() {
                @Override
                public void onSuccess(OdooResultDto<BillRouting> odooResultDto) {
                    result.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getBillRoutingDetail(int bill_routing_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("bill_routing_id", bill_routing_id);

            mOdoo.callRoute("/share_van_order/get_bill_routing_detail", params, new SharingOdooResponse<BillRouting>() {
                @Override
                public void onSuccess(BillRouting billLading) {
                    result.onSuccess(billLading);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }


}
