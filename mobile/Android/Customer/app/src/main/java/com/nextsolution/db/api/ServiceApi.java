package com.nextsolution.db.api;

import com.nextsolution.db.dto.BillService;
import com.nextsolution.vancustomer.enums.StatusType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class ServiceApi extends BaseApi{
    private static final String SERVICE_MODEL = "sharevan.service.type";
    private static final String ROUTE_LIST_ACTIVE = "/share_van_order/service/list_active";
    public static void getService(boolean status, IResponse result) {
//        ODomain domain = new ODomain();
//        domain.add("status_service", "=", status ? "active": "inactive");
//
//        mOdoo.searchRead(SERVICE_MODEL, null, domain, 0, 0, "", new SharingOdooResponse<OdooResultDto<BillService>>() {
//            @Override
//            public void onSuccess(OdooResultDto<BillService> o) {
//                result.onSuccess(o);
//            }
//        });
        JSONObject params;
        String status_service = StatusType.RUNNING;
        try {
            params = new JSONObject();
            params.put("status",status_service);
            mOdoo.callRoute(ROUTE_LIST_ACTIVE, params, new SharingOdooResponse<OdooResultDto<BillService>>() {
                @Override
                public void onSuccess(OdooResultDto<BillService> billServices) {
                    result.onSuccess(billServices);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }
}
