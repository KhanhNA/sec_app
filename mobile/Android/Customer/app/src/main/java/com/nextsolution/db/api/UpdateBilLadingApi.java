package com.nextsolution.db.api;

import com.nextsolution.db.dto.BillLading;
import com.nextsolution.vancustomer.util.TsUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class UpdateBilLadingApi extends  BaseApi{
    public static void getBillUpdate(String bill_lading_detail_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("bill_lading_detail_id", bill_lading_detail_id);

            mOdoo.callRoute("/share_van_order/get_bill_lading_update", params, new SharingOdooResponse<OdooResultDto<BillLading>>() {
                @Override
                public void onSuccess(OdooResultDto<BillLading> obj) {
                    if(obj != null && TsUtils.isNotNull(obj.getRecords())) {
                        result.onSuccess(obj.getRecords().get(0));
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }


    }
}
