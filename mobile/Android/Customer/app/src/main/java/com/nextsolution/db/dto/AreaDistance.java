package com.nextsolution.db.dto;

import com.google.android.gms.maps.model.LatLng;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AreaDistance extends BaseModel {
    private LatLng fromLocation;
    private LatLng toLocation;
    private String from_name_seq;
    private String to_name_seq;
    private String from_warehouse_name;
    private String to_warehouse_name;
    /**
     * type
     * 1.kho -> hub
     * 2.hub -> hub
     * 3.hub -> depot
     * 4.depot -> depot
     * 5.depot -> hub
     * 6.hub -> kho
     */
    private Integer type;
    private Double distance;//đơn vị m
    private Double duration;//đơn vị s
    private Double price;
}
