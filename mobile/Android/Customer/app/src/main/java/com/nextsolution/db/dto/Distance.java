package com.nextsolution.db.dto;

public class Distance {
    public String text;
    public double value;

    public Distance(String text, double value) {
        this.text = text;
        this.value = value;
    }
}
