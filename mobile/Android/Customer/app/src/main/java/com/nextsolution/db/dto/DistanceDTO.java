package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.tsolution.base.BaseModel;

import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistanceDTO extends BaseModel {
    private Long id;
    private String code;
    private Double fromLatitude;
    private Double fromLongtitude;
    private Double toLatitude;
    private Double toLongitude;

    private Double minutes;
    private Double cost;//quãng đường.
    private String startAddress;
    private String endAddress;
    private Double distanceInfo;

}
