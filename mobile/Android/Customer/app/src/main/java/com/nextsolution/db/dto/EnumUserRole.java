package com.nextsolution.db.dto;

public class EnumUserRole {
    public static String CUSTOMER_MANAGER = "CUSTOMER_MANAGER";
    public static String CUSTOMER_STOCKMAN = "CUSTOMER_STOCKMAN";
    public static String FLEET_MANAGER = "FLEET_MANAGER";
    public static String FLEET_DRIVER = "FLEET_DRIVER";
    public static String SHAREVAN_MANAGER = "SHAREVAN_MANAGER";
    public static String SHAREVAN_STOCKMAN = "SHAREVAN_STOCKMAN";
}
