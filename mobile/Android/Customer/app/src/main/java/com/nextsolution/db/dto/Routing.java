package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Routing extends BaseModel {
    private Integer id;
    private String status;
    private Integer driver_id;
    private Integer vehicle_id;
    private Float latitude;
    private Float longitude;
    private String warehouse_name;
    private Integer from_routing_plan_day_id;
    private String ship_type;
    private String driver_name;
    private String driver_phone;
    private String license_plate;
    private String vehicle_name;
    private OdooDateTime accept_time;
    private String type;// nhập hay xuất.


    public String getAccept_time_str(){
        if(accept_time != null){
            return AppController.formatDateTime.format(accept_time);
        }
        return  null;
    }

}
