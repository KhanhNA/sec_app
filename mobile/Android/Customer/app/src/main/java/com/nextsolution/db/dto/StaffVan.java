package com.nextsolution.db.dto;

@lombok.Getter
@lombok.Setter
public class StaffVan  {
  private String __typename;
  private Fleet fleet;
  private Staff staff;
  private Van van;

  
}

