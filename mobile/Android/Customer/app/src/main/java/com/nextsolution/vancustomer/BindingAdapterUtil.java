package com.nextsolution.vancustomer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Build;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputLayout;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.db.dto.BillRoutingDetail;
import com.nextsolution.db.dto.BillService;
import com.nextsolution.db.dto.Cargo;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.db.dto.EnumUserRole;
import com.nextsolution.db.dto.Routing;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.adapter.ImageBaseAdapter;
import com.nextsolution.vancustomer.confirm_change.ConfirmChangeVM;
import com.nextsolution.vancustomer.customer.AutoCompleteAdapter;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.databinding.LayoutRoutBinding;
import com.nextsolution.vancustomer.databinding.LayoutWarehouseAddressBinding;
import com.nextsolution.vancustomer.enums.BillRoutingStatus;
import com.nextsolution.vancustomer.model.OrderPackage;
import com.nextsolution.vancustomer.util.BillLadingHistoryStatus;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.nextsolution.vancustomer.enums.StatusType;
import com.nextsolution.vancustomer.util.RoutingDayStatus;
import com.nextsolution.vancustomer.util.RoutingDayType;
import com.nextsolution.vancustomer.util.StringUtils;
import com.nextsolution.vancustomer.util.TsUtils;
import com.nextsolution.vancustomer.widget.NonSwipeableViewPager;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BindingAdapterUtil {
    @BindingAdapter("animation")
    public static void setIsBusy(View view, Animation animation) {
        view.startAnimation(animation);
    }

    @BindingAdapter("biddingOrderStatus")
    public static void biddingOrderStatus(TextView textView, String status) {
        Resources res = textView.getResources();
        switch (status) {
            case "0":
                textView.setText(res.getString(R.string.Have_not_received));
                textView.setTextColor(res.getColor(R.color.color_normal_text));
                break;
            case "1":
                textView.setText(res.getString(R.string.delivering));
                textView.setTextColor(res.getColor(R.color.color_pending));
                break;
            case "2":
                textView.setText(res.getString(R.string.complete));
                textView.setTextColor(res.getColor(R.color.primaryColor));
                break;
        }
    }
    @BindingAdapter("bill_lading_status")
    public static void bill_lading_status(TextView textView, String status) {
        Resources res = textView.getResources();
        if(status!=null){
            if(BillLadingHistoryStatus.all.toString().equals(status)){
                textView.setText(res.getString(R.string.ALL));
            }
            else if(BillLadingHistoryStatus.running.toString().equals(status)){
                textView.setText(res.getString(R.string.Running));
            }
            else if(BillLadingHistoryStatus.finished.toString().equals(status)){
                textView.setText(res.getString(R.string.Finished));
            }
            else if(BillLadingHistoryStatus.deleted.toString().equals(status)){
                textView.setText(res.getString(R.string.Deleted));
            }
        }
    }

    @BindingAdapter("getWarehouseName")
    public static void getWarehouseName(TextView textView, Warehouse warehouse) {
        if (warehouse != null) {
            textView.setText(warehouse.getName());
        }
    }
    @BindingAdapter("setSwipeable")
    public static void setSwipeable(NonSwipeableViewPager view, boolean aBoolean) {
        view.setSwipeable(aBoolean);
    }

    @BindingAdapter("getWarehouseAddress")
    public static void getWarehouseAddress(TextView textView, Warehouse warehouse) {
        if (warehouse != null) {
            textView.setText(warehouse.getAdministrative());
        }
    }

    /**
     * type 0: xuất hàng - depotId = fromDepotId, 1: nhập hàng - depotId = toDepotId
     * orderStatus: 0: lái xe chưa nhận hàng, 1: toàn bộ lái xe đã nhận hàng, 2: toàn bộ hàng đã được giao thành công.
     * <p>
     * - Nếu this.type = 0 (xuất hàng)
     * + order status : 0 - chưa xuất hàng
     * + order status : 1, 2 - đã xuất hàng thành công
     * - Nếu this.type = 1 (Nhập hàng)
     * + order status : 0, 1 - chưa nhận hàng
     * + order status : 2 - đã nhận hàng thành công.
     * </p
     */
    @BindingAdapter({"status_bidding_order", "type_bidding_order"})
    public static void show_ic_map(ImageView view, String status, Integer type) {
        switch (type) {
            case 0:
                if (status.equals("0")) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
                break;
            case 1:
                if (!status.equals("2")) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
                break;
        }
    }

    @BindingAdapter("vehicleOrderStatus")
    public static void vehicleOrderStatus(TextView textView, BiddingVehicle vehicle) {
        Resources res = textView.getResources();
        int status = vehicle.getVehicleOrderStatus();
        switch (status) {
            case 0:
                textView.setText(res.getString(R.string.Have_not_received));
                textView.setTextColor(res.getColor(R.color.color_normal_text));
                break;
            case 1:
                textView.setText(res.getString(R.string.delivering));
                textView.setTextColor(res.getColor(R.color.color_pending));
                break;
            case 2:
                textView.setText(res.getString(R.string.complete));
                textView.setTextColor(res.getColor(R.color.primaryColor));
                break;
        }
    }

    @BindingAdapter("timeHourMin")
    public static void timeHourMin(TextView textView, Double time) {
        Resources res = textView.getResources();
        if (time == null || time == 0) {
            textView.setText("0 " + res.getString(R.string.minute));
        } else {
            String timeStr = "";
            if (time >= 60) {
                int hours = (int) (time / 60);
                int min = (int) (time - hours * 60);
                timeStr = hours + " " + res.getString(R.string.hour) + " " + min + " " + res.getString(R.string.minute);
            } else {
                timeStr = time+" " + res.getString(R.string.minute);
            }
            textView.setText(timeStr);
        }

    }

    @BindingAdapter("warehouseExport")
    public static void warehouseExport(TextView textView, List<BillPackage> billPackages) {
        Resources res = textView.getResources();
        if (billPackages != null && billPackages.size() > 0) {
            BillPackage dto = billPackages.get(0);
            switch (textView.getId()) {
                case R.id.txtWarehouseExport:
                case R.id.txtWarehouseNameExport:
                    textView.setText(dto.getWarehouse_name());
                    break;
                case R.id.txtAddressExport:
                    textView.setText(dto.getAddress());
                    break;
                case R.id.txtPhoneExport:
                    textView.setText(dto.getPhone());
                    break;
            }
        }
    }


    @BindingAdapter({"listBillService", "cancelAble"})
    public static void listBillService(ChipGroup chipGroup, List<BillService> list, boolean cancelAble) {
        if (!TsUtils.isNotNull(list)) {
            return;
        }
        chipGroup.removeAllViews();
        if (!cancelAble) {
            Resources rs = chipGroup.getResources();
            LayoutInflater inflater = (LayoutInflater) chipGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            TextView textView = (TextView) inflater.inflate(R.layout.label, null);
            textView.setText(rs.getString(R.string.attach_service));
            chipGroup.addView(textView);
        }

        if (TsUtils.isNotNull(list)) {
            for (BillService service : list) {
                Chip chip = new Chip(chipGroup.getContext());
                chip.setTag(service);
                chip.setTextSize(12f);
                chip.setText(service.getName());
                chip.setCloseIconVisible(cancelAble);
                chip.setCheckable(false);
                chip.setOnCloseIconClickListener(v -> {
                    list.remove(v.getTag());
                    chipGroup.removeView(v);
                });
                chipGroup.addView(chip);

            }
        }
    }

    @SuppressLint("SetTextI18n")
    @BindingAdapter({"listBillPackage", "viewModel"})
    public static void listBillPackage(ChipGroup chipGroup, List<BillPackage> listBillPackage, ConfirmChangeVM confirmChangeVM) {
        chipGroup.removeAllViews();
        if (TsUtils.isNotNull(listBillPackage)) {
            Resources rs = chipGroup.getContext().getResources();
            for (BillPackage billPackage : listBillPackage) {
                if (billPackage.getQuantity_package() == 0)
                    continue;

                Chip chip = new Chip(chipGroup.getContext());
                chip.setTag(billPackage);
                chip.setTextSize(12f);
                chip.setText(billPackage.getQuantity_package() + " - " + billPackage.getItem_name());
                chip.setCloseIconVisible(false);
                chip.setClickable(false);
                chip.setEllipsize(TextUtils.TruncateAt.END);
                chip.setCheckable(false);
                if (confirmChangeVM != null
                        && confirmChangeVM.mapPickup.get(billPackage.getFrom_change_bill_package_id()) != null
                        && confirmChangeVM.mapPickup.get(billPackage.getFrom_change_bill_package_id()).getRest() != 0) {
                    chip.setChipStrokeWidth(1);
                    chip.setText(billPackage.getQuantity_package() + "/"
                            + (confirmChangeVM.mapPickup.get(billPackage.getFrom_change_bill_package_id()).getQuantity_package())
                            + " - " + billPackage.getItem_name());
                    chip.setChipStrokeColor(ColorStateList.valueOf(rs.getColor(R.color.color_price)));
                }

                chipGroup.addView(chip);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @BindingAdapter("listBillWarehouse")
    public static void listBillWarehouse(ChipGroup chipGroup, List<BillPackage> listBillPackage) {
        chipGroup.removeAllViews();
        if (TsUtils.isNotNull(listBillPackage)) {
            Resources rs = chipGroup.getContext().getResources();
            for (BillPackage billPackage : listBillPackage) {
                if (billPackage.getQuantity_package() == 0)
                    continue;

                Chip chip = new Chip(chipGroup.getContext());
                chip.setTag(billPackage);
                chip.setTextSize(12f);
                chip.setText(billPackage.getQuantity_package() + " - " + billPackage.getItem_name());
                chip.setCloseIconVisible(false);
                chip.setTextColor(rs.getColor(R.color.color_normal_text));
                chip.setChipBackgroundColor(ColorStateList.valueOf(rs.getColor(R.color.white)));
                chip.setChipStrokeWidth(1f);
                chip.setEllipsize(TextUtils.TruncateAt.END);
                chip.setChipStrokeColor(ColorStateList.valueOf(rs.getColor(R.color.primaryColor)));
                chip.setClickable(false);
                chip.setCheckable(false);
                chipGroup.addView(chip);
            }
        }
    }


    @BindingAdapter("format_Odoo_date")
    public static void format_Odoo_date(TextView textView, OdooDate date) {
        try {
            textView.setText(MyDateUtils.convertDateToString(date, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter("hub_name")
    public static void hub_name(TextView textView, AreaDistance areaDistance) {
        if (areaDistance != null) {
            if (areaDistance.getTo_warehouse_name() != null) {
                textView.setText(areaDistance.getTo_warehouse_name());
            } else {
                textView.setText(areaDistance.getFrom_warehouse_name());
            }
        }
    }

    @BindingAdapter("format_Odoo_date_time")
    public static void format_Odoo_date_time(TextView textView, OdooDateTime date) {
        try {
            textView.setText(MyDateUtils.convertDateToString(date, MyDateUtils.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @BindingAdapter("textHtml")
    public static void textHtml(WebView webview, String description) {
        if (description != null) {
            Resources res = webview.getResources();
            webview.getSettings().setUseWideViewPort(true);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setDomStorageEnabled(true);
            webview.getSettings().setDefaultFontSize((int) res.getDimension(R.dimen.text_notification_system_size));
            webview.loadData(description, "text/html", "UTF-8");
        }
    }

    @BindingAdapter({"status_routing_day", "isConfirm"})
    public static void show_map(ImageView view, String status_routing_day, Boolean isConfirm) {
        if (isConfirm) {
            if (status_routing_day.equals("1") || status_routing_day.equals("0") || status_routing_day.equals("4")) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("show_time_expected")
    public static void show_time_expected(TextView view, RoutingDay routingDay) {
        if (routingDay != null && routingDay.getExpected_to_time() != null && routingDay.getStatus().equals("0") && !routingDay.getArrived_check()) {
            if (RoutingDayStatus.SHIPPING.equals(routingDay.getStatus()) && routingDay.getExpected_to_time().getTime() >= java.util.Calendar.getInstance().getTimeInMillis()) {
                Resources resources = view.getResources();
                Calendar calendar = Calendar.getInstance();
                int current_date = calendar.get(Calendar.DAY_OF_YEAR);
                calendar.setTime(routingDay.getExpected_to_time());
                int routing_date = calendar.get(Calendar.DAY_OF_YEAR);
                if (current_date != routing_date
                        || routingDay.getStatus().equals(RoutingDayStatus.COMPLETED)
                        || routingDay.getStatus().equals(RoutingDayStatus.ORDER_CANCELLED)) {
                    view.setVisibility(View.GONE);
                } else {
                    int minutes = (int) ((routingDay.getExpected_to_time().getTime() - (new Date()).getTime()) / (1000 * 60));
                    if (minutes < 0) {
                        view.setTextColor(resources.getColor(R.color.red));
                    } else if (minutes <= 30) {
                        view.setTextColor(resources.getColor(R.color.warning_color));
                    } else {
                        view.setTextColor(resources.getColor(R.color.color_normal_text));
                    }
                    view.setVisibility(View.VISIBLE);
                }
            } else {
                view.setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @BindingAdapter({"status_routing_day", "is_button"})
    public static void showButton(MaterialButton view, String status_routing_day, Boolean is_button) {
        if (is_button) {
            if (status_routing_day != null)
                view.setEnabled(status_routing_day.equals("1"));
        } else {
            view.setEnabled(false);
        }
    }

    @BindingAdapter({"bill_package", "routing_day_status"})
    public static void quantity_bill_package(TextView view, BillPackage billPackage, String routing_day_status) {
//        viewHolder.quantity_import==null ?  viewHolder.quantityQrChecked+`/`+viewHolder.quantity_export+`` : viewHolder.quantityQrChecked+`/`+viewHolder.quantity_import+``
        if (billPackage != null) {
            if (billPackage.getQuantity_import() != null) {
                if ("0".equals(routing_day_status) || "1".equals(routing_day_status)) {
                    view.setText(billPackage.getQuantityQrChecked() + "/" + billPackage.getQuantity_import());
                } else {
                    view.setText(billPackage.getQuantity_import() + "");
                }
            } else {
                if ("0".equals(routing_day_status) || "1".equals(routing_day_status)) {
                    view.setText(billPackage.getQuantityQrChecked() + "/" + billPackage.getQuantity_export());
                } else {
                    view.setText(billPackage.getQuantity_export() + "");
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @BindingAdapter("warehouse_display")
    public static void warehouse_display(TextView view, RoutingDay routingDay) {
        Resources rs = view.getResources();
        if (routingDay != null) {
            if (routingDay.getType().equals(RoutingDayType.WAREHOUSE_EXPORT)) {
                view.setText(rs.getString(R.string.WAREHOUSE_EXPORT));
                if (routingDay.getStatus().equals(RoutingDayStatus.ORDER_CANCELLED)) {
                    view.setCompoundDrawablesRelativeWithIntrinsicBounds(rs.getDrawable(R.drawable.ic_arrow_up_red), null, null, null);
                } else {
                    view.setCompoundDrawablesRelativeWithIntrinsicBounds(rs.getDrawable(R.drawable.ic_arrow_up), null, null, null);
                }
            } else {
                view.setText(rs.getString(R.string.WAREHOUSE_IMPORT));
                if (routingDay.getStatus().equals(RoutingDayStatus.ORDER_CANCELLED)) {
                    view.setCompoundDrawablesRelativeWithIntrinsicBounds(rs.getDrawable(R.drawable.ic_arrow_down_red), null, null, null);
                } else {
                    view.setCompoundDrawablesRelativeWithIntrinsicBounds(rs.getDrawable(R.drawable.ic_arrow_down), null, null, null);
                }
            }
        }
    }


    @BindingAdapter("autoCompleteAdapter")
    public static void setAdapter1(AutoCompleteTextView view, AutoCompleteAdapter adapter) {
        view.setAdapter(adapter);
    }


    @BindingAdapter(value = {"numberStar", "type"}, requireAll = false)
    public static void showStar(TextView view, Integer numberStar, String type) {
        try {
            if (StringUtils.isNullOrEmpty(type)) {
                view.setVisibility(View.GONE);
                return;
            } else if (!type.equals("2") && !type.equals("3")) {
                view.setVisibility(View.GONE);
                return;
            } else {
                view.setVisibility(View.VISIBLE);
                if (numberStar == null || numberStar == 0) {
                    view.setVisibility(View.GONE);
                    return;
                } else {
                    view.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            view.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.ic_stub));
            return;
        }
        loadImage(view, imageUrl, view.getContext());
    }

    @BindingAdapter("getStatusBillLading")
    public static void getStatusBillLading(TextView view, BillLading billLading) {
        if (billLading!=null) {
            Resources resources = view.getResources();
            switch (billLading.getStatus()) {
                case StatusType.DELETED:
                    view.setText(resources.getString(R.string.Deleted));
                    view.setTextColor(resources.getColor(R.color.color_price));
                    break;
                case StatusType.RUNNING:
                    if("running".equals(billLading.getBill_state())){
                        view.setText(resources.getString(R.string.Running));
                        view.setTextColor(resources.getColor(R.color.color_normal_text));
                    }else{
                        view.setText(resources.getString(R.string.Finished));
                        view.setTextColor(resources.getColor(R.color.primaryColor));
                    }
                    break;
            }
            return;
        }
    }
    @BindingAdapter("selectOrderPackage")
    public static void selectOrderPackage(TextView view, OrderPackage orderPackage) {
        if (orderPackage!=null) {
            view.setText(orderPackage.getName());
        }
    }

    @SuppressLint({"SetTextI18n", "InflateParams"})
    @BindingAdapter("listCargoType")
    public static void listCargoType(ChipGroup chipGroup, List<CargoTypes> list) {
        chipGroup.removeAllViews();
        Resources rs = chipGroup.getResources();
        LayoutInflater inflater = (LayoutInflater) chipGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        TextView textView = (TextView) inflater.inflate(R.layout.label, null);
        textView.setText(rs.getString(R.string.list_cargo_type));
        chipGroup.addView(textView);


        if (TsUtils.isNotNull(list)) {
            for (CargoTypes cargoType : list) {
                Chip chip = new Chip(chipGroup.getContext());
                chip.setTag(cargoType);
                chip.setTextSize(12f);
                chip.setTextColor(rs.getColor(R.color.primaryDarkColor));
                chip.setText(cargoType.getCargo_quantity() + " - " + cargoType.getType());
                chip.setCloseIconVisible(false);
                chip.setCheckable(false);
                chip.setClickable(false);
                chipGroup.addView(chip);

            }
        } else {
            chipGroup.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("listCargo")
    public static void listCargo(ChipGroup chipGroup, List<Cargo> list) {
        Resources rs = chipGroup.getResources();
        chipGroup.removeAllViews();
        if (TsUtils.isNotNull(list)) {
            for (Cargo cargo : list) {

                Chip chip = new Chip(chipGroup.getContext());
                chip.setTag(cargo);
                chip.setTextSize(12f);
                chip.setTextColor(rs.getColorStateList(R.color.color_chip_state_list));
                chip.setChipBackgroundColor(rs.getColorStateList(R.color.bg_chip_state_list));
                chip.setText(cargo.getQr_code());
                chip.setCloseIconVisible(false);
                chip.setCheckable(true);
                chip.setChecked(cargo.checked == null ? false : cargo.checked);
                chip.setClickable(false);
                chipGroup.addView(chip);

            }
        } else {
            chipGroup.setVisibility(View.GONE);
        }
    }

    @BindingAdapter("hideBtnRatingCustomer")
    public static void hideBtnRatingCustomer(ConstraintLayout view, RoutingDay routingPlan) {
        if (routingPlan != null) {
            if (routingPlan.getStatus() != null && routingPlan.getStatus().equals(RoutingDayStatus.COMPLETED)) {
                if (routingPlan.getRating_drivers().getNum_rating() != null && (Calendar.getInstance().getTime().getTime() - routingPlan.getAccept_time().getTime()) / (1000 * 60 * 60) < StaticData.getOdooSessionDto().getRating_customer_duration_key()) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
            } else {
                view.setVisibility(View.GONE);
            }
            return;
        }
    }

    @BindingAdapter("child_recycleView")
    public static void child_recycleView(RecyclerView view, List<String> list) {
        if (list != null) {
            ImageBaseAdapter imageBaseAdapter = new ImageBaseAdapter(view.getContext(), R.layout.item_image_100dp, list);
            view.setAdapter(imageBaseAdapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.HORIZONTAL, false);
            view.setLayoutManager(layoutManager);
        }
    }

    private static void loadImage(ImageView imgReward, String icon, Context context) {
        if (icon.contains("http")) {
            GlideUrl url = new GlideUrl(icon, new LazyHeaders.Builder()
                    .addHeader("Cookie", StaticData.sessionCookie)
                    .build());
            Glide.with(context)
                    .load(url)
                    .error(R.drawable.ic_stub)
                    .into(imgReward);
        } else {
            Glide.with(context)
                    .load(icon)
                    .error(R.drawable.ic_stub)
                    .into(imgReward);
        }
//        if(imgReward.getHeight()<imgReward.getWidth()){
//            imgReward.setRotation(90);
//        }

    }


    @BindingAdapter("orderStatus")
    public static void colorBackgroundRoutingDay(TextView view, String status) {
        Resources rs = view.getResources();
        if (!StringUtils.isNullOrEmpty(status)) {
            switch (status) {
                case RoutingDayStatus.IN_CLAIM:
                    view.setText(rs.getString(R.string.waiting_for_confirm_change));
                    view.setTextColor(rs.getColor(R.color.warning_color));
                    break;
                case RoutingDayStatus.THE_DRIVER_HAS_ARRIVED:
                    view.setTextColor(rs.getColor(R.color.warning_color));
                    view.setText(rs.getString(R.string.Driver_confirmed));
                    break;
                case RoutingDayStatus.COMPLETED:
                    view.setText(rs.getString(R.string.completed));
                    view.setTextColor(rs.getColor(R.color.background_button));
                    break;
                case RoutingDayStatus.ORDER_CANCELLED:
                    view.setText(rs.getString(R.string.Order_cancelled));
                    view.setTextColor(rs.getColor(R.color.red));
                    break;
                case RoutingDayStatus.SHIPPING:
                    view.setText(rs.getString(R.string.Shipping));
                    view.setTextColor(rs.getColor(R.color.color_normal_text));
                    break;
            }
        }
    }


    @BindingAdapter("timer")
    public static void calculatorTime(TextView view, OdooDateTime time) {
        if (time != null) {
            Resources resources = view.getResources();
            if ((System.currentTimeMillis() - time.getTime()) < 60 * 1000) {
                view.setText(resources.getString(R.string.now));
            } else {
                view.setText(DateUtils.getRelativeDateTimeString(
                        view.getContext(), // Suppose you are in an activity or other Context subclass
                        time.getTime(), // The time to display
                        DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                        DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                        0));
            }
        }
    }

    @BindingAdapter("timer")
    public static void calculatorTime(TextView view, String time) {
        if (time != null) {
            Date date = MyDateUtils.convertStringToDate(time, MyDateUtils.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss);
            if ((System.currentTimeMillis() - date.getTime()) < 60 * 1000) {
                view.setText(R.string.now);
            } else {
                view.setText(DateUtils.getRelativeDateTimeString(
                        view.getContext(), // Suppose you are in an activity or other Context subclass
                        date.getTime(), // The time to display
                        DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                        DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                        0));
            }
        }
    }

    @BindingAdapter("textChanged")
    public static void quantityPackageChange(EditText et, Integer number) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!StringUtils.isNumberInt(et.getText().toString())) {
                    et.setError(et.getContext().getResources().getString(R.string.error_quantity));
                    return;
                }
                if (Double.parseDouble(et.getText().toString()) > number) {
                    et.setError(et.getContext().getResources().getString(R.string.error_quantity));
                } else {
                    et.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @BindingAdapter("billDetails")
    public static void billLadingDetails(LinearLayout linearLayout, List<BillRoutingDetail> billLadingDetails) {
        Context context = linearLayout.getContext();
        if (billLadingDetails == null) return;
        linearLayout.removeAllViews();
        for (BillRoutingDetail billLadingDetail : billLadingDetails) {
            LayoutWarehouseAddressBinding binding = LayoutWarehouseAddressBinding.inflate(LayoutInflater.from(context));
            binding.setBillDetail(billLadingDetail);
            linearLayout.addView(binding.getRoot());
        }
    }

    @BindingAdapter("billRoutingStatus")
    public static void billRoutingStatus(TextView textView, BillRouting billRouting) {
        if (billRouting != null) {
//            if(!billRouting.isRouting_scan() && BillRoutingStatus.Shipping.equals(billRouting.getStatus_routing())){
//                textView.setText(R.string.not_yet_process);
//                textView.setTextColor(textView.getResources().getColor(R.color.color_normal_text));
//            }
//            else
            if (BillRoutingStatus.Success.equals(billRouting.getStatus_routing())) {
                textView.setText(R.string.completed);
                textView.setTextColor(textView.getResources().getColor(R.color.primaryColor));
            } else if (BillRoutingStatus.Shipping.equals(billRouting.getStatus_routing())) {
                textView.setText(R.string.shiping);
                textView.setTextColor(textView.getResources().getColor(R.color.color_normal_text));
            } else if (BillRoutingStatus.Cancel.equals(billRouting.getStatus_routing())) {
                textView.setText(R.string.cancelled);
                textView.setTextColor(textView.getResources().getColor(R.color.color_price));
            } else if (BillRoutingStatus.In_claim.equals(billRouting.getStatus_routing())) {
                textView.setText(R.string.in_claim);
                textView.setTextColor(textView.getResources().getColor(R.color.da_cam));
            } else if (BillRoutingStatus.Waiting.equals(billRouting.getStatus_routing()) || !billRouting.isRouting_scan()) {
                textView.setText(R.string.not_yet_process);
                textView.setTextColor(textView.getResources().getColor(R.color.color_normal_text));
            }
        }
    }

    @BindingAdapter("rout")
    public static void rout(LinearLayout linearLayout, List<Routing> routingList) {
        Context context = linearLayout.getContext();
        if (routingList == null) return;
        linearLayout.removeAllViews();
        for (Routing routing : routingList) {
            LayoutRoutBinding binding = LayoutRoutBinding.inflate(LayoutInflater.from(context));
            binding.setRouting(routing);
            linearLayout.addView(binding.getRoot());
        }
    }

    @BindingAdapter("bottomMenu")
    public static void bottomMenu(BottomNavigationView bottomNavigationView, String type) {
        // clear previous menu
        bottomNavigationView.getMenu().clear();

        // add new menu item
        if (EnumUserRole.SHAREVAN_STOCKMAN.equals(type)) {
            bottomNavigationView.inflateMenu(R.menu.bottom_navigation_sharevan_stockman);
        } else {
            bottomNavigationView.inflateMenu(R.menu.bottom_navigation_customer);
        }
    }


}
