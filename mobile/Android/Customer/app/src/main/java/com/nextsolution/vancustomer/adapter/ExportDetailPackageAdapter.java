package com.nextsolution.vancustomer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.databinding.ItemUpdatePackageBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public class ExportDetailPackageAdapter extends RecyclerView.Adapter<ExportDetailPackageAdapter.ViewHolder> {

    List<BillPackage> updatedList;
    Context context;

    public ExportDetailPackageAdapter(Context context) {
        this.context = context;
    }

    public void update(List<BillPackage> updatedList) {
        this.updatedList = updatedList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_update_package, parent, false);
        return new ViewHolder((ItemUpdatePackageBinding) binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BillPackage update = (this.updatedList).get(position);
        holder.bind(update);
        setSize(holder, update);
        setWeight(holder, update);
        setQuantity(holder, update);
    }

    @SuppressLint("SetTextI18n")
    private void setQuantity(ViewHolder holder, BillPackage update) {
        SpannableString spanQuantity;
        if (!update.getQuantity_package().equals(update.getOrigin_bill_package().getQuantity_package())) {
            spanQuantity = new SpannableString(update.getQuantity_package() + "/" + update.getOrigin_bill_package().getQuantity_package());
            spanQuantity.setSpan(new StyleSpan(Typeface.BOLD), 0, update.getQuantity_package().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanQuantity.setSpan(new RelativeSizeSpan(1.4f), 0, update.getQuantity_package().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanQuantity.setSpan(new ForegroundColorSpan(Color.parseColor("#289767")), 0, update.getQuantity_package().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        } else {
            spanQuantity = new SpannableString(update.getOrigin_bill_package().getQuantity_package() + "");
            spanQuantity.setSpan(new RelativeSizeSpan(1.4f), 0, update.getOrigin_bill_package().getQuantity_package().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }
        holder.itemBinding.txtQuantityExport.setText(spanQuantity);
    }

    private void setSize(ViewHolder holder, BillPackage update) {

        SpannableString spanLength;
        if(!update.getLength().equals(update.getOrigin_bill_package().getLength())) {
            spanLength = new SpannableString(update.getLengthStr() + "/" + update.getOrigin_bill_package().getLengthStr() + " cm - ");
            spanLength.setSpan(new StyleSpan(Typeface.BOLD), 0, update.getLengthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanLength.setSpan(new RelativeSizeSpan(1.4f), 0, update.getLengthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanLength.setSpan(new ForegroundColorSpan(Color.parseColor("#289767")), 0, update.getLengthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }else {
            spanLength = new SpannableString(update.getOrigin_bill_package().getLengthStr() + " cm - ");
            spanLength.setSpan(new RelativeSizeSpan(1.4f), 0, update.getOrigin_bill_package().getLengthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }

        SpannableString spanWidth;
        if(!update.getWidth().equals(update.getOrigin_bill_package().getWidth())){
            spanWidth = new SpannableString(update.getWidthStr() + "/" + update.getOrigin_bill_package().getWidthStr() + " cm - ");
            spanWidth.setSpan(new StyleSpan(Typeface.BOLD), 0, update.getWidthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanWidth.setSpan(new RelativeSizeSpan(1.4f), 0, update.getWidthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanWidth.setSpan(new ForegroundColorSpan(Color.parseColor("#289767")), 0, update.getWidthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }else {
            spanWidth = new SpannableString(update.getOrigin_bill_package().getWidthStr() + " cm - ");
            spanWidth.setSpan(new RelativeSizeSpan(1.4f), 0, update.getOrigin_bill_package().getWidthStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }


        SpannableString spanHeight;
        if(!update.getHeight().equals(update.getOrigin_bill_package().getHeight())) {
            spanHeight = new SpannableString(update.getHeightStr() + "/" + update.getOrigin_bill_package().getHeightStr() + " cm ");
            spanHeight.setSpan(new StyleSpan(Typeface.BOLD), 0, update.getHeightStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanHeight.setSpan(new RelativeSizeSpan(1.4f), 0, update.getHeightStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanHeight.setSpan(new ForegroundColorSpan(Color.parseColor("#289767")), 0, update.getHeightStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }else {
            spanHeight = new SpannableString(update.getOrigin_bill_package().getHeightStr() + " cm ");
            spanHeight.setSpan(new RelativeSizeSpan(1.4f), 0, update.getOrigin_bill_package().getHeightStr().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }

        SpannableStringBuilder spanBuilder = new SpannableStringBuilder();
        spanBuilder.append(spanLength).append(spanWidth).append(spanHeight).append(context.getString(R.string.size_unit));
        holder.itemBinding.txtSize.setText(spanBuilder);

    }

    @SuppressLint("SetTextI18n")
    private void setWeight(ViewHolder holder, BillPackage update) {
        SpannableString spanWeight;
        if(!update.getNet_weight().equals(update.getOrigin_bill_package().getNet_weight())) {
            spanWeight = new SpannableString(update.getNet_weight_str() + "/" + update.getOrigin_bill_package().getNet_weight_str() + " kg");

            spanWeight.setSpan(new RelativeSizeSpan(1.4f), 0, update.getNet_weight_str().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanWeight.setSpan(new StyleSpan(Typeface.BOLD), 0, update.getNet_weight_str().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spanWeight.setSpan(new ForegroundColorSpan(Color.parseColor("#289767")), 0, update.getNet_weight_str().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }else {
            spanWeight = new SpannableString(update.getOrigin_bill_package().getNet_weight_str() + " kg");
            spanWeight.setSpan(new RelativeSizeSpan(1.4f), 0, update.getOrigin_bill_package().getNet_weight_str().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        holder.itemBinding.txtWeight.setText(spanWeight);
    }

    @Override
    public int getItemCount() {
        return this.updatedList == null ? 0 : (this.updatedList).size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemUpdatePackageBinding itemBinding;

        public ViewHolder(ItemUpdatePackageBinding view) {
            super(view.getRoot());
            this.itemBinding = view;
        }

        public void bind(BillPackage update) {
            itemBinding.setVariable(BR.updated, update);
            itemBinding.executePendingBindings();
        }
    }
}
