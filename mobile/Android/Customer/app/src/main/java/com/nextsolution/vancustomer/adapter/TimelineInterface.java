package com.nextsolution.vancustomer.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;

public interface TimelineInterface {
    Drawable getMaker(Context context);
}
