package com.nextsolution.vancustomer.confirm_change;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.databinding.DialogEditBillPackageBinding;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

public class EditBillPackageDialog extends BottomSheetDialogFragment implements AdapterListener {
    private DialogEditBillPackageBinding binding;
    private ConfirmChangeVM confirmChangeVM;
    private OnConfirm onConfirm;
    private XBaseAdapter goodsAdapter;
    public BillLadingDetail billLadingDetail;
    private HashMap<Long, Integer> itemQuantities;// lưu lại split quantity của từng item.
    private boolean isConfirm;
    HashMap<Long, BillPackage> mapPickup;


    EditBillPackageDialog(ConfirmChangeVM confirmChangeVM, HashMap<Long, BillPackage> mapPickup, BillLadingDetail billLadingDetail, OnConfirm onConfirm) {
        this.confirmChangeVM = confirmChangeVM;
        this.mapPickup = mapPickup;
        this.onConfirm = onConfirm;
        this.itemQuantities = new HashMap<>();
        this.billLadingDetail = billLadingDetail;

        //cập nhật số lượng có thể chọn của từng gói
        for (BillPackage billPackage : billLadingDetail.getBillPackages()) {
            BillPackage temp = mapPickup.get(billPackage.getFrom_change_bill_package_id());

            temp.setRest(temp.getRest() + billPackage.getQuantity_package());//nhớ trừ số lượng còn lại nếu không confirm
            temp.setSplitQuantity(temp.getSplitQuantity() - billPackage.getQuantity_package());//nhớ thêm số lượng đã chia lại nếu không confirm
            billPackage.setSplitQuantity(temp.getSplitQuantity());
            billPackage.setRest(temp.getQuantity_package());
            itemQuantities.put(billPackage.getFrom_change_bill_package_id(), billPackage.getQuantity_package());
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_bill_package, container, false);
        binding.setListener(this);
        isConfirm = false;
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        return binding.getRoot();
    }

    private void initView() {
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        goodsAdapter = new XBaseAdapter(R.layout.item_update_bill_edit, billLadingDetail.getBillPackages(), this);
        binding.rcGoods.setAdapter(goodsAdapter);
        binding.rcGoods.setLayoutManager(new LinearLayoutManager(getContext()));

    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
            setupFullHeight(bottomSheetDialog);
        });
        return dialog;
    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        //trả lại rest, quantity, splitQuantity nếu không confirm
        if (!isConfirm && itemQuantities.size() > 0) {
            List<BillPackage> billPackages = billLadingDetail.getBillPackages();
            for (BillPackage billPackage : billPackages) {
                BillPackage temp = mapPickup.get(billPackage.getFrom_change_bill_package_id());
                temp.setRest(temp.getRest() - itemQuantities.get(billPackage.getFrom_change_bill_package_id()));
                temp.setSplitQuantity(temp.getSplitQuantity() + itemQuantities.get(billPackage.getFrom_change_bill_package_id()));
                billPackage.setSplitQuantity(temp.getSplitQuantity() + itemQuantities.get(billPackage.getFrom_change_bill_package_id()));
                billPackage.setRest(temp.getRest() - itemQuantities.get(billPackage.getFrom_change_bill_package_id()));
                billPackage.setQuantity_package(itemQuantities.get(billPackage.getFrom_change_bill_package_id()));
            }
        }
        super.onDismiss(dialog);

    }

    public void addReceiptWareHouse() {
        if (confirmChangeVM.addReceiptWareHouse(billLadingDetail)) {
            confirmChangeVM.setCapacity(billLadingDetail);
            isConfirm = true;
            //cập nhật số lượng còn lại.
            List<BillPackage> billPackages = billLadingDetail.getBillPackages();
            for (BillPackage billPackage : billPackages) {
                BillPackage export = mapPickup.get(billPackage.getFrom_change_bill_package_id());
                export.setRest(export.getRest() - billPackage.getQuantity_package());
                export.setSplitQuantity(export.getSplitQuantity() + billPackage.getQuantity_package());
                billPackage.setSplitQuantity(export.getSplitQuantity() + billPackage.getQuantity_package());
                billPackage.setRest(export.getRest() - billPackage.getQuantity_package());
            }
            onConfirm.onConfirm(billLadingDetail);
            this.dismiss();
        } else {
            Toast.makeText(getContext(), R.string.some_item_is_invalid, Toast.LENGTH_SHORT).show();
        }
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = ViewGroup.LayoutParams.MATCH_PARENT;
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }


    @Override
    public void onItemClick(View view, Object o) {
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }

    public interface OnConfirm {
        void onConfirm(BillLadingDetail bill);
    }
}
