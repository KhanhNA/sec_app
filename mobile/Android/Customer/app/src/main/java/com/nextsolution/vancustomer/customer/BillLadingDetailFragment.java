package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.PagerAdapter;
import com.nextsolution.vancustomer.customer.vm.CustomerOrdersVM;
import com.nextsolution.vancustomer.databinding.BillLadingDetailFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class BillLadingDetailFragment extends BaseFragment {
    BillLadingDetailFragmentBinding mBinding;
    private MenuItem preItem;
    CustomerOrdersVM customerOrdersVM;
    BillLadingDetail billLadingDetail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (BillLadingDetailFragmentBinding) binding;
        customerOrdersVM = (CustomerOrdersVM) viewModel;
        initView();
        getData();
        navigationView();

        return v;
    }
    private void initView(){
        mBinding.toolbar.setTitle(R.string.detail_warehouse);
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void getData() {
        Intent intent = getBaseActivity().getIntent();
        if (intent != null && intent.hasExtra(Constants.MODEL)) {
            billLadingDetail = (BillLadingDetail) intent.getSerializableExtra(Constants.MODEL);
        }
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        switch (view.getId()) {
            case R.id.btnBack:
                getActivity().onBackPressed();
                break;
            case R.id.toolbar:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void navigationView() {

        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());

        BillLadingDetailChildFragment billLadingDetailChildFragment = new BillLadingDetailChildFragment(billLadingDetail);
        myPagerAdapter.addFragment(billLadingDetailChildFragment);

        ServiceArisingFragment serviceArisingFragment = new ServiceArisingFragment();
        myPagerAdapter.addFragment(serviceArisingFragment);

        mBinding.frameContainer.setAdapter(myPagerAdapter);

        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.bill_lading_detail:
                    mBinding.frameContainer.setCurrentItem(0);
                    break;
                case R.id.services_arising:
                    mBinding.frameContainer.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bill_lading_detail_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CustomerOrdersVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
