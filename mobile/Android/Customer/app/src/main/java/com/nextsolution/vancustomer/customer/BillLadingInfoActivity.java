package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.BillLadingInfoVM;
import com.nextsolution.vancustomer.databinding.BillLadingInfoActivityBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.text.DateFormatSymbols;

public class BillLadingInfoActivity extends BaseActivity implements NetworkManager.NetworkHandler {
    private BillLadingInfoVM billLadingInfoVM;
    private BillLadingInfoActivityBinding mBinding;
    private XBaseAdapter adapter;

    boolean isOnline;
    private NetworkManager networkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        billLadingInfoVM = (BillLadingInfoVM) viewModel;
        mBinding = (BillLadingInfoActivityBinding) binding;
        mBinding.toolbar.setTitle(R.string.bill_lading_info);

        initToolBar();

//        mBinding.txtSubscription.setOnClickListener(v->showSubscription());
        setUpRecycleView();
        getData();
    }

    private void initToolBar() {
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }
    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void showSubscription() {
        String temp = "";
        switch (billLadingInfoVM.getBillLading().get().getFrequency()) {
            case 1:
                temp = getString(R.string.repeat) + " " + getString(R.string.every_day);
                break;
            case 2:
                temp = getString(R.string.repeat_every) + " " + getDayOfWeekStr(billLadingInfoVM.getBillLading().get().getDay_of_week());
                break;
            case 3:
                temp = getString(R.string.repeat_at) + " " + billLadingInfoVM.getBillLading().get().getDay_of_month() + " " + getString(R.string.every_month);
                break;
        }
        mBinding.txtSubscription.setText(temp);
    }

    public String getDayOfWeekStr(int dayOfWeek) {
        return new DateFormatSymbols(getResources().getConfiguration().locale).getWeekdays()[dayOfWeek + 1];
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_SUCCESS)) {
            billLadingInfoVM.getEmptyData().set(false);
            adapter.notifyDataSetChanged();
            showSubscription();
        } else if (action.equals(Constants.GET_DATA_FAIL)) {
            billLadingInfoVM.getEmptyData().set(true);
        }
    }


    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemInfoBillLading) {
            BillLadingDetail billLadingDetail = (BillLadingDetail) o;
            Intent intent = new Intent(this, CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.MODEL, billLadingDetail);
            bundle.putSerializable(Constants.FRAGMENT, BillLadingDetailFragment.class);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }

    private void getData() {
        Intent intent = getIntent();
        if (intent.hasExtra(Constants.ITEM_ID)) {
            Integer billLadingId;
            try {
                billLadingId = Integer.parseInt(intent.getStringExtra(Constants.ITEM_ID));
                billLadingInfoVM.getData(billLadingId, this::runUi);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.bill_lading_info_item, billLadingInfoVM.getBillLadingDetails(), this);
        mBinding.RcBillLadingInfo.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mBinding.RcBillLadingInfo.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v, ViewModel vm) {
        if (v.getId() == R.id.toolbar) {
            onBackPressed();
        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.bill_lading_info_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillLadingInfoVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.tvNetwork.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.tvNetwork.setVisibility(View.GONE);
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }
}
