package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.CargoTypesDialogVM;
import com.nextsolution.vancustomer.databinding.CargoTypesDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;

public class CargoTypesDialog extends DialogFragment {
    CargoTypesDialogVM cargoTypesDialogVM;
    CargoTypesDialogBinding mBinding;
    CargoTypes cargoTypes = new CargoTypes();
    String qrCode;

    public CargoTypesDialog(CargoTypes cargoTypes, String qrCode) {
        this.cargoTypes = cargoTypes;
        this.qrCode = qrCode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.cargo_types_dialog, container, false);
        cargoTypesDialogVM = ViewModelProviders.of(this).get(CargoTypesDialogVM.class);
        mBinding.setViewModel(cargoTypesDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        getData();
        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                autoDismiss();
            }
        }.start();
        return mBinding.getRoot();
    }

    private void autoDismiss() {
        dismiss();
    }

    public void getData() {
        cargoTypesDialogVM.getCargoTypes().set(cargoTypes);
        cargoTypesDialogVM.getQr_code().set(qrCode);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }
}
