package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolution.db.dto.AddressDTO;
import com.nextsolution.db.dto.CareerDTO;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.GridImageAdapter;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.customer.vm.CreateAccountChildVM;
import com.nextsolution.vancustomer.databinding.CreateAccountStep1Binding;
import com.nextsolution.vancustomer.databinding.CreateAccountStep2Binding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.ErrorCreateAccount;
import com.nextsolution.vancustomer.ui.fragment.ListDialogFragment;
import com.nextsolution.vancustomer.util.StringUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.FullyGridLayoutManager;
import com.nextsolution.vancustomer.widget.GlideEngine;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateAccountChildFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    int layout;
    CreateAccountChildVM viewModel;
    private GridImageAdapter imageSelectAdapter;
    ListDialogFragment listProvinceDialog;
    ListDialogFragment listDistrictDialog;
    ListDialogFragment listCareerDialog;
    ConfirmOTPDialog confirmOTPDialog;
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    int step = 0;

    public CreateAccountChildFragment(int layout, int step) {
        this.layout = layout;
        this.step = step;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        viewModel = ViewModelProviders.of(getBaseActivity()).get(CreateAccountChildVM.class);
        binding.setVariable(BR.viewModel, viewModel);
        getData();
        if (step == 1)
            initAdapterImage(savedInstanceState);
        return view;
    }

    private void getData() {
        if (step == 0) {
            viewModel.initDataStep1(this::runUi);
            CreateAccountStep1Binding mBinding = (CreateAccountStep1Binding) binding;
            mBinding.edtPhone.setHint(R.string.PHONE_NUMBER);
            mBinding.edtPhone.setDefaultCountry("VN");

        }

    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.btnNextStep2) {
            if (validateStep1()) {
                handleDataBeforeNextStep(1);
                nextStep(1);
            }
        } else if (view.getId() == R.id.btnNextStep3) {
            CreateAccountStep2Binding mbinding = (CreateAccountStep2Binding) binding;
            mbinding.btnNextStep3.startAnimation();
            if (validateStep2()) {
                handleDataBeforeNextStep(2);
                viewModel.createAccount(this::runUi);
//                nextStep(2);
            } else {
                mbinding.btnNextStep3.dispose();
            }
        } else if (view.getId() == R.id.llAddress) {

            showDialogDatePicker();
        }
    }

    private void handleDataBeforeNextStep(int position) {
        if (position == 1) {
            CreateAccountStep1Binding mBinding = (CreateAccountStep1Binding) binding;
            viewModel.getCustomer().setName(viewModel.getFirst_name().get().trim() + " " + viewModel.getLast_name().get().trim());
            viewModel.getCustomer().setAddress(viewModel.getDistrict().get() + ", " + viewModel.getProvince().get());
            viewModel.getCustomer().setPhone(viewModel.getPhone().get());
            viewModel.getCustomer().setBirthday(viewModel.getYear().get() + "-" + viewModel.getMonth().get() + "-" + viewModel.getDay().get());
            if (mBinding.rbMale.isChecked()) {
                viewModel.getCustomer().setGender("male");
            } else {
                viewModel.getCustomer().setGender("female");
            }
        } else if (position == 2) {
            viewModel.getCustomer().setEmail(viewModel.getMail().get());
        }
    }

    private void showDialogDatePicker() {
        Context scene = getContext();
        if (scene != null) {
            Calendar maxDate = Calendar.getInstance();
            maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) - 15);
            Calendar minDate = Calendar.getInstance();
            minDate.set(Calendar.YEAR, minDate.get(Calendar.YEAR) - 60);
            Integer year = maxDate.get(Calendar.YEAR);
            Integer month = maxDate.get(Calendar.MONTH);
            Integer day = maxDate.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(scene, this, year, month, day);
            if (maxDate != null) {
                datePickerDialog.getDatePicker().setMaxDate(maxDate.getTime().getTime());
            }
            if (minDate != null) {
                datePickerDialog.getDatePicker().setMinDate(minDate.getTime().getTime());
            }
            datePickerDialog.show();
        }
    }

    public void nextStep(Integer position) {
        EventBus.getDefault().post(position);
    }

    private void runUi(Object[] params) {
        String action = (String) params[0];
        switch (action) {
            case "CHOOSE_PROVINCE":
                AddressDTO province = (AddressDTO) viewModel.getListProvince().get(0);
                viewModel.getProvince().set(province.getName());
                getDistrict(province.getId());
                viewModel.getCustomer().setProvince_id(province.getId());
                break;
            case "CHOOSE_DISTRICT":
                AddressDTO district = (AddressDTO) viewModel.getListDistrict().get(0);
                viewModel.getDistrict().set(district.getName());
                viewModel.getCustomer().setDistrict_id(district.getId());
                break;
            case Constants.SUCCESS:
                CreateAccountStep2Binding mBinding = (CreateAccountStep2Binding) binding;
                mBinding.btnNextStep3.revertAnimation();
                confirmOTPDialog = new ConfirmOTPDialog(viewModel.getPhone().get(), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppController.getInstance().getSharePre().edit().putString(Constants.USER_NAME, viewModel.getPhone().get()).apply();
                        ToastUtils.showToast(getActivity(), getResources().getString(R.string.CREATE_ACCOUNT_SUCCESS), getResources().getDrawable(R.drawable.ic_check));
                        confirmOTPDialog.dismiss();
                        getActivity().onBackPressed();
                    }
                });
                confirmOTPDialog.show(getChildFragmentManager(), "ABC");
                break;
            case Constants.FAIL:
                CreateAccountStep2Binding mBinding1 = (CreateAccountStep2Binding) binding;
                mBinding1.btnNextStep3.revertAnimation();
                String error = getString(R.string.CREATE_ACCOUNT_FAIL);
                if (params.length > 1) {
                    String action_error = (String) params[1];
                    switch (action_error){
                        case ErrorCreateAccount.NOT_ENOUGH_IMAGE:
                            error = getString(R.string.not_enough_image_request);
                            break;
                        case ErrorCreateAccount.NO_PHONE_NUMBER:
                            error = getString(R.string.No_phone_number);
                            break;
                        case ErrorCreateAccount.NO_NAME:
                            error = getString(R.string.NOT_EMPTY_NAME);
                            break;
                        case ErrorCreateAccount.NO_BIRTHDAY:
                            error = getString(R.string.No_birthday);
                            break;
                        case ErrorCreateAccount.NO_GENDER:
                            error = getString(R.string.No_gender);
                            break;
                        case ErrorCreateAccount.NO_ADDRESS:
                            error = getString(R.string.no_address);
                            break;
                        case ErrorCreateAccount.NO_PROVINCE:
                            error = getString(R.string.No_province);
                            break;
                        case ErrorCreateAccount.NO_DISTRICT:
                            error = getString(R.string.no_district);
                            break;
                        case ErrorCreateAccount.ACCOUNT_ALREADY_EXISTS:
                            error = getString(R.string.account_already_exists);
                            break;
                        case ErrorCreateAccount.CREATE_COMPANY_FAIL:
                            error = getString(R.string.create_company_fail);
                            break;
                        default:
                            error = getString(R.string.CREATE_ACCOUNT_FAIL);
                            break;
                    }
                }
                ToastUtils.showToast(getActivity(), error, getResources().getDrawable(R.drawable.ic_fail));
                break;

        }

    }

    public void chooseProvince() {

        listProvinceDialog = new ListDialogFragment(R.layout.location_item, R.string.province_field, "", viewModel.getListProvince(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                AddressDTO dto = (AddressDTO) o;
                viewModel.getProvince().set(dto.getName());
                viewModel.getCustomer().setProvince_id(dto.getId());
                viewModel.getListDistrict().clear();
                getDistrict(dto.getId());
                listProvinceDialog.dismiss();

            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listProvinceDialog.show(getChildFragmentManager(), "ABC");
    }

    public void getDistrict(Integer parent_id) {
        viewModel.getDistrict(parent_id, this::runUi);
    }

    public void chooseDistrict() {
        listDistrictDialog = new ListDialogFragment(R.layout.location_item, R.string.district_field, "", viewModel.getListDistrict(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                AddressDTO dto = (AddressDTO) o;
                viewModel.getDistrict().set(dto.getName());
                viewModel.getCustomer().setDistrict_id(dto.getId());
                listDistrictDialog.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listDistrictDialog.show(getChildFragmentManager(), "ABC");
    }

    public void chooseCareer() {
        listCareerDialog = new ListDialogFragment(R.layout.career_item, R.string.career_field, "", viewModel.getListCareer(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                CareerDTO dto = (CareerDTO) o;
                viewModel.getCareer().set(dto.getName());
                viewModel.getCustomer().setCareer_id(dto.getId());
                listCareerDialog.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        listCareerDialog.show(getChildFragmentManager(), "ABC");
    }

    @Override
    public int getLayoutRes() {
        return this.layout;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CreateAccountChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (dayOfMonth < 10) {
            viewModel.getDay().set("0" + dayOfMonth);
        } else {
            viewModel.getDay().set(dayOfMonth + "");
        }
        if (month + 1 < 10) {
            viewModel.getMonth().set("0" + (month + 1));
        } else {
            viewModel.getMonth().set(month + "");
        }
        viewModel.getYear().set(year + "");
    }

    //Check validate step 1
    private boolean validateStep1() {
        CreateAccountStep1Binding mBinding = (CreateAccountStep1Binding) binding;
        boolean check = true;
        if (StringUtils.isNullOrEmpty(viewModel.getFirst_name().get())) {
            mBinding.ipFirstName.setError(getString(R.string.not_empty));
            check = false;
        }
        mBinding.edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(viewModel.getFirst_name().get())) {
                    mBinding.ipFirstName.setErrorEnabled(false);
                }
            }
        });
        if (StringUtils.isNullOrEmpty(viewModel.getLast_name().get())) {
            mBinding.ipLastName.setError(getString(R.string.not_empty));
            check = false;
        }
        mBinding.edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(viewModel.getLast_name().get())) {
                    mBinding.ipLastName.setErrorEnabled(false);
                }
            }
        });
        if (StringUtils.isNullOrEmpty(mBinding.edtPhone.getPhoneNumber())) {
            mBinding.edtPhone.setError(getString(R.string.not_empty));
            check = false;
        }
        //check độ dài phone number
//        else if (mBinding.edtPhone.getPhoneNumber().length() <= 10) {
//            mBinding.edtPhone.setError(getString(R.string.phone_number_is_too_short));
//            check = false;
//        } else if (mBinding.edtPhone.getPhoneNumber().length() >= 16) {
//            mBinding.edtPhone.setError(getString(R.string.phone_number_is_too_long));
//            check = false;
//        }
        else if (StringUtils.isNumeric(mBinding.edtPhone.getPhoneNumber())) {
            mBinding.edtPhone.setError(getString(R.string.format_error));
        } else {
            viewModel.getPhone().set(mBinding.edtPhone.getPhoneNumber().trim());
        }
        mBinding.edtPhone.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                Log.d("TAG", "onViewAttachedToWindow: ");
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                Log.d("TAG", "onViewDetachedFromWindow: ");
            }
        });
        return check;
    }

    //validate step 2
    private boolean validateStep2() {
        CreateAccountStep2Binding mBinding = (CreateAccountStep2Binding) binding;
        boolean check = true;
        if (StringUtils.isNullOrEmpty(viewModel.getMail().get())) {
            mBinding.ipEmail.setError(getString(R.string.not_empty));
            check = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(viewModel.getMail().get()).matches()) {
            check = false;
            mBinding.ipEmail.setError(getString(R.string.Incorrect_email_format));
        }
        mBinding.edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(viewModel.getMail().get())) {
                    mBinding.ipEmail.setErrorEnabled(false);
                }
            }
        });


        if (StringUtils.isNullOrEmpty(viewModel.getPassword().get())) {
            mBinding.ipPassword.setError(getString(R.string.not_empty));
            check = false;
        }
        mBinding.edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(viewModel.getPassword().get())) {
                    mBinding.ipPassword.setErrorEnabled(false);
                }
            }
        });
        mBinding.edtConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(viewModel.getPassword().get())) {
                    mBinding.ipConfirmPassword.setErrorEnabled(false);
                }
            }
        });


        if (StringUtils.isNullOrEmpty(viewModel.getConfirmPassword().get())) {
            mBinding.ipConfirmPassword.setError(getString(R.string.not_empty));
            check = false;
        } else if (!StringUtils.isNullOrEmpty(viewModel.getPassword().get()) && !viewModel.getConfirmPassword().get().equals(viewModel.getPassword().get())) {
            mBinding.ipConfirmPassword.setError(getString(R.string.INCORRECT_PASSWORD));
            check = false;
        }
        mBinding.edtConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!StringUtils.isNullOrEmpty(viewModel.getPassword().get())) {
                    mBinding.ipConfirmPassword.setErrorEnabled(false);
                }
            }
        });
        if (viewModel.getLstFileSelected() == null || viewModel.getLstFileSelected().size() != 3) {
            check = false;
            mBinding.lbErrorImage.setVisibility(View.VISIBLE);
        } else {
            mBinding.lbErrorImage.setVisibility(View.GONE);
        }
        return check;
    }

    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .maxSelectNum(3)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new CreateAccountChildFragment.MyResultCallback(imageSelectAdapter, viewModel));
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private CreateAccountChildVM viewModel;

        public MyResultCallback(GridImageAdapter adapter, CreateAccountChildVM viewModel) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.viewModel = viewModel;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            viewModel.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }
    }

    private void initAdapterImage(Bundle savedInstanceState) {
        CreateAccountStep2Binding mBinding = (CreateAccountStep2Binding) binding;

        FullyGridLayoutManager manager = new FullyGridLayoutManager(getContext(),
                3, GridLayoutManager.VERTICAL, false);
        mBinding.rcImages.setLayoutManager(manager);

        mBinding.rcImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(getContext(), 8), false));


        imageSelectAdapter = new GridImageAdapter(getContext(), this::pickImage);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setOnClear(() -> viewModel.getIsNotEmptyImage().set(false));
        mBinding.rcImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setSelectMax(3);
        imageSelectAdapter.setOnItemClickListener((v, position) -> openOptionImage(position));
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }

}
