package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.confirm_change.ConfirmEditBillLadingActivity;
import com.nextsolution.vancustomer.databinding.HomeFragmentBinding;
import com.nextsolution.vancustomer.enums.BillRoutingStatus;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.TroubleType;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.ui.MainActivity;
import com.nextsolution.vancustomer.view_by_bill_lading.BillLadingVM;
import com.nextsolution.vancustomer.view_by_bill_lading.BillRoutingDetailActivity;
import com.nextsolution.vancustomer.view_by_bill_lading.ListBillLadingActivity;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.util.Date;

public class HomeFragment extends BaseFragment implements CalendarView.OnCalendarSelectListener,
        CalendarView.OnYearChangeListener, NetworkManager.NetworkHandler {
    private final MainActivity mActivity;
    HomeFragmentBinding mBinding;
    private BillLadingVM billLadingVM;
    private BaseAdapterV3 billAdapter;
    String date;
    private int mYear;
    IReloadRoutingDay iReloadRoutingDay;

    boolean isOnline;
    private NetworkManager networkManager;

    final RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if(mActivity == null) return;
            mActivity.animationFAB(getTag(),  (0 == dy));
        }
    };

    public HomeFragment(MainActivity mActivity,IReloadRoutingDay iReloadRoutingDay) {
        this.mActivity = mActivity;
        this.iReloadRoutingDay = iReloadRoutingDay;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        billLadingVM = (BillLadingVM) viewModel;
        mBinding = (HomeFragmentBinding) binding;
        initToolbar();
        initView();
        initLoadMore();

        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, c.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));
        Date toDate = c.getTime();
        c.set(java.util.Calendar.MONTH, c.get(java.util.Calendar.MONTH) - 1);
        c.set(java.util.Calendar.DAY_OF_MONTH, 20);
        Date fromDate = c.getTime();

        date = mBinding.calendar.getCurYear() + "-" + mBinding.calendar.getCurMonth() + "-" + mBinding.calendar.getCurDay();
        billLadingVM.getListDate(fromDate, toDate, this::runUi);
        reFresh();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }
    public void reFresh(){
        billLadingVM.getBillLadingByDate(date, false, this::runUi);
    }

    private void initLoadMore() {
        billAdapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            billLadingVM.loadMore(date, this::runUi);
        });
        billAdapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        billAdapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getBillLadingSuccess":
                billAdapter.getLoadMoreModule().loadMoreComplete();
                billAdapter.notifyDataSetChanged();
                break;
            case "noMore":
                billAdapter.getLoadMoreModule().loadMoreEnd();
                break;
            case "getListDate":
                mBinding.calendar.update();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mBinding.rcBillLading.removeOnScrollListener(mScrollListener);
    }

    private void initView() {
        billAdapter = new BaseAdapterV3(R.layout.item_bill_lading, billLadingVM.getBillLadingList(), this);
        mBinding.rcBillLading.setAdapter(billAdapter);
        mBinding.swRefresh.setOnRefreshListener(() -> {
            billLadingVM.onRefresh(date, this::runUi);
        });
        mBinding.calendar.setOnCalendarSelectListener(this);
        mBinding.calendar.setOnYearChangeListener(this);
        mBinding.calendar.setSchemeDate(billLadingVM.getMScheme());
        mBinding.calendarLayout.setModeOnlyWeekView();
        mBinding.calendar.setSelectDefaultMode();
        mBinding.calendar.setOnMonthChangeListener((year, month) -> getListDateByMonth(month));
        mBinding.rcBillLading.addOnScrollListener(mScrollListener);

    }

    private void getListDateByMonth(int month) {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, 20);
        c.set(java.util.Calendar.MONTH, month - 2);
        Date fromDate = c.getTime();
        c.set(java.util.Calendar.DAY_OF_MONTH, 10);
        c.set(java.util.Calendar.MONTH, month);
        Date toDate = c.getTime();
        billLadingVM.getListDate(fromDate, toDate, this::runUi);
    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemBillLading) {
            BillRouting billRouting = (BillRouting) o;
            Intent intent;
            if (billRouting.getStatus_routing().equals(BillRoutingStatus.In_claim) && billRouting.getTrouble_type().equals(TroubleType.NORMAL)) {
                intent = new Intent(getBaseActivity(), ConfirmEditBillLadingActivity.class);
                intent.putExtra(Constants.ITEM_ID, billRouting.getChange_bill_lading_detail_id() + "");
                startActivity(intent);
            } else {
                intent = new Intent(getActivity(), BillRoutingDetailActivity.class);
                intent.putExtra(Constants.ITEM_ID, ((BillRouting) o).getId()+"");
                startActivityForResult(intent,Constants.REQUEST_FRAGMENT_RESULT);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Constants.REQUEST_FRAGMENT_RESULT&& resultCode==Constants.RESULT_OK){
            billLadingVM.getBillLadingByDate(date, false, this::runUi);
            iReloadRoutingDay.reloadRoutingDay();
        }
    }

    @SuppressLint("SetTextI18n")
    private void initToolbar() {
        mBinding.btnBack.setOnClickListener(v -> {
            if (mBinding.calendar.isYearSelectLayoutVisible()) {
                mBinding.calendar.closeYearSelectLayout();
            }
        });
        mBinding.tvMonthDay.setOnClickListener(v -> {
            mBinding.calendar.showYearSelectLayout(mYear);
            mBinding.tvYear.setVisibility(View.GONE);
            mBinding.tvMonthDay.setText(String.valueOf(mYear));
        });
        mBinding.flCurrent.setOnClickListener(v -> mBinding.calendar.scrollToCurrent());
        mBinding.tvYear.setText(String.valueOf(mBinding.calendar.getCurYear()));
        mYear = mBinding.calendar.getCurYear();
        String monthStr = mBinding.calendar.getCurMonth() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        String dayStr = mBinding.calendar.getCurDay() + "";
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        mBinding.tvMonthDay.setText(dayStr + " / " + monthStr);
        mBinding.tvCurrentDay.setText(String.valueOf(mBinding.calendar.getCurDay()));
    }

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        mBinding.tvYear.setVisibility(View.VISIBLE);
        String monthStr = calendar.getMonth() + "";
        String dayStr = calendar.getDay() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        mBinding.tvMonthDay.setText(dayStr + " / " + monthStr);
        mBinding.tvYear.setText(String.valueOf(calendar.getYear()));
        mYear = calendar.getYear();
        if (isClick) {
            date = calendar.getYear() + "-" + calendar.getMonth() + "-" + calendar.getDay();
            billLadingVM.getBillLadingByDate(date, false, this::runUi);
        }
    }


    @Override
    public void onYearChange(int year) {
        mBinding.tvMonthDay.setText(String.valueOf(year));

    }

    public void scrollToCurrent() {
        mBinding.calendar.scrollToCurrent();
    }


    @Override
    public int getLayoutRes() {
        return R.layout.home_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BillLadingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v -> {
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }

    public interface IReloadRoutingDay{
        void reloadRoutingDay();
    }
}
