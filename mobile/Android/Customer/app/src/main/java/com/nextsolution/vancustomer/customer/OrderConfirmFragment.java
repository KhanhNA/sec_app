package com.nextsolution.vancustomer.customer;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.Insurance;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.ReceiptWarehouseAdapter;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.customer.vm.OrderVMV2;
import com.nextsolution.vancustomer.databinding.OrderConfirmFragmentBinding;
import com.nextsolution.vancustomer.enums.ActionClick;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.OrderPackageType;
import com.nextsolution.vancustomer.model.OrderPackage;
import com.nextsolution.vancustomer.model.RecurrentModel;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.service.service_dto.NotificationService;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.ui.fragment.ListDialogFragment;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.OnSingleClickListener;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OrderConfirmFragment extends BaseFragment implements NetworkManager.NetworkHandler {
    private OrderVMV2 orderVM;
    private OrderConfirmFragmentBinding mBinding;
    DialogConfirm dialogConfirm;
    RecurrentModel recurrentModel;
    private ListDialogFragment dialogFragment;
    private ReceiptWarehouseAdapter adapter;
    private TransshipmentPointDialog transshipmentPointDialog;

    boolean isOnline;
    private NetworkManager networkManager;


    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        if (!orderVM.getIsLoading().get()) {
            orderVM.priceCalculate();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        orderVM = ViewModelProviders.of(getBaseActivity()).get(OrderVMV2.class);
        binding.setVariable(BR.viewModel, orderVM);
        orderVM.getBillLadingField().get().setStart_date(new OdooDate(new Date().getTime()));
        mBinding = (OrderConfirmFragmentBinding) binding;
        initView();
        orderVM.getOrderPackage(this::runUi);
        return view;
    }

    private void initView() {
        adapter = new ReceiptWarehouseAdapter(R.layout.item_selected_warehouse, orderVM.getBillLadingField().get().getArrBillLadingDetail(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                if (!OrderPackageType.EXPRESS.equals(orderVM.getBillLadingField().get().getOrder_package().getType())) {
                    List<AreaDistance> areaDistanceList = new ArrayList<>();
                    BillLadingDetail billLadingDetail = (BillLadingDetail) o;
                    areaDistanceList.addAll(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getAreaDistances());
                    areaDistanceList.addAll(billLadingDetail.getAreaDistances());
                    transshipmentPointDialog = new TransshipmentPointDialog(areaDistanceList, orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getName(), billLadingDetail.getWarehouse().getName());
                    transshipmentPointDialog.show(getChildFragmentManager(), "abc");
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        mBinding.rcReceiveWarehouse.setAdapter(adapter);
        mBinding.rcReceiveWarehouse.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        mBinding.spSelectFrequency.setEndIconOnClickListener(v -> {
            orderVM.getBillLadingField().get().setFrequency(null);
            mBinding.txtFrequency.setText(null);
        });

        mBinding.spSelectSubscribe.setEndIconOnClickListener(v -> {
            showConfirmDeleteSubscribe();
        });
        mBinding.btnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                createOrder();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        networkManager.stop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(String action) {
        if (action.equals(Constants.DISCONNECTED_NETWORK)) {
            if (orderVM.getIsLoading().get()) {
                orderVM.getIsLoading().set(false);
            }
        }
    }

//    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
//    public void onEvent(NotificationService notificationService) {
//        if (notificationService.getClick_action().equals(ActionClick.TARGET_BILL_LADING_INFO_ACTIVITY)) {
//            ToastUtils.showToast(getActivity(), getString(R.string.create_order_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
//            getActivity().finish();
//        }
//    }

    private void showConfirmDeleteSubscribe() {
        dialogConfirm = new DialogConfirm(getString(R.string.confirm_un_select_subscribe), getString(R.string.msg_unsubscribe), v -> {
            orderVM.getBillLadingField().get().setSubscribe(null);
            orderVM.getBillLadingField().get().setFrequency(null);
            orderVM.getBillLadingField().get().setEndDate(null);
            orderVM.getBillLadingField().notifyChange();
            dialogConfirm.dismiss();
            orderVM.priceCalculate();
        });
        dialogConfirm.show(getChildFragmentManager(), "");
    }

    public void selectSub() {
        DialogSubscribe dialog = new DialogSubscribe(selectedSubs -> {
            orderVM.getBillLadingField().get().setSubscribe(selectedSubs);
            orderVM.getBillLadingField().get().setEndDate(selectedSubs);

            orderVM.getBillLadingField().notifyChange();
            if (recurrentModel == null) {
                recurrentModel = new RecurrentModel();
            }
            recurrentModel.setSubscribe(selectedSubs);
            recurrentModel.setFrequency(null);
            orderVM.getBillLadingField().get().setFrequency(null);
            mBinding.txtFrequency.setText(null);
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    public void selectOrderPackage() {
        dialogFragment = new ListDialogFragment(R.layout.item_order_package, R.string.select_order_package,
                "", orderVM.getOrderPackages(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                orderVM.getBillLadingField().get().setOrder_package((OrderPackage) o);
                orderVM.getBillLadingField().notifyChange();
                orderVM.priceCalculate();
                dialogFragment.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        dialogFragment.show(getChildFragmentManager(), dialogFragment.getTag());
    }

    public void selectStartDate() {
        Calendar c = Calendar.getInstance();
        if (orderVM.getBillLadingField().get().getStart_date() != null) {
            c.setTime(orderVM.getBillLadingField().get().getStart_date());
        }
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (view, year1, month1, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year1, month1, dayOfMonth);
            orderVM.getBillLadingField().get().setStart_date(new OdooDate(calendar.getTime().getTime()));
            orderVM.getBillLadingField().get().setEndDate(orderVM.getBillLadingField().get().getSubscribe());
            orderVM.getBillLadingField().notifyChange();
        }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
        datePickerDialog.show();

    }

    public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        return AppController.formatDate.format(c.getTime());
    }

    public void selectInsurance() {
        dialogFragment = new ListDialogFragment(R.layout.item_insurance, R.string.select_insurance
                , (listVM, runUi) -> listVM.getInsurance(runUi)
                , this);
        dialogFragment.show(getChildFragmentManager(), dialogFragment.getTag());
    }

    public void clearInsurance() {
        orderVM.getBillLadingField().get().setInsurance(null);
        orderVM.getBillLadingField().notifyChange();
        orderVM.priceCalculate();
    }

    public void selectFrequency() {

        RecurrentDialog dialog = new RecurrentDialog(recurrentModel, result -> {
            String temp = "";
            switch (result.getFrequency()) {
                case 1:
                    temp = getString(R.string.repeat) + " " + getString(R.string.every_day);
                    break;
                case 2:
                    temp = getString(R.string.repeat_every) + " " + getDayOfWeekStr(result.getDay_of_week());
                    break;
                case 3:
                    temp = getString(R.string.repeat_at) + " " + result.getDay_of_month() + " " + getString(R.string.every_month);
                    break;
            }
            mBinding.txtFrequency.setText(temp);
            mBinding.spSelectFrequency.setError("");
            recurrentModel = result;
            orderVM.getBillLadingField().get().setFrequency(recurrentModel.getFrequency());
            orderVM.getBillLadingField().get().setDay_of_month(recurrentModel.getDay_of_month());
            orderVM.getBillLadingField().get().setDay_of_week(recurrentModel.getDay_of_week());
            orderVM.priceCalculate();

        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }


    public String getDayOfWeekStr(int dayOfWeek) {
        return new DateFormatSymbols(getResources().getConfiguration().locale).getWeekdays()[dayOfWeek + 1];
    }


    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemInsurance) {
            onSelectedInsurance((Insurance) o);
        }
    }

    private void onSelectedInsurance(Insurance o) {
        orderVM.getBillLadingField().get().setInsurance(o);
        orderVM.getBillLadingField().notifyChange();
        orderVM.priceCalculate();
        dialogFragment.dismiss();
    }

    public void createOrder() {
        //1. show confirm dialog
        System.out.println("confirm");
        orderVM.setAddressWarehouse();
        boolean isValid = true;
        if (orderVM.getBillLadingField().get().getSubscribe() != null) {
            if (orderVM.getBillLadingField().get().getFrequency() == null) {
                mBinding.spSelectFrequency.setError(getString(R.string.INVALID_FIELD));
                isValid = false;
            }
        }
        if (isValid) {
            dialogConfirm = new DialogConfirm(getString(R.string.create_order), getString(R.string.msg_create_order), v -> {
                orderVM.createOrder(OrderConfirmFragment.this::runUi);
                dialogConfirm.dismiss();
            });
            dialogConfirm.showNow(getFragmentManager(), "confirmcreateorder");
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "createOrderSuccess":
                ToastUtils.showToast(getActivity(), getString(R.string.create_order_success), getResources().getDrawable(R.drawable.ic_done_white_48dp));
                getActivity().finish();
                break;
            case "createOrderFail":
                ToastUtils.showToast(getActivity(), getString(R.string.create_order_fail), getResources().getDrawable(R.drawable.ic_danger));
                break;
            case "getOrderPackage":
                for (BaseModel orderPackage : orderVM.getOrderPackages()) {
                    if (OrderPackageType.NORMAL.equals(((OrderPackage) orderPackage).getType())) {
                        orderVM.getBillLadingField().get().setOrder_package((OrderPackage) orderPackage);
                        orderVM.getBillLadingField().notifyChange();
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.order_confirm_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVMV2.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v -> {
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }
}
