package com.nextsolution.vancustomer.customer;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.ReceiptWarehouseAdapter;
import com.nextsolution.vancustomer.customer.vm.OrderVMV2;
import com.nextsolution.vancustomer.databinding.ReceiptWarehouseFragmentBinding;
import com.nextsolution.vancustomer.enums.WarehouseType;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.util.KeyboardUtil;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

public class ReceiptWarehouseFragment extends BaseFragment implements NetworkManager.NetworkHandler {
    private ReceiptWarehouseFragmentBinding mBinding;
    private OrderVMV2 orderVM;
    private ReceiptWarehouseAdapter warehouseAdapter;
    private SelectWareHouseDialog selectWareHouseDialog;

    boolean isOnline;
    private NetworkManager networkManager;


    @Override
    public void onResume() {
        super.onResume();
        warehouseAdapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        orderVM = ViewModelProviders.of(getBaseActivity()).get(OrderVMV2.class);
        binding.setVariable(BR.viewModel, orderVM);
        mBinding = (ReceiptWarehouseFragmentBinding) binding;
        initView();

        return binding.getRoot();

    }

    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void initView() {
        //kho đầu tiên là kho nhận
        //tính từ kho thứ 2
        warehouseAdapter = new ReceiptWarehouseAdapter(R.layout.item_receipt_warehouse, orderVM.getBillLadingField().get().getArrBillLadingDetail(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                openSelectWarehouse((BillLadingDetail) o);
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        recyclerView.setAdapter(warehouseAdapter);
    }

    private void openSelectWarehouse(BillLadingDetail o) {
        selectWareHouseDialog =
                new SelectWareHouseDialog(orderVM, o, new SelectWareHouseDialog.OnConfirm() {
                    @Override
                    public void onConfirm(BillLadingDetail bill) {
                        orderVM.getBillLadingField().get().getArrBillLadingDetail().set(o.index - 1, bill);
                        orderVM.getBillLadingField().notifyChange();
                        LatLng latLngImportWarehouse = new LatLng(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(o.index - 1).getWarehouse().getLatitude(),
                                orderVM.getBillLadingField().get().getArrBillLadingDetail().get(o.index - 1).getWarehouse().getLongitude());
                        LatLng latLngExportWarehouse = new LatLng(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getLatitude(),
                                orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getLongitude());
                        setExpDistanceExpect(latLngExportWarehouse, latLngImportWarehouse, o.index - 1);
                        warehouseAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onDelete(BillLadingDetail bill) {
                        orderVM.getBillLadingField().get().getArrBillLadingDetail().remove(o.index - 1);
                        orderVM.getSelectedWarehouses().remove(bill.getWarehouse().getId());
                        orderVM.getBillLadingField().notifyChange();
                        warehouseAdapter.notifyDataSetChanged();
                    }
                });
        selectWareHouseDialog.show(requireFragmentManager(), selectWareHouseDialog.getTag());
    }

    public void setExpDistanceExpect(LatLng start, LatLng end, Integer indexBillLadingDetail) {
        orderVM.getDistance(start, end, indexBillLadingDetail);
    }

    public void openSelectWarehouse() {
        selectWareHouseDialog = new SelectWareHouseDialog(orderVM, new SelectWareHouseDialog.OnConfirm() {
            @Override
            public void onConfirm(BillLadingDetail bill) {
                bill.setWarehouse_type(WarehouseType.Import);//kho nhập

                LatLng latLngImportWarehouse = new LatLng(bill.getWarehouse().getLatitude(),
                        bill.getWarehouse().getLongitude());

                LatLng latLngExportWarehouse = new LatLng(orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getLatitude(),
                        orderVM.getBillLadingField().get().getArrBillLadingDetail().get(0).getWarehouse().getLongitude());

                orderVM.getBillLadingField().get().getArrBillLadingDetail().add(bill);
                setExpDistanceExpect(latLngExportWarehouse, latLngImportWarehouse, orderVM.getBillLadingField().get().getArrBillLadingDetail().size() - 1);
                orderVM.getBillLadingField().notifyChange();
                warehouseAdapter.notifyDataSetChanged();
            }

            @Override
            public void onDelete(BillLadingDetail bill) {
            }
        });
        selectWareHouseDialog.show(requireFragmentManager(), selectWareHouseDialog.getTag());
    }

    public void nextStep() {
        KeyboardUtil.hideKeyboard(getActivity());
        int validate = orderVM.isValidReceiptWarehouse();
        if (validate == 0) {
            EventBus.getDefault().post(2);
        } else if (validate == 2) {
            ToastUtils.showToast(getString(R.string.some_warehouse_is_empty));
        } else {
            ToastUtils.showToast(getString(R.string.please_split_all_package));
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.receipt_warehouse_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVMV2.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcWareHouse;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v -> {
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }
}
