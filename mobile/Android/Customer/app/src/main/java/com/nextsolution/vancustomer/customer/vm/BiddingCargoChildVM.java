package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.util.Log;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingCargoChildVM extends BaseViewModel {
    ObservableBoolean orderByNewest = new ObservableBoolean(true);
    ObservableBoolean isLoading = new ObservableBoolean();
    ObservableBoolean emptyData = new ObservableBoolean();
    ObservableField<String> txtSearch = new ObservableField<>();
    ObservableList<BiddingOrder> biddingOrders = new ObservableArrayList<>();
    Integer offset = 0;
    Integer totalRecords = 0;

    int type = 0;

    public BiddingCargoChildVM(@NonNull Application application) {
        super(application);
        txtSearch.set("");
    }

    public void getData(Integer type, List<Integer> status, RunUi runUi) {
        getBiddingCargo(type, status, false, runUi);
    }

    /**
     * @param type   0: xuất hàng - depotId = fromDepotId, 1: nhập hàng - depotId = toDepotId
     * @param status trạng thái của đơn
     */
    private void getBiddingCargo(Integer type, List<Integer> status, Boolean isLoadMore, RunUi runUi) {
        isLoading.set(true);
        if (!isLoadMore) {
            biddingOrders.clear();
            offset = 0;
            totalRecords = 0;
        }
        DepotAPI.getCargoBidding(type, status, orderByNewest.get(), offset, txtSearch.get(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    getDataSuccess(runUi, o);
                } else {
                    runUi.run(Constants.GET_DATA_FAIL);
                }
                emptyData.set(biddingOrders.size() == 0);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void getDataSuccess(RunUi runUi, Object o) {
        List<BiddingOrder> resultDto = (List<BiddingOrder>) o;
        biddingOrders.addAll(resultDto);
        biddingOrders.size();
        runUi.run(Constants.GET_DATA_SUCCESS);
    }

    public void loadMore(int type, List<Integer> status, RunUi runUi) {
        offset += 10;
        if (offset < totalRecords) {
            Log.d("getListOrderHistory ", "loadMore");
            getBiddingCargo(type, status, true, runUi);
        } else {
            runUi.run("noMore");
        }
    }
}
