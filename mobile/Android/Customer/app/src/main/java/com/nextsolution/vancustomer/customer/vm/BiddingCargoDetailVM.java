package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.db.dto.BiddingPackage;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingCargoDetailVM extends BaseViewModel<BiddingOrder> {
    ObservableBoolean isLoading = new ObservableBoolean();
    BiddingPackage biddingPackage= new BiddingPackage();
    ObservableField<BiddingOrder> biddingOrder = new ObservableField<>();
    List<CargoTypes> cargoTypes = new ArrayList<>();
    List<BiddingVehicle> biddingVehicles = new ArrayList<>();
    public BiddingCargoDetailVM(@NonNull Application application) {
        super(application);
    }


    public void getBiddingCargoDetail(int biddingId, RunUi runUi) {
        isLoading.set(true);
        DepotAPI.getBiddingCargoDetail(biddingId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if(o != null) {
                    biddingPackage = (BiddingPackage) o;
                    biddingOrder.set(biddingPackage.getBidding_order());
                    cargoTypes.addAll(biddingPackage.getBidding_order().getCargo_types());
                    biddingVehicles.addAll(biddingOrder.get().getBidding_vehicles());
                    runUi.run(Constants.GET_DATA_SUCCESS);
                }else {
                    runUi.run(Constants.GET_DATA_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }
}
