package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingCargoVM extends BaseViewModel {
    String txtSearch="";
    ObservableBoolean hideMaps=new ObservableBoolean();
    //type: 0-Xuất, 1:Nhập
    ObservableInt type = new ObservableInt(0);
    BiddingOrder biddingOrder= new BiddingOrder();
    public BiddingCargoVM(@NonNull Application application) {
        super(application);
        hideMaps.set(false);
    }
    public void getBiddingCargo(int type, List<Integer> status, RunUi runUi){
        DepotAPI.getCargoBidding(type,  status, true,0, txtSearch, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(runUi,o);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    private void getDataSuccess(RunUi runUi,Object o){
        List<BiddingOrder> resultDto = (List<BiddingOrder>) o;
            if(resultDto.size()>0){
                biddingOrder=resultDto.get(0);
                runUi.run(Constants.GET_DATA_SUCCESS);
            }else{
                runUi.run(Constants.GET_DATA_FAIL);
            }


    }
}
