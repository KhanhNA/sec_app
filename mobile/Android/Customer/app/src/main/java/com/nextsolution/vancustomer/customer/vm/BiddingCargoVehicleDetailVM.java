package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.DepotAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiddingCargoVehicleDetailVM extends BaseViewModel {

    ObservableBoolean isLoading = new ObservableBoolean();

    ObservableField<BiddingOrder> biddingOrder= new ObservableField<>();
    ObservableField<BiddingVehicle> biddingVehicle= new ObservableField<>();
    ObservableList<CargoTypes> cargoTypes = new ObservableArrayList<>();
    ObservableBoolean isSuccess = new ObservableBoolean();
    public BiddingCargoVehicleDetailVM(@NonNull Application application) {
        super(application);
        isSuccess.set(false);
    }
    public void confirmBiddingCargoVehicle(RunUi runUi){
        isLoading.set(true);
        DepotAPI.confirmBiddingCargoVehicle(biddingOrder.get().getId(), biddingVehicle.get().getId(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                Boolean aBoolean= (Boolean) o;
                if(aBoolean!=null && aBoolean){
                    runUi.run(Constants.GET_DATA_SUCCESS);
                }else{
                    runUi.run(Constants.GET_DATA_FAIL);
                }

            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
}
