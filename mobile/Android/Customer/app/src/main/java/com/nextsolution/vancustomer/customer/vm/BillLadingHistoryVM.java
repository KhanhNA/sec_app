package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.util.BillLadingHistoryStatus;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillLadingHistoryVM extends BaseViewModel {
    private ObservableField<Boolean> emptyData = new ObservableField<>();
    private ObservableField<Boolean> isLoading = new ObservableField<>(false);
    private ObservableField<String> orderCode = new ObservableField<>();
    private ObservableField<String> status = new ObservableField<>();
    private List<BillLading> listBillByDay;
    private ObservableField<String> fromDate;
    private ObservableField<String> toDate;
    private Integer offset = 0;
    private Integer totalRecord;
    Date fDate = new Date();
    Date tDate =new Date();

    public void init(RunUi runUi) {
        isLoading.set(true);
        status.set(BillLadingHistoryStatus.all.toString());
        getData(runUi, false);
    }

    public BillLadingHistoryVM(@NonNull Application application) {
        super(application);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        fDate.setTime(cal.getTime().getTime());
        tDate = Calendar.getInstance().getTime();
        toDate = new ObservableField<>(MyDateUtils.convertDateToString(tDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        fromDate = new ObservableField<>(MyDateUtils.convertDateToString(fDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        listBillByDay = new ArrayList<>();
    }


    public void getData(RunUi runUi, Boolean loadMore) {
        if (!loadMore) {
            listBillByDay.clear();
            offset = 0;
        }
        OrderApi.getListOrderHistory(fDate, tDate, orderCode.get(), status.get(), offset, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                getListData(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void getListData(Object response, RunUi runUi) {
            OdooResultDto<BillLading> resultDto = (OdooResultDto<BillLading>) response;
        if (resultDto!=null && resultDto.getRecords()!=null && resultDto.getRecords().size() > 0) {
                totalRecord = resultDto.getTotal_record();
                this.listBillByDay.addAll(resultDto.getRecords());
                runUi.run(Constants.GET_DATA_SUCCESS);
            } else {
                runUi.run(Constants.GET_DATA_FAIL);
            }
    }

    public void loadMore(RunUi runUi) {
        offset += 10;
        if (offset < totalRecord) {
            isLoading.set(true);
            getData(runUi, true);
        } else {
            runUi.run("noMore");
        }
    }


}
