package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillLadingInfoVM extends BaseViewModel {
    private ObservableField<Boolean> emptyData = new ObservableField<>();
    ObservableField<Boolean> isLoading = new ObservableField<>();
    ObservableField<BillLading> billLading;
    ObservableList<BillLadingDetail> billLadingDetails;

    public BillLadingInfoVM(@NonNull Application application) {
        super(application);
        billLading = new ObservableField<>();
        billLadingDetails = new ObservableArrayList<>();
        emptyData.set(false);
        isLoading.set(true);
    }

    public void getData(Integer id, RunUi runUi) {

        OrderApi.getBillLadingDetail(id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getListData(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });


    }

    public void getListData(Object o, RunUi runUi) {
        OdooResultDto<BillLading> resultDto = (OdooResultDto<BillLading>) o;
        if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
            billLading.set(resultDto.getRecords().get(0));
            billLadingDetails.clear();
            if (billLading.get().getArrBillLadingDetail() != null) {
                billLadingDetails.addAll(billLading.get().getArrBillLadingDetail());
                runUi.run(Constants.GET_DATA_SUCCESS);
            } else {
                runUi.run(Constants.GET_DATA_FAIL);
            }
        } else {
            runUi.run(Constants.GET_DATA_FAIL);
        }
        isLoading.set(false);
    }

}
