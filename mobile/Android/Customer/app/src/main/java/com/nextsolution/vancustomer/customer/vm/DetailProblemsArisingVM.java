package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.db.dto.ServiceArisingDTO;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailProblemsArisingVM extends BaseViewModel {
    ObservableField<RoutingDay> routingDay =  new ObservableField<>();
    ObservableField<Driver> driver =  new ObservableField<>();
    ObservableField<Vehicle> vehicle= new ObservableField<>();
    ObservableList<ServiceArisingDTO> sosTypes = new ObservableArrayList<>();
    public DetailProblemsArisingVM(@NonNull Application application) {
        super(application);
    }
    public void getServiceArising(Integer routingDayDetailID, RunUi runUi) {

        RoutingApi.GetServiceArising(routingDayDetailID, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        try {

            OdooResultDto<ServiceArisingDTO> resultDto = (OdooResultDto<ServiceArisingDTO>) o;
            sosTypes.clear();
            if(resultDto.getRecords().size()>0){
                sosTypes.addAll(resultDto.getRecords());
            }
            runUi.run(Constants.GET_DATA_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
