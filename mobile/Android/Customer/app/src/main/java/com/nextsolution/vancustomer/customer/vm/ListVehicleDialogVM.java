package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.nextsolution.db.dto.BiddingVehicle;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListVehicleDialogVM extends BaseViewModel {
    ObservableList<BiddingVehicle> biddingVehicles= new ObservableArrayList<>();
    public ListVehicleDialogVM(@NonNull Application application) {
        super(application);
    }
}
