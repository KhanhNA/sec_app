package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import com.google.android.gms.maps.model.LatLng;
import com.haibin.calendarview.Calendar;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;


import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseViewModel;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import lombok.Getter;
import lombok.Setter;


/**
 *
 */
@Getter
@Setter
public class OrderVM extends BaseViewModel {
    ObservableField<Boolean> isLoading = new ObservableField<>();
    ObservableField<Boolean> isSpinKitView = new ObservableField<>();
    ObservableBoolean isConfirm = new ObservableBoolean();
    private ObservableField<Boolean> emptyData = new ObservableField<>();

    String warehouse_code;

    HashMap<String, Calendar> mScheme;
    ObservableList<BillPackage> billPackages = new ObservableArrayList<>();
    Integer offset = Constants.FIRST_PAGE;
    String status;
    private Integer totalRecord = 0;
    ObservableList<RoutingDay> listRoutingDay = new ObservableArrayList<>();

    public OrderVM(@NonNull Application application) {
        super(application);
        isLoading.set(true);
        warehouse_code = "";
        mScheme = new HashMap<>();
        emptyData.set(false);
        isConfirm.set(true);
        isSpinKitView.set(true);
    }

    private void DumpData(List<OdooDate> result) {
        mScheme.clear();
        for (OdooDate temp : result) {
            mScheme.put(getSchemeCalendar(temp, 0xFF40db25, "20").toString(),
                    getSchemeCalendar(temp, 0xFF40db25, "20"));
        }

    }


    /**
     * @param date  ngày
     * @param color màu
     * @param text  progress
     * @return
     */
    private Calendar getSchemeCalendar(Date date, int color, String text) {
        java.util.Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int year = cal.get(java.util.Calendar.YEAR);
        int month = cal.get(java.util.Calendar.MONTH) + 1;
        int day = cal.get(java.util.Calendar.DAY_OF_MONTH);

        Calendar calendar = new Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        calendar.setSchemeColor(color);
        calendar.setScheme(text);
        return calendar;
    }

    public void getListDate(java.util.Date fromDate, java.util.Date toDate, RunUi runUi) {

        OrderApi.getListDate(fromDate, toDate,false, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<OdooDate> odooResultDto = (OdooResultDto<OdooDate>) o;

                if (odooResultDto != null && odooResultDto.getRecords() != null) {
                    List<OdooDate> listDate = odooResultDto.getRecords();
                    DumpData(listDate);

                    runUi.run("getListDate");
                } else {
                    runUi.run(Constants.GET_DATA_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });


    }

    public void getRoutingPlanDayById(Integer id, RunUi runUi) {

        RoutingApi.getRoutingPlanDayById(id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                responseRoutingById(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void responseRoutingById(Object var3, Object... params) {
        System.out.println("result" + var3);
        RoutingVanDay aRoute = (RoutingVanDay) var3;
        StaticData.addNewRoute(aRoute);
        RunUi runUi = (RunUi) params[0];
        runUi.run(aRoute);
    }


    public LatLng getLatLng(int index) {
        RoutingVanDay org = StaticData.getRoutingVanDays().get(index);
        LatLng orgLatLng = new LatLng(org.getLatitude(), org.getLongitude());
        return orgLatLng;
    }


    public void loadMore(Date date, RunUi runUi) {
        offset += 10;
        if (offset < totalRecord) {
            getBillByDate(date, true, runUi);
        } else {
            runUi.run("noMore");
        }
    }


    public void getBillByDate(Date date, Boolean loadMore, RunUi runUi) {
        isSpinKitView.set(true);
        isLoading.set(true);
        if (!loadMore) {
            offset = 0;
            listRoutingDay.clear();
        }
        OrderApi.getListByDate(date, warehouse_code, status, offset, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isSpinKitView.set(false);
                isLoading.set(false);
                OdooResultDto<RoutingDay> list = (OdooResultDto<RoutingDay>) o;
                if (list != null && list.getRecords() != null && list.getRecords().size() > 0) {
                    totalRecord = list.getTotal_record();
                    listRoutingDay.addAll(list.getRecords());
                    runUi.run("getBillByDate");
                } else {
                    runUi.run(Constants.GET_DATA_FAIL);
                }
            }

            @Override
            public void onFail(Throwable error) {
                isSpinKitView.set(false);
                isLoading.set(false);
            }
        });
    }
}
