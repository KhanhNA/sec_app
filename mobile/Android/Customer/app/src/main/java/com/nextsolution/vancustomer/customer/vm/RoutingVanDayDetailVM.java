package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingVanDayDetailVM extends BaseViewModel {
    List<BillLadingDetail> billLadingDetails;
    RoutingVanDay routingVanDay;
    BillLading billLading;
    Integer position;
    List<String> list_warehouse_name;
    public RoutingVanDayDetailVM(@NonNull Application application) {
        super(application);
        billLading= new BillLading();
        billLadingDetails = new ArrayList<>();
        list_warehouse_name= new ArrayList<>();
    }

    public void getData(RunUi runUi){
        List<BillLadingDetail> data= new ArrayList<>();
        BillLadingDetail billLadingDetail= new BillLadingDetail();
        billLadingDetail.setId(1);
        data.add(billLadingDetail);
        BillLadingDetail billLadingDetail1= new BillLadingDetail();
        billLadingDetail1.setId(2);
        data.add(billLadingDetail1);
        billLadingDetails.clear();
        billLadingDetails.addAll(data);
        runUi.run(Constants.GET_DATA_SUCCESS);
    }
}
