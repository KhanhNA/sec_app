package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.BillService;
import com.nextsolution.db.dto.Hub;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.util.StringUtils;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelectWareHouseVM extends BaseViewModel {

    private ObservableField<BillLadingDetail> billLadingDetail = new ObservableField<>();
    private OrderVMV2 orderVM;
    public SelectWareHouseVM(@NonNull Application application) {
        super(application);
        billLadingDetail.set(new BillLadingDetail());
    }

    /**
     *
     * @return 0: đúng
     * 1: sai thông tin kho, thời gian
     * 2: sai thông tin package
     */
    public int addReceiptWareHouse() {
        int isValid = 0;
        BillLadingDetail receipt = billLadingDetail.get();
        assert receipt != null;
        if(receipt.getWarehouse() == null){
            isValid = 1;
            addError("warehouse", R.string.warehouse_is_not_empty);
        }else {
            clearErro("warehouse");
            if(StringUtils.isNullOrEmpty(receipt.getWarehouse().getPhone())){
                isValid = 1;
                addError("phone", R.string.INVALID_FIELD);
            }else {
                clearErro("phone");
            }
        }
//
//        if(receipt.getExpected_from_time() == null){
//            isValid = 1;
//            addError("fromTime", R.string.INVALID_FIELD);
//        }else {
//            clearErro("fromTime");
//        }
//
//        if(receipt.getExpected_to_time() == null){
//            isValid = 1;
//            addError("toTime", R.string.INVALID_FIELD);
//        }else {
//            clearErro("toTime");
//        }

        List<BillPackage> receiptList = billLadingDetail.get().getBillPackages();
        boolean isFull = true;
        for (int i = receiptList.size() - 1; i >= 0; i--) {
            if (receiptList.get(i).isSelect()) {
                if(receiptList.get(i).getQuantity_package() > receiptList.get(i).getRest()){
                    return 3;
                }
                if(receiptList.get(i).getQuantity_package() == 0){
                    receiptList.get(i).setSelect(false);
                    continue;
                }
            }else {
                receiptList.get(i).setQuantity_package(0);
            }
            if( receiptList.get(i).getQuantity_package() != 0){
                isFull = false;
            }
        }

        if(isFull){
            isValid = 2;
            addError("fullPackage", R.string.INVALID_FIELD);
        }else {
            clearErro("fullPackage");
        }

        return isValid;
    }

    public AreaDistance getAreaDistance(Hub from, Hub to, int type){
        AreaDistance areaDistance = new AreaDistance();
        areaDistance.setType(type);

        areaDistance.setFromLocation(new LatLng(from.getLatitude(), from.getLongitude()));
        areaDistance.setToLocation(new LatLng(to.getLatitude(), to.getLongitude()));

        areaDistance.setFrom_name_seq(from.getName_seq());
        areaDistance.setTo_name_seq(to.getName_seq());
        areaDistance.setFrom_warehouse_name(from.getName());
        areaDistance.setTo_warehouse_name(to.getName());

        return  areaDistance;
    }

}
