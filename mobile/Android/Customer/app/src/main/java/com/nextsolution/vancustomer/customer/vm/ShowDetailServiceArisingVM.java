package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.ServiceArisingDTO;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.Vehicle;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class    ShowDetailServiceArisingVM extends BaseViewModel {
    ObservableField<Driver> driver= new ObservableField<>();
    ObservableField<Vehicle> vehicle= new ObservableField<>();
    ObservableList<ServiceArisingDTO> sosTypes = new ObservableArrayList<>();
    ObservableBoolean emptyData= new ObservableBoolean();
    public ShowDetailServiceArisingVM(@NonNull Application application) {
        super(application);
        emptyData.set(true);
    }

    public void getServiceArising(Integer routingDayDetailID, RunUi runUi) {

        RoutingApi.GetServiceArising(routingDayDetailID, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        try {

            OdooResultDto<ServiceArisingDTO> resultDto = (OdooResultDto<ServiceArisingDTO>) o;
            sosTypes.clear();
            if(resultDto.getRecords().size()>0){
                emptyData.set(false);
                sosTypes.addAll(resultDto.getRecords());
            }
            else{
                emptyData.set(true);
            }
            runUi.run(Constants.GET_DATA_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
