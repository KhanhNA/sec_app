package com.nextsolution.vancustomer.enums;

public class BillRoutingStatus {
    public static String Cancel = "-1";//hủy toàn bộ đơn hàng
    public static String In_claim = "0";//Trong trạng thái sos hoặc sự cố.
    public static String Shipping = "1";
    public static String Success = "2";
    public static String Waiting = "3";

}
