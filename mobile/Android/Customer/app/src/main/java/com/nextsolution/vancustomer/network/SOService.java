package com.nextsolution.vancustomer.network;

import com.nextsolution.db.dto.Customer;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.model.ApiResponseModel;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface SOService {

    @Multipart
    @POST("/share_van_order/accept_package")
    Call<ResponseBody> confirmRouting(
            @Header("Cookie") String cookie,
            @Part("routingPlan") RequestBody body,
            @Part MultipartBody.Part[] files);

    @Multipart
    @POST("/share_van_order/create_individual_customer")
    Call<ResponseBody> createAccount(
            @Part("customer") RequestBody body,
            @Part("secret_key") String secret_key,
            @Part MultipartBody.Part[] files);
    @Multipart
    @POST("/customer/create_update_warehouse")
    Call<List<Warehouse>> createWarehouse(
            @Header("Cookie") String cookie,
            @Part("warehouse") RequestBody body);

    @Multipart
    @POST("/share_van_order/update_avatar")
    Call<ResponseBody> editAvatar(
            @Header("Cookie") String cookie,
            @Part MultipartBody.Part files);

    @Multipart
    @POST("/share_van_order/cancel_routing_once_day")
    Call<ApiResponseModel> cancelRoutingPlanDay(
            @Header("Cookie") String cookie,
            @Part("bill_lading_detail") Integer bill_lading_detail,
            @Part("description") String description);

    @FormUrlEncoded
    @POST("/mobile/logout")
    Call<ResponseBody> logOut(
            @Header("Cookie") String cookie,
            @Field("user_id") String user_id);

    @Multipart
    @POST("/location")
    Call<DistanceDTO> getDistance(
            @Part("f_lat") Double from_lat,
            @Part("f_lon") Double from_long,
            @Part("t_lat") Double to_lat,
            @Part("t_lon") Double to_long
    );
}
