package com.nextsolution.vancustomer.routing.maps;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.db.dto.Van;
import com.nextsolution.vancustomer.routing.RoutingVM;
import com.nextsolution.vancustomer.base.StaticData;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionView {
    private static final String TAG = "DirectionView";
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;

    private GoogleMap map;
    private Activity activity;
    private RoutingVM routingVM;
    FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    Polyline directPolyline = null;
    private boolean isExecute = false;
    public DirectionView(Activity activity, RoutingVM routingVM, GoogleMap map) {

        this.activity = activity;
        this.map = map;
        this.routingVM = routingVM;
//        buildGoogleApiClient();

        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
//                updateLocationMarker(locationResult);
            }
        };
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this.activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());
        }

    }

    private void updateLocationMarker(LocationResult locationResult){
        System.out.println("update location");
        if (locationResult == null) {
            return;
        }
        Van van = StaticData.getStaffVan().getVan();
        if(van == null){
            return;
        }
        Marker marker = van.getMarker();
        if(marker == null){
            return;
        }
        van.setLatitude(locationResult.getLastLocation().getLatitude());
        van.setLongitude(locationResult.getLastLocation().getLongitude());
        marker.setPosition(new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude()));
        reDrawRoute();
    }
    public void reDrawRoute(){
        if(directPolyline != null){
            directPolyline.remove();
        }
        Van van = StaticData.getStaffVan().getVan();
        if(van == null || van.getLatitude() == null || van.getLongitude() == null){
            return;
        }
        RoutingVanDay currentRouteProced = StaticData.getCurrentRouteProcessed();
        if(currentRouteProced == null || currentRouteProced.getLatitude() == null ||
                currentRouteProced.getLongitude() == null){
            return;
        }
        LatLng origin = new LatLng(van.getLatitude(), van.getLongitude());
        LatLng dest = new LatLng(currentRouteProced.getLatitude(), currentRouteProced.getLongitude());
        direct(origin, dest);
    }
    private void direct(LatLng origin, LatLng dest){
        String url = getDirectionsUrl(origin, dest);
//        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        new DownloadDirection().execute(url);

    }



    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
//        private GoogleMap map;
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions polyline = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                polyline = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyline = polyline.addAll(points);
//                lineOptions.width(12);
                polyline.color(Color.RED);
                polyline.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            if(polyline != null && polyline.getPoints() != null && polyline.getPoints().size() > 0) {
                directPolyline = map.addPolyline(polyline);

//                map.moveCamera(CameraUpdateFactory.newLatLng(lineOptions.getPoints().get(0)));
            }
        }

    }

    private class DownloadDirection extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = getDirection(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
//        parserTask.setMap(map);

            parserTask.execute(result);
            isExecute = false;
        }

        /**
         * A method to download json data from url
         */
        private String getDirection(String strUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(strUrl);

                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.connect();

                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb = new StringBuffer();

                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                data = sb.toString();

                br.close();

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        }


    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving&key=AIzaSyCmzEKuZOtAuDR5-iHmJvLScbSolUJEhBk";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }
    public GoogleMap getMap() {
        return map;
    }

    public void setMap(GoogleMap map) {
        this.map = map;
    }

}
