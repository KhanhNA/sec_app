package com.nextsolution.vancustomer.service.service_dto;

public class NoteEvent {
    private String mNote;

    public NoteEvent(String note) {
        mNote = note;
    }

    public String getNote() {
        return mNote;
    }
}
