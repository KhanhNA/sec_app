package com.nextsolution.vancustomer.ui;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.king.zxing.Intents;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.nextsolution.db.api.DriverApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.EnumUserRole;
import com.nextsolution.db.dto.NotificationDTO;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.PagerAdapter;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.customer.BiddingCargoFragment;
import com.nextsolution.vancustomer.customer.CargoViewPagerFragment;
import com.nextsolution.vancustomer.customer.HomeFragment;
import com.nextsolution.vancustomer.customer.InfoCustomerFragment;
import com.nextsolution.vancustomer.customer.ListRoutingVanDayActivity;
import com.nextsolution.vancustomer.customer.OrderFragment;
import com.nextsolution.vancustomer.customer.OrderFragmentV2;
import com.nextsolution.vancustomer.databinding.ActivityMainBinding;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.ActionClick;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.nextsolution.vancustomer.service.service_dto.NotificationService;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.ui.fragment.NotificationFragment;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.view_by_bill_lading.ListBillLadingActivity;
import com.nextsolution.vancustomer.viewmodel.MainVM;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import pub.devrel.easypermissions.EasyPermissions;


public class MainActivity extends BaseActivity<ActivityMainBinding> implements NetworkManager.NetworkHandler{
    private final int REQUEST_PERMISSION_CAMERA = 1001;
    // The current item selected (bottom navigation)
    private MenuItem preItem;

    MainVM mainVM;
    Integer totalNotification;
    BadgeDrawable badgeDrawable;
    OrderFragment orderFragment;
    HomeFragment homeFragment;
    // the current displayed fragment
    private String mCurrentFragmentTag;

    AddFloatingActionButton mFabMain;

    NotificationFragment notificationFragment;
    boolean isOnline;
    private NetworkManager networkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        navigationView();
        getTokenFireBase();
        mainVM = (MainVM) viewModel;
        mFabMain = binding.getRoot().findViewById(R.id.fab_expand_menu_button);
        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                openIntentFromNotification(extras);
            }
        }
        // Delete cache file
        PictureFileUtils.deleteCacheDirFile(this, PictureMimeType.ofImage());
        checkNotification();
    }

    public void checkNotification() {
        badgeDrawable = binding.navigation.getOrCreateBadge(R.id.actionNotification);
        totalNotification = (AppController.getInstance().getSharePre().getInt(Constants.IS_NOTIFICATION, -1));
        if (totalNotification > 0) {
            badgeDrawable.setVisible(true);
            badgeDrawable.setBackgroundColor(getResources().getColor(R.color.red));
        } else {
            badgeDrawable.setVisible(false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // check connected network
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
        // Notification
        checkNotification();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(String action) {
        if (action.equals("confirm routing success")) {
            homeFragment.reFresh();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NotificationService notificationService) {

        try {
            if (ActionClick.NOTIFICATION_SYSTEM.equals(notificationService.getClick_action()) ||
                    ActionClick.NOTIFICATION_ROUTING_DAY.equals(notificationService.getClick_action())) {
                orderFragment.reFresh();
                homeFragment.reFresh();
                if (binding.mainViewPager.getCurrentItem() == 1) {
                    SharedPreferences.Editor editor = AppController.getInstance().getSharePre().edit();
                    editor.putInt(Constants.IS_NOTIFICATION, 1);
                    editor.commit();
                    badgeDrawable.setVisible(true);
                }
                Gson gson = new GsonBuilder()
                        .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                        .create();
                NotificationDTO notificationModel = gson.fromJson(notificationService.getMess_object(), NotificationDTO.class);
                notificationFragment.pushNotification(notificationModel);

            } else if (ActionClick.NOTIFICATION_BILL_ROUTING_DETAIL.equals(notificationService.getClick_action())) {
                homeFragment.reFresh();
                orderFragment.reFresh();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getTokenFireBase() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    // Luu token
                    AppController.getInstance().getEditor().putString("token_fb", token).apply();

                    DriverApi.updateToken(token, new SharingOdooResponse() {
                        @Override
                        public void onSuccess(Object o) {
                            Log.e("token_firebase", token);
                        }

                        @Override
                        public void onFail(Throwable error) {

                        }
                    });

                });
    }

    private void navigationView() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        orderFragment = new OrderFragment(this, new OrderFragment.IReloadRoutingDay() {
            @Override
            public void reloadRoutingDay() {
                homeFragment.reFresh();
            }
        });
        homeFragment = new HomeFragment(this, new HomeFragment.IReloadRoutingDay() {
            @Override
            public void reloadRoutingDay() {
                orderFragment.reFresh();
            }
        });
        myPagerAdapter.addFragment(homeFragment);
        myPagerAdapter.addFragment(orderFragment);

        if (StaticData.getPartner().getStaff_type_name() != null && StaticData.getPartner().getStaff_type_name().equals(EnumUserRole.SHAREVAN_STOCKMAN)) {
            BiddingCargoFragment biddingCargoFragment = new BiddingCargoFragment(this);
            myPagerAdapter.addFragment(biddingCargoFragment);
        }
        notificationFragment = new NotificationFragment();
        myPagerAdapter.addFragment(notificationFragment);

        InfoCustomerFragment infoCustomerFragment = new InfoCustomerFragment(this);
        myPagerAdapter.addFragment(infoCustomerFragment);


        binding.mainViewPager.setAdapter(myPagerAdapter);

        binding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.homeFragment:
                    binding.mainViewPager.setCurrentItem(0);
                    break;
                case R.id.actionHome:
                    binding.mainViewPager.setCurrentItem(1);
                    break;
                case R.id.actionBidding:
                    binding.mainViewPager.setCurrentItem(2);
                    break;
                case R.id.actionNotification:
                    if (EnumUserRole.SHAREVAN_STOCKMAN.equals(StaticData.getPartner().getStaff_type_name())) {
                        binding.mainViewPager.setCurrentItem(3);
                    } else {
                        binding.mainViewPager.setCurrentItem(2);
                    }
                    SharedPreferences.Editor editor = AppController.getInstance().getSharePre().edit();
                    editor.putInt(Constants.IS_NOTIFICATION, 0);
                    editor.apply();
                    badgeDrawable.setVisible(false);
                    break;
                case R.id.actionProfile:
                    if (EnumUserRole.SHAREVAN_STOCKMAN.equals(StaticData.getPartner().getStaff_type_name())) {
                        binding.mainViewPager.setCurrentItem(4);
                    } else {
                        binding.mainViewPager.setCurrentItem(3);
                    }
                    break;
            }
            return false;
        });
        binding.mainViewPager.setOffscreenPageLimit(3);
        binding.mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    binding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mCurrentFragmentTag = myPagerAdapter.getItem(position).getTag();
                binding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = binding.navigation.getMenu().getItem(position);
                showFloatingActionMenuIfRequired();
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });

        binding.actionCreateOrder.setOnClickListener(v -> {
            binding.multipleActions.collapse();
            Intent intent = new Intent(this, CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.FRAGMENT, OrderFragmentV2.class);
            intent.putExtras(bundle);
            startActivity(intent);
        });
        binding.actionScanQr.setOnClickListener(v -> {
            binding.multipleActions.collapse();
            String[] perms = {Manifest.permission.CAMERA};
            if (EasyPermissions.hasPermissions(this, perms)) {
                gotoQrScanActivity();
            } else {
                // Do not have permissions, request them now
                EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                        REQUEST_PERMISSION_CAMERA, perms);
            }
        });

        binding.actionCreateCargo.setOnClickListener(v -> {
            binding.multipleActions.collapse();
            Intent intent = new Intent(this, CommonActivity.class);
            intent.putExtra(Constants.FRAGMENT, CargoViewPagerFragment.class);
            startActivity(intent);
        });

//        binding.actionListBill.setOnClickListener(v -> {
//            binding.multipleActions.collapse();
//            Intent intent = new Intent(this, ListBillLadingActivity.class);
//            startActivity(intent);
//        });

    }

    private void gotoQrScanActivity() {
        Intent qrScan = new Intent(this, QrScannerActivity.class);
        startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN);
    }

    public void navigateToPosition(int position) {
        binding.mainViewPager.setCurrentItem(position);
    }

    private void revealFloatingActionMenu() {
        binding.multipleActions.collapse();
        ViewPropertyAnimator animator = mFabMain.animate().scaleX(1).scaleY(1).alpha(1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                binding.multipleActions.setVisibility(View.VISIBLE);
            }
        });
        animator.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            for (int item : grantResults) {
                if (item != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            gotoQrScanActivity();
        }
    }

    private void concealFloatingActionMenu() {
        binding.multipleActions.collapse();
        ViewPropertyAnimator animator = mFabMain.animate().scaleX(0).scaleY(0).alpha(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                binding.multipleActions.setVisibility(View.GONE);
            }
        });
        animator.start();
    }

    /**
     * @param fragmentTag fragment đang hiển thị
     * @param isShow      có hiển thị fab hay ko
     */
    public void animationFAB(String fragmentTag, boolean isShow) {
        synchronized (this) {
            boolean currentShow = binding.multipleActions.getVisibility() == View.VISIBLE;
            //nếu trạng thái hiện tại và trạng thái mong muốn bằng nhau thì ko làm gì cả
            if (currentShow == isShow) {
                return;
            }
            if (TextUtils.equals(mCurrentFragmentTag, fragmentTag)) {
                if (isShow) {
                    revealFloatingActionMenu();
                } else {
                    concealFloatingActionMenu();
                }
            }
        }
    }

    /**
     * Display the Floating Action Menu if it is required
     */
    private void showFloatingActionMenuIfRequired() {

        if (preItem.getItemId() == R.id.actionHome || preItem.getItemId() == R.id.actionBidding || preItem.getItemId() == R.id.homeFragment) {
            revealFloatingActionMenu();
        } else {
            concealFloatingActionMenu();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(Intents.Scan.RESULT)) {
            Intent intent = new Intent(getBaseActivity(), ListRoutingVanDayActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.ITEM_ID, data.getStringExtra(Intents.Scan.RESULT));
            bundle.putString(Constants.FROM_ACTIVITY, "MAIN_ACTIVITY");
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.REQUEST_ACTIVITY_RESULT);
        } else if (requestCode == Constants.REQUEST_ACTIVITY_RESULT) {
            if (resultCode == Constants.RESULT_OK) {
                orderFragment.listenerFromMainActivity(data.getStringExtra(Constants.ROUTING_CODE));
            }
//            else if (resultCode == Constants.RESULT_FAIL) {
//                ToastUtils.showToast(this, getString(R.string.QrCode_result_wrong), getDrawable(R.drawable.ic_fail));
//            }
        }
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                openIntentFromNotification(extras);
            }
        }

    }

    private void openIntentFromNotification(Bundle extras) {
        String routing_plan_code = extras.getString(Constants.ITEM_ID);
        Intent intent = new Intent(getBaseActivity(), ListRoutingVanDayActivity.class);
        intent.putExtra(Constants.ITEM_ID, routing_plan_code);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DialogConfirm dialogConfirm = new DialogConfirm(getResources().getString(R.string.notice), getResources().getString(R.string.msg_exit_app))
                .setOnClickListener(v -> finish());
        dialogConfirm.show(getSupportFragmentManager(), MainActivity.class.getName());
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MainVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            binding.tvNetwork.setVisibility(View.VISIBLE);
            binding.progressLayout.setVisibility(View.VISIBLE);
            binding.progressLayout.setOnClickListener(v->{
                Log.d("progress_click", "onNetworkUpdate");
            });
        } else {
            binding.progressLayout.setVisibility(View.GONE);
            binding.tvNetwork.setVisibility(View.GONE);
        }
    }


//    private void callAppChat(){
//        try {
//            Intent launchIntent = AppController.getInstance().getPackageManager().getLaunchIntentForPackage(Constants.chatAppId);
//            if (launchIntent!= null){
//                launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                Bundle bundle = AppController.getInstance().getDataApp();
////                bundle.putString("INTENT_USER_NAME");
////                bundle.putString("AVATAR_URL");
////                bundle.putString("DISPLAY_NAME");
//                launchIntent.putExtras(bundle);
//                startActivity(launchIntent);
//            } else {
//                // TODO: 1/7/2020 show dialog download chat app
//                ToastUtils.showToast("You must download mingalaba chat app");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private void showDialogLogout() {
//        DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.message_logout), "",
//                v -> {
//                    // Delete user & pass
//                    AppController.getInstance().getSharePre().edit().putString(Constants.USER_NAME, "").apply();
//                    AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
//                    // Delete cache merchant
//                    AppController.getInstance().clearCache();
//                    // Delete local db
//                    // Intent loginActivity
//                    Intent intent = new Intent(getBaseActivity(), LoginActivityV2.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//                    //cập nhật lại số đơn hàng khi đăng nhập lại.
//                    startActivity(intent);
//                });
//        dialogConfirm.show(getSupportFragmentManager(), dialogConfirm.getTag());
//    }


}
