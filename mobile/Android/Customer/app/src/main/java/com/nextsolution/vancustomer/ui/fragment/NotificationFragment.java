package com.nextsolution.vancustomer.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nextsolution.db.dto.NotificationDTO;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.PagerAdapter;
import com.nextsolution.vancustomer.databinding.NotificationFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.viewmodel.NotificationVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class NotificationFragment extends BaseFragment {
    NotificationFragmentBinding mBinding;
    private MenuItem preItem;
    NotificationVM notificationVM;
    NotificationChildFragment routing;
    NotificationChildFragment system;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (NotificationFragmentBinding) binding;
        notificationVM = (NotificationVM) viewModel;
        navigationView();
        return v;
    }

    private void navigationView() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());
        //routing
        routing = new NotificationChildFragment(R.layout.item_notification_routing, Constants.TYPE_NOTIFICATION_ROUTING);

        //system
        system = new NotificationChildFragment(R.layout.item_notification_system, Constants.TYPE_NOTIFICATION_SYSTEM);
        //add Fragment to list fragment
        myPagerAdapter.addFragment(routing);
        myPagerAdapter.addFragment(system);

        mBinding.frameContainer.setAdapter(myPagerAdapter);
//        notificationVM.getNotification(this::runUi);
        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nvRouting:
                    mBinding.frameContainer.setCurrentItem(0);
                    break;
                case R.id.nvSystem:
                    mBinding.frameContainer.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }
    public void pushNotification(NotificationDTO model){
        if(model.getType().equals(Constants.TYPE_NOTIFICATION_SYSTEM)){
            system.pushNotification(model);
        }else if(model.getType().equals(Constants.TYPE_NOTIFICATION_ROUTING)){
            routing.pushNotification(model);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.notification_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
