package com.nextsolution.vancustomer.view_by_bill_lading;

import android.app.Application;

import com.nextsolution.db.api.IResponse;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillRouting;
import com.nextsolution.db.dto.BillRoutingDetail;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.ApiResponseModel;
import com.nextsolution.vancustomer.util.RoutingDayStatus;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillRoutingDetailVM extends BaseViewModel<BillRouting> {

    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableBoolean isCancelOrder = new ObservableBoolean(); // true : được hủy, false: không được hủy

    public BillRoutingDetailVM(@NonNull Application application) {
        super(application);
        model.set(new BillRouting());
        isCancelOrder.set(true);
    }


    public void getBillRoutingDetail(int bill_routing_id, RunUi runUi) {
        isLoading.set(true);
        RoutingApi.getBillRoutingDetail(bill_routing_id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    model.set((BillRouting) o);
                    checkCanCancelOrder(((BillRouting) o).getArrBillLadingDetail());
                    runUi.run("getBillRoutingSuccess");
                } else {
                    runUi.run("getBillRoutingFail");
                }
                isLoading.set(false);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    private void checkCanCancelOrder(List<BillRoutingDetail> billRoutingDetails) {
        if (billRoutingDetails != null) {
            for (BillRoutingDetail item : billRoutingDetails) {
                if(item.getStatus().equals("2")){
                    isCancelOrder.set(false);
                    return;
                }
            }
        }
    }

    public void cancelOrder(String reason, RunUi runUi) {
        isLoading.set(true);
        //model.get().getArrBillLadingDetail().get(0).getId() tương ứng với id của bill routing
        OrderApi.cancelOrder(model.get().getArrBillLadingDetail().get(0).getId(), reason, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null && ((ApiResponseModel) o).status == 200) {
                    runUi.run("updateOrderSuccess");
                } else {
                    runUi.run("updateOrderFail");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }


}
