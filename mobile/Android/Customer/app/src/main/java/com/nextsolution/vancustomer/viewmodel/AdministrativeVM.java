package com.nextsolution.vancustomer.viewmodel;

import android.app.Application;

import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.WarehouseApi;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.model.Administrative;
import com.tsolution.base.BaseViewModel;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdministrativeVM extends BaseViewModel {
    private List<Administrative> lstAdministrative;
    private ObservableBoolean isLoading = new ObservableBoolean();
    public AdministrativeVM(@NonNull Application application) {
        super(application);
        lstAdministrative = new ArrayList<>();
    }

    public void getAdministrative(int parentId, RunUi runUi) {
        isLoading.set(true);
        lstAdministrative.clear();
        WarehouseApi.getAdministrative(parentId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                lstAdministrative.addAll((Collection<? extends Administrative>) o);
                runUi.run("getAdministrativeSuccess");
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("getAdministrativeFail");
            }
        });
    }

}
