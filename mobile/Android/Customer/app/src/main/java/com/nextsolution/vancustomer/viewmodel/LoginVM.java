package com.nextsolution.vancustomer.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.nextsolution.db.api.BaseApi;
import com.nextsolution.db.api.CustomerApi;
import com.nextsolution.db.api.NotificationAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.NotificationDTO;
import com.nextsolution.db.dto.Partner;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.http.HttpHelper;
import com.nextsolution.vancustomer.model.UserInfo;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.util.ApiResponse;
import com.nextsolution.vancustomer.enums.Constants;


import com.nextsolution.vancustomer.util.TsUtils;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.model.OdooSessionDto;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.RetrofitClient;
import com.tsolution.base.exceptionHandle.AppException;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

public class LoginVM extends BaseViewModel<UserInfo> {
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private UserInfo userInfo;
    SharedPreferences sharedPreferences;


    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }


    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", true));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        getConfigUrl();
        model.set(userInfo);
    }

    private void getConfigUrl() {
        AppController.SERVER_URL = sharedPreferences.getString("U_LOGIN", AppController.SERVER_URL);
        AppController.BILLING_URL = sharedPreferences.getString("U_BILLING", AppController.BILLING_URL);
        AppController.TRACKING_SOCKET = sharedPreferences.getString("U_SOCKET", AppController.TRACKING_SOCKET);
//        AppController.DISTANCE_URL = sharedPreferences.getString("U_DISTANCE", AppController.DISTANCE_URL);
        AppController.DATABASE = sharedPreferences.getString("U_DATABASE", AppController.DATABASE);
    }


    public void requestLogin() {
        if (model.get() != null) {
            UserInfo userInfo = model.get();
            getConfigUrl();
            BaseApi.requestLogin(userInfo.getUserName()
                    , userInfo.getPassWord()
                    , new IOdooResponse<OdooSessionDto>() {
                        @Override
                        public void onResponse(OdooSessionDto odooSessionDto, Throwable throwable) {
                            onLoginResponse(odooSessionDto, throwable);
                            if(AppController.getInstance().getSharePre().getInt(Constants.IS_NOTIFICATION,-1)==-1){
                                isNotification();
                            }
                        }
                    });
        }

    }


    private void getUserInfo(String fDate, Integer partnerId) {

//        CustomerApi.getCustomer(partnerId, new IResponse<OdooResultDto<Partner>>() {
//            @Override
//            public void onSuccess(OdooResultDto<Partner> o) {
//                getUserInfoSuccess(o, partnerId);
//            }
//        });
        CustomerApi.getInfoCustomer(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                OdooResultDto<Partner> partnerOdooResultDto = (OdooResultDto<Partner>) o;
                getUserInfoSuccess(partnerOdooResultDto, partnerId);
            }

            @Override
            public void onFail(Throwable error) {

            }

        });
    }


    private void handleError(Throwable throwable) {
        responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
        throwable.printStackTrace();
    }

    private void getUserInfoSuccess(OdooResultDto<Partner> odooResultDto, Object... objects) {

        if (odooResultDto != null && TsUtils.isNotNull(odooResultDto.getRecords())) {

            Partner partner = odooResultDto.getRecords().get(0);
//            partner.setStaff_type_name(EnumUserRole.SHAREVAN_STOCKMAN);
//            partner.setDepot_id(1);
            System.out.println("result:" + odooResultDto.toString());
            responseLiveData.postValue(ApiResponse.success(odooResultDto));
            StaticData.setPartner(partner);
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }


    //forget password
    public void sendPassword() {
        // TODO: 21/02/2020 send OTP
        try {
            view.action("sendOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void confirmOTP() {
        // TODO: 21/02/2020 confirm OTP
        try {
            view.action("confirmOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void onLoginResponse(OdooSessionDto odooSessionDto, Throwable volleyError) {
        if (volleyError == null && odooSessionDto != null) {
            StaticData.setOdooSessionDto(odooSessionDto);
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());

            SharedPreferences.Editor editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);

            StaticData.userName = getModelE().getUserName();
            StaticData.password = getModelE().getPassWord();

            editor.putString(Constants.USER_NAME, getModelE().getUserName());
            if (userInfo.getIsSave() != null && userInfo.getIsSave()) {
                editor.putString(Constants.MK, getModelE().getPassWord());
                editor.putBoolean("chkSave", true);
            } else {
                editor.putBoolean("chkSave", true);
                editor.putString(Constants.MK, "");
            }
            editor.commit();
            String fDate = "27/04/2020 00:00:00";
            getUserInfo(fDate, odooSessionDto.getPartner_id());

        } else {
            if(volleyError != null){
                volleyError.printStackTrace();
            }
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
            RetrofitClient.TOKEN = "";
            HttpHelper.TOKEN_DCOM = "";
        }
    }

    public void isNotification() {
        NotificationAPI.isNotification(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getDataSuccess(o);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o) {
        OdooResultDto<NotificationDTO> resultDto = (OdooResultDto<NotificationDTO>) o;
        try{
            SharedPreferences.Editor editor = AppController.getInstance().getEditor();
            if (resultDto != null && resultDto.getRecords() != null&&resultDto.getRecords().size()>0) {
                if(resultDto.getRecords().get(0).getTotal_message_not_seen()!=null)
                    editor.putInt(Constants.IS_NOTIFICATION,resultDto.getRecords().get(0).getTotal_message_not_seen() );
            }
            editor.commit();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
