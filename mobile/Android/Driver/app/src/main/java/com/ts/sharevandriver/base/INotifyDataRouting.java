package com.ts.sharevandriver.base;

public interface INotifyDataRouting {
    void notifyData(boolean isChange);
}
