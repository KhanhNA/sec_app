package com.ts.sharevandriver.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.ts.sharevandriver.base.AppController;
import com.tsolution.base.BaseModel;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CargoType extends BaseModel implements Cloneable{
    private Integer id;
    private Double length;
    private Double width;
    private String phone;
    private Double height;
    private String type;
    private Integer from_weight;
    private Integer to_weight;
    private String size_standard_seq;
    private Integer cargo_quantity;
    private Integer total_weight;
    private List<Cargo> cargos;


    //properties for ui
    private transient HashMap<String, Integer> selectedCargo;//những cargo đã dc quet
    private transient int screenType;//0:Chưa vận chuyển, 1: Đang vận chuyển, 2: Đã vận chuyển


    public String getLengthStr() {
        return this.length != null ? AppController.getInstance().formatNumber(length) : "";
    }

    public String getWidthStr() {
        return this.width != null ? AppController.getInstance().formatNumber(width) : "";
    }

    public String getHeightStr() {
        return this.height != null ? AppController.getInstance().formatNumber(height) : "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CargoType)) return false;
        CargoType that = (CargoType) o;
        return getId().equals(that.getId());
    }

    @Override
    public Object clone() {
        CargoType clone = null;
        try {
            clone = (CargoType) super.clone();
            clone.selectedCargo = (HashMap<String, Integer>) this.selectedCargo.clone();//deep clone hashmap
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
