package com.ts.sharevandriver.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Topic extends BaseModel {
    private String stt;
    private String topicNameEnglish;
    private String topicNameVietnamese;

    public Topic(String stt, String topicNameEnglish, String topicNameVietnamese) {
        this.stt = stt;
        this.topicNameEnglish = topicNameEnglish;
        this.topicNameVietnamese = topicNameVietnamese;
    }
}
