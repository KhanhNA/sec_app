package com.ts.sharevandriver.service.location_service;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
public class LocLatLng implements Serializable {
    public double latitude;
    public double longitude;
    public String time;

    public LocLatLng() {
    }

    public LocLatLng(double latitude, double longitude, String time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = time;
    }

}
