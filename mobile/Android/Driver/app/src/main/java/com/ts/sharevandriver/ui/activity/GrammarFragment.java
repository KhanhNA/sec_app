package com.ts.sharevandriver.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.GrammarFragmentBinding;
import com.ts.sharevandriver.ui.fragment.BaseEnglishFragment;
import com.ts.sharevandriver.viewmodel.GrammarVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.Objects;

public class GrammarFragment extends BaseFragment<GrammarFragmentBinding> {
    GrammarVM grammarVM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        grammarVM = (GrammarVM) viewModel;

        initToolBar();
        return v;
    }

    private void initToolBar() {
        binding.toolbar.setTitle(R.string.Grammar);
        getBaseActivity().setSupportActionBar(binding.toolbar);
        if (Objects.requireNonNull(getActivity()).getActionBar() != null) {
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
            getActivity().getActionBar().setDisplayShowHomeEnabled(true);
        }
        binding.toolbar.setNavigationOnClickListener(v -> getBaseActivity().onBackPressed());
    }

    @Override
    public int getLayoutRes() {
        return R.layout.grammar_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return GrammarVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onItemClick(View v, Object o) {
        super.onItemClick(v, o);
        switch (v.getId()) {
            case R.id.tv_base_eng:
                goToFrgBaseEnglish();
                break;
            case R.id.tv_clauses:
                goToClauseFragment();
                break;
            case R.id.tv_sound:
                goToAccentFragment();
                break;
            default:
        }
    }

    private void goToAccentFragment() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.FRAGMENT, BaseEnglishFragment.class);
        bundle.putString(Constants.KEY_BASE,"SOUND");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void goToClauseFragment() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.FRAGMENT, BaseEnglishFragment.class);
        bundle.putString(Constants.KEY_BASE,"CLAUSE");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void goToFrgBaseEnglish() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.FRAGMENT, BaseEnglishFragment.class);
        bundle.putString(Constants.KEY_BASE,"BASE_ENGLISH");
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
