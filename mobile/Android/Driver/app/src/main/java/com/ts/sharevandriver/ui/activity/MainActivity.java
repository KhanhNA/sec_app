package com.ts.sharevandriver.ui.activity;

import static com.ts.sharevandriver.base.Constants.PERMISSIONS_REQUEST_LOCATION;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.badge.BadgeDrawable;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.PagerAdapter;
import com.ts.sharevandriver.api.DriverApi;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.MainActivityBinding;
import com.ts.sharevandriver.enums.ClickAction;
import com.ts.sharevandriver.enums.ObjectStatus;
import com.ts.sharevandriver.enums.VehicleStateStatus;
import com.ts.sharevandriver.model.NotificationModel;
import com.ts.sharevandriver.service.location_service.AutostartReceiver;
import com.ts.sharevandriver.service.location_service.NetworkManager;
import com.ts.sharevandriver.service.location_service.TrackingService;
import com.ts.sharevandriver.service.notification_service.NotificationService;
import com.ts.sharevandriver.ui.fragment.BiddingFragment;
import com.ts.sharevandriver.ui.fragment.DialogConfirm;
import com.ts.sharevandriver.ui.fragment.MeFragment;
import com.ts.sharevandriver.ui.fragment.ProgrammesFragment;
import com.ts.sharevandriver.ui.fragment.WalletFragment;
import com.ts.sharevandriver.viewmodel.MainActivityVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends BaseActivity implements NetworkManager.NetworkHandler {
    private static final int ALARM_MANAGER_INTERVAL = 15000;
    boolean doubleBackToExitPressedOnce = false;
    BadgeDrawable badgeDrawable;
    Integer totalNotification;
    ProgrammesFragment programmesFragment;
    MeFragment meFragment;
    private MenuItem preItem;
    private MainActivityBinding mBinding;
    private AlarmManager alarmManager;
    private PendingIntent alarmIntent;
    private NetworkManager networkManager;
    private boolean isOnline;
    private MainActivityVM mMainActivityVM;

    private void registerEventReceiver() {
        IntentFilter eventFilter = new IntentFilter();
        eventFilter.addAction(VansInfoActivity.ACTION_UPDATE_DIRECTION);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = (MainActivityBinding) binding;
        mMainActivityVM = (MainActivityVM) viewModel;
        navigationView();
        getTokenFireBase();

        if (getIntent() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null && !extras.isEmpty()) {
                openIntentFromNotification(extras);
            }
        }
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmIntent = PendingIntent.getBroadcast(this, 0, new Intent(this, AutostartReceiver.class), 0);

        if (VehicleStateStatus.Shipping.equals(AppController.getVehicleStatus())) {
            if (!StaticData.getDriver().getVehicle().getIot_type()) {
                startTrackingService();
            } else {
                stopTrackingService();
            }
        } else {
            stopTrackingService();
        }

        mBinding.swLocationService.setChecked(AppController.getInstance().isServiceRunning(TrackingService.class));

        mBinding.swLocationService.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                startTrackingService();
            } else {
                stopTrackingService();

            }
        });
        // Delete cache file
        PictureFileUtils.deleteCacheDirFile(this, PictureMimeType.ofImage());

        registerEventReceiver();
    }

    private void startTrackingService() {
        Set<String> requiredPermissions = new HashSet<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);
        }

        if (!requiredPermissions.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(requiredPermissions.toArray(new String[requiredPermissions.size()]), PERMISSIONS_REQUEST_LOCATION);
            }
            return;
        }
        startService(new Intent(this, TrackingService.class));
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                ALARM_MANAGER_INTERVAL, ALARM_MANAGER_INTERVAL, alarmIntent);
    }

    private void stopTrackingService() {
        alarmManager.cancel(alarmIntent);
        stopService(new Intent(this, TrackingService.class));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
        Log.i(MainActivity.class.getName(), "onStart: ...");
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        networkManager.stop();
        Log.i(MainActivity.class.getName(), "onStop: ...");
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NotificationService notificationService) {
        try {
            String clickAction = notificationService.getClick_action();
            if (ClickAction.DRIVER_INFO.equals(clickAction)) {
                meFragment.getDriverReward();
            }

            Gson gsonn = new GsonBuilder()
                    .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                    .create();
            NotificationModel notificationModell = gsonn.fromJson(notificationService.getMess_object(), NotificationModel.class);

            if (!clickAction.equals(ClickAction.BIDDING_DETAIL)) {//update lại danh sách thông báo
                if (mBinding.mainViewPager.getCurrentItem() == 2) {//vị tri của tab thông báo
                    SharedPreferences.Editor editor = AppController.getInstance().getSharePre().edit();
                    editor.putInt(Constants.IS_NOTIFICATION, 1);
                    editor.apply();
                    badgeDrawable.setVisible(true);
                }
                Gson gson = new GsonBuilder()
                        .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                        .create();
                NotificationModel notificationModel = gson.fromJson(notificationService.getMess_object(), NotificationModel.class);

                //trường hợp được gán xe
                if (ObjectStatus.FIVE.equals(notificationModel.getObject_status()) && notificationModel.getClick_action().equals(ClickAction.VEHICLE_INFO)) {
                    if (!StaticData.getDriver().getVehicle().getIot_type() && !AppController.getInstance().isServiceRunning(TrackingService.class)) {
                        startTrackingService();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(Integer tabPosition) {
        mBinding.mainViewPager.setCurrentItem(tabPosition);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_LOCATION) {

            if (VehicleStateStatus.Shipping.equals(AppController.getVehicleStatus())) {
                if (!StaticData.getDriver().getVehicle().getIot_type()) {
                    startTrackingService();
                } else {
                    stopTrackingService();
                }
            } else {
                stopTrackingService();
            }
        }
    }

    private void getTokenFireBase() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    // Luu token
                    AppController.getInstance().getEditor().putString("token_fb", token).apply();

                    DriverApi.updateToken(token, new IResponse() {
                        @Override
                        public void onSuccess(Object o) {
                            Log.e("save_token_firebase", token);
                        }

                        @Override
                        public void onFail(Throwable error) {

                        }
                    });
                    // Log and toast
                    Log.e("token_firebase", token);
                });
    }


    @SuppressLint("NonConstantResourceId")
    private void navigationView() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getSupportFragmentManager());

        meFragment = new MeFragment();
        programmesFragment = new ProgrammesFragment();
        BiddingFragment biddingFragment = new BiddingFragment();
        WalletFragment walletFragment = new WalletFragment();
        myPagerAdapter.addFragment(walletFragment);
        myPagerAdapter.addFragment(biddingFragment);
        myPagerAdapter.addFragment(programmesFragment);
        myPagerAdapter.addFragment(meFragment);

        mBinding.mainViewPager.setAdapter(myPagerAdapter);

        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.actionHome:
                    mBinding.mainViewPager.setCurrentItem(0);
                    break;
                case R.id.actionFun:
                    mBinding.mainViewPager.setCurrentItem(1);
                    break;
                case R.id.actionProgrammes:
                    mBinding.mainViewPager.setCurrentItem(2);
                    break;
                case R.id.actionProfile:
                    mBinding.mainViewPager.setCurrentItem(3);
                    break;
            }
            return false;
        });
        mBinding.mainViewPager.setOffscreenPageLimit(4);
        mBinding.mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null && !extras.isEmpty()) {
                openIntentFromNotification(extras);
            }
        }

    }

    private void openIntentFromNotification(Bundle extras) {
        String item_id = extras.getString(Constants.ITEM_ID);
        String click_action = extras.getString(Constants.CLICK_ACTION);
        Intent intent = new Intent(click_action);
        intent.putExtra(Constants.ITEM_ID, item_id);
        if (click_action != null) {
            // TODO: 24/09/2020 gửi thêm trạng thái thông báo dã đọc hay chưa
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            EventBus.getDefault().unregister(this);
////            unregisterReceiver(mBroadcast);
//            super.onBackPressed();
//            return;
//        }
//        if (mBinding.mainViewPager.getCurrentItem() != 0) {
//            mBinding.mainViewPager.setCurrentItem(0);
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, getString(R.string.message_exit), Toast.LENGTH_SHORT).show();
//
//        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        DialogConfirm dialogConfirm = new DialogConfirm(getResources().getString(R.string.notice), getResources().getString(R.string.msg_exit_app))
                .setOnClickListener(v -> finish());
        dialogConfirm.show(getSupportFragmentManager(), MainActivity.class.getName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.main_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MainActivityVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.tvNetwork.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNetwork.setVisibility(View.GONE);
        }
    }
}
