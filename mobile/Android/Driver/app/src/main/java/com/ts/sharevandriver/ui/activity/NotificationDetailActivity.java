package com.ts.sharevandriver.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.ActivityNotificationDetailBinding;
import com.ts.sharevandriver.service.location_service.NetworkManager;
import com.ts.sharevandriver.viewmodel.NotificationDetailVM;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;

public class NotificationDetailActivity extends BaseActivity<ActivityNotificationDetailBinding> implements NetworkManager.NetworkHandler {
    NotificationDetailVM notificationDetailVM;

    private NetworkManager networkManager;
    private boolean isOnline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationDetailVM = (NotificationDetailVM) viewModel;

        String notification_id = getIntent().getExtras().getString(Constants.ITEM_ID);

        notificationDetailVM.getNotificationById(notification_id);
        initToolbar();
    }
    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(this, this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    protected void onStop() {
        super.onStop();
        networkManager.stop();
    }

    private void initToolbar() {
        binding.toolbar.setTitle(R.string.notification_detail);
        getBaseActivity().setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.activity_notification_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return NotificationDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline=isOnline;
        if (!isOnline) {
            binding.tvNetwork.setVisibility(View.VISIBLE);
            binding.progressLayout.setVisibility(View.VISIBLE);
            binding.progressLayout.setOnClickListener(v->{
                Log.d("click_progress", "onNetworkUpdate");
            });
        } else {
            binding.tvNetwork.setVisibility(View.GONE);
            binding.progressLayout.setVisibility(View.GONE);
        }
    }
}