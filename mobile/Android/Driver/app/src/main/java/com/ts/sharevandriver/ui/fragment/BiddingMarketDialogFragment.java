package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.DialogBiddingMarketPlaceBinding;
import com.ts.sharevandriver.model.BiddingInformation;
import com.ts.sharevandriver.viewmodel.MarketPlaceVM;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import lombok.Getter;

@Getter
public class BiddingMarketDialogFragment extends DialogFragment {
    DialogBiddingMarketPlaceBinding binding;
    private BiddingInformation biddingInformation;
    private final mAction mAction;
    private MarketPlaceVM marketPlaceVM;
    CountDownTimer countDownTimer;

    public BiddingMarketDialogFragment(MarketPlaceVM marketPlaceVM, mAction mAction) {
        this.mAction = mAction;
        this.marketPlaceVM = marketPlaceVM;
    }

    public BiddingMarketDialogFragment(MarketPlaceVM marketPlaceVM, BiddingInformation biddingInformation, BiddingMarketDialogFragment.mAction mAction) {
        this.biddingInformation = biddingInformation;
        this.mAction = mAction;
        this.marketPlaceVM = marketPlaceVM;
    }

    public BiddingInformation getBiddingInformation() {
        return biddingInformation;
    }

    public void setBiddingInformation(BiddingInformation biddingInformation) {
        this.biddingInformation = biddingInformation;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_bidding_market_place, container, false);
        binding.setDialog(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setCancelable(false);
        marketPlaceVM.setOnReceiveRout(false);
        binding.btnAccept.setOnClickListener(v -> {
            dismiss();
            mAction.onAccept();
        });
        binding.btnReject.setOnClickListener(v -> {
            dismiss();
            mAction.onReject();
        });

        if (biddingInformation.getAccept_time_package() == null) {
            biddingInformation.setAccept_time_package(60);
        }
        countDownTimer = new CountDownTimer(biddingInformation.getAccept_time_package() * 1000, 1000) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                binding.btnReject.setText(getString(R.string.reject) + " (" + millisUntilFinished / 1000 + ")");
            }

            public void onFinish() {
                binding.btnReject.callOnClick();
            }

        }.start();


        return binding.getRoot();

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        super.onDismiss(dialog);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

    public interface mAction {
        void onAccept();

        void onReject();
    }


}
