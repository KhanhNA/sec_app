package com.ts.sharevandriver.ui.fragment;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.ICallBack;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.databinding.FragmentMapBinding;
import com.ts.sharevandriver.enums.SosStatus;
import com.ts.sharevandriver.enums.VehicleStateStatus;
import com.ts.sharevandriver.model.DriverDistance;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.ts.sharevandriver.model.section.ItemRoutingNote;
import com.ts.sharevandriver.model.section.RootNode;
import com.ts.sharevandriver.ui.activity.RoutingDetailActivity;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.viewmodel.MapsVM;
import com.ts.sharevandriver.widget.OnSingleClickListener;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.google.android.gms.maps.model.JointType.ROUND;


public class MapsFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int LOCATION_SETTING_CODE = 111;
    private static final int REQUEST_CODE_SOS = 252;
    private static final String TAG = "MapsFragment";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private final int REQUEST_CODE_CONFIRM_ROUTING = 999;
    FusedLocationProviderClient mFusedLocationProviderClient;
    AlertDialog alert;
    RoutingPlanFragmentTab planFragment;
    private FragmentMapBinding mBinding;
    //vars
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private BottomSheetBehavior behavior;
    //widgets
    private MapsVM mapsVM;
    private PolylineOptions polylineOptions, blackPolylineOptions;
    private Polyline blackPolyline, greyPolyLine;
    private VansInfoDialog dialog;
    ProgressFragment progressFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
            }
        }
        mapsVM = (MapsVM) viewModel;
        mBinding = (FragmentMapBinding) binding;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        initBottomSheet(view);
        initEvenClick();
        initMap(false);
        placeAutoComplete();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (StaticData.getDriver() != null) {
            if (StaticData.getDriver().getVehicle() == null)
                mBinding.icVansInfo.setVisibility(View.GONE);
        }
    }

    public void showVansInfo(View view) {
        if (StaticData.getDriver().getVehicle() == null) {
            view.setVisibility(View.GONE);
            return;
        }

        dialog = new VansInfoDialog((parkingPoint) -> {
            mMap.addMarker(
                    new MarkerOptions()
                            .title(parkingPoint.getName())
                            .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.vans_map_maker)))
                            .position(new LatLng(parkingPoint.getLatitude(), parkingPoint.getLongitude()))
            );
            requestDrawDestination(new LatLng(parkingPoint.getLatitude(), parkingPoint.getLongitude()), "#347ee6");
        });
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initResoldSos() {
        mBinding.btnResoldSOS.setVisibility(View.VISIBLE);
        mBinding.btnResoldSOS.setAnimation("animation/sos_animation.json");
        mBinding.btnResoldSOS.playAnimation();
        DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.confirm_the_problem_is_fixed)
                , getString(R.string.msg_confirm_resold_sos)).setAnimationFile("animation/success_animation.json");
        dialogConfirm.setOnClickListener(v -> {
            mapsVM.updateSosStatus(this::runUi);
            dialogConfirm.dismiss();
        });

        mBinding.btnResoldSOS.setOnClickListener(v -> {
            dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
        });

    }

    public void openSOSActivity() {

        Intent intent = new Intent(getActivity(), SOSActivity.class);
        ItemRoutingNote current = mapsVM.currentLocationRoutingPlan();
        if (current != null) {
            intent.putExtra("ROUTING_PLAN_ID", current.getId());
            intent.putExtra("ORDER_NUMBER", current.getOrderNumber());
        }
        startActivityForResult(intent, REQUEST_CODE_SOS);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        init();
        getDeviceLocation();
        mapsVM.getRouting(this::runUi);

        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getLocationPermission();
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);


    }

    /**
     * khởi tạo map
     */
    private void init() {

        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(getActivity(), new ICallBack() {
            @Override
            public void callBack() {
                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        }));
        mBinding.icGps.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                getDeviceLocation();
            }
        });
        mMap.setOnInfoWindowClickListener(marker -> {
            if (marker.getTag() instanceof ShareVanRoutingPlan) {
                ShareVanRoutingPlan routingPlan = (ShareVanRoutingPlan) marker.getTag();
                Intent intent = new Intent(getActivity(), RoutingDetailActivity.class);
                intent.putExtra(Constants.ITEM_ID, routingPlan.getRouting_plan_day_code());
                startActivityForResult(intent, REQUEST_CODE_CONFIRM_ROUTING);
            } else if (marker.getTag() instanceof RootNode) {
                // TODO: 12/25/2020 list rootNode same location
                openRoutingDetailActivity(mapsVM.getSelectedRoutNode().get());
            }
        });
        hideSoftKeyboard();

        mMap.setOnInfoWindowCloseListener(marker -> {
            RootNode rootNode = mapsVM.getCurrentNode();
            mapsVM.getSelectedRoutNode().set(rootNode);
            if (rootNode != null) {
                mBinding.txtDistance.setVisibility(View.GONE);
                mBinding.lbNextWarehouse.setVisibility(View.VISIBLE);
                getDirection(mapsVM.getCurrentNode());
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        mMap.setOnMarkerClickListener(marker -> {
            if (marker.getTag() instanceof RootNode) {
                mBinding.lbNextWarehouse.setVisibility(View.GONE);
                mBinding.lbNextWarehouse.setVisibility(View.GONE);
                mBinding.txtDistance.setVisibility(View.GONE);
//                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                RootNode rootNode = (RootNode) marker.getTag();
                mapsVM.getSelectedRoutNode().set(rootNode);
                getDirection(rootNode);
            }

            return false;
        });

    }

    private void initEvenClick() {
        mBinding.icRoutingList.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                openRoutingPlanFragment();
            }
        });
        mBinding.icVansInfo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showVansInfo(v);
            }
        });
        mBinding.btnSOS.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                openSOSActivity();
            }
        });
    }


    private void getDirection(RootNode rootNode) {
        try {
            if (mLocationPermissionsGranted) {
//                final Task location = mFusedLocationProviderClient.getLastLocation();
//                location.addOnCompleteListener(task -> {
//                    if (task.isSuccessful() && task.getResult() != null) {
//                        Location currentLocation = (Location) task.getResult();
//                        LatLng from = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
//                        LatLng to = new LatLng(rootNode.getLat(), rootNode.getLng());
//                        mapsVM.getDirection(from, to, this::runUi);
//                    } else {
//                        buildAlertMessageNoGps(268);
//                    }
//                });
                mFusedLocationProviderClient.getLastLocation()
                        .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {

                                    LatLng from = new LatLng(location.getLatitude(), location.getLongitude());
                                    LatLng to = new LatLng(rootNode.getLat(), rootNode.getLng());
                                    mapsVM.getDirection(from, to, MapsFragment.this::runUi);
                                } else {
                                    buildAlertMessageNoGps(268);
                                }
                            }
                        });
            } else {
                getLocationPermission();
            }
        } catch (SecurityException ignored) {
            ignored.printStackTrace();
        }
    }

    private void initBottomSheet(View v) {
        CoordinatorLayout coordinatorLayout = v.findViewById(R.id.coordinator_map);
        // The View with the BottomSheetBehavior
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                Log.e("onStateChanged", "onStateChanged:" + newState);

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        behavior.setHideable(true);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        mBinding.txtDistance.setVisibility(View.GONE);
        behavior.setPeekHeight(mBinding.txtDistance.getHeight(), true);
        bottomSheet.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                openRoutingDetailActivity(mapsVM.getSelectedRoutNode().get());
            }
        });
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getRouting":
                requireActivity().runOnUiThread(() -> {
                    ItemRoutingNote routingNote = mapsVM.currentLocationRoutingPlan();
                    if (routingNote != null) {
                        mapsVM.getSelectedRoutNode().set(mapsVM.getCurrentNode());
                        getDirection(mapsVM.getCurrentNode());
                        if (VehicleStateStatus.Shipping.equals(AppController.getVehicleStatus())) {
                            // TODO: 07/10/2020 interval, distance lấy từ server
                            AppController.getInstance().startLocationService(routingNote.getId(), StaticData.getOdooSessionDto().getSave_log_duration(), StaticData.getOdooSessionDto().getDistance_check_point(), StaticData.getOdooSessionDto().getTime_mobile_notification_key(), routingNote.getLat(), routingNote.getLng());
                        } else {
                            AppController.getInstance().stopLocationService();
                        }
                        requestDrawDestination(new LatLng(routingNote.getLat(), routingNote.getLng()), "#289767");
                    } else {
                        AppController.getInstance().stopLocationService();
                    }

                    addMarker(mapsVM.getLstCartNode());
                });
                break;
            case "drawDestination":
                requireActivity().runOnUiThread(() -> {
                    drawDestination(mapsVM.getRoute().points, (String) objects[1], true);
                });
                break;
            case "getDirectionSuccess":
                DriverDistance driverDistance = (DriverDistance) objects[1];
                String msg = getString(R.string.distance) + ": "
                        + AppController.getInstance().formatNumber(driverDistance.getCost() / 1000) + "km, "
                        + getString(R.string.Estimated_time_to_warehouse) + ": "
                        + minuteToHour(driverDistance.getMinutes().intValue());
                mBinding.txtDistance.setText(msg);
                mBinding.txtDistance.setVisibility(View.VISIBLE);
                break;
            case "updateSosSuccess":
                StaticData.getDriver().getVehicle().setSos_status(SosStatus.NOT_SOS);
                mBinding.btnResoldSOS.setVisibility(View.GONE);
                ToastUtils.showToast(getActivity(), getString(R.string.msg_confirm_resold_sos), getResources().getDrawable(R.drawable.ic_check));
                break;
        }
    }

    private String minuteToHour(int minute) {
        String formatted;
        int hours = minute / 60; //since both are ints, you get an int
        int minutes = minute % 60;
        if (hours == 0) {
            formatted = minutes + " " + getString(R.string.minute);
        } else {
            formatted = hours + " " + getString(R.string.hour) + ", " + minutes + " " + getString(R.string.minute);
        }
        return formatted;
    }

    /**
     * vẽ đường đi theo danh sách chặng đường google trả về
     */
    private void drawDestination(List<LatLng> polyLineList, String color, boolean isClear) {

//        if(isClear){
//            blackPolyline.remove();
//            greyPolyLine.remove();
//        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polyLineList) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
        mMap.animateCamera(mCameraUpdate);

        polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.GRAY);
        polylineOptions.width(5);
        polylineOptions.startCap(new SquareCap());
        polylineOptions.endCap(new SquareCap());
        polylineOptions.jointType(ROUND);
        polylineOptions.addAll(polyLineList);
        greyPolyLine = mMap.addPolyline(polylineOptions);

        blackPolylineOptions = new PolylineOptions();
        blackPolylineOptions.width(8);
        blackPolylineOptions.color(Color.parseColor(color));
        blackPolylineOptions.startCap(new SquareCap());
        blackPolylineOptions.endCap(new SquareCap());
        blackPolylineOptions.jointType(ROUND);
        blackPolyline = mMap.addPolyline(blackPolylineOptions);


        ValueAnimator polylineAnimator = ValueAnimator.ofInt(0, 100);
        polylineAnimator.setDuration(2000);
        polylineAnimator.setInterpolator(new LinearInterpolator());

        polylineAnimator.addUpdateListener(valueAnimator -> {
            List<LatLng> points = greyPolyLine.getPoints();
            int percentValue = (int) valueAnimator.getAnimatedValue();
            int size = points.size();
            int newPoints = (int) (size * (percentValue / 100.0f));
            List<LatLng> p = points.subList(0, newPoints);
            blackPolyline.setPoints(p);
        });

        polylineAnimator.start();
    }

    /**
     * Lấy vị trí hiện tại kết hợp với điểm đến tiếp theo để vẽ đường đi
     */
    private void requestDrawDestination(LatLng targetLatLng, String color) {
        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        try {
            if (mLocationPermissionsGranted) {
                mFusedLocationProviderClient.getLastLocation()
                        .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {

                                    moveCamera(new LatLng(location.getLatitude(), location.getLongitude()),
                                            "My Location");
                                    if (targetLatLng != null) {
                                        mapsVM.getDestination(new LatLng(location.getLatitude(), location.getLongitude())
                                                , targetLatLng, color,
                                                MapsFragment.this::runUi);
                                    }

                                } else {
                                    buildAlertMessageNoGps(424);
                                }
                            }
                        });

//                final Task location = mFusedLocationProviderClient.getLastLocation();
//                location.addOnCompleteListener(task -> {
//                    if (task.isSuccessful() && task.getResult() != null) {
//                        Location currentLocation = (Location) task.getResult();
//
//                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
//                                "My Location");
//                        if (targetLatLng != null) {
//                            mapsVM.getDestination(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())
//                                    , targetLatLng, color,
//                                    this::runUi);
//                        }
//
//                    } else {
//                        buildAlertMessageNoGps(424);
//                    }
//                });
            } else {
                getLocationPermission();

            }
        } catch (SecurityException ignored) {
        }
    }

    /**
     * Thêm đánh dấu các kho nhận trả hàng trên google map
     *
     * @param listPlan danh sách routing plan
     */
    private void addMarker(List<BaseNode> listPlan) {
        for (BaseNode plan : listPlan) {
            RootNode rootNode = (RootNode) plan;
            if (rootNode.getLat() != null && rootNode.getLng() != null) {
//                if(rootNode.getStatus()){
//                    mMap.addMarker(new MarkerOptions()
//                            .title(rootNode.getWarehouseName())
//                            .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.warehouse_marker_complete)))
//                            .position(new LatLng(rootNode.getLat(), rootNode.getLng()))
//                    ).setTag(rootNode);
//                }else{
                mMap.addMarker(new MarkerOptions()
                        .title(rootNode.getWarehouseName())
                        .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(R.layout.map_marker)))
                        .position(new LatLng(rootNode.getLat(), rootNode.getLng()))
                ).setTag(rootNode);
//                }

            }
        }

    }


    /**
     * Tạo bitmap từ drawable
     *
     * @return
     */
    private Bitmap loadBitmapFromView(Integer drawable) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(drawable, null);
        if (v.getMeasuredHeight() <= 0) {
            v.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            v.draw(c);
            return b;
        }
        return null;
    }

    public void openRoutingPlanFragment() {
        planFragment = new RoutingPlanFragmentTab(isChange -> {
            if (isChange.aBoolean) {
                refresh();
            }
        });
        planFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.fullScreenDialog);
        planFragment.show(getChildFragmentManager(), planFragment.getTag());

    }

    private void openRoutingDetailActivity(RootNode rootNode) {
        int node_position = 0;
        List<BaseNode> children = rootNode.getChildNode();
        for (int i = 0; i < children.size(); i++) {
            if (((ItemRoutingNote) children.get(i)).getStatus().equals(0)) {
                node_position = i;
                break;
            }
        }
        boolean isRequire = false;
        RootNode currentNode = mapsVM.getCurrentNode();
        RootNode selectedNode = mapsVM.getSelectedRoutNode().get();
        if (currentNode != null && selectedNode != null) {//kiểm tra tuyến hiện tại có phải là tuyến tiếp theo ko
            isRequire = selectedNode.getWarehouseName().equals(currentNode.getWarehouseName());
        }
        Intent intent = new Intent(getActivity(), RoutingDetailActivity.class);
        ArrayList<Integer> listRoutingId = getListRoutingPlanId(rootNode);
        intent.putIntegerArrayListExtra("LIST_ROUTING_PLAN_ID", listRoutingId);

        intent.putExtra("ROOT_NODE", rootNode);
        intent.putExtra("IS_REQUIRE", isRequire);
        intent.putExtra("NODE_POSITION", node_position);
        intent.putExtra(Constants.ITEM_ID, ((ItemRoutingNote) children.get(node_position)).getRouting_plan_day_code());
        startActivityForResult(intent, REQUEST_CODE_CONFIRM_ROUTING);

    }

    private ArrayList<Integer> getListRoutingPlanId(RootNode rootNode) {
        ArrayList<Integer> listRoutingId = new ArrayList<>();
        for (BaseNode item : rootNode.getChildNode()) {
            listRoutingId.add(((ItemRoutingNote) item).getId());
        }
        return listRoutingId;
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
                getDeviceLocation();
            } else {
                requestPermissions(
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            requestPermissions(
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationPermissionsGranted = false;

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        mLocationPermissionsGranted = false;
                        return;
                    }
                }
                mLocationPermissionsGranted = true;
                initMap(false);
            }
        }

    }

    private void initMap(boolean isCheckLocationPermission) {
        if (mLocationPermissionsGranted) {
            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (!isCheckLocationPermission && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps(568);
            } else {
                Log.d(TAG, "initMap: initializing map");
                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            }
        } else {
            getLocationPermission();
        }
    }


    private void buildAlertMessageNoGps(int line) {
        if (alert == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getString(R.string.msg_turn_on_gps))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok), (dialog, id)
                            -> startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SETTING_CODE));
            alert = builder.create();
        }
        if (!alert.isShowing()) {
            Log.e("LINE", line + "");
            alert.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CONFIRM_ROUTING && resultCode == Activity.RESULT_OK) {
            refresh();
        } else if (requestCode == VansInfoDialog.REQUEST_VAN_INFO && resultCode == Activity.RESULT_OK) {
            ItemRoutingNote routingPlan = mapsVM.currentLocationRoutingPlan();
            if (routingPlan != null) {
                if (VehicleStateStatus.Shipping.equals(AppController.getVehicleStatus()) && !routingPlan.isCheck_point()) {
                    // TODO: 07/10/2020 interval, distance lấy từ server
                    AppController.getInstance().startLocationService(routingPlan.getId(), StaticData.getOdooSessionDto().getSave_log_duration(),
                            StaticData.getOdooSessionDto().getDistance_check_point(),
                            StaticData.getOdooSessionDto().getTime_mobile_notification_key(),
                            routingPlan.getLat(), routingPlan.getLng());
                } else {
                    AppController.getInstance().stopLocationService();
                }
            }
        } else if (requestCode == LOCATION_SETTING_CODE) {
            progressFragment = new ProgressFragment(getString(R.string.get_your_loaction), new ProgressFragment.IProgressFragment() {
                @Override
                public void handleProgress() {
                    progressFragment.dismiss();
                    if (AppController.getInstance().checkHighAccuracyLocationMode()) {
                        initMap(true);
                    } else {
                        ToastUtils.showToast(getString(R.string.please_set_high_accuracy_location_mode));
                        buildAlertMessageNoGps(615);
                    }
                }
            });
            progressFragment.show(getFragmentManager(), "abc");
        } else if (requestCode == REQUEST_CODE_SOS && resultCode == Activity.RESULT_OK) {
            if (StaticData.getDriver().getVehicle() != null) {
                if (StaticData.getDriver().getVehicle().getSos_status().equals(SosStatus.CONTINUABLE)) {
                    initResoldSos();
                } else if (StaticData.getDriver().getVehicle().getSos_status().equals(SosStatus.CRASH)) {
                    refresh();
                }
            }
        }
    }

    public void refresh() {
        Log.d("MapsFragment", "onRefresh");
        mMap.clear();
        mapsVM.getRouting(this::runUi);
        if (planFragment != null && planFragment.isVisible()) {
            planFragment.refresh();
        }
    }

    /**
     * khởi tạo thanh tìm kiếm địa chỉ theo map
     */
    private void placeAutoComplete() {
//         Initialize Places.
        if (getActivity() != null) {
            Places.initialize(getActivity().getApplicationContext(), AppController.API_GOOGLE_MAP_SEARCH);
        }
        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Specify the types of place data to return.
        if (autocompleteFragment != null) {
            ((EditText) autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input)).setTextSize(15.0f);
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG));
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(@NonNull Place place) {
//                    CameraPosition cameraPosition = new CameraPosition.Builder().target(
//                            place.getLatLng())                   // Sets the center of the map to Mountain View
//                            .zoom(DEFAULT_ZOOM)                   // Sets the zoom
//                            .bearing(0)                // Sets the orientation of the camera to east
//                            .tilt(0)                   // Sets the tilt of the camera
//                            .build();
//                    if (place.getLatLng() != null) {
//                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                    }
                    if (place.getLatLng() != null) {
                        moveCamera(place.getLatLng(), place.getName() + "");
                    }
                }

                @Override
                public void onError(@NonNull Status status) {

                }
            });
        }
    }

    public void hideIcVan() {
        mBinding.icVansInfo.setVisibility(View.GONE);
    }

    public void showIcVan() {
        mBinding.icVansInfo.setVisibility(View.VISIBLE);
    }


    /**
     * lấy vị trí hiện tại
     */
    private void getDeviceLocation() {
        if (mMap == null) return;
        try {
            if (mLocationPermissionsGranted) {
//                final Task location = mFusedLocationProviderClient.getLastLocation();
//                location.addOnCompleteListener(task -> {
//                    if (task.isSuccessful() && task.getResult() != null) {
//                        Location currentLocation = (Location) task.getResult();
//
//                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
//                                "My Location");
//
//                    } else {
//                        buildAlertMessageNoGps(701);
//                    }
//                });
                mFusedLocationProviderClient.getLastLocation()
                        .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    moveCamera(new LatLng(location.getLatitude(), location.getLongitude()),
                                            "My Location");
                                } else {
                                    buildAlertMessageNoGps(701);
                                }
                            }
                        });
            } else {
                getLocationPermission();
            }
        } catch (SecurityException ignored) {
        }
    }

    /**
     * Di chuyển camera đến vị tri @latLng
     *
     * @param latLng vị trí cần di chuyển đến
     */
    private void moveCamera(LatLng latLng, String title) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MapsFragment.DEFAULT_ZOOM), 2000, null);
//        if (!title.equals("My Location")) {
//            MarkerOptions options = new MarkerOptions()
//                    .position(latLng)
//                    .title(title);
//            mMap.addMarker(options);
//        }

        hideSoftKeyboard();
    }

    private void hideSoftKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_map;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return MapsVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void updateData() {
        dialog.getInfoUser();
    }

    public void updateRouting() {
        mapsVM.getRouting(this::runUi);
    }
}
