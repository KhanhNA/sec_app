package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.ProgrammesFragmentBinding;
import com.ts.sharevandriver.ui.activity.GrammarFragment;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.viewmodel.ProgrammesVM;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

public class ProgrammesFragment extends BaseFragment<ProgrammesFragmentBinding> {
    ProgrammesVM programmesVM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        programmesVM = (ProgrammesVM) viewModel;
        return v;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onItemClick(View v, Object o) {
        switch (v.getId()) {
            case R.id.tv_grammar:
                goToGrammarFragment();
                break;
            case R.id.tv_vocabulary:
                ToastUtils.showToast("Vocabulary");
                break;
        }
    }

    private void goToGrammarFragment() {
        Intent intent = new Intent(getContext(), CommonActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.FRAGMENT, GrammarFragment.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.programmes_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ProgrammesVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
