package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.Topic;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseEngFrgVM extends BaseViewModel {

    ObservableList<Topic> list = new ObservableArrayList<>();
    ObservableList<Topic> listClause = new ObservableArrayList<>();
    ObservableList<Topic> listPronunciation = new ObservableArrayList<>();

    public BaseEngFrgVM(@NonNull Application application) {
        super(application);
        list.add(new Topic("1", "Nouns", "Danh từ"));
        list.add(new Topic("2", "Adjectives", "Tính từ"));
        list.add(new Topic("3", "Verbs", "Động từ"));
        list.add(new Topic("4", "Adverbs", "Trạng từ"));
        list.add(new Topic("5", "Prepositions", "Giới từ"));
        list.add(new Topic("6", "Relevant knowledge", "Kiến thức liên quan"));
        list.add(new Topic("7", "Spell rule-Irregular-phrasal verb", "QT chính tả-ĐT BQT-Cụm động từ"));
        list.add(new Topic("8", "English overview", "Tổng quan tiếng anh"));

        listClause.add(new Topic("1", "Clause DCadj", "Mệnh đề tính ngữ"));
        listClause.add(new Topic("2", "Clause DCadv", "Mệnh đề trạng ngữ"));
        listClause.add(new Topic("3", "Clause DCn", "Mệnh đề danh ngữ"));
        listClause.add(new Topic("4", "Overview DC", "Tổng quan mệnh đề"));

        listPronunciation.add(new Topic("1", "Sound phenomenon in words", "Hiện tượng âm trong từ"));
        listPronunciation.add(new Topic("2", "Sound phenomenon in sentences", "Hiện tượng âm thanh câu"));
    }

    public void getData(RunUi runUi) {
        runUi.run("getData");
    }
}
