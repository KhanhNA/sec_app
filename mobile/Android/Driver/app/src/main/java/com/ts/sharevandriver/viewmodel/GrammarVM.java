package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.model.Topic;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GrammarVM extends BaseViewModel {

    public GrammarVM(@NonNull Application application) {
        super(application);

    }

}
