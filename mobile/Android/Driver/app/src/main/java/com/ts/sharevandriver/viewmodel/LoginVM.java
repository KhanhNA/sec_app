package com.ts.sharevandriver.viewmodel;

import android.app.Application;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.model.OdooSessionDto;
import com.ts.sharevandriver.api.BaseApi;
import com.ts.sharevandriver.api.CustomerApi;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.enums.VehicleStateStatus;
import com.ts.sharevandriver.model.Driver;
import com.ts.sharevandriver.model.UserInfo;
import com.ts.sharevandriver.utils.ApiResponse;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

public class LoginVM extends BaseViewModel<UserInfo> {
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    private UserInfo userInfo;
    SharedPreferences sharedPreferences;


    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }


    public LoginVM(@NonNull Application application) {
        super(application);
        userInfo = new UserInfo();
        sharedPreferences = AppController.getInstance().getSharePre();
        userInfo.setIsSave(sharedPreferences.getBoolean("chkSave", false));
        userInfo.setUserName(sharedPreferences.getString(Constants.USER_NAME, ""));
        userInfo.setPassWord(sharedPreferences.getString(Constants.MK, ""));
        model.set(userInfo);
    }

    private void getConfigUrl() {
        AppController.SERVER_URL = sharedPreferences.getString("U_LOGIN", AppController.SERVER_URL);
        AppController.LOCATION_SERVICE_URL = sharedPreferences.getString("U_LOCATION", AppController.LOCATION_SERVICE_URL);
        AppController.DATABASE = sharedPreferences.getString("U_DATABASE", AppController.DATABASE);
    }

    public void requestLogin() {
        if (model.get() != null) {
            // TODO: 23/09/2020 configServer  bỏ :getConfigUrl();
            getConfigUrl();
            UserInfo userInfo = model.get();
            BaseApi.requestLogin(userInfo.getUserName() + "", userInfo.getPassWord() + "", new IOdooResponse<OdooSessionDto>() {
                @Override
                public void onResponse(OdooSessionDto odooSessionDto, Throwable throwable) {
                    onLoginResponse(odooSessionDto, throwable);

                }

            });
        }

    }




    private void getUserInfo() {

        CustomerApi.getDriver( new IResponse<OdooResultDto<Driver>>() {
            @Override
            public void onSuccess(OdooResultDto<Driver> o) {
                getUserInfoSuccess(o);
            }

            @Override
            public void onFail(Throwable error) {
                responseLiveData.postValue(ApiResponse.error(new Throwable()));
            }
        });
//        Warehouse warehouse = new Warehouse();
////        warehouse.setWarehouse_code("123");
//        warehouse.setName("thonv test22222");
//        List ids = new ArrayList<Integer>();
//        ids.add(8);
//        WarehouseApi.updateRecord(ids,warehouse, new IResponse<Boolean>() {
//            @Override
//            public void onSuccess(Boolean o) {
//                System.out.println("test");
//            }
//        });
    }



    private void handleError(Throwable throwable) {
        responseLiveData.postValue(ApiResponse.notConnect(new Throwable()));
        throwable.printStackTrace();
    }

    private void getUserInfoSuccess(OdooResultDto<Driver> odooResultDto) {

        if (odooResultDto != null && odooResultDto.getRecords().size() > 0) {
            Driver driver = odooResultDto.getRecords().get(0);
            System.out.println("result:" + odooResultDto.toString());
            responseLiveData.postValue(ApiResponse.success(odooResultDto));
            StaticData.setDriver(driver);
        } else {
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }


    //forget password
    public void sendPassword() {
        // TODO: 21/02/2020 send OTP
        try {
            view.action("sendOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void confirmOTP() {
        // TODO: 21/02/2020 confirm OTP
        try {
            view.action("confirmOTP", null, null, null);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }

    public void onLoginResponse(OdooSessionDto odooSessionDto, Throwable volleyError) {
        if (volleyError == null && odooSessionDto != null) {
            StaticData.setOdooSessionDto(odooSessionDto);
            Gson gson = new Gson();
            String json = gson.toJson(getModelE());

            SharedPreferences.Editor editor = AppController.getInstance().getEditor();
            editor.putString("UserInfo", json);

            editor.putString(Constants.USER_NAME, getModelE().getUserName().trim());
            if (userInfo.getIsSave() != null && userInfo.getIsSave()) {
                editor.putString(Constants.MK, getModelE().getPassWord().trim());
                editor.putBoolean("chkSave", true);
            } else {
                editor.putBoolean("chkSave", false);
                editor.putString(Constants.MK, "");
            }
            editor.commit();

            getUserInfo();
//            responseLiveData.postValue(ApiResponse.success(new OdooResultDto<Driver>()));

        } else {
            if(volleyError != null){
                volleyError.printStackTrace();
            }
            responseLiveData.postValue(ApiResponse.error(new Throwable()));
        }
    }
}
