package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import com.luck.picture.lib.entity.LocalMedia;
import com.ts.sharevandriver.model.BillPackage;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportRoutingVM extends BaseViewModel {

    public ObservableBoolean isNotEmptyImage = new ObservableBoolean();

    private List<BillPackage> billPackages;

    private String warehouse_name;
    private String warehouse_address;
    private String warehouse_phone;
    private List<LocalMedia> lstFileSelected;

    public ReportRoutingVM(@NonNull Application application) {
        super(application);
        billPackages = new ArrayList<>();
    }

    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }

}
