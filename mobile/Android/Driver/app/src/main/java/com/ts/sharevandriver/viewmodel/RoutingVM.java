package com.ts.sharevandriver.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.ts.sharevandriver.api.RoutingApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.RunUi;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.ShareVanRoutingPlan;
import com.ts.sharevandriver.model.section.ItemRoutingNote;
import com.ts.sharevandriver.model.section.RootNode;
import com.ts.sharevandriver.utils.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.workable.errorhandler.ErrorHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingVM extends BaseViewModel {
    private ObservableInt records = new ObservableInt();
    private ObservableBoolean isLoading = new ObservableBoolean();
    private List<ShareVanRoutingPlan> routingPlans;
    List<BaseNode> lstCartNode;

    public RoutingVM(@NonNull Application application) {
        super(application);
        routingPlans = new ArrayList<>();
        lstCartNode = new ArrayList<>();

    }

    /**
     * lấy danh sách routing plan
     *
     * @param runUi callBack
     */
    public void getRoutingByStatus(String search, ArrayList<Integer> status, RunUi runUi) {
        routingPlans.clear();
        isLoading.set(true);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String currentDay = format.format(new Date());
        RoutingApi.getRouting(currentDay, StaticData.getDriver().getId(), search, status, new IResponse() {
            @Override
            public void onSuccess(Object o) {
                routingPlans.addAll((List<ShareVanRoutingPlan>) o);
                records.set(((List<ShareVanRoutingPlan>) o).size());
                setDataToListNode();
                isLoading.set(false);
                runUi.run("getRouting");
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
            }
        });
    }

    /**
     * gom nhóm routing theo kho
     */
    public void setDataToListNode() {
        ErrorHandler.create().run(() -> {
            lstCartNode.clear();
            if (TsUtils.isNotNull(routingPlans)) {
                String currentWarehouse = "";//kho hiện tại
                Integer company_id = 0;//kho hiện tại
                List<BaseNode> childItems = null;
                for (ShareVanRoutingPlan routing : routingPlans) {
                    if(routing.getWarehouse_id()!=null){ // Không phải kho Depot check warehouse_name và company_id
                        //nếu tuyến mới có kho giống kho hiện tại thì gom vào list
                        if (routing.getWarehouse_name().equals(currentWarehouse) && routing.getCompany_id()==company_id) {
                            assert childItems != null;
                            ItemRoutingNote itemRoutingNote = new ItemRoutingNote(routing.getId(),
                                    routing.getRouting_plan_day_code(),
                                    routing.getOrder_number(),
                                    routing.getBill_routing_name(),
                                    routing.getType(),
                                    routing.getStatus(),
                                    routing.getLatitude(),
                                    routing.getLongitude(),
                                    routing.isCheck_point(),
                                    routing.getTrouble_type());
                            childItems.add(itemRoutingNote);
                        }
                        else {//nếu ko phải thì thêm nhóm mới vào list Node
                            childItems = new ArrayList<>();
                            RootNode entity = new RootNode(childItems, routing.getWarehouse_name(), routing.getAddress(), routing.getPhone());
                            ItemRoutingNote itemRoutingNote = new ItemRoutingNote(routing.getId(),
                                    routing.getRouting_plan_day_code(),
                                    routing.getOrder_number(),
                                    routing.getBill_routing_name(),
                                    routing.getType(),
                                    routing.getStatus(),
                                    routing.getLatitude(),
                                    routing.getLongitude(),
                                    routing.isCheck_point(),
                                    routing.getTrouble_type());
                            childItems.add(itemRoutingNote);
                            entity.setExpanded(false);
                            lstCartNode.add(entity);
                        }
                    }else{ // kho depot chỉ cần check warehouse_name
                        //nếu tuyến mới có kho giống kho hiện tại thì gom vào list
                        if (routing.getWarehouse_name().equals(currentWarehouse)) {
                            assert childItems != null;
                            ItemRoutingNote itemRoutingNote = new ItemRoutingNote(routing.getId(),
                                    routing.getRouting_plan_day_code(),
                                    routing.getOrder_number(),
                                    routing.getBill_routing_name(),
                                    routing.getType(),
                                    routing.getStatus(),
                                    routing.getLatitude(),
                                    routing.getLongitude(),
                                    routing.isCheck_point(),
                                    routing.getTrouble_type());
                            childItems.add(itemRoutingNote);
                        }
                        else {//nếu ko phải thì thêm nhóm mới vào list Node
                            childItems = new ArrayList<>();
                            RootNode entity = new RootNode(childItems, routing.getWarehouse_name(), routing.getAddress(), routing.getPhone());
                            ItemRoutingNote itemRoutingNote = new ItemRoutingNote(routing.getId(),
                                    routing.getRouting_plan_day_code(),
                                    routing.getOrder_number(),
                                    routing.getBill_routing_name(),
                                    routing.getType(),
                                    routing.getStatus(),
                                    routing.getLatitude(),
                                    routing.getLongitude(),
                                    routing.isCheck_point(),
                                    routing.getTrouble_type());
                            childItems.add(itemRoutingNote);
                            entity.setExpanded(false);
                            lstCartNode.add(entity);
                        }

                    }
                    currentWarehouse = routing.getWarehouse_name();
                    company_id = routing.getCompany_id();
                }
            }
            if (lstCartNode.size() > 0) {
                ((RootNode) lstCartNode.get(0)).setExpanded(true);
            }

        });
    }


    /**
     * trả về location điểm đến tiếp theo
     *
     * @return
     */
    public ItemRoutingNote currentLocationRoutingPlan() {
        for (BaseNode baseNode : lstCartNode) {
            RootNode rootNode = (RootNode) baseNode;
            for (BaseNode childNode : rootNode.getChildNode()) {
                ItemRoutingNote rout = (ItemRoutingNote) childNode;
                if (rout.getStatus() == 0) {
                    return rout;
                }
            }
        }
        return null;
    }
}
