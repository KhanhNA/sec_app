package com.ts.sharevandriver.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.MultiMonthView;
import com.haibin.calendarview.RangeMonthView;
import com.ts.sharevandriver.enums.DayOffStatus;
import com.ts.sharevandriver.enums.DayOffType;
import com.ts.sharevandriver.model.DayOff;

import java.util.List;

/**
 * Created by Good_Boy on 2020/11/15.
 */

public class CustomMultiMonthView extends RangeMonthView {
    private final Paint mCurrentDayPaint = new Paint();
    private final Paint mPointPaint = new Paint();
    private final int mPadding;
    private final float mPointRadius;

    private int mRadius;

    //-------------------------
    private final Paint mProgressPaint = new Paint();
    private final Paint mNoneProgressPaint = new Paint();

    public CustomMultiMonthView(Context context) {
        super(context);
        mPointPaint.setAntiAlias(true);
        mPointPaint.setStyle(Paint.Style.FILL);
        mPointPaint.setTextAlign(Paint.Align.CENTER);
        mPointPaint.setColor(0xFFFFFFFF);

        mCurrentDayPaint.setAntiAlias(true);
        mCurrentDayPaint.setStyle(Paint.Style.FILL);
        mCurrentDayPaint.setColor(0xFFeaeaea);

        mPadding = dipToPx(getContext(), 3);
        mPointRadius = dipToPx(context, 2.2f);

//        ------------------------------

        mProgressPaint.setAntiAlias(true);
        mProgressPaint.setStyle(Paint.Style.STROKE);
        mProgressPaint.setStrokeWidth(dipToPx(context, 2f));

        mNoneProgressPaint.setAntiAlias(true);
        mNoneProgressPaint.setStyle(Paint.Style.STROKE);
        mNoneProgressPaint.setStrokeWidth(dipToPx(context, 2f));

    }



    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
    }

    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme,
                                     boolean isSelectedPre, boolean isSelectedNext) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        if (isSelectedPre) {
            if (isSelectedNext) {
                canvas.drawRect(x, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            } else {
                canvas.drawRect(x, cy - mRadius, cx, cy + mRadius, mSelectedPaint);
                canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            }
        } else {
            if(isSelectedNext){
                canvas.drawRect(cx, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            }
            canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            //
        }

        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, int y, boolean isSelected) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        drawScheme(canvas, calendar, cx, cy);

    }

    private void drawScheme(Canvas canvas, Calendar calendar, int cx, int cy) {
        List<Calendar.Scheme> schemes = calendar.getSchemes();
        int angle = 180;

        if(schemes != null && schemes.size() >= 2){
           mProgressPaint.setColor(schemes.get(0).getShcemeColor());
           mNoneProgressPaint.setColor(schemes.get(1).getShcemeColor());

           RectF progressRectF = new RectF(cx - mRadius, cy - mRadius, cx + mRadius, cy + mRadius);
           canvas.drawArc(progressRectF, -90, angle, false, mProgressPaint);

           RectF noneRectF = new RectF(cx - mRadius, cy - mRadius, cx + mRadius, cy + mRadius);
           canvas.drawArc(noneRectF, angle - 90, 360 - angle, false, mNoneProgressPaint);
       }
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine + y;
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;

        boolean isInRange = isInRange(calendar);
        boolean isEnable = !onCalendarIntercept(calendar);

        if (calendar.isCurrentDay()) {
            if(!isSelected) {
                canvas.drawCircle(cx, cy, mRadius, mCurrentDayPaint);
                drawScheme(canvas, calendar, cx, cy);
            }
        }

        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mSchemeTextPaint : mOtherMonthTextPaint);

        } else {
            canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mCurMonthTextPaint : mOtherMonthTextPaint);
        }

    }



    private static int getAngle(int progress) {
        return (int) (progress * 3.6);
    }

    /**
     * dp to px
     *
     * @param context context
     * @param dpValue dp
     * @return px
     */
    private static int dipToPx(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
