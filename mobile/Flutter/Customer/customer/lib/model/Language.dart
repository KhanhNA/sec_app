import 'package:json_annotation/json_annotation.dart';
import 'base.dart';
part 'Language.g.dart';
@JsonSerializable(explicitToJson: true)
class Language extends _Language {
  static String CODE_VN = "vi";
  static String CODE_EN = "en";
  static String CODE_MY = "my";

  String error;
  Language.withError(this.error);
  Language();
  factory Language.fromJson(Map<String, dynamic> js) =>Base().fromJs<Language>(js, (js) => _$LanguageFromJson(js));

  Map<String, dynamic> toJson() => _$LanguageToJson(this);
}

class _Language extends Base {
    int sortOrder;
    String code;
    _Language();
}
