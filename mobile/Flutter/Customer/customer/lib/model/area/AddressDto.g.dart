// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AddressDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressDto _$AddressDtoFromJson(Map<String, dynamic> json) {
  return AddressDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..parent_id = json['parent_id'] as int;
}

Map<String, dynamic> _$AddressDtoToJson(AddressDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'parent_id': instance.parent_id,
    };
