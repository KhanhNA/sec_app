

import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base.dart';
import 'HubDto.dart';
part 'ZoneDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class ZoneDto extends _ZoneDto{

  factory ZoneDto.fromJson(Map<String, dynamic> js) => _$ZoneDtoFromJson(js);

  Map<String, dynamic> toJson() => _$ZoneDtoToJson(this);

  ZoneDto();
}

class _ZoneDto extends Base{
   int id;
   String name;
   String code;
   String name_seq;
   HubDto depotInfo;
}