import 'package:json_annotation/json_annotation.dart';

part 'dashboard.g.dart';

@JsonSerializable(explicitToJson: true)
class Dashboard {
  int poWaitForImport;//po cho nhan chi xuat hien o fc
  int soWaitForExport; //sale order cho xuat

  //giong het nhau
  int stoWaitForExport; //lenh xuat cho xuat
  int stoDelivery; //lenh xuat dang giao
  int stoWaitForImport; //lenh xuat cho nhap

  int inventoryWarningOutOfDate; //ton sap het han
  int inventoryOutOfDate; //ton het han
  int storeId;
  @JsonKey(ignore: true)
  String error;
  Dashboard();

  factory Dashboard.fromJson(Map<String, dynamic> json) => _$DashboardFromJson(json);
  Dashboard.withError(this.error);
  Map<String, dynamic> toJson() => _$DashboardToJson(this);
}
