// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'VehicleDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VehicleDto _$VehicleDtoFromJson(Map<String, dynamic> json) {
  return VehicleDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..license_plate = json['license_plate'] as String
    ..color = json['color'] as String
    ..model_year = json['model_year'] as int
    ..fuel_type = json['fuel_type'] as String
    ..body_length = json['body_length'] as int
    ..body_width = json['body_width'] as int
    ..height = json['height'] as int
    ..vehicle_type = json['vehicle_type'] as String
    ..engine_size = (json['engine_size'] as num)?.toDouble()
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..driver = json['driver'] == null
        ? null
        : DriverDto.fromJson(json['driver'] as Map<String, dynamic>)
    ..vehicle_status = json['vehicle_status'] as int;
}

Map<String, dynamic> _$VehicleDtoToJson(VehicleDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'license_plate': instance.license_plate,
      'color': instance.color,
      'model_year': instance.model_year,
      'fuel_type': instance.fuel_type,
      'body_length': instance.body_length,
      'body_width': instance.body_width,
      'height': instance.height,
      'vehicle_type': instance.vehicle_type,
      'engine_size': instance.engine_size,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'driver': instance.driver?.toJson(),
      'vehicle_status': instance.vehicle_status,
    };
