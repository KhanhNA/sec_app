// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SubscribeDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubscribeDto _$SubscribeDtoFromJson(Map<String, dynamic> json) {
  return SubscribeDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..name = json['name'] as String
    ..description = json['description'] as String
    ..subscribe_code = json['subscribe_code'] as String
    ..value = json['value'] as int;
}

Map<String, dynamic> _$SubscribeDtoToJson(SubscribeDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'subscribe_code': instance.subscribe_code,
      'value': instance.value,
    };
