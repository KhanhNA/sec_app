


import '../base_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'login_body.g.dart';

@JsonSerializable(explicitToJson: true)
class LoginBody extends BaseModel{
  String login;
  String password;
  @JsonKey(name: 'db')
  String db;

  LoginBody(this.login, this.password, this.db);

  factory LoginBody.fromJson(Map<String, dynamic> json) => _$LoginBodyFromJson(json);

  Map<String, dynamic> toJson() => _$LoginBodyToJson(this);

  @override
  String toString() {
    return 'LoginBody{login: $login, password: $password, db: $db}';
  }
}