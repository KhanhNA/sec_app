// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NotificationDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationDto _$NotificationDtoFromJson(Map<String, dynamic> json) {
  return NotificationDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..notification_id = json['notification_id'] as int
    ..is_read = json['is_read'] as bool
    ..create_uid = json['create_uid'] as int
    ..create_date = json['create_date'] as String
    ..sent_date = json['sent_date'] as String
    ..title = json['title'] as String
    ..content = json['content'] as String
    ..type = json['type'] as String
    ..click_action = json['click_action'] as String
    ..message_type = json['message_type'] as String
    ..item_id = json['item_id'] as String
    ..object_status = json['object_status'] as String
    ..image_256 = json['image_256'] as String
    ..description = json['description'] as String
    ..total_message_not_seen = json['total_message_not_seen'] as int;
}

Map<String, dynamic> _$NotificationDtoToJson(NotificationDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'notification_id': instance.notification_id,
      'is_read': instance.is_read,
      'create_uid': instance.create_uid,
      'create_date': instance.create_date,
      'sent_date': instance.sent_date,
      'title': instance.title,
      'content': instance.content,
      'type': instance.type,
      'click_action': instance.click_action,
      'message_type': instance.message_type,
      'item_id': instance.item_id,
      'object_status': instance.object_status,
      'image_256': instance.image_256,
      'description': instance.description,
      'total_message_not_seen': instance.total_message_not_seen,
    };
