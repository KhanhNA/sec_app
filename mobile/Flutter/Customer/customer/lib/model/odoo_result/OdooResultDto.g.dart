// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OdooResultDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OdooResultDto _$OdooResultDtoFromJson(Map<String, dynamic> json) {
  return OdooResultDto()
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..length = json['length'] as int
    ..total_record = json['total_record'] as int
    ..records = json['records'] as List;
}

Map<String, dynamic> _$OdooResultDtoToJson(OdooResultDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'length': instance.length,
      'total_record': instance.total_record,
      'records': instance.records,
    };
