import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/area/AreaDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';

part 'WarehouseDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class WarehouseDto extends _WarehouseDto {
  factory WarehouseDto.fromJson(Map<String, dynamic> js) =>
      _$WarehouseDtoFromJson(js);

  Map<String, dynamic> toJson() => _$WarehouseDtoToJson(this);

  WarehouseDto();
}

class _WarehouseDto extends Base {
  int id;
  String warehouse_code;
  String name;

  int state_id;
  int area_id;
  int district;
  int ward;
  int country_id;

  String province_name;
  String district_name;
  String ward_name;
  String address;

  double latitude;
  double longitude;
  String phone;
  AreaDto areaInfo;

  String name_seq;

  String fromWareHouseCode;

  String wjson_address;
  String placeId;

  String getVillage() {
    return ward_name != null ? this.ward_name+', ' : "";
  }

  String getFullAddress() {
    return address +
        ", " +
        getVillage()+
        getDistrictName() +
        getProvinceName();
  }

  String getDistrictName() {
    return district_name != null ? this.district_name+', ' : "";
  }

  String getProvinceName() {
    return province_name != null ? this.province_name+', ' : "";
  }

  String getAdministrative() {
    return (getVillage() + getDistrictName() + getProvinceName())==''?null: getVillage() + getDistrictName() + getProvinceName();
  }

  String getAddressDetail() {
    return address==null? address + "\n" + getAdministrative(): null;
  }
}
