import 'dart:io';

import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/base/util/DateUtils.dart';
import 'package:customer/model/area/AddressDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/create_account/api/create_account_repositoy.dart';
import 'package:get/get.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class CreateAccountVM extends BaseVM {
  final notificationStatus = ['step1', 'step2'];
  ListController<AddressDto> districts;
  ListController<AddressDto> provinces;
  ListController<AddressDto> careers;
  List<Asset> listImage= [];
  RxInt quantityImage= 0.obs;
  final repository = CreateAccountRepository();
  String areaType = "province";
  String firstName;
  String lastName;
  RxBool validateFirstName= true.obs;

  RxString password = "".obs;
  RxString passwordConfirm = "".obs;
  RxString email = "".obs;
  RxString dayOfBirth = "".obs;
  Rx<AddressDto> career = Rx(AddressDto()); // career selected
  Rx<AddressDto> province = Rx(AddressDto()); // province selected
  Rx<AddressDto> district = Rx(AddressDto()); // district selected

  CreateAccountVM();

  var genderObs = ''.obs;
  List<String> gender = ["male", "female"];

  void set selectedPeriod(String selectedGender) {
    genderObs.value = selectedGender;
  }

  String get selectedGender => genderObs.value;
}
