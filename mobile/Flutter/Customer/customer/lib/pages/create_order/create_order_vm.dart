import 'dart:collection';

import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/RecurrentModel.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/order_package/OrderPackageDto.dart';
import 'package:customer/model/product_type/ProductTypeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/create_order/api/create_order_repository.dart';
import 'package:get/get.dart';

class CreateOrderVM extends BaseVM {
  final createOrderStep = ['Step_1', 'Step_2', 'Step_3'];
  ListController<WarehouseDto> warehouseListController;
  ListController<BillServiceDto> serviceListController;
  ListController<SubscribeDto> subscribeListController;
  ListController<InsuranceDto> insuranceListController;
  ListController<OrderPackageDto> orderPackageListController;
  RxList<ProductTypeDto> productTypeList = RxList();
  final repository = CreateOrderRepository();
  RxList<BillServiceDto> billServiceDto = RxList();

  // step 1
  int createOrderId = 0; // value render id bill package
  int indexSelectedTemp = 0;
  Rx<BillPackageDto> billPackageDto = Rx(BillPackageDto()); // temp new package
  // step 2
  Map<int, int> hashMapQuantity =
      Map(); // HashMap<id_package_created,quantity_package>
  Map<int, BillPackageDto> hashMapBillPackage =
      Map(); // HashMap<id_package,BillPackage>
  Rx<BillLadingDto> billLadingDto =
      Rx(BillLadingDto()); // Bill package push server
  RxList<BillPackageDto> listBillPackage = RxList(); // temp list package

  Rx<BillLadingDetailDto> exportBillLadingDetail = Rx(BillLadingDetailDto());
  Rx<BillLadingDetailDto> importBillLadingDetail = Rx(BillLadingDetailDto());

  RxDouble quantity_package = 0.0.obs;
  Rx<DateTime> picked = Rx(DateTime.now());

  var frequencyObs = "Hàng ngày".obs;
  List<String> listFrequency = ["Hàng ngày", "Hàng tuần", 'Hàng tháng'];

  void set selectedPeriod(String selectedFrequency) {
    frequencyObs.value = selectedFrequency;
  }

  String get selectedFrequency => frequencyObs.value;
  RxList listDay = RxList([
    'CN',
    'T2',
    'T3',
    'T4',
    'T5',
    'T6',
    'T7',
  ]);
  RxList<int> listDate= RxList();
  Rx<RecurrentModel> recurrentModel= Rx(RecurrentModel());
}
