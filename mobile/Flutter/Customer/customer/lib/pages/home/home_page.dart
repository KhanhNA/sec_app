import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/widget/swipper_banner.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/menu_tab/menu_tab.dart';
import 'package:customer/pages/home/home_controller.dart';
import 'package:customer/pages/home/home_item_builder.dart';
import 'package:customer/pages/home/home_vm.dart';
import 'package:customer/pages/main/routes/app_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'search_float_bar.dart';

class HomePage extends BaseView<HomeVM, HomeController> {
  List<MenuTab> menuTab = [
    MenuTab('assets/images/truck.png', 'menu.Routing_day'.tr, Routes.ROUTING),
    MenuTab('assets/images/list_order.png', 'List orders'.tr, Routes.LIST_ORDER),
    MenuTab('assets/images/warehouse.png', 'Warehouse'.tr, Routes.WAREHOUSE_MANAGER),
    MenuTab('assets/images/reward_point.png', 'Reward point'.tr, 'Routing test'),
    MenuTab('assets/images/wallet.png', 'Wallet'.tr, 'Notification test'),
    MenuTab('assets/images/telegram.png', 'menu.Info'.tr, 'Info test'),
  ];
  List<String> banners = <String>[
    'https://scontent.fhan3-1.fna.fbcdn.net/v/t1.0-9/91110052_2805867949529110_148021057104642048_o.jpg?_nc_cat=110&ccb=1-3&_nc_sid=e3f864&_nc_ohc=UlSMhgAVPzMAX9AL8vG&_nc_ht=scontent.fhan3-1.fna&oh=d3d7bb34793c3b8a3c5270b9540240d0&oe=6072CB1D',
    'https://scontent.fhan3-3.fna.fbcdn.net/v/t31.0-8/12489400_871510432964881_7636134494817280366_o.png?_nc_cat=101&ccb=1-3&_nc_sid=19026a&_nc_ohc=P17APJ4fjCQAX8z2RcB&_nc_ht=scontent.fhan3-3.fna&oh=4476136c7d15c5d8d733e7fa8eb1a9f5&oe=6075AB3A'
  ];

  @override
  Widget body(BuildContext context) {
    List<Widget> listWidgets = menuTab.map((e) {
      return Container(
        height: 80,
        width: 100,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8), color: Colors.white),
        child: InkWell(
            onTap: () => Navigator.pushNamed(context, e.keyPage),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  e.icon,
                  width: 40,
                  height: 32,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Text(e.name,
                      style: TextStyle(
                        fontSize: 13.0,
                        height: 1.5,
                        fontWeight: FontWeight.normal,
                        decoration: TextDecoration.none,
                      )),
                )
              ],
            )),
      );
    }).toList();
    return SliverView(
      widgetBefore: [
        SliverToBoxAdapter(
            child: Container(
                child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Image.asset(
                    'assets/images/truck.png',
                    height: 48,
                    fit: BoxFit.fitHeight,
                  ),
                  Container(
                    child: context.scanQrCode(onQrData: (qrCode) {
                      viewModel.searchKeyword.value = qrCode;
                      return true;
                    },),

                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Text(
                'Hello, ' + AppUtils.partnerDto.name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 26,
                    fontWeight: FontWeight.normal),
              ),
            ),
            SearchBar(controller),
            Container(
              height: 240,
              child: GridView.count(
                controller: ScrollController(),
                  crossAxisCount: 3,
                  children: List.generate(listWidgets.length, (index) {
                    return Center(
                      child: listWidgets[index],
                    );
                  })),
            ),
            SwipperBanner(banners: banners),
          ],
        ))),
        // SliverView
      ],
      listController: viewModel.listPromotion,
      itemBuilder: HomeItemBuilder(controller).buildItem,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 20,
      ),
    );
  }

  @override
  HomeController putController() {
    return HomeController(HomeVM());
  }
}
