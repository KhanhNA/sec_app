import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchBar extends GetView<BaseController> {
  final BaseController controller;
  String textSearch;
  SearchBar(this.controller,{this.textSearch= 'input text search'});

  @override
  Widget build(BuildContext context)  {
    return  Column(
      children: [
        context.textSearch(
            onchange: (text) {
              controller.textSearch.value = text;
            },
            hintText: textSearch,
            initValue: controller.textSearch.value
        ),
      ],
    );
  }
}
