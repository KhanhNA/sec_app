import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/base/ui/text/text_ui.dart';
import 'package:customer/enum/warehouse_type.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/pages/list_order/bill_lading_detail/bill_lading_detail_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BillLadingDetailItem{
  final BillLadingDetailController controller1;
  BillLadingDetailItem(this.controller1);
  Widget buildItem(
      BuildContext context, BillLadingDetailDto billLadingDetailDto, int index) {
    return CommonUI.card(
      child: Column(
        children: [
          context.listItemHeader(
            (''),
            context.textDrawableStart(
                "viewDetail".tr,
                CustomIcons.fromName('FontAwesome.0xf105'),
                Theme.of(context).primaryColor,
                isStart: false),
            onTap: () {
              controller1.details(billLadingDetailDto);
            },
          ),
          context.divider(),
          context.listItemView(billLadingDetailDto?.warehouse?.name, billLadingDetailDto.warehouse_type==WarehouseType().Export? 'Bill_lading.warehouse_export'.tr : 'Bill_lading.warehouse_import'.tr),
          context.listItemView('Bill_lading.order_code'.tr, billLadingDetailDto?.name),
          context.listItemView('Bill_lading.address'.tr, billLadingDetailDto?.address),
          context.listItemView('Bill_lading.total_weight'.tr, billLadingDetailDto?.total_weight?.toString()),
          context.listItemView('Bill_lading.total_amount'.tr, billLadingDetailDto?.price?.toString()??'0'),
        ],
      ),
    );
  }
}