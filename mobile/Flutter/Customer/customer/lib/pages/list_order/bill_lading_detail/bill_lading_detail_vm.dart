import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';

class BillLadingDetailVM extends BaseVM{
  int bill_ladding_id;
  final billLadingDto = BillLadingDto().obs;
  ListController<BillLadingDetailDto> listController= ListController<BillLadingDetailDto>();
}