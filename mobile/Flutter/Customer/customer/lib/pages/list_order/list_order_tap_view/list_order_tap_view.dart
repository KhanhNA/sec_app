import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/pages/list_order/list_order_tap_view/List_order_tap_view_vm.dart';
import 'package:customer/pages/list_order/list_order_tap_view/list_order_tap_view_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:customer/base/ui/list/list_ui.dart';

import 'list_order_tap_view_item.dart';
import 'search_float_bar.dart';



class ListOrderTapView extends BaseView<ListOrderTapViewVM,ListOrderTapViewController>{

  @override
  Widget body(BuildContext context) {
    controller.changeStatus(status);
    return SliverView(
      floatBar: SearchBar(controller),
      listController: viewModel.listControllers[status],
      itemBuilder: ListOrderTapViewItem(controller).buildItem,
      // widgetBefore: [context.sliverTotalRows(viewModel.totalElements)],
    );
  }

  @override
  ListOrderTapViewController putController() {
    return ListOrderTapViewController(ListOrderTapViewVM());
  }
  BillLadingStatus status;
  ListOrderTapView(this.status);
}
