import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:customer/pages/login/login_controller.dart';
import 'package:responsive_widgets/responsive_widgets.dart';

class CustomSelectLanguageWidget extends StatelessWidget {

  final LoginController controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return ContainerResponsive(
        child: ButtonTheme(
          alignedDropdown: true,
          child: DropdownButton(
            underline: null,
            elevation: 4,
            dropdownColor: Theme.of(context).primaryColor,
            hint: Row(
              children: [
                Image.asset(
                  'assets/images/flag_en.png',
                  width: 30,
                  height: 30,
                ),
              ],
            ),
            value: controller.lang,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 20,
            iconEnabledColor: Colors.white,
            onChanged: (value) => controller.changeLanguage(value),
            items: [
              DropdownMenuItem(
                  value: 'vi',
                  child: Row(
                    children: [
                      Padding (
                        padding: EdgeInsets.only(right: 5),
                        child: Image.asset(
                          'assets/images/flag_vn.png',
                          width: 20,
                          height: 20,
                        ),
                      ),
                      Text(
                        'Việt Nam',
                        style: Theme.of(context).textTheme.bodyText1.apply(
                            color: Colors.white
                        ),
                      ),
                    ],
                  )
              ),
              DropdownMenuItem(
                  value: 'en',
                  child: Row(
                    children: [
                      Padding (
                        padding: EdgeInsets.only(right: 5),
                        child: Image.asset(
                          'assets/images/flag_en.png',
                          width: 20,
                          height: 20,
                        ),
                      ),
                      Text(
                        'English',
                        style: Theme.of(context).textTheme.bodyText1.apply(
                            color: Colors.white
                        ),
                      ),
                    ],
                  )
              ),
              // DropdownMenuItem(
              //   value: 'es-MX',
              //   child: Image.asset(
              //     'assets/images/es-flag.jpg',
              //     width: 30,
              //     height: 30,
              //   ),
              // ),
            ],
          ),
        )
    );
  }
}

class Language {
  String $value;
  String $image;
  String $name;

  Language ({
    this.$value = "",
    this.$image = "",
    this.$name = "",
  });

  String get value => $value;
  String get image => $image;
  String get name => $name;

}