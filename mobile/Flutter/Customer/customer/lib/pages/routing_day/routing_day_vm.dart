import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/base/util/DateUtils.dart';
import 'package:customer/model/bill_routing/BillRoutingDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import 'api/routing_day_repository.dart';

class RoutingDayVM extends BaseVM {
  RxString date = "".obs;


  final RoutingDayRepository repository = RoutingDayRepository();
  ListController<BillRoutingDto> listController;
  RxInt totalElements = 0.obs;
  BillRoutingDto billRoutingDto = BillRoutingDto();
  initData(){
    var now = DateTime.now();
    date.value = DateTimeUtils.convertDateToString(now, DateTimeUtils.DATE_JSON);
  }
}
