import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';
import 'package:driver/base/auth/refresh_token_body.dart';
import 'package:driver/base/auth/token.dart';
import 'package:driver/base/util/AppUtils.dart';

import 'api_auth_repository.dart';


class DioLoggingInterceptors extends InterceptorsWrapper {

  final Dio _dio;
  // final SharedPreferencesManager _sharedPreferencesManager = locator<SharedPreferencesManager>();
  final box = GetStorage();
  DioLoggingInterceptors(this._dio){
    AppUtils.token = box.read(AppUtils.keyAccessToken);
  }

  @override
  Future onRequest(RequestOptions options) async {
    options.headers.addAll({'Accept-Encoding':'identity', 'Accept-Language':'vi'});
    print("--> ${options.method != null ? options.method.toUpperCase() : 'METHOD'} ${"" + (options.baseUrl ?? "") + (options.path ?? "")}");
    print("Headers:");
    options.headers.forEach((k, v) => print('$k: $v'));
    if (options.queryParameters != null) {
      print("queryParameters:");
      options.queryParameters.forEach((k, v) => print('$k: $v'));
    }
    if (options.data != null) {
      print("Body: ${options.data}");
    }
    print("--> END ${options.method != null ? options.method.toUpperCase() : 'METHOD'}");

    if (options.headers.containsKey('requirestoken')) {
      options.headers.remove('requirestoken');
      print('accessToken: ${box.read(AppUtils.keyAccessToken)}');
      String accessToken = box.read(AppUtils.keyAccessToken);
      options.headers.addAll({'Authorization': 'Bearer $accessToken'});
    }
    return options;
  }

  @override
  Future onResponse(Response response) {
    print(
        "<-- ${response.statusCode} ${(response.request != null ? (response.request.baseUrl + response.request.path) : 'URL')}");
    print("Headers:");
    response.headers?.forEach((k, v) => print('$k: $v'));
    print("Response: ${response.data}");
    print("<-- END HTTP");
    return super.onResponse(response);
  }

  @override
  Future onError(DioError dioError) async {
    print(
        "<-- ${dioError.message} ${(dioError.response?.request != null ? (dioError.response.request.baseUrl + dioError.response.request.path) : 'URL')}");
    print(
        "${dioError.response != null ? dioError.response.data : 'Unknown Error'}");
    print('--------------------------------------------------------------------------------------------------------');
    print('-                                                                                                      -');
    print('-                                                                                                      -');
    print('- ${dioError?.response?.data['localizedMessage']}                                                               -');
    print('-                                                                                                      -');
    print('-                                                                                                      -');
    print('--------------------------------------------------------------------------------------------------------');
    print("<-- End error");

    int responseCode = dioError.response.statusCode;
    String oldAccessToken = box.read(AppUtils.keyAccessToken);
    if (oldAccessToken != null && responseCode == 401 && box != null) {
      _dio.interceptors.requestLock.lock();
      _dio.interceptors.responseLock.lock();

      String refreshToken = box.read(AppUtils.keyRefreshToken);
      RefreshTokenBody refreshTokenBody = RefreshTokenBody('refresh_token', refreshToken);
      ApiAuthRepository apiAuthRepository = ApiAuthRepository();
      Token token;
      try {
        token = await apiAuthRepository.postRefreshAuth(refreshTokenBody);
      }catch(ex){
        _dio.interceptors.requestLock.unlock();
        _dio.interceptors.responseLock.unlock();
        return;
      }
      AppUtils.updateToken(token);
      // ApiAuthProvider.token = newAccessToken;
      RequestOptions options = dioError.response.request;
      options.headers.addAll({'requirestoken': true});
      _dio.interceptors.requestLock.unlock();
      _dio.interceptors.responseLock.unlock();
      return _dio.request(options.path, options: options);
    } else {
      super.onError(dioError);
      // throw Exception('Exception');//dioError;
    }
  }
}