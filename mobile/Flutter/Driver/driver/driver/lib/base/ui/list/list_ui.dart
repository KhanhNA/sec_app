import 'package:driver/pages/bottomsheet/list/BottomSheetList.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_more_list/loading_more_list.dart';
import 'package:driver/base/controller/BaseZone.dart';
import 'package:driver/base/ui/common_ui.dart';
import 'package:driver/base/ui/qr/ScanQrWidget.dart';
import 'package:driver/icon/custom_icons.dart';
import 'package:driver/theme/app_theme.dart';

extension ListUi on BuildContext {
  Widget listItemView(String lbl, String value,
      {padding: AppTheme.cardFieldPadding,
      background: Colors.transparent,
      styleLb,
      styleVl}) {
    return this.listItemViewWidget(
        lbl,
        Text(
          value ?? '',
          style: styleVl ?? Theme.of(this).textTheme.bodyText2,
          textAlign: TextAlign.right,
        ),
        padding: padding,
        background: background,
        styleLb: styleLb);
  }

  Widget listItemViewV2(BuildContext context,String lbl, String value) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Text(
                lbl,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 14),
              )),
          Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: Text(
                value,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 14),
              ))
        ],
      ),
    );
  }

  Widget itemView(
    String value,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Text(
        value,
        style: TextStyle(
            color: Colors.black, fontSize: 12, fontWeight: FontWeight.normal),
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget itemTitleView(
    String value,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Text(
        value,
        style: TextStyle(
            color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold),
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget listItemViewWidget(String lbl, Widget value,
      {padding: AppTheme.cardFieldPadding,
      background: AppTheme.white,
      styleLb}) {
    return Container(
        color: background,
        padding: padding,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          FittedBox(
              fit: BoxFit.contain,
              child: Text(
                GetUtils.isNullOrBlank(lbl) ? ' ' : lbl,
                style: styleLb ?? Theme.of(this).textTheme.caption,
              )),
          Expanded(
              child: Container(
            child: value,
            alignment: Alignment.centerRight,
          ))
        ]));
  }

  Widget listItemSubViewStatusWidget(String lbl, String status, Color color,
      {padding: AppTheme.cardFieldPadding,
      background: Colors.transparent,
      styleLb,
      styleVl}) {
    return listItemViewStatusWidget(lbl, status, color,
        padding: padding,
        background: background,
        styleLb: styleLb,
        styleVl: Theme.of(this).textTheme.bodyText1);
  }

  Widget listItemViewStatusWidget(String lbl, String status, Color color,
      {padding: AppTheme.cardHeaderPadding,
      background: Colors.transparent,
      styleLb,
      styleVl}) {
    return Container(
      padding: padding,
      child: Row(
        children: [
          Expanded(
              child: Text(lbl,
                  style: styleVl ?? Theme.of(this).textTheme.headline6)),
          Text(status ?? ' '),
          Padding(
              padding: EdgeInsets.only(left: 3),
              child: Icon(
                Icons.circle,
                size: 10,
                color: color,
              ))
        ],
      ),
    );
  }

  Widget listItemViewWidgetFixWidth(String lbl, Widget value,
      {width: 100.0, lblStyle, paddingStyle: AppTheme.cardFieldPadding}) {
    return Padding(
        padding: paddingStyle,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          SizedBox(
              width: width,
              child: Text(
                lbl == null ? '' : lbl,
                style: lblStyle == null
                    ? Theme.of(this).textTheme.bodyText1
                    : lblStyle,
                maxLines: 3,
              )),
          Expanded(
              child: Container(
            child: value,
            alignment: Alignment.centerRight,
          ))
        ]));
  }

  Widget actionWidgetExpanded(Widget child,
      {visible: true,
      onTap,
      showSuccess = false,
      String successMessage = 'success',
      showError = true,
      String errorMessage,
      ButtonStyle style,
      showConfirmDlg = false,
      String titleConfirm,
      String messageConfirm,
      String ok,
      String cancel}) {
    return actionWidget(child,
        visible: visible,
        onTap: onTap,
        showSuccess: showSuccess,
        successMessage: successMessage,
        showError: showError,
        errorMessage: errorMessage,
        style: style,
        isExpanded: true,
        showConfirmDlg: showConfirmDlg,
        titleConfirm: titleConfirm,
        messageConfirm: messageConfirm,
        ok: ok,
        cancel: cancel);
  }

  Widget actionWidget(Widget child,
      {visible: true,
      onTap,
      showSuccess = false,
      String successMessage = 'successMessage',
      showError = true,
      String errorMessage,
      ButtonStyle style,
      isExpanded = false,
      showConfirmDlg = false,
      String titleConfirm,
      String messageConfirm,
      String ok,
      String cancel}) {
    void action() {
      if (showConfirmDlg) {
        CommonUI.showConfirmDialog(
                title: titleConfirm,
                message: messageConfirm,
                ok: ok,
                cancel: cancel)
            .then((value) {
          if (value == ButtonActionType.OK) {
            BaseZone.run(() => onTap(),
                showError: showError,
                errorMessage: errorMessage,
                showSuccess: showSuccess,
                successMessage: successMessage);
          }
        });
      } else {
        BaseZone.run(onTap,
            showError: showError,
            errorMessage: errorMessage,
            showSuccess: showSuccess,
            successMessage: successMessage);
      }
    }

    final btn = OutlinedButton(
        child: child,
        style: style ??
            OutlinedButton.styleFrom(
                backgroundColor: Theme.of(this).primaryColor,
                textStyle: Theme.of(this).primaryTextTheme.bodyText2),
        onPressed: onTap == null ? null : action);
    return Visibility(
        visible: visible, child: isExpanded ? Expanded(child: btn) : btn);
  }

  Widget listItemSubHeader(String lbl, Widget value,
      {lblStyle: AppTheme.normalBold,
      visible: true,
      onTap,
      divider = true,
      showSuccess = false,
      String successMessage = 'success',
      showError = true,
      String errorMessage,
      ButtonStyle btnStyle,
      padding: AppTheme.cardFieldPadding}) {
    return listItemHeader(lbl, value,
        lblStyle: lblStyle,
        visible: visible,
        onTap: onTap,
        divider: divider,
        showSuccess: showSuccess,
        successMessage: successMessage,
        showError: showError,
        errorMessage: errorMessage,
        btnStyle: btnStyle,
        padding: padding);
  }

  Widget listItemHeader(String lbl, Widget value,
      {lblStyle,
      visible: true,
      onTap,
      divider = true,
      showSuccess = false,
      String successMessage = 'success',
      showError = true,
      String errorMessage,
      ButtonStyle btnStyle,
      padding: AppTheme.cardHeaderPadding}) {
    return Container(
        padding: padding,
        child: Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Expanded(
                child: Text(
              lbl == null ? '' : lbl,
              style: lblStyle == null
                  ? Theme.of(this).textTheme.headline6
                  : lblStyle,
            )),
            value == null
                ? Container()
                : this.actionWidget(value,
                    visible: visible,
                    onTap: onTap,
                    showError: showError,
                    showSuccess: showSuccess,
                    successMessage: successMessage,
                    errorMessage: errorMessage,
                    style: btnStyle ??
                        TextButton.styleFrom(
                            side: BorderSide.none,
                            minimumSize: Size(1, 1),
                            padding: EdgeInsets.zero,
                            textStyle:
                                Theme.of(this).primaryTextTheme.bodyText1,
                            tapTargetSize: MaterialTapTargetSize.shrinkWrap))
          ]),
        ]));
  }

  Widget textOnTap(String lbl, {onTap}) {
    return InkWell(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
            child: Text(
              lbl,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Colors.black),
            ),
          ),
          Icon(Icons.navigate_next)
        ],
      ),
    );
  }
  Widget titleContainer(String lbl, Icon icon) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          icon,
          Padding(
            padding: const EdgeInsets.fromLTRB(5,0,0,0),
            child: Text(lbl, style: TextStyle(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 12),),
          )
        ],
      ),
    );
  }

  Widget datePicker(
      {DateTime initialDate,
      DateTime firstDate,
      DateTime lastDate,
      void Function(DateTime) onValue,
      String fieldLabelText,
      String hintText}) {
    return Row(children: [
      Expanded(
          child: InputDatePickerFormField(
        initialDate: initialDate,
        firstDate: (firstDate ?? initialDate),
        lastDate: lastDate ?? DateTime.now(),
        fieldLabelText: fieldLabelText,
        fieldHintText: hintText,
      )),
      FittedBox(
          fit: BoxFit.contain,
          child: InkWell(
              child: Icon(
                Icons.calendar_today_outlined,
                color: AppTheme.grey,
              ),
              onTap: () {
                showDatePicker(
                        context: this,
                        initialDate: initialDate,
                        firstDate: firstDate ?? initialDate,
                        lastDate: lastDate ?? DateTime.now())
                    .then((value) {
                  if (onValue != null) onValue(value);
                });
              }))
    ]);
  }

  Widget textSearchAndQrCode(
      {@required void Function(String text) onchange,
      String hintText,
      TextEditingController textController,
      bool Function(String qrCode) onQrData,
      String initValue}) {
    return Container(
        padding: EdgeInsets.only(right: 15, top: 8),
        child: Row(
          children: [
            ScanQrWidget(
                onData: (String qrCode) {
                  if (onQrData != null) {
                    return onQrData(qrCode);
                  } else {
                    return false;
                  }
                },
                child: IgnorePointer(
                  child: IconButton(
                    icon: Icon(CustomIcons.fromName('CustomIcons.0xe80f')),
                    iconSize: 30,
                    color: Color(0xFF919395),
                    onPressed: () {},
                  ),
                )),
            Expanded(
              child: Container(
                  height: 40,
                  // decoration: _boxDecoration,
                  child: new TextFormField(
                    onChanged: (text) => onchange(text),
                    textInputAction: TextInputAction.search,
                    textAlignVertical: TextAlignVertical.center,
                    controller: textController,
                    initialValue: initValue,
                    decoration: InputDecoration(
                      hintText: hintText,
                      filled: true,
                      hintStyle: TextStyle(color: AppTheme.txtHintColor),
                      prefixIcon:
                          Icon(CustomIcons.fromName('CustomIcons.0xe810')),
                      fillColor: AppTheme.backgroundTextFile,
                      isDense: true,
                      contentPadding: EdgeInsets.all(2),
                    ),
                  )),
            )
          ],
        ));
  }

  Widget textSearch(
      {@required void Function(String text) onchange,
      String hintText,
      TextEditingController textController,
      String initValue}) {
    return Container(
        padding: EdgeInsets.only(right: 15, top: 8),
        child: Row(
          children: [
            Expanded(
              child: Container(
                  height: 40,
                  // decoration: _boxDecoration,
                  child: new TextFormField(
                    onChanged: (text) => onchange(text),
                    textInputAction: TextInputAction.search,
                    textAlignVertical: TextAlignVertical.center,
                    controller: textController,
                    initialValue: initValue,
                    decoration: InputDecoration(
                      hintText: hintText,
                      filled: true,
                      hintStyle: TextStyle(color: AppTheme.txtHintColor),
                      prefixIcon:
                          Icon(CustomIcons.fromName('CustomIcons.0xe810')),
                      fillColor: AppTheme.backgroundTextFile,
                      isDense: true,
                      contentPadding: EdgeInsets.all(2),
                    ),
                  )),
            )
          ],
        ));
  }

  Widget totalRows(RxInt obs) {
    return Padding(
      padding: EdgeInsets.only(left: 20, top: 10, bottom: 5),
      child: Obx(() => Text(
            "${'All'.tr} (${CommonUI.formatNumber(obs.value)})",
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
    );
  }

  Widget sliverTotalRows(RxInt obs) {
    return SliverToBoxAdapter(
        // alignment: Alignment.centerLeft,
        child: this.totalRows(obs));
  }

  Widget divider({padding: AppTheme.cardDividerPadding}) {
    return Container(
        child: Divider(
          color: Theme.of(this).dividerColor,
          height: 1,
          thickness: 1,
        ),
        padding: padding);
  }

  Widget dividerVertical({padding: AppTheme.cardDividerPadding}) {
    return Container(
        decoration: new BoxDecoration(
            border: Border(
                right: true
                    ? BorderSide(color: AppTheme.divider)
                    : BorderSide.none)));
  }

  Widget wrapListChipGroup<T>(List<T> list,
      {Rx<T> obs, onSelected, String Function(T) display}) {
    return Wrap(
        spacing: 0,
        runSpacing: -8,
        children: this.listChipGroup(list,
            obs: obs, onSelected: onSelected, display: display));
  }

  List<Widget> listChipGroup<T>(List<T> list,
      {Rx<T> obs, onSelected, String Function(T) display}) {
    List<Widget> choices = [];
    list.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(left: 2, right: 2),
        child: ChoiceChip(
          label: Text(
            display == null ? item.toString() : display(item),
            style: item == obs.value //controller.repackingPlanningStatus
                ? TextStyle(color: AppTheme.white)
                : TextStyle(color: AppTheme.textBold),
          ),
          labelStyle: TextStyle(color: Colors.black, fontSize: 12.0),
          shape: RoundedRectangleBorder(
            side: BorderSide(color: AppTheme.backgroundStrokeTextFile),
            borderRadius: BorderRadius.circular(16.0),
          ),
          backgroundColor: Theme.of(this).backgroundColor,
          selectedColor: Theme.of(this).primaryColor,
          selected: obs.value == item,
          onSelected: (selected) {
            obs.value = item;
            // print(item.name);
            // Get.find<RepackController>().typePackSelect(item);
            // Get.find<RepackController>().getListRepacking();
            // controller.repackingPlanningStatus = selected;
            if (onSelected != null) onSelected();
          },
        ),
      ));
    });
    return choices;
  }

  // Widget datePicker({DateTime initDate, DateTime firstDate, DateTime lastDate, onSelect}){
  //   return GestureDetector(
  //                   onTap: () {
  //                     showDatePicker(
  //                         context: this,
  //                         builder: (BuildContext context, Widget child) {
  //                           return AppTheme.themeCalender(child);
  //                         },
  //                         initialDate: initDate??DateTime.now(),
  //                         firstDate: firstDate??DateTime(2020),
  //                         lastDate:lastDate??DateTime.now())
  //                         .then((date) => onSelect(date));
  //                   },
  //                   child: Center(
  //                     child: Container(
  //                       width: (size.width - 33) / 2 - 1,
  //                       padding: EdgeInsets.only(left: 20, top: 12, bottom: 12),
  //                       child: MyDatePicker(),
  //                     ),
  //                   ),
  //                 );
  // }
//xem sau

  // Widget listItemDescription(String desciption, {background: AppTheme.white, styleLb: AppTheme.normal, styleVl: AppTheme.normalBold}) {
  //   return Column(children: [Text('description'.tr, style: AppTheme.normalBold),
  //     Expanded(child: Text(
  //       (desciption) ?? ' ',
  //       style: styleLb,
  //       maxLines: 5,
  //     ),
  //     )
  //
  //   ]);
  // }
  Widget listItemDescription(String description,
      {background: AppTheme.white,
      styleLb: AppTheme.normal,
      styleVl: AppTheme.normalBold}) {
    return Container(
        padding: AppTheme.cardFieldPadding,
        color: background,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        ('description'.tr),
                        style: styleVl,
                      )),
                ]),
          ),
          (description != null && description.isNotEmpty)
              ? Container(
                  padding: AppTheme.edgeTop,
                  child: Text(
                    (description),
                    style: styleLb,
                    maxLines: 5,
                  ),
                )
              : SizedBox(),
        ]));
  }

  showBottomSheet<T>(
      Widget title,
      Function(BuildContext context, T item, int index) itemBuilder,
      LoadingMoreBase<T> sourceList) {
    showModalBottomSheet(
      context: this,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(8),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (context) {
        return BottomSheetList(title, itemBuilder, sourceList);
      },
    );
  }
}

// class ListUI {
// static Widget listItemView(String lbl, String value, {padding: AppTheme.edgeTop, background: AppTheme.white, styleLb: null, styleVl: AppTheme.normalBold}) {
//   lbl = lbl == null ? '' : lbl;
//   value = value == null ? '' : value;
//   return Container(
//       color: background,
//       padding: padding,
//       child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
//         FittedBox(
//             fit: BoxFit.contain,
//             child: Text(
//               (lbl),
//               style: styleLb,
//             )),
//         Expanded(
//             child: Text(
//           value,
//           style: styleVl,
//           textAlign: TextAlign.right,
//         ))
//       ]));
// }

// static Widget greyDivider() {
//   return Container(
//     height: 1,
//     color: Colors.grey[200],
//   );
// }

// static Widget listItemDescription(String desciption, {background: AppTheme.white, styleLb: AppTheme.normal, styleVl: AppTheme.normalBold}) {
//   return Container(
//       color: background,
//       child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//         Container(
//           child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
//             FittedBox(
//                 fit: BoxFit.contain,
//                 child: Text(
//                   ('description'.tr),
//                   style: styleVl,
//                 )),
//           ]),
//         ),
//         (desciption != null && desciption.isNotEmpty)
//             ? Container(
//                 padding: AppTheme.edgeTop,
//                 child: Text(
//                   (desciption),
//                   style: styleLb,
//                   maxLines: 5,
//                 ),
//               )
//             : SizedBox(),
//       ]));
// }

// static Widget listItemViewWidget(String lbl, Widget value, {lblStyle: AppTheme.normal, paddingStyle: AppTheme.edgeTop}) {
//   return Padding(
//       padding: paddingStyle,
//       child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
//         FittedBox(fit: BoxFit.contain, child: Text(lbl == null ? '' : lbl, style: lblStyle)),
//         Expanded(
//             child: Container(
//           child: value,
//           alignment: Alignment.centerRight,
//         ))
//       ]));
// }

// static Widget listItemViewWidgetFixWidth(String lbl, Widget value, {width: 100.0, lblStyle: AppTheme.normal, paddingStyle: AppTheme.edgeTop}) {
//   return Padding(
//       padding: paddingStyle,
//       child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
//         SizedBox(width: width, child: Text(lbl == null ? '' : lbl, style: lblStyle)),
//         Expanded(
//             child: Container(
//           child: value,
//           alignment: Alignment.centerRight,
//         ))
//       ]));
// }

// static Widget listItemHeaderWidget(String lbl, Widget value, {lblStyle: AppTheme.normal}) {
//   return Padding(padding: AppTheme.edgeTop, child: Row(
//       // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [Expanded(child: Text(lbl == null ? '' : lbl, style: lblStyle)), FittedBox(fit: BoxFit.contain, child: Container(child: value))]));
// }

// static Widget listItemHeader(String lbl, Widget value, {lblStyle: AppTheme.normalBold, visible: true, onTap, divider=true}) {
//   return Padding(
//       padding: AppTheme.edgeTop,
//       child: Column(children: [Row(
//           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             Expanded(child: Text(lbl == null ? '' : lbl, style: lblStyle)),
//             Visibility(
//                 visible: visible,
//                 child:onTap==null?value: InkWell(
//                     child: value,
//                     onTap: () {
//                       BaseZone.run(() => onTap());
//                     }))
//             // divider?Divider(color: AppTheme.grey):Divider(color: AppTheme.grey)
//           ]),
//             Divider()
//
//             // FittedBox(fit: BoxFit.contain, child: Container(child: value))
//           ]));
// }

// static Widget listItemEditTextWidget(String lbl, Widget value, {id, lblStyle: AppTheme.normal}) {
//   return Padding(
//       padding: AppTheme.edgeTop, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Expanded(child: Text(lbl == null ? '' : lbl, style: lblStyle)), Expanded(child: value)]));
// }

// static Widget listItemFormEditTextWidget(String lbl, Widget value, {id, lblStyle: AppTheme.normal}) {
//   return Padding(
//       padding: AppTheme.edgeTop,
//       child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
//         Expanded(
//             child: MyTextFormField(
//                 // name: 'text',
//                 )),
//         Expanded(child: value)
//       ]));
// }

// static Widget headerItem(String lbl, Color colors) {
//   return Container(
//     color: colors,
//     padding: EdgeInsets.only(left: 16, top: 8, right: 16, bottom: 8),
//     child: Row(
//       children: <Widget>[Flexible(child: new Text(lbl == null ? '' : lbl, style: AppTheme.title))],
//     ),
//   );
// }

// static List<Widget> productPackingInfoWidgets(ProductPacking productPacking) {
//   List<Widget> widgets = [];
//   if (productPacking.barcode != null) {
//     widgets.add(ListUI.listItemView(("barCode").tr, productPacking.barcode));
//   }
//   if (productPacking.packingType != null) {
//     var packingType = productPacking.packingType;
//     widgets.add(ListUI.listItemView("Pack type".tr, "${packingType.code} - ${packingType.quantity}"));
//   }
//   if (productPacking.uom != null) {
//     //widgets.add(ListUI.listItemView(tr("uom"), productPacking.uom));
//   }
//   if (productPacking.code != null) {
//     widgets.add(ListUI.listItemView(("packCode").tr, productPacking.code));
//   }
//
//   if (productPacking.product != null) {
//     var product = productPacking.product;
//     if (product.availableDate != null) {
//       widgets.add(ListUI.listItemView(("availableDate").tr, DateTimeUtils.convertDateToString(product.availableDate, DateTimeUtils.DATE_SHOW_FORMAT)));
//     }
//   }
//   return widgets;
// }

// static Widget header(String lbl, String value, {padding: AppTheme.edgeTop, background: AppTheme.white, styleLb: AppTheme.normal, styleVl: AppTheme.normalBold}) {
//   lbl = lbl == null ? '' : lbl;
//   value = value == null ? '' : value;
//   return Container(
//       color: background,
//       padding: padding,
//       child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
//         Expanded(
//             child: Text(
//           (lbl),
//           style: styleLb,
//         )),
//         Text(
//           value,
//           style: styleVl,
//           textAlign: TextAlign.right,
//         )
//       ]));
// }

// static List<Widget> purchaseOrderDetailInfoItem(Po po, String langCode) {
//   return [
//     ListUI.listItemViewWidget(
//         "\#" + po.code,
//         InkWell(
//           onTap: () {
//             Get.to(PurchaseOrderDetailPage(po))
//                 .then((value) {
//
//             });
//           },
//           child: Container(
//             padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
//             child: TextUI.textDrawableStart('viewDetail'.tr,
//                 Icons.arrow_forward_ios_outlined, AppTheme.mainColor,
//                 isStart: false),
//             alignment: Alignment.centerRight,
//           ),
//         ),
//         lblStyle: AppTheme.normalBold),
//     Container(padding: AppTheme.edgeTop, child: ListUI.greyDivider()),
//     ListUI.listItemView("manufacturer".tr, po.manufacturer != null ? (po.manufacturer.code + " - " + po.manufacturer.getManufacturerName(langCode)) : ''),
//     ListUI.listItemView("distributor".tr, po.distributor != null ? (po.distributor.code + " - " + po.distributor.name) : ''),
//     ListUI.listItemView("createDate".tr, po.createDateString),
//     ListUI.listItemView("totalAmount", "${po.total}"),
//     Container(padding: AppTheme.edgeTop, child: ListUI.greyDivider()),
//     Container(padding: AppTheme.edgeTop, child: ListUI.listItemDescription(po?.description))
//   ];
// }
//
// static List<Widget> purchaseOrderDetailPackInfoItem(PoDetail poDetail, String langCode) {
//   return [
//     ListUI.listItemView(poDetail.product.name, "", styleVl: AppTheme.title, padding: EdgeInsets.only(top: 5), styleLb: AppTheme.title),
//     ListUI.listItemView("pack.PACK_ID".tr, poDetail.productPacking.code),
//     ListUI.listItemView("pack.PACK_TYPE".tr, "${poDetail.productPacking.packingType.code} - ${poDetail.productPacking.packingType.quantity}"),
//     ListUI.listItemView("pack.QUANTITY".tr, "${poDetail.quantity}"),
//     ListUI.listItemView("price".tr, "${poDetail.productPackingPrice.price}"),
//     ListUI.listItemView("totalAmount".tr, "${poDetail.productPackingPrice.price * poDetail.quantity}"),
//   ];
// }
// }
