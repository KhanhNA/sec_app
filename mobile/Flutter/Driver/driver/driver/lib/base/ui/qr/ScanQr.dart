import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:driver/theme/app_theme.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRViewExample extends StatefulWidget {
  final bool Function(String qrCode) onData;
  final bool showCode;
  final Widget Function(String qrCode) buildShowWidget;

  const QRViewExample(this.onData,
      {Key key, this.showCode = false, this.buildShowWidget})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => QRViewExampleState();
}

class QRViewExampleState extends State<QRViewExample> {
  Barcode result;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool isClosed = false;
  String currentQrCode = "";

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget showCode = _buildShowCodeWidget();

    return Scaffold(
      body: Stack(children: [
        Column(
          children: <Widget>[
            Expanded(child: _buildQrView(context)),
          ],
        ),
        Align(
          child: Padding(
            child: showCode,
            padding: EdgeInsets.only(bottom: 20),
          ),
          alignment: Alignment.bottomCenter,
        )
      ]),
    );
  }

  Widget _buildShowCodeWidget(){
    if (widget.showCode != null && widget.showCode && currentQrCode.isNotEmpty) {
      if (widget.buildShowWidget != null) {
        return widget.buildShowWidget(currentQrCode);
      } else {
        return Container(
          padding: EdgeInsets.all(5),
          child: Text(currentQrCode, style: AppTheme.normal,), color: Color(0x33000000),);
      }
    } else {
      return SizedBox();
    }
  }
  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 500 ||
            MediaQuery.of(context).size.height < 500)
        ? 200.0
        : 400.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      cameraFacing: CameraFacing.back,
      onQRViewCreated: (controller) {
        controller.getCameraInfo().then((CameraFacing value) => {
              if (value == CameraFacing.front) {controller.flipCamera()}
            });
        setState(() {
          this.controller = controller;
        });
        controller.scannedDataStream.listen((barcode) {
          if (barcode.code.isNotEmpty) {
            bool close = this.widget.onData(barcode.code);
            if (close != null && close && !isClosed) {
              isClosed = true;
              Navigator.pop(context);
            } else {
              setState(() {
                currentQrCode = barcode.code;
              });
            }
          }
        });
      },
      formatsAllowed: [BarcodeFormat.qrcode],
      overlay: QrScannerOverlayShape(
        borderColor: Colors.red,
        borderRadius: 10,
        borderLength: 30,
        borderWidth: 7,
        cutOutSize: scanArea,
      ),
    );
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
