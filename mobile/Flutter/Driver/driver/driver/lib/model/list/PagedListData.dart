import '../base.dart';

typedef S ItemCreator<S>(Map<String, dynamic> js);

class PagedListData<T> {
  ItemCreator<T> creator;

  PagedListData(this.creator);

  PagedListData.withContent(this.content) {
    this.totalElements = content == null ? 0 : content.length;
    this.totalPages = 1;
  }

  List<T> content;
  int numberOfElements;
  int totalElements;
  int totalPages;
  bool last;
  int number;

  String error;

  PagedListData.withError(this.error);

  fromJson(Map<String, dynamic> js) {
    this.numberOfElements = js['total_records'];
    this.last = js['last'];
    this.number = js['number'];
    this.totalElements = js['total_record'];
    if (totalElements == 0 || totalElements==null) {
      this.totalPages = 0;
    } else
      this.totalPages = (totalElements / 10).floor() + 1;
    var data = js['records'] as List;
    // final uuid = new Uuid().v4();
    content = data.map((i) {
      return this.creator(i);
    }).toList();
  }
// fromJsonOdoo(Map<String, dynamic> js, {uuid = '0', step = 1}) {
//   this.numberOfElements = js['numberOfElements'];
//   this.last = js['last'];
//   this.number = js['number'];
//   this.totalElements = js['total_records'];
//   this.totalPages = this.totalElements % 10 + 1;
//   var data = js['records'] as List;
//   // final uuid = new Uuid().v4();
//   content = data.map((i) {
//     if (step == 0) {
//       i['uuId'] = uuid;
//     }
//     Base.step[uuid] = step;
//     return this.creator(i);
//   }).toList();
// }
// setPagesListData(List<T> content,int totalElements,int totalPages){
//   this.content=content;
//   this.totalPages=totalPages;
//   this.totalElements=totalElements;
// }
}
