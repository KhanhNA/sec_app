import '../base.dart';
import 'package:driver/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';
part 'PartnerDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class PartnerDto extends _PartnerDto{

  factory PartnerDto.fromJson(Map<String, dynamic> js) => _$PartnerDtoFromJson(js);

  get obs => null;

  Map<String, dynamic> toJson() => _$PartnerDtoToJson(this);

  PartnerDto();
}

class _PartnerDto extends Base{
   int id;
   String name;
   String display_name;
   bool date;
   String title;
   String parent_id;
   String parent_name;
   String uri_path;
   int depot_id;
   int staff_type;
   String staff_type_name;
   String driver_type;
   String ref;
   String lang;
   double active_lang_count;
   String tz;
   String tz_offset;
   String vat;
   String same_vat_partner_id;

   String website;
   String comment;

   double credit_limit;
   bool active;
   String employee;
   String
  function;
   String type;
   String street;
   String street2;
   String zip;
   String city;
   // OdooRelType state_id;
   // OdooRelType country_id;
   double partner_latitude;
   double partner_longitude;
   String email;
   String email_formatted;
   String phone;
   String mobile;
   String is_company;
//     int industry_id;
   String company_type;
   // OdooRelType company_id;
   double color;

   String partner_share;
   String contact_address;
   String commercial_company_name;
   String company_name;

   String name_seq;
   String im_status;
   String date_localization;

   String activity_state;
   int activity_user_id;
   int activity_type_id;
   String activity_date_deadline;
   String activity_summary;
   String activity_exception_decoration;
   String activity_exception_icon;
   String message_is_follower;
   String message_unread;
   double message_unread_counter;
   String message_needaction;
   double message_needaction_counter;
   String message_has_error;
   double message_has_error_counter;
   double message_attachment_count;
   int message_main_attachment_id;
   String email_normalized;
   bool is_blacklisted;
   double message_bounce;

//     int user_id;
   String signup_token;
   String signup_type;
   String signup_expiration;
   String signup_valid;
   String signup_url;
   int partner_gid;
   String additional_info;
   String phone_sanitized;
   String phone_blacklisted;
   String message_has_sms_error;
   String image_1920;
   String image_1024;
   String image_512;
   String image_256;
   String image_128;

   String walletAcount;
   int walletId;//client id
}