import 'package:driver/base/ui/list/list_ui.dart';
import 'package:driver/base/ui/get/base_get_view.dart';
import 'package:driver/pages/notification/notification_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'notification_system_controller.dart';
import 'notification_system_tap_view_item.dart';

class NotificationSystemTapView extends BaseView<NotificationVM,NotificationSystemController>{

  NotificationSystemTapView();

  @override
  Widget body(BuildContext context) {
    return SliverView(
      // floatBar: SearchImportStock(controller),
      listController: viewModel.listNotificationSystemController,
      itemBuilder: NotificationSystemTapViewItem(controller).buildItem,
      // widgetBefore: [context.sliverTotalRows(viewModel.totalElementsNotificationSystem)],
    );
  }

  @override
  putController() {
    return NotificationSystemController(NotificationVM());
  }

}