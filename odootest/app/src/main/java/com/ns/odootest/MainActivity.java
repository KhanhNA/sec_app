package com.ns.odootest;

import android.os.Bundle;

import com.ns.odoo.core.rpc.helper.OdooSessionDto;
import com.ns.odoo.core.rpc.helper.utils.gson.OdooResultDto;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.wrapper.BaseClient;
import com.ns.odoolib_retrofit.wrapper.OdooClient;
import com.ns.odootest.model.RoutingDayPlan;
import com.ns.odootest.utils.HttpLogger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        OdooClient client = new OdooClient(this, "http://192.168.1.99:8070/");
        client.authenticate("admin", "admin", "dlp", new IOdooResponse<OdooSessionDto>() {
            @Override
            public void onResponse(OdooSessionDto response, Throwable error) {
                JSONObject paramObject = new JSONObject();
                try {
                    paramObject.put("driver_id", 5);

                    paramObject.put("date", "2020-07-23");

                    client.retrofitRequest("share_van_order/routing_plan_day", paramObject, new IOdooResponse<OdooResultDto<RoutingDayPlan>>() {
                        @Override
                        public void onResponse(OdooResultDto<RoutingDayPlan> response, Throwable error) {
                            System.out.println("response1:" + response);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        Cache cache = new Cache(new File(mContext.getCacheDir(), "HttpCache"), 1024 * 1024 * 10);
        OkHttpClient.Builder mBuilder = new OkHttpClient.Builder()
//                .cache(cache)
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    Request.Builder newRequest = request.newBuilder() //.addHeader("Authorization", TOKEN_DCOM)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept-Language", "1");
                    return chain.proceed(newRequest.build());
                });
        ;


        Retrofit retrofitJson = new Retrofit.Builder()
//                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl("http://192.168.1.99:8070/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(mBuilder.build())
                .build();

//        OdooServices api = retrofitJson.create(OdooServices.class);
//        JSONObject paramObject = new JSONObject();
//        JSONObject request = new JSONObject();
//        try {
//
//            paramObject.put("driver_id", 5);
//            paramObject.put("date", "2020-07-23");
//
//
//            request.put("jsonrpc", "2.0");
//
//            request.put("params", paramObject);
//        } catch (Exception e) {
//        }
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), request.toString());
//
//        api.getDataJson("share_van_order/routing_plan_day", requestBody).enqueue(new Callback<Object>() {
//            @Override
//            public void onResponse(Call<Object> call, Response<Object> response) {
//                System.out.println("success");
//            }
//
//            @Override
//            public void onFailure(Call<Object> call, Throwable t) {
//                System.out.println("fail");
//            }
//        });
//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
