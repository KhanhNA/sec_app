package com.ns.odootest.account;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.ns.addons.slider.SliderView;
import com.ns.odootest.R;
import com.ns.odootest.config.IntroSliderItems;

public class AppIntro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_intro);
        SliderView sliderView = (SliderView) findViewById(R.id.sliderView);
        IntroSliderItems sliderItems = new IntroSliderItems();
        if (!sliderItems.getItems().isEmpty()) {
            sliderView.setItems(getSupportFragmentManager(), sliderItems.getItems());
        } else {
            finish();
        }
    }
}
