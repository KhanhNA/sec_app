package com.ns.res;

import android.content.Context;

import com.ns.odoo.core.orm.OModel;
import com.ns.odoo.core.orm.fields.OColumn;
import com.ns.odoo.core.orm.types.OVarchar;
import com.ns.odoo.core.support.OUser;

public class ResPartnerCategory extends OModel {

    OColumn name = new OColumn("Name", OVarchar.class);

    public ResPartnerCategory(Context context, OUser user) {
        super(context, "res.partner.category", user);
    }
}
