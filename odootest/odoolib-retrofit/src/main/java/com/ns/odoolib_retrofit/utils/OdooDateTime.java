package com.ns.odoolib_retrofit.utils;

import java.util.Date;

public class OdooDateTime extends Date {
    public OdooDateTime(long date) {
        super(date);
    }

    public OdooDateTime() {
        super();
    }
}
