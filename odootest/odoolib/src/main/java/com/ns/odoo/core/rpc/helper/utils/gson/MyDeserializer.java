package com.ns.odoo.core.rpc.helper.utils.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class MyDeserializer<T> implements JsonDeserializer<T> {
    GsonBuilder mGsonBuilder;
    public MyDeserializer(GsonBuilder gsonBuilder) {
        mGsonBuilder = gsonBuilder;
    }

    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonNull()) {
            return null;
        }
        if(typeOfT != Boolean.class) {
            if (json.isJsonPrimitive() && json.getAsJsonPrimitive().isBoolean()) {
                return null;
            }
        }

        return context.deserialize(json, typeOfT);
    }
}
