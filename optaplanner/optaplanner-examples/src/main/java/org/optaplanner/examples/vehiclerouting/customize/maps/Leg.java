package org.optaplanner.examples.vehiclerouting.customize.maps;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Leg {
    private Data2Point distance;
    private Data2Point duration;
}
