package org.optaplanner.examples.vehiclerouting.customize.types;

public enum WarehouseType {
    PICKUP(0), DELIVERY(1);
    //Instance variable
    private Integer value;
    //Constructor to initialize the instance variable
    WarehouseType(Integer v) {
        this.value = v;
    }
    public int getType() {
        return this.value;
    }

    public static WarehouseType of(Integer v){
        switch (v){
            case 0:
                return PICKUP;
            case 1:
                return DELIVERY;
        }
        return PICKUP;
    }
}
