/*
 * Copyright 2013 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.solver.score;


import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;
import org.optaplanner.examples.vehiclerouting.customize.StaticData;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentStandstill;
import org.optaplanner.examples.vehiclerouting.customize.entity.TimeWindowedShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.types.DistanceInfo;
import org.optaplanner.examples.vehiclerouting.customize.types.WarehouseType;
import org.optaplanner.examples.vehiclerouting.domain.*;
//import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VehicleRoutingEasyScoreCalculatorV2 implements EasyScoreCalculator<VehicleRoutingSolutionV2> {


    @Override
    public HardSoftLongScore calculateScore(VehicleRoutingSolutionV2 solution) {
        boolean timeWindowed = false;//solution instanceof TimeWindowedVehicleRoutingSolution;
        List<ShipmentPot> shipmentPotList = solution.getShipmentPotList();
        List<VehicleV2> vehicleList = solution.getVehicleList();
        Map<VehicleV2, Integer> vehicleDemandMap = new HashMap<>(vehicleList.size());

        // Map<String, Integer> customerMap = new HashMap<>();

        long hardPickupScore = 0L;
        long hardScore = 0l;
        long softScore = 0L;
        Pair<Long, Long> hardSoftScore = Pair.of(0L, 0L), tmp;
        for (VehicleV2 vehicle : vehicleList) {
//            vehicleDemandMap.put(vehicle, 0);
            //check constraint pickup-delivery
            //hardPickupScore += scorePickupDelivery(vehicle);
            //hardScore += calcHardSoftScore(vehicle);
            //hardScore += calcSoftScore(vehicle);

            tmp = calcHardSoftScore(solution, vehicle, timeWindowed);
            hardSoftScore.set(hardSoftScore.getLeft() + tmp.getLeft(), hardSoftScore.getRight() + tmp.getRight());
            if(hardSoftScore.getLeft() < 0L){
                return HardSoftLongScore.of(hardSoftScore.getLeft(), hardSoftScore.getRight());
            }

        }



        // Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
        return HardSoftLongScore.of(hardSoftScore.getLeft(), hardSoftScore.getRight());
    }


    //    @Override
//    public HardSoftLongScore calculateScore(VehicleRoutingSolutionV2 solution) {
//        boolean timeWindowed = false; //solution instanceof TimeWindowedVehicleRoutingSolution;
//        List<ShipmentPot> shipmentPotList = solution.getShipmentPotList();
//        List<VehicleV2> vehicleList = solution.getVehicleList();
//        Map<VehicleV2, Integer> vehicleDemandMap = new HashMap<>(vehicleList.size());
//
//       // Map<String, Integer> customerMap = new HashMap<>();
//
//        long hardPickupScore = 0L;
//        for (VehicleV2 vehicle : vehicleList) {
//            vehicleDemandMap.put(vehicle, 0);
//            //check constraint pickup-delivery
//            //hardPickupScore += scorePickupDelivery(vehicle);
//        }
//
//        long hardScore = 0l;
//        long softScore = 0L;
//
//
//        for (ShipmentPot shipmentPot : shipmentPotList) {
//            ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
//            if (previousStandstill != null) {
//                VehicleV2 vehicle = shipmentPot.getVehicle();
//                vehicleDemandMap.put(vehicle, vehicleDemandMap.get(vehicle) + shipmentPot.getDemand());
//
//                // Score constraint distanceToPreviousStandstill
//                softScore -= shipmentPot.getDistanceFromPreviousStandstill();
//                if (shipmentPot.getNextShipmentPot() == null) {
//                    // Score constraint distanceFromLastCustomerToDepot
//                   // softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
//                }
////                if (timeWindowed) {
////                    TimeWindowedCustomer timeWindowedCustomer = (TimeWindowedCustomer) customer;
////                    long dueTime = timeWindowedCustomer.getDueTime();
////                    Long arrivalTime = timeWindowedCustomer.getArrivalTime();
////                    if (dueTime < arrivalTime) {
////                        // Score constraint arrivalAfterDueTime
////                        hardScore -= (arrivalTime - dueTime);
////                    }
////                }
//            }
//        }
//
//        for (Map.Entry<VehicleV2, Integer> entry : vehicleDemandMap.entrySet()) {
//            int capacity = entry.getKey().getCapacity();
//            int demand = entry.getValue();
//            if (demand > capacity) {
//                // Score constraint vehicleCapacity
//                hardScore -= (demand - capacity);
//            }
//        }
//        hardPickupScore = 0L;
//        for (VehicleV2 vehicle : vehicleList) {
//            //vehicleDemandMap.put(vehicle, 0);
//            //check constraint pickup-delivery
//            hardScore += calcHardSoftScore(vehicle);
//        }
//
//
//        // Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
//        return HardSoftLongScore.of(hardScore, softScore);
//    }
    private long scorePickupDelivery(VehicleV2 vehicle) {
        ShipmentPot shipmentPot = vehicle.getNextShipmentPot();
        if (shipmentPot == null) {
            return 0;
        }
        Double shipmentDemand = 0d;
        String shipKey;
        Map<String, Double> pickupMap = new HashMap<>();
        Integer tongnhan = 0;

        while (shipmentPot != null) {
            if (shipmentPot.getShipType() == WarehouseType.PICKUP) {
                shipKey = "NHAN";
                String temKey = shipmentPot.getDepotId() + "----" + shipKey;
                if (pickupMap.get(temKey) != null) {
                    Double a = pickupMap.get(temKey) + shipmentPot.getDemand();
                    pickupMap.put(temKey, a);
                } else {
                    pickupMap.put(temKey, shipmentPot.getDemand());
                }
            } else {
                Double a = pickupMap.get(shipmentPot.getDepotId() + "----" + "NHAN");
                if (a != null) {
                    if (a < shipmentPot.getDemand() * (-1)) {
                        return -4000000;
                    }
                    pickupMap.put(shipmentPot.getDepotId() + "----" + "NHAN", a + shipmentPot.getDemand());
                } else {
                    return -3000000;
                }
            }
            ;

            shipmentDemand += shipmentPot.getDemand();
            if (shipmentDemand < 0) {
                return -3000000;
            }
            shipmentPot = shipmentPot.getNextShipmentPot();
        }
        return 0;
    }

    private long calcHardScore(VehicleV2 vehicle) { //ra soat tren cung 1 xe
        ShipmentPot shipmentPot = vehicle.getNextShipmentPot();
        if (shipmentPot == null) {
            return 0;
        }
        Integer shipmentDemand = 0;
        Integer shipKey;
        Map<Integer, Double> pickupMap = new HashMap<>();
        Double tongnhan = 0d, tmpShipTotal, pickupScore = 0d;

        while (shipmentPot != null) {
            shipKey = shipmentPot.getDepotId();
            tmpShipTotal = pickupMap.get(shipKey);//lay tong pickup va delivery cua 1 depot den hien tai
            tmpShipTotal = tmpShipTotal == null ? 0 : tmpShipTotal;
            tmpShipTotal += shipmentPot.getDemand();//lay bo sung
            pickupScore = 0d;
            if (tmpShipTotal < 0) {
                pickupScore = -1000000 + tmpShipTotal;//1m
                return pickupScore.longValue();
            }
            if (tmpShipTotal > vehicle.getCapacity()) {
                pickupScore -= (tmpShipTotal - vehicle.getCapacity());
                return pickupScore.longValue();
            }
            if (pickupScore < 0) {
                return pickupScore.longValue();
            }
            pickupMap.put(shipKey, tmpShipTotal);

            shipmentPot = shipmentPot.getNextShipmentPot();//lay diem den tiep theo
        }
        return 0;
    }

    private Pair<Long, Long> calcHardSoftScore(VehicleRoutingSolutionV2 solution, VehicleV2 vehicle, boolean timeWindowed) { //ra soat tren cung 1 xe

        long hardScore = 0;
        long softScore = 0;

        Integer shipmentDemand = 0;

        Map<Integer, Double> pickupMap = new HashMap<>();
        Map<VehicleV2, Long> totalTime = new HashMap<>();
        Integer tongnhan = 0, tmpShipTotal, pickupScore = 0;

        ShipmentPot shipmentPot = vehicle.getNextShipmentPot();
        if (shipmentPot == null) {
            return Pair.of(hardScore, softScore); //HardSoftLongScore.of(hardScore, softScore);
        }
        while (shipmentPot != null && hardScore >= 0) {//lap tuan tu tu depot den het cac diem nhan

            softScore += calcSoftScoreAtPot(vehicle, shipmentPot, timeWindowed);
            hardScore += calcHardScoreAtPot(vehicle, shipmentPot, timeWindowed, pickupMap, totalTime);
            shipmentPot = shipmentPot.getNextShipmentPot();
        }
        return Pair.of(hardScore, softScore);//HardSoftLongScore.of(hardScore, softScore);
    }

    private long calcSoftScoreAtPot(VehicleV2 vehicle, ShipmentPot shipmentPot, boolean timeWindowed) {
        ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
        long softScore = 0;

        if (previousStandstill != null) {
            HashMap<String, DistanceInfo<Double, Double>> locDataMap = StaticData.getLocDataMap();
//            String timeKey = "F" + previousStandstill.getCode() + "-T" + shipmentPot.getCode();
//            softScore -= shipmentPot.getDistanceFromPreviousStandstill();
//            softScore -= locDataMap.get(timeKey).getCost();
            softScore -= StaticData.getCost(previousStandstill, shipmentPot);
            if (shipmentPot.getNextShipmentPot() == null) {
                // Score constraint distanceFromLastCustomerToDepot
                softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
            }
        }
        if(shipmentPot.getNextShipmentPot() == null){
            softScore -= vehicle.getVehicleCost();
        }

        ShipmentStandstill previous = shipmentPot.getPreviousStandstill();

        if (timeWindowed) {
            TimeWindowedShipmentPot timeWindowedShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
            long dueTime = timeWindowedShipmentPot.getDueTime();
            Double arrivalTime = timeWindowedShipmentPot.getArrivalTime();
            if (dueTime < arrivalTime) {
                // Score constraint arrivalAfterDueTime
                softScore -= (arrivalTime - dueTime);
               // return hardScore;
            }
        }


        return softScore;
    }
    private long calcHardScoreAtPot(VehicleV2 vehicle, ShipmentPot shipmentPot, boolean timeWindowed, Map<Integer, Double> pickupMap,
                                    Map<VehicleV2, Long> totalTime) {
        Integer shipKey = shipmentPot.getDepotId();
        Double tmpShipTotal, hardScore = 0d;
        tmpShipTotal = pickupMap.get(shipKey);//lay tong pickup va delivery cua 1 depot den hien tai
        tmpShipTotal = tmpShipTotal == null ? 0 : tmpShipTotal;
        tmpShipTotal += shipmentPot.getDemand();//lay bo sung

        if (tmpShipTotal < 0) {
            hardScore = -1000000 + tmpShipTotal;//1m
            System.out.println("hardscore shiptotal < 0: " + tmpShipTotal);
            return hardScore.longValue();
        }
        if (tmpShipTotal > vehicle.getCapacity()) {
            System.out.println("hardscore shiptotal < capacity: " + tmpShipTotal + "_" + vehicle.getCapacity());
            hardScore -= (tmpShipTotal - vehicle.getCapacity());
            return hardScore.longValue();
        }


        ShipmentStandstill previous = shipmentPot.getPreviousStandstill();

//        if (timeWindowed) {
//            TimeWindowedShipmentPot timeWindowedShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
//            long dueTime = timeWindowedShipmentPot.getDueTime();
//            Long arrivalTime = timeWindowedShipmentPot.getArrivalTime();
//            if (dueTime < arrivalTime) {
//                // Score constraint arrivalAfterDueTime
//                hardScore -= (arrivalTime - dueTime);
//                return hardScore;
//            }
//        }


//        if (timeWindowed && previous != null ) {
//            HashMap<String, DistanceInfo<Long, Long>> locDataMap = StaticData.getLocDataMap();
//            TimeWindowedShipmentPot timeWindowedshipmentPot = (TimeWindowedShipmentPot) shipmentPot;
//            Long arrivalTime = totalTime.get(vehicle);
//            arrivalTime = arrivalTime == null?0: arrivalTime;
//            arrivalTime += StaticData.getTime(previous, shipmentPot);
//            long dueTime = timeWindowedshipmentPot.getDueTime();
//
//            if (dueTime < arrivalTime) {
//                // Score constraint arrivalAfterDueTime
//                hardScore -= (arrivalTime - dueTime);
//                return hardScore;
//
//            }
//            totalTime.put(vehicle, arrivalTime);
//        }


        pickupMap.put(shipKey, tmpShipTotal);

        return hardScore.longValue();
    }
}
