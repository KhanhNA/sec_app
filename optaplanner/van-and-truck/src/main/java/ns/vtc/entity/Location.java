package ns.vtc.entity;

import lombok.Data;

@Data
public class Location {
    protected String name = null;
    protected double latitude;
    protected double longitude;
    public Location(String name, double latitude, double longitude){
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;

    }
}
