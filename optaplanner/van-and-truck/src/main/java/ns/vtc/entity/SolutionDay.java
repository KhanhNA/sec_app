package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="Solution_Day")
@Data
public class SolutionDay {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private long id;

  @Column(name="hard_Score")
  private long hardScore;

  @Column(name="soft_Score")
  private long softScore;

  @Column(name="solve_time")
  private LocalDateTime solve_time;

  @Column(name="date_Plan")
  private LocalDate datePlan;

  @Column(name="group_code")
  private String groupCode;

}
