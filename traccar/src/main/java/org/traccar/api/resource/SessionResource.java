/*
 * Copyright 2015 Anton Tananaev (anton@traccar.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.traccar.api.resource;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import oauth2.OAuth2AuthenticationDto;
import org.traccar.Context;
import org.traccar.Main;
import org.traccar.api.BaseResource;
import org.traccar.api.UserPrincipal;
import org.traccar.api.UserSecurityContext;
import org.traccar.database.StatisticsManager;
import org.traccar.helper.DataConverter;
import org.traccar.helper.Log;
import org.traccar.helper.LogAction;
import org.traccar.helper.ServletHelper;
import org.traccar.model.Position;
import org.traccar.model.User;

import javax.annotation.security.PermitAll;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

@Path("session")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class SessionResource extends BaseResource {

    public static final String USER_ID_KEY = "userId";
    public static final String USER_COOKIE_KEY = "user";
    public static final String PASS_COOKIE_KEY = "password";

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String WWW_AUTHENTICATE = "WWW-Authenticate";
    public static final String BASIC_REALM = "Basic realm=\"api\"";
    public static final String X_REQUESTED_WITH = "X-Requested-With";
    public static final String XML_HTTP_REQUEST = "XMLHttpRequest";

    @javax.ws.rs.core.Context
    private HttpServletRequest request;

    @javax.ws.rs.core.Context
    private SecurityContext securityContext;

    @javax.ws.rs.core.Context
    private ResourceInfo resourceInfo;

    @PermitAll
    @GET
    public User get(@QueryParam("token") String token) throws SQLException, UnsupportedEncodingException {
        Long userId = (Long) request.getSession().getAttribute(USER_ID_KEY);
        if (userId == null) {
            Cookie[] cookies = request.getCookies();
            String email = null, password = null;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(USER_COOKIE_KEY)) {
                        byte[] emailBytes = DataConverter.parseBase64(
                                URLDecoder.decode(cookie.getValue(), StandardCharsets.US_ASCII.name()));
                        email = new String(emailBytes, StandardCharsets.UTF_8);
                    } else if (cookie.getName().equals(PASS_COOKIE_KEY)) {
                        byte[] passwordBytes = DataConverter.parseBase64(
                                URLDecoder.decode(cookie.getValue(), StandardCharsets.US_ASCII.name()));
                        password = new String(passwordBytes, StandardCharsets.UTF_8);
                    }
                }
            }
            if (email != null && password != null) {
                User user = Context.getPermissionsManager().login(email, password);
                if (user != null) {
                    userId = user.getId();
                    request.getSession().setAttribute(USER_ID_KEY, userId);
                }
            } else if (token != null) {
                User user = Context.getUsersManager().getUserByToken(token);
                if (user != null) {
                    userId = user.getId();
                    request.getSession().setAttribute(USER_ID_KEY, userId);
                }
            }
        }

        if (userId != null) {
            Context.getPermissionsManager().checkUserEnabled(userId);
            return Context.getPermissionsManager().getUser(userId);
        } else {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).build());
        }
    }

    @PermitAll
    @POST
    public User add(
            @FormParam("access_token") String access_token) throws SQLException {

        String authHeader = access_token;
        if (authHeader != null) {
            User userNew = getUserMe("BEARER " + authHeader.replace(",", " "));
            LogAction.login(userNew.getId());
            userNew.setJsession(request.getSession().toString());
            request.getSession().setAttribute(USER_ID_KEY, userNew.getId());
            return userNew;
        } else {
            LogAction.failedLogin(ServletHelper.retrieveRemoteAddress(request));
            throw new WebApplicationException(Response.status(Response.Status.UNAUTHORIZED).build());
        }

        /*if (email.equals("admin")) {
            String authHeader = access_token;
//            String authHeader = request.getParameter("token");
            if (authHeader != null) {
                User userNew = getUserMe("BEARER " + authHeader.replace(",", " "));
                LogAction.login(userNew.getId());
                return userNew;
            } else {
                LogAction.failedLogin(ServletHelper.retrieveRemoteAddress(request));
                throw new WebApplicationException(Response.status(Response.Status.UNAUTHORIZED).build());
            }
//            auth(authHeader);
//            User user = Context.getPermissionsManager().login(email, password);
//            if (user != null) {
//                request.getSession().setAttribute(USER_ID_KEY, user.getId());
//                LogAction.login(user.getId());
//                return user;
//            } else {
//                LogAction.failedLogin(ServletHelper.retrieveRemoteAddress(request));
//                throw new WebApplicationException(Response.status(Response.Status.UNAUTHORIZED).build());
//            }
        }
        User user = Context.getPermissionsManager().login(email, password);
        if (user != null) {
            request.getSession().setAttribute(USER_ID_KEY, user.getId());
            LogAction.login(user.getId());
            return user;
        } else {
            LogAction.failedLogin(ServletHelper.retrieveRemoteAddress(request));
            throw new WebApplicationException(Response.status(Response.Status.UNAUTHORIZED).build());
        }*/
    }

    @PermitAll
    @POST
    @Path("monitor")
    public User monitor(
            @FormParam("email") String email, @FormParam("password") String password,
            @FormParam("deviceId") Integer deviceId) throws SQLException {
        User user = Context.getPermissionsManager().login(email, password);
        if (user != null) {
            Context.getMonitorManager().addMonitor(user.getId(), deviceId);
            request.getSession().setAttribute(USER_ID_KEY, user.getId());
            LogAction.login(user.getId());
            return user;
        } else {
            LogAction.failedLogin(ServletHelper.retrieveRemoteAddress(request));
            throw new WebApplicationException(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }

    @DELETE
    @Path("send")
    public Response remove() {
        LogAction.logout(getUserId());
        request.getSession().removeAttribute(USER_ID_KEY);
        return Response.noContent().build();
    }

    @POST
    @Path("logout")
    public Response logout() {
        LogAction.logout(getUserId());
        request.getSession().removeAttribute(USER_ID_KEY);
        return Response.noContent().build();
    }

    private void auth(String req) {

        SecurityContext sc = null;

        try {


        } catch (SecurityException e) {
            Log.exceptionStack(e.getCause());
        }

        if (sc != null) {
            this.securityContext = sc;
        } else {
            Method method = resourceInfo.getResourceMethod();
            if (!method.isAnnotationPresent(PermitAll.class)) {
                Response.ResponseBuilder responseBuilder = Response.status(Response.Status.UNAUTHORIZED);
                if (!XML_HTTP_REQUEST.equals(request.getHeader(X_REQUESTED_WITH))) {
                    responseBuilder.header(WWW_AUTHENTICATE, BASIC_REALM);
                }
                throw new WebApplicationException(responseBuilder.build());
            }
        }

    }

    private User getUserMe(String authHeader) {
        StringBuilder strBuf = new StringBuilder();
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        try {
            // Declare the connection to SSO api url
            URL url = new URL("https://mfunctions.com:9999/user/me");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty(AUTHORIZATION_HEADER, authHeader);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : " + conn.getResponseCode());
            }
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String output = null;
            while ((output = reader.readLine()) != null) {
                strBuf.append(output);
            }
            ObjectMapper mapper = new ObjectMapper();
            OAuth2AuthenticationDto auth2AuthenticationDto = mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT).readValue(strBuf.toString(),
                    OAuth2AuthenticationDto.class);
            System.out.println(auth2AuthenticationDto.getName());
            User user = Context.getPermissionsManager().login(auth2AuthenticationDto.getName().toLowerCase());
            Main.getInjector().getInstance(StatisticsManager.class).registerRequest(user.getId());
            request.getSession().setAttribute(SessionResource.USER_ID_KEY, user.getId());
            return user;
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
        return null;
    }

    private void getPositions(long deviceId, Date fd, Date td) {
        try {

            Collection<Position> positions = Context.getDataManager().getPositions(deviceId, fd, td);
            for (Position position : positions) {
                Context.getConnectionManager().updatePosition(position);
                Thread.sleep(1000);
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
    }
}
